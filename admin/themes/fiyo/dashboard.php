<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/

defined('_FINDEX_') or die('Access Denied');
loadLang(__dir__);


if(USER_LEVEL > 2) die();

$totalA = $totalB = $totalC = $totalD = 0;

$totalA = DB::table(FDBPrefix.'article')
				->select('COUNT(id) as total, id')
				->limit(1)
				->get()[0]['total'];

$totalB = DB::table(FDBPrefix.'comment')
				->select('COUNT(id) as total')
				->limit(1)
				->get()[0]['total'];
				
$y = date('Y');
$m = date('m');

$totalC = DB::table(FDBPrefix.'statistic')
				->select('COUNT(id) as total, id')
				->where("YEAR(time) = '$y' AND MONTH(time) = '$m'")
				->limit(1)
				->get()[0]['total'];

$totalD = DB::table(FDBPrefix.'statistic')
				->select('COUNT(time) as total')
				->where("YEAR(time) = '$y'")
				->limit(1)
				->get()[0]['total'];


?>
<div id="app_header">
	<div class="warp_app_header">
		<?php if(USER_LEVEL <= 2) : ?>
			<div class="app_title">Dashboard</div>
			<div class="app_link">

			<?php if(USER_LEVEL < 3) : ?>		
			<a class="btn btn-primary" Value="Create" href="?app=article&act=add" title="<?php echo Add_new_article; ?>"><i class="icon-pencil"></i> <?php echo New_Article; ?></a>
				
			<?php endif; ?>
			<?php if(USER_LEVEL < 2) : ?>
			<a class="btn btn-default" Value="Create" href="?app=config" title="<?php echo Add_new_article; ?>"><i class="icon-gears"></i> Setting</a>
				
			<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>


<div style="padding-bottom: 10px; width: 100%;">

	<div class="col-lg-12 full">
		<div class="grid-2 auto-grid">
			<div class="mini-box border-blue online-user"><h3><?= number($totalA); ?></h3>
				<span>Jumlah Artikel</span> <i class="icon icon-file-text"></i>
			</div>
			<div class="mini-box border-green monthly-visitor"><h3><?= number($totalB); ?></h3>
				<span>Interaksi</span><i class="icon icon-comments"></i>
			</div>
			<div class="mini-box border-red today-visitor"><h3><?= number($totalC); ?></h3>
				<span>Pengunjung Bulan ini</span><i class="icon icon-bar-chart" style=''></i>
			</div>
			<div class="mini-box border-yellow -visitor"><h3><?= number($totalD); ?></h3>
				<span>Pengunjung Tahun ini</span><i class="icon icon-signal"></i>
			</div>
		</div>
	</div>

<div class="clearfix"></div>
<br>
<div class="box-left">
	<div class="box">

		<header>			
			<h5>Artikel Populer</h5>
		</header>
		<div class="div-table" id="data-1">
			<?php
				$sql = (object) DB::table(FDBPrefix.'article')
					->select('title, id, hits')
					->where('status = 1')
					->orderBy('hits DESC')
					->limit(10)
					->get();
				$i = 0;
				foreach($sql as $row) :
				$i++;	
				$link = make_permalink('?app=article&view=item&id=' . $row['id']);
				$judul = "<a href='$link' target='_blank'> $row[title] </a>";
			?>
			<div class="">
				<b style='opacity: .4;'>#<?php echo $i; ?></b>&nbsp;  <?php echo $judul; ?> <small class='right'  style='opacity: .7'> <i class='icon-signal'></i> <?php echo number($row['hits']); ?></small>
			</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div class="box">
		<header>			
			<h5>Komentar Terbaru</h5>
		</header>
		<div class="div-table" id="data-1">

			<?php
				$sql = (object) DB::table(FDBPrefix.'comment')
					->select('name, id, link, status, comment')
					->orderBy('date DESC')
					->limit(10)
					->get();
				$i = 0;
				foreach($sql as $row) :
					
				$link = make_permalink($row['link']);
				$judul = "<a href='$link' target='_blank'> $row[comment] </a>";
				$edit = "<a class='btn btn-primary btn-sm'>Tinjau</a>";
				$edit = "<a class='btn btn-primary btn-sm' href='?app=article&view=comment&act=edit&id=$row[id]'>Tinjau</a>";
				$i++;
			?>
				<div class="">
				<b style='opacity: .4;'><i class='icon-comment-o'></i></b>&nbsp;  <?php echo $judul; ?> <small class='right'  style='opacity: .7'> <?php echo $edit; ?></small>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>

<div class="box-right">

	<div class="box" id="visitor-stat">
		<header>			
			<h5>Visitors</h5>
		</header>
		<div class="padding-10">
			<canvas id="barstatistik"></canvas>

		</div>
	</div>
	
	<div class="box">
		<header>			
			<h5><?= dashboard_News; ?></h5>
		</header>
		<div>
			<div id="news" class="list-blog img-left"></div>
		</div>
	</div>
</div>


</div>
<script>


$(function() {

	
	$.get("?app=theme&api=module&module=feeds").done(function (r) {
		$("#news").html(r);

	});
	'use strict';

	/* send post data */
	var tanggal = [];

	window.chartColors = {
			red: '#e74c3c',
			orange: 'rgb(255, 159, 64)',
			yellow: 'rgb(255, 205, 86)',
			green: '#3ea932',
			blue: '#d6e3ef',
			bluelight: '#9ab6ce',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(201, 203, 207)',
			yellow_o: 'rgba(255, 205, 86, 0.1)',
		};
		
	$.get("?app=theme&api=module&module=statistic_data").done(function (r) {
		
		var $label = r.data.date;
		var $unique = r.data.unique;
		var $visitor = r.data.visitor;
		

		var config = {
			type: 'bar',
			data: {
				labels: $label,
				datasets: [ {
					label: 'Unique Visitors',
					fill: true,
					borderColor: window.chartColors.blue,
					backgroundColor: window.chartColors.blue,
					data: $visitor,
				},{
					label: 'Page Views',
					backgroundColor: window.chartColors.bluelight,
					data: $visitor,
					fill: false,
				}]
			},
			options: {
				legend: {
					display: true,
					position: 'bottom',
					labels: {
						
					}
				},
				responsive: true,
				title: {
					display: false,
					text: 'Chart.js Line Chart'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: false
						},
						fontSize: 2				
					}],
					yAxes: [{
						barPercentage: 12,
						barThickness: 26,
						maxBarThickness: 2,
						minBarLength: 2,
						display: true,
						scaleLabel: {
							display: false,
							labelString: 'Visitor'
						},
						ticks: {
							beginAtZero: true,
							userCallback: function(label, index, labels) {
								/* when the floored value is the same as the value we have a whole number */
								if (Math.floor(label) === label) {
									return label;
								}

							},
						}
					}]
				}
			}
		};


		var ctx = document.getElementById('barstatistik').getContext('2d');
		window.myLine2 = new Chart(ctx, config);
			
	});
});
</script>


