function checkFirstVisit() {
	$(window).bind('onbeforeunload',function(){
		return false;
		window.location.replace(document.URL);

	});  
	$(document).bind('keydown keyup', function(e) {
		if(e.which === 116) {
		   console.log('blocked');
			e.preventDefault();	
		   window.location.replace(document.URL);
		}
		if(e.which === 82 && e.ctrlKey) {
		   console.log('blocked');
			e.preventDefault();	
		   window.location.replace(document.URL);
		}
	});
}

function bname(path) {
	if(path)
		return path.split('/').reverse()[0];
	else	
		return "";
}

function basename(path, text = "Choose file...") {
	if(path) {
		path = bname(path);
		return path.split('\\').reverse()[0];
	} 
	else 
		return text;
}

function stringify(s) {		
	return s.replace(/"/g, "'");
}

function getEditor(editor) {
	try{
		return stringify(CKEDITOR.instances['editor'].getData());
	} catch (e) {

	}
}

function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}


$(function() {

	$("body").fadeIn(500);
	//checkFirstVisit();
    $('a[href=#]').on('click', function(e){
      e.preventDefault();
    });    
	
    $('.minimize-box').on('click', function(e) {
        e.preventDefault();
        var $icon = $(this).children('i');
        if ($icon.hasClass('icon-chevron-down')) {
            $icon.removeClass('icon-chevron-down').addClass('icon-chevron-up');
        } else if ($icon.hasClass('icon-chevron-up')) {
            $icon.removeClass('icon-chevron-up').addClass('icon-chevron-down');
        }
    });
	
    $('.close-box').click(function() {
        $(this).closest('.box').hide('slow');
    });

    $('.changeSidebarPos').on('click', function(e) {
        $('body').toggleClass('hide-sidebar');
		$('body').removeClass('user-sidebar no-transform');
		$("#left").css("left","");
		$('.changeSidebarPos').toggleClass('removeSidebar');
		$.ajax({
			type: 'POST',
			url: "themes/fiyo/module/view.php",
			data: "view=true",
			success: function(data){
			}
		});	
	});
	


    $('.userSideBar').on('click', function(e) {
        $('body').toggleClass('user-sidebar');
        $('body').removeClass('hide-sidebar');
    });
    
    $('.removeSidebar').on('click', function(e) {
		removeSidebar();
	});
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		
		var start = end = 0;
		$(".drawer-trigger").draggable({
			axis: "x",
			drag: function(e,ui){	
				$("#left").css("left",start + ui.offset.left+
				"px");		
				if(ui.offset.left > -100) {					
					$("body").addClass("hide-sidebar no-transform");					
				}
				else { 
					$("body").removeClass("hide-sidebar no-transform");
				}	
			},
			containment: [-250,0,0,"#wrap"],  
			stop: function(e, ui) {

				diff = Math.abs(start - ui.offset.left);
				if(start == 0) {					
					
					
				}
				else { 
					$("body").addClass("hide-sidebar no-transform");
					$("#left").css("left",0);
				}	

			},
			start: function(e, ui) {				
				start = ui.offset.left;
			}
		
		
		});
	}

	function removeSidebar() {
        $('body').removeClass('hide-sidebar');
        $('body').removeClass('user-sidebar');
		$('body').removeClass('no-transform');
		$("#left").css("left",'');

	}
    $('li.accordion-group > a').on('click',function(e){
        $(this).children('span').children('i').toggleClass('icon-angle-down');
    });		
	
	$('.spinner').change(function () {
		$(this).next('label').hide() ;
	});
	
	loader();

	
});

	
$(document).ajaxComplete(function(e) {
  });

function logs(data) {
	return console.log(data);
}

function formNumber() {
	$('form').submit(function() {
		$("input.currency, input.decimal", $(this)).val(function () {return $(this).autoNumeric('get') });
		$('.error').parents('.panel-collapse').height('auto').removeClass('collapsed').addClass('in').before().find('a').removeClass('collapsed');		
	});
}

function reformNumber() {
	$("input.currency, input.decimal").each(function () {
		dec = $(this).data('decimal');
		if(!dec) dec = ',';
		
		sep = $(this).data('separator');
		if(!sep) sep = '.';
		
		max = $(this).attr('max');
		if(!max) max = null;
		
		decl = $(this).data('declong');
		if(!decl) decl = 0;

		val =  $(this).val();
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');

		if(dec != '.') {
			val = val.replace(dec,'.');
		}

		$(this).autoNumeric('init', {
			aSep: sep,
			aDec: dec,
			mDec: decl				
		}).autoNumeric('set', val);		
	});	
}
function loader() {
	
	v = $(".input-append.time, .input-append.datetime,.input-append.date").length;
	
	$(".bootstrap-datetimepicker-widget").slice(1, 4).remove();
	

	$("input, select, textarea").addClass('form-control');	
	$('input[type="checkbox"]:not(.wrapped)').addClass('wrapped').wrap("<label>");
	$('input[type="radio"]:not(.wrapped)').addClass('wrapped').parent().wrap("<label>");
	$('input[type="number"]').attr("type","text").addClass('spinner').addClass('numeric');
	$('input[type="currency"]').attr("type","text").addClass('numeric currency');
	$('input[type="decimal"]').attr("type","text").addClass('numeric decimal');
	$('input[type="checkbox"],input[type="radio"]:not(.switch-radio)').after("<span class='input-check'>");	
	
	$('.switch-radio').each(function () { $(this).attr("id",($(this).attr('name')+$(this).val())) });	

	$("input.form-control[type=password]:not(.wrapped)").addClass('wrapped').parent().wrapInner("<div>");
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {	
		$(".btn").find("[class^='icon-'], [class*='icon-']").parent().addClass("btn-icon-only");
		$('body').on( 'change keyup keydown paste cut', 'textarea', function (){
			$(this).height(0).height(this.scrollHeight - 10);
		}).find( 'textarea' ).change();
		$("input").removeAttr("size");
		$(".dataTables_filter input").prependTo(".dataTables_filter").attr("placeholder","Search...");
		$(".dataTables_filter label").remove();

		$(".app_link.tabs").prependTo("#mainApp");

	}else {
		
	
		

		var beforePrint = function(e) {  
			target = $("iframe").contents().find("#print-it");
			height = target.height();

			target.css({"width": "100vw", "position":"fixed", "left":0, "right": "0" , "top" : 0, "z-index": 20});
			$(".modal .modal-dialog").css({"width": "100%", "left":0, "right": "0" , "top" : 0,"margin": 0});
			$(".modal-content").css({"border-radius" : 0}).height(height);
			$(".modal .frame iframe").height(height);
		};


		var afterPrint = function() {		
			target = $("iframe").contents().find("#print-it");
			target.removeAttr("style");
			
			$(".modal-content, .modal .modal-dialog, #print-it").removeAttr("style");
			$(".modal-header, .modal-footer").show();
			
		};

		if (window.matchMedia) {
			var mediaQueryList = window.matchMedia('print');
			mediaQueryList.addListener(function(mql) {
				if (mql.matches) {
					beforePrint();
				} else {
					afterPrint();
				}
			});
		}

		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;

		$('input[type="file"]:not(.wrapped,.file-image-preview):not([disabled])').addClass('wrapped').wrap("<label class='input-append file input-group'>").wrap("<div class='form-file form-control'>").change(function () {	t = $(this); 		
			//check size		
			text = [];
			var fileExtension = t.attr("extention");
			fileExtension = $.trim(fileExtension).split(",");

			if(Array.isArray() && fileExtension.length > 0)
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				text.push("Only formats are allowed : "+fileExtension.join(', '));
			}

			var size = this.files[0].size;
			max = $(this).attr('max');
			if(typeof max !== "undefined")
			{	
				if(size > max * 1000) {
					text.push("File size is to large!");	t.val(null);	
				}
			}	
			
			if(text.length) {
				if(text.length == 1) text = text[0];
				notice("error", text);
			}
			return t.parent().parent().find(".form-file-text").val(t.val());
		}).after(function () {t = $(this);v = t.attr("value"); required = t.attr("required");  return "<input type='text' name='" + t.attr("name") + "-txt' readonly " + required + " placeholder= '" + basename(v) + "' class='form-file-text'>";}).parent().before('<span class="add-on input-group-addon"><i class="icon-file-text-o"></i></span>').wrapInner("<div>");
		
		
		$('input[type="file"].file-image-preview:not([disabled])').addClass('wrapped').wrap("<label class='input-append file input-group'>").wrap("<div class='form-file file-image form-control'>").before("<div class=\"preview-image\"></div>").change(function () {	t = $(this); 		
			//check size		
			var size = this.files[0].size;
			max = $(this).attr('max');
			if(typeof max !== "undefined")
			{if(size > max * 1000) {	notice("error","File size is to large!");	t.val(null);	}} 				
			return t.parent().parent().find(".form-file-text").val(t.val());
		
		}).after(function () {t = $(this);v = t.attr("value");  required = t.attr("required"); return "<input type='text' name='" + t.attr("name") + "-txt' readonly " + required + " placeholder= '" + basename(v, 'Select a picture...') + "' class='form-file-text'>";}).parent().wrapInner("<div>");
		
		$(".form-file-text").click(function () {t = $(this);t.parent().trigger("click");});

		$('input[type="file"]').change(function () {
			v = $(this);
			v.parent().find(".form-file-text").val(v.val());
		});
	
		if ($.isFunction($.fn.datetimepicker) ) {
			$('input[type=date]:not(.wrapped):not([disabled])').addClass('wrapped date').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append date input-group'>").datetimepicker({
				format: "yyyy-MM-dd",
				pickTime: true
			}).find(".picker-switch").remove();	
		
			$('input[type=time]:not(.wrapped):not([disabled])').addClass('wrapped time').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-time"></i></span>').parent().wrapInner("<div class='input-append time input-group'>")
			.datetimepicker({
				format: 'hh:mm',
				pickTime: true,
				pickDate: false,		
				pickSeconds: false,
			});
			
			$('input[type=datetime]:not(.wrapped):not([disabled])').addClass('wrapped datetime').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append datetime input-group'>")
			.datetimepicker({
				format: 'yyyy-MM-dd hh:mm:ss',
				pickTime: true,
				pickDate: true,		
			});
			
			
			$("*").scroll(function() {$(".bootstrap-datetimepicker-widget").hide()});
			
			$("#weeklyDatePicker").datetimepicker({
				format: 'MM-DD-YYYY'
			});
			
			$('input[type=multidate]:not(.wrapped):not([disabled])').multiDatesPicker({
				dateFormat: "yy-mm-dd",	
			}).change(function () {
				var dates = $(this).multiDatesPicker('value');
				if($(this).val() == '')	{	
					$(this).val('');
				}
				else{$(this).val(dates);}
				v = dates.split(",");
			}).addClass('wrapped multidate').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append multidate input-group'>").find('div span').click(function () {$(this).parent().find('input').focus()});
			
			$(".input-group input").focusin(function () {
				$(this).parent().next().find('i').click();
				if($(this).parent().parent().hasClass('date'))
					$(".bootstrap-datetimepicker-widget").find(".picker-switch").hide();
				else
					$(".bootstrap-datetimepicker-widget").find(".picker-switch").show();	
			
			}).focusout(function () {
				$(".bootstrap-datetimepicker-widget").hide();
			});
		
			$('.input-group.date i,.input-group.datetime i').click(function () {
				if($(this).parent().parent().parent().hasClass("date") || $(this).parent().parent().hasClass("date"))	
					$('.bootstrap-datetimepicker-widget').find(".picker-switch").hide();
				else
					$('.bootstrap-datetimepicker-widget').find(".picker-switch").show();
				
				t = $(this).offset().top;
				hl = $(document).height();
				
				l = $(this).offset().left;
				wl = $(document).width();
				
				h =  $('.bootstrap-datetimepicker-widget').height();


				$('.bootstrap-datetimepicker-widget').css("margin-top" , "");
				if((hl - t) < 320) {			
					$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "-"+ parseInt(h+ 10) +"px"}).addClass("drop-top");
				}
				if((wl - l) < 320) {	
					if((hl - t) < 320) {			
						$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "-"+ parseInt(h+ 45) +"px"}).addClass("drop-top");
					} else {
						$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "2px"}).addClass("drop-top");
					}		
				}
			});
		}
		
	loadSpinner();
		
	}

	$("input[type=email]").addClass("email").wrap("<span class='form-control-wrap'>");

	
	$("select").addClass("select").wrap("<span class='form-control-wrap'>");

	$("input[required]:not(.required)").wrap("<span class='form-control-wrap'>").addClass('required').after('<div class="required-input"><i title="Required" data-placement="top">*</i></div>');

	$("input[type=text]:not(.required)").wrap("<span class='form-control-wrap'>");

	$("textarea[required]:not(.required)").addClass('required').after('<div class="required-input"><i title="Required" data-placement="top">*</i></div>').parent().wrapInner("<div>");
		
	$("select[required]:not(.wrapped)").addClass('wrapped').addClass('required').parent().append('<div class="required-input"><i title="Required" data-placement="top">*</i></div>').wrapInner("<div>");

	$('.required-input i').tooltip();
	
	$("#editor").attr("required","required");

	if ($.isFunction($.fn.validate)) {
		$("form.validate").validate({ ignore: ":hidden:not(select)",});
		$("body").find("#content form").validate({ ignore: ":hidden:not(select)" });		
	}
	
	$(".cb-enable").click(function(){
		if($(this).has('span').length == 1 ) {
			var parent = $(this).parents('.switch');
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}		
	});

	$(".cb-disable").click(function(){
		if($(this).has('span').length == 1) {
			var parent = $(this).parents('.switch');
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}
	});	
	
	$(".cb").click(function(){
		if($(this).parents('.switch-group').length == 0 ) {
			var parent = $(this).parents('.switch');
			parent.find("input").removeAttr("checked");
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}
	});	
	
	$('.selectbox li').click(function(){
		$(this).toggleClass('active');
		var checkBoxes = $("input[type='checkbox']", this);
		checkBoxes.prop("checked", !checkBoxes.prop("checked"));
	});
	$('.selections-all').click(function(){
		var t = $('.selectbox li').addClass('active');
		var checkBoxes = $("input[type='checkbox']", t);
		checkBoxes.prop("checked",true);
	});
	$('.selections-reset').click(function(){
		var t = $('.selectbox li').removeClass('active');
		var checkBoxes = $("input[type='checkbox']", t);
		checkBoxes.prop("checked",false);
	});

 
	if ($.isFunction($.fn.popover)) {
		$('[data-toggle=popover]').popover();
		$('[data-popover=tooltip]').popover();
	}

	if ($.isFunction($.fn.tooltip)) {
		$('[data-toggle=tooltip]').tooltip();
		$('[data-tooltip=tooltip]').tooltip();
		$('.tips').tooltip();
	}
	
	if ($.isFunction($.fn.alphanumeric)) {
		$('.alphanumeric').alphanumeric();
		$('.alphadot').alphanumeric({allow:"."});
		$('.nocaps').alpha({nocaps:true});
		$('.numeric').numeric();
		$('.numericdot').numeric({allow:"."});
		$('.selainchar').alphanumeric({ichars:'.1a'});
		$('.web').alphanumeric({allow:':/.-_'});
		$('.email').alphanumeric({allow:':.-_@'});

	} 
	
	

	
	$('.accordion-toggle').click(function () {
		$(this).parent().next().css("display", "")
	});
	$('.accordion-toggle collapsed').bind(function () {});
	

	 //Get the value of Start and End of Week
	$('#weeklyDatePicker').on('dp.change', function (e) {
		var value = $("#weeklyDatePicker").val();
		var firstDate = moment(value, "MM-DD-YYYY").day(0).format("MM-DD-YYYY");
		var lastDate =  moment(value, "MM-DD-YYYY").day(6).format("MM-DD-YYYY");
		$("#weeklyDatePicker").val(firstDate + " - " + lastDate);
	});
	

	$(".table-scroll").scroll(function() {
		v = $(this).scrollLeft();
		if(v > 1) $(this).addClass('moving_scroll');
		else $(this).removeClass('moving_scroll');
	});

	
	$(".inner").scroll(function() {
		v = $(this).scrollTop();
		if(v > 1) $(this).addClass('moving_scroll');
		else $(this).removeClass('moving_scroll');
	});

	  $(".file-image-preview").change(function() {
		readURL(this);
	  });

	
	$(".bootstrap-datetimepicker-widget").hide();		

	$("input.currency, input.decimal").each(function () {
		dec = $(this).data('decimal');
		if(!dec) dec = ','
		
		sep = $(this).data('separator');
		if(!sep) sep = '.'
		
		max = $(this).attr('max');
		if(!max) max = null
		
		decl = $(this).data('declong');
		if(!decl) decl = 0

		val =  $(this).val();
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');

		if(dec != '.') {
			val = val.replace(dec,'.');
		}

		$(this).autoNumeric('init', {
			aSep: sep,
			aDec: dec,
			mDec: decl				
		}).autoNumeric('set', val);		
	});		


	loadChoosen();
	noticeabs();

	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		

	}else {
		fc = $("#mainApps form input.form-control:visible:not([readonly],disabled,.date,.datetime,.time)").get(0);
		if(!$(".add").length)
		$(fc).focus();
		
		
	}
	
}


$(document).on('keypress',function(e) {
	//if add exists
	if($(".add").length)
	//if press + AND modal is not open AND input is not infocus
	if(e.which == 43 && !$('#formItem').hasClass('in') && !$('input').is(':focus')) {
		loadUrl($(".add[href]", $("#mainApps")).first().attr('href'));
	}
});

document.addEventListener("keydown", KeyCheck); 
function KeyCheck(event)
{
   var KeyID = event.keyCode;
   switch(KeyID)
   {
      case 8: //backspace
		if(!$('input').is(':focus'))
	  	$(".app_link .key-backspace").click();
      break;
      case 46: //delete
		  $("input.delete, submit.delete").parents('form').submit();
		  $("[type=submit].delete").parents('form').submit();
      break;
	  case 13: //delete
	  	if($("#confirmDelete").hasClass('in'))
		  $("#confirmDelete [name=delete]").click();
      break;
	  case 27: //delete
	  
      break;
      default:
		  
      break;
   }
}


function readURL(input, image = false) {
	if (input.files && input.files[0]) {	
	  var reader = new FileReader();
	  reader.onload = function(e) {
		var size = input.files[0].size;
		max = $(input).attr('max');
		if(typeof max !== "undefined")
		{
			if(size > max)
			return false;
		} 

		  $(input).parent().find('.preview-image').html("<img src='" +  e.target.result + "' style='max-width:100%'> </img>").after("<span class='icon-times delete-image'></span>");

		  $(".icon-times").click(function(z) {
			  $(this).parent().find(".preview-image").html("");
			  $(this).parent().find("input").val("");
			  $(this).parent().find(".form-file-text").val("Select a picture...");
			  $(this).remove();
			z.preventDefault();
		});
	  }
	  
	  reader.readAsDataURL(input.files[0]);
	}
  }
  

function selectCheck() {
	$('input[type="checkbox"]').click(function(){	
		var $checked = $(this).is(':checked');
		var $target = $(this).attr('target');
		var $subs = $(this).attr('sub-target');
		if($subs) {
			$target = $(this).attr('value');
			var $checkbox = $('input[data-parent="'+$target+'"]');
		} else {
			var $checkbox = $('input[name="'+$target+'"]');
		}
		$('input[type="checkbox"]').next().removeClass('input-error');
		$('input[type="radio"]').next().removeClass('input-error');		
		if($checked) {			
			$checkbox.prop('checked', 1);					
			$($checkbox).parents('.data tr').addClass('active');					
		}
		else {
			$checkbox.prop('checked', 0);	
			$($checkbox).parents('.data tr').removeClass('active');				
		}
	});
	
	$('[target-radio], label[target], radio-name[target]').click(function(e){			
		var $target = $(this).attr('target-radio');
		var $type = $(this).attr('target-type');
		var $checkbox = $('input[data-name="'+$target+'"]');
		var $checked = $($checkbox).is(':checked');
		if($type == 'multiple')
		var $checkbox = $('input[name="'+$target+'"]');
		$('input[type="checkbox"]').next().removeClass('input-error');
		$('input[type="radio"]').next().removeClass('input-error');
		if($(e.target).is('.switch *, a[href]')) {
		} else {
			$('tr').removeClass('active');	
			if($checked) {
				$checkbox.prop('checked', 0);					
			}
			else {
				$checkbox.prop('checked', 1);	
				if($('tr')) $(this).addClass('active');
			}
		}
	});	
	
}

function loadScrollbar() {
	
}
function loadChoosen() {
	if ($.isFunction($.fn.chosen) ) {
		$("select.deselect").chosen({disable_search_threshold: 10,allow_single_deselect: true});
		$("select").chosen({disable_search_threshold: 10}).change(function(event) {
			var ini = $(this).val();
			$(this).removeClass(function (index, classNames) {
				var current_classes = classNames.split(" "), // change the list into an array
				classes_to_remove = []; // array of classes which are to be removed		
				$.each(current_classes, function (index, class_name) {
				// if the classname begins with bg add it to the classes_to_remove array
					if (/s-.*/.test(class_name)) {
						classes_to_remove.push(class_name);
					}
				});
				// turn the array back into a string
				return classes_to_remove.join(" ");
			}).addClass(function () {
				$(this).removeClass($(this).attr("delete-class"));
				t = $('option:selected', this).attr('class');
				$(this).attr("delete-class",t);
				if(t)
				return t;
			});
			
		}).addClass(function () {
			$(this).removeClass($(this).attr("delete-class"));
			t = $('option:selected', this).attr('class');
			$(this).attr("delete-class",t);
			if(t)
			return t;
		});
	}

	$("select").ready(function(){	
		var cl = $(this).val();
		$(this).next('.chosen-container').attr('rel','selected-'+cl);
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			title = $(".app_title").html();
			$(".navbar-logo").html(title);
			$(".app_title").html("");
			
		} else {
			
		}
	});

	
	$(".chosen-container").before(function () {
		$(this).prev().addClass("chosen-select");
		s = $(this).parent().find("select");
		s.css({ position: "absolute", visibility: "hidden", display: "block", width: "auto" });
		panjang = s.width();

		jumlah = $(this).parent().find("select option").length;
		if(jumlah > 12 ) jumlah = 12;
		posisi = $(this).offset().top;


		butuh = 25 * jumlah;
		tinggiDokumen 	= $(document).height();
		tinggiMain 		= $("#mainApps").height();
		tinggiKotak 	= $("#mainApps #app_header").next().height();

		tinggi = Math.max(tinggiDokumen, tinggiKotak, tinggiMain, 600);
		
		p = panjang + 15;
		$(this).attr('style', 'min-width: ' + 	p + 'px !important');
		if(parseInt(tinggi - posisi)-80 >= butuh) {	
		} else {	
			
			$(this).prev().addClass("drop-top");
		}
		
	});

	
	$(".select").change(function () { 
		s = $(this);
		s.css({ position: "absolute", visibility: "hidden", display: "block" });
		panjang = s.width();

		p = panjang + 15;
		$(this).next().attr('style', 'min-width: ' + 	p + 'px !important');
		
	});
}
function loadTable(url,display) {
	if(url) {		
        var tr = true;
        var file = url;
	} else  {
		var tr = false;
        var file = null;
	}
	$('table.data').show();
	var i = 1;
	
	if ($.isFunction($.fn.dataTable)) {
		oTable = $('table.data').dataTable({
			"iDisplayLength": display,
			"bProcessing": tr,
			"bServerSide": tr,
			"sAjaxSource": file,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				selectCheck();
				$('[data-toggle=tooltip]').tooltip();
				$('[data-tooltip=tooltip]').tooltip();
				$('.tips').tooltip();				
				loadChoosen();
				$("tr").click(function(e){
					var i =$("td:first-child",this).find("input[type='checkbox']");					
					var c = i.is(':checked');
					if($(e.target).is('.switch *, a, button, submit, .btn, .button, a *')) {

					} else if(i.length) {
						if(c) {
							i.prop('checked', 0);		
							$(this).removeClass('active');			
						}
						else {
							i.prop('checked', 1);
							$(this).addClass('active');
							$('.input-error').removeClass('input-error');
							
						}
					}
				});		
				$('input[type="checkbox"],input[type="radio"]').wrap(function() {
					if($(this).closest("label").length == 0)
					return "<label>";
				});
				$('input[type="checkbox"],input[type="radio"]:not(.switch-radio)').after("<span class='input-check'>");
				$('table.data tbody a[href]').on('click', function(e){
				   if ($(this).attr('target') !== '_blank'){
					e.preventDefault();	
					loadUrl(this);
				   }				
				});
				i = 0;
			}
		});
		$('table.data th input[type="checkbox"]').parents('th').unbind('click.DT');
	}
}
function loadSpinner() {
	if ($.isFunction($.fn.spinner)) {
		$('.spinner').spinner();
		$('.spinner min-nol').spinner({ min: 0});
		$('#spinnerfast').spinner({ min: -1000, max: 1000, increment: 'fast' });
		$('#spinnerhide').spinner({ min: 0, max: 100, showOn: 'both' });
		$('#spinnernull').spinner({ min: -100, max: 100, allowNull: true });
		$('#spinnerdisable').spinner({ min: -100, max: 100 });
		$('#spinnermaxlen').spinner();
		$('#spinner5').spinner();	
	}
}
function noticeabs(data, text) {
	$("#alert").html("");
	if(text != null) {
		var a = $("<div class='alert "+data+" alert-"+data+"'>"+text+"</div>");	
		if(data) {
			$(".inner .alert").remove();
		} else {
			a = $('.alert');
		}
		a.hide().appendTo("#alert_top");	
		a.fadeIn('slow');
		var a = a.css({margin:'0 auto'});	
		a.on('click', function(e) {
			$(this).fadeOut();		
		}); 
		
		setTimeout(function(){
			a.fadeOut('slow');
		}, 10000);
		setTimeout(function(){				
			a.remove();	
		}, 11000);	
	}
	 else {
		var a = $(data);
		if(data) {
			$(".inner .alert").remove();
		} else {
			a = $('.alert').addClass('alert-big');
		}
		a.hide().appendTo("#alert_top");	
		a.fadeIn('slow');
		var a = a.css({margin:'0 auto'});	
		a.on('click', function(e) {
			$(this).fadeOut();		
		}); 
		
		setTimeout(function(){
			a.fadeOut('slow');
		}, 10000);
		setTimeout(function(){				
			a.remove();	
		}, 11000);	

	}
}

function notice(data, text) {
	$("#alert").html("");
	if(text != null) {
		
		if(Array.isArray(text)) {
			var str = '<ul>'
			text.forEach(function(tx) {
				str += '<li>'+ tx + '</li>';
			}); 
			str += '</ul>';
			text = str;
		}
		var a = $("<div class='alert "+data+" alert-"+data+"'>"+text+"</div>").hide().appendTo("#alert").fadeIn().css({display:'block'});		
	

	} 
	else if(typeof data === 'object') {
		$("#alert").html("");
		var a = $("<div class='alert "+data.status+" alert-"+data.status+"'>"+data.text+"</div>").hide().appendTo("#alert").fadeIn().css({display:'block'});	
	}
	else {
		var a = $(data).hide().appendTo("#alert").fadeIn().css({display:'block'});	
	}
	
	$("#alert script").removeAttr('style');	
	setTimeout(function(){
		a.fadeOut('slow');
	}, 10000);
	setTimeout(function(){				
		a.remove();	
	}, 11000);	
	a.on('click', function(e) {
		$(this).fadeOut();
	});
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

(function($) {    
    $.LoadingDot = function(el, options) {        
        var base = this;        
        base.$el = $(el);                
        base.$el.data("LoadingDot", base);        
        base.dotItUp = function($element, maxDots) {
            if ($element.text().length == maxDots) {
                $element.text("");
            } else {
                $element.append(".");
            }
        };
        
        base.stopInterval = function() {    
            clearInterval(base.theInterval);
        };
        
        base.init = function() {
        
            if ( typeof( speed ) === "undefined" || speed === null ) speed = 300;
            if ( typeof( maxDots ) === "undefined" || maxDots === null ) maxDots = 3;            
            base.speed = speed;
            base.maxDots = maxDots;                                    
            base.options = $.extend({},$.LoadingDot.defaultOptions, options);                        
            base.$el.html("<span>" + base.options.word + "<em></em></span>");            
            base.$dots = base.$el.find("em");
            base.$loadingText = base.$el.find("span");
            
            base.$el.css("position", "relative");
            base.$dots.css({"position": "absolute"});
                          
            base.theInterval = setInterval(base.dotItUp, base.options.speed, base.$dots, base.options.maxDots);
            
        };        
        base.init();    
    };
    
    $.LoadingDot.defaultOptions = {
        speed: 300,
        maxDots: 3,
        word: "Loading"
    };
    
    $.fn.LoadingDot = function(options) {        
        if (typeof(options) == "string") {
            var safeGuard = $(this).data('LoadingDot');
			if (safeGuard) {
				safeGuard.stopInterval();
			}
        } else { 
            return this.each(function(){
                (new $.LoadingDot(this, options));
            });
        }         
    };
    
})(jQuery);
