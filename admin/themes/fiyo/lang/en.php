<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2015 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined("_FINDEX_") or die("Access Denied");

/************************************
*		  Global Translate 			*
*************************************/
define('dashboard_News', 'Berita dari Fiyo CMS');