<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$app = [
	"name" 		=> "Transaction",
	"table" 	=> FDBPrefix."payment_transaction",
	"type"		=> "create",
	"index"		=> "id_transaction",
	"index_item"=> "id_transaction",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index"=> "invoice_id", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];
