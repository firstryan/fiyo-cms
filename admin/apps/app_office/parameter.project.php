<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$app = [
	"name" 		=> "Project",
	"table" 	=> FDBPrefix."office_project",
	"table_item"=> FDBPrefix."office_project_item",
	"type"		=> "create",
	"index"		=> "id_project",
	"index_item"=> "id_item",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index"=> "project_id", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];

//modified parameter when TAB view
$tab = explode("/", app_param('type'));
if(in_array('task',$tab)) {
	$app["table_item"] =  FDBPrefix."office_task";
	$app["index_item"] =  "id_task";
}

	
//modified parameter when TAB view
$tab = explode("/", app_param('type'));
if(in_array('contract',$tab)) {
	$app["table_item"] =  FDBPrefix."office_task";
	$app["index_item"] =  "id_task";

}

//modified parameter when TAB view
$tab = explode("/", app_param('type'));
if(in_array('document',$tab)) {
	$app["table_item"] =  FDBPrefix."office_docs";
	$app["index_item"] =  "id_doc";

}

//modified parameter when TAB view
$tab = explode("/", app_param('type'));
if(in_array('invoice',$tab)) {
	$app["table_item"] =  FDBPrefix."office_invoice";
	$app["index_item"] =  "id_invoice";

}