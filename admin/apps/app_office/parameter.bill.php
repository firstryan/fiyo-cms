<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$app = [
	"name" 		=> "Billing",
	"table" 	=> FDBPrefix."office_bill",
	"table_item" => FDBPrefix."office_bill_item",
	"type"		=> "create",
	"index"		=> "id_bill",
	"index_item" => "id_item",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index" => "bill_id", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];