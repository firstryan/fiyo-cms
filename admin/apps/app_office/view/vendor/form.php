<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
$(function () {
	$("form#mainForm").submit(function (e) {
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			notice(result);		//print notice alert
			id = parseInt(result.id); // set ID
			if(result.status == 'success' && id > 0){

				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
		});
	});
	

});
</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Data</h5>
		</header>							
			<div>
			<table>
				<tr>		
					<td>
						Nama
					</td>
					<td>
						<?= Form::text('name','',["required" => "required", "size" => "50"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Personal
					</td>
					<td>
						<?= Form::text('personal','',["required" => "required", "size" => "50"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Email
					</td>
					<td>
						<?= Form::email('email'); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Website
					</td>
					<td>
						<?= Form::url('website'); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Phone
					</td>
					<td>
						<?= Form::text('phone'); ?> &nbsp;  Fax.
						<?= Form::text('fax'); ?>
					</td>
				</tr>
				<tr>		
					<td>
						HP
					</td>
					<td>
						<?= Form::text('hp'); ?> 
					</td>
				</tr>
				<tr>		
					<td>
						Negara
					</td>
					<td>
						<?= Form::text('country','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Provinsi
					</td>
					<td>
						<?= Form::text('state','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Kota
					</td>
					<td>
						<?= Form::text('city','',["required" => "required"]); ?> &nbsp;  Zip.
						<?= Form::text('postcode'); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Alamat
					</td>
					<td>
						<?= Form::textarea('address','',["required" => "required", "rows" => "2"]); ?>
					</td>
				</tr>
			</table>		
		</div>  
	</div>
</div>

<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>

<?php 
// if(Req::get('id')) include('form.item.php');
?>