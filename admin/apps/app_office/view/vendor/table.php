<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>	

<script type="text/javascript">	


$(function() {

	url = "?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=list&token=<?php echo USER_TOKEN; ?>";
	if ($.isFunction($.fn.dataTable)) {
		oTable = $('table.data').dataTable({
			"bProcessing": true,
   			"bFilter": true,
			"bServerSide": true,
			"sAjaxSource": url,
			"bJQueryUI": true,  
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				selectCheck();
				$('[data-toggle=tooltip]').tooltip();
				$('[data-tooltip=tooltip]').tooltip();
				$('.tips').tooltip();	

				$("tr").click(function(e){
					var i =$("td:first-child",this).find("input[type='checkbox']");					
					var c = i.is(':checked');
					if($(e.target).is('.switch *, a[href]')) {					   
					} else if(i.length){
						if(c) {
							i.prop('checked', 0);		
							$(this).removeClass('active');			
						}
						else {
							i.prop('checked', 1);
							$(this).addClass('active');
						}
					}
				});			

				$(".editor.activator label").click(function(){
					var parent = $(this).parents('.switch');
					var id = $('.number',parent).attr('value');	
					var value = $('.type',parent).attr('value');
					if(value == 1) value = 0; else value = 1;
					$.ajax({
						url: "?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>status",
						data: "stat="+value+"&id="+id,
						success: function(data){
						if(value == 1)
							$('.type',parent).val("1");
						else 
							$('.type',parent).val("0");				
							notice(data);	
						}
					});
				});
				
				$(".editor.featured label").click(function(){
					var parent = $(this).parents('.switch');
					var id = $('.number',parent).attr('value');	
					var value = $('.type',parent).attr('value');
					if(value == 1) value = 0; else value = 1;
					$.ajax({
						url: "?app=article&api=article-status",
						data: "fp="+value+"&id="+id,
						success: function(data){
						if(value == 1)
							$('.type',parent).val("1");
						else 
							$('.type',parent).val("0");
							notice(data);		
						}
					});
				});
							
				$(".editor .cb-enable").click(function(){		
					var parent = $(this).parents('.switch');
					$('.cb-disable',parent).removeClass('selected');
					$(this).addClass('selected');
					$('.checkbox',parent).attr('checked', false);	
				});

				$(".editor .cb-disable").click(function(){		
					var parent = $(this).parents('.switch');
					$('.cb-enable',parent).removeClass('selected');
					$(this).addClass('selected');
					$('.checkbox',parent).attr('checked', false);	
				});
		
				
				$('table.data tbody a[href]').on('click', function(e){
				   if ($(this).attr('target') !== '_blank'){
					e.preventDefault();	
					loadUrl(this);
				   }				
				});
			}
			
		});

		
		$('table.data th input[type="checkbox"]').parents('th').unbind('click.DT');
		if ($.isFunction($.fn.chosen) ) {
			$("select").chosen({disable_search_threshold: 10});
		}
		
		$('.filter').change(function () {
			loadTable()
		});

		function loadTable() {
			var cat 	= $('.category').val();
			var user 	= $('.user').val();
			var year 	= $('.year').val();
			var month 	= $('.month').val();
			var level 	= $('.level').val();
			oTable.fnReloadAjax( "?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=list&cat="+cat+"&year="+year+"&month="+month+"&user="+user+"&level="+level+"&token=<?php echo USER_TOKEN; ?>" );
		} 
		// loadTable();
		
	
		$("#form").submit(function(e){
			e.preventDefault();
			var ff = this;
			var form = $(this);
			var checked = $('input[name="check[]"]:checked').length > 0;
			if(checked) {	
				$('#confirmDelete').modal('show');	
				$('#confirm').on('click', function(){
					data = form.serialize();
					$.get("?app=<?=$app['root'];?>&api=<?=Req::get('view');?>&type=delete", data)
					.done(function (result) {
						notice(result);		
						loadTable();						
						$(".modal").modal('hide');
					})
					.fail(function (result) {
						logs(result.responseText);		
					});
				});		
			} else {
				noticeabs("<?php echo alert('error',Please_Select_Item); ?>");
				$('input[name="check[]"]').next().addClass('input-error');
				return false;
			}
		});
	}
});
</script>


<?= Form::open(["id" => "form", "url"=>"", 'method' => 'POST']); ?>
	<div id="app_header">
		<div class="warp_app_header">		
			<div class="app_title"><?=$app['name'];?></div>
			<div class="app_link">			
				<a class="add btn btn-primary" href="?app=<?=Req::get('app');?>&view=<?=Req::get('view');?>&act=add"><i class="icon-plus"></i><?php echo Add; ?></a>
				<button type="submit" class="delete btn btn-danger btn-grad" title="<?php echo Delete; ?>" value="<?php echo Delete; ?>" name="delete"><i class="icon-trash"></i> &nbsp;<?php echo Delete; ?></button>
				<input type="hidden" value="true" name="delete_confirm"  style="display:none" />

				<!-- <span class="filter-table visible-lg-inline-block">
					<span>
						Jenis
						<select name="cat" class="category filter"  data-placeholder="<?php echo Choose_category; ?>" style="min-width:120px;">
						<option>All</option>
						<?php	
							$sql = DB::table(FDBPrefix.'cashflow_category')->get(); 
							foreach($sql as $rowc){
								if(Req::get('type')==$rowc['id_cat']) $s = "selected"; else $s = "";
								echo "<option value='$rowc[id_cat]' $s>$rowc[category]</option>";
							}						
						?>
						</select>
					</span>

					
					<span>
						Bulan
						<?php		
						Month;							
							$month = [
								"All",
								"Januari",
								"Februari",
								"Maret",
								"April",
								"Mei",
								"Juni",
								"Juli",
								"Agustus",
								"September",
								"Oktober",
								"November",
								"Desember",
							];
							echo Form::select(
								'type',$month, Req::session('MONTH'), ['class' => 'month filter']
							);
						?>	
					</span>
					
					
					
					<span>
						Tahun
						<?php		
							$qr = DB::table($app['table_group'])->select("MIN(YEAR(date)) as year")->get();
							if($qr) $year = $qr[0]['year'];
							
							$year_now = date('Y');
							$year_low = min($year_now, $year);
							$year_max = max($year_now, $year);

							$year = [0 => "All"];
							for($i = $year_low; $i <= $year_max; $i++) {
								$year[$i] = $i;
							}

							echo Form::select(
								'year',$year, Req::session('YEAR'), ['class' => 'year filter']
							);
						?>	
					</span>
					
				</span> -->

				<?php notice(); ?>
			</div>
		</div>		 
	</div>
	<table class="data">
		<thead>
			<tr>								  
				<th style="width:1% !important;" class="no" colspan="0" id="ck">  
					<input type="checkbox" id="checkall" target="check[]">
				</th>
				<th style="width:15% !important;">Nama</th>
				<th style="width:10% !important;">Personal</th>
				<th style="width:10% !important;">Email</th>
				<th style="width:10% !important;">Telp.</th>
				<th style="width:20% !important;">Address</th>
			</tr>
		</thead>
		<tbody>
		
			<tr><td colspan="7" class="text-center">Loading...</td></tr>	
			
        </tbody>			
	</table>
<?=Form::close(); ?>



<div id="cetak" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Print Preview</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
        <div class="frame"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
//JS script
$('.cetak').on('click', function(e){
  e.preventDefault();
  
  	$('#cetak iframe').remove();
  	$('#cetak').modal('show').find('.frame').append("<iframe src=" + $(this).data('url') + " class='full'></iframe> ");
});

</script>