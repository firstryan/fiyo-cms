<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
		<header>
			<h5>Transaction</h5>
			<button type="button" class="btn btn-primary btn-lg right add-transaction" data-toggle="modal" data-target="#formtransaction"><i class="icon-plus"></i> <?= Add; ?></button>
		</header>							
		<span class="table-transaction">				
		</span>  
	</div>
</div>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadTransaction(ref) {
        if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=transaction/list&id="+pid+"&act="+act,
            success: function(data){ 
                $(".table-transaction").html(data);
                
                $(".delete-transaction").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=transaction/delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							$(".modal").modal('hide');
							notice(result);		
							loadTransaction();
							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-transaction").click(function(e) {					
                    i = $(this).data('id');
                    e.preventDefault();
					$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=transaction/data&id=" + i)
						.done(function (result) {
							if(result.status == 'ok') 
							loadForm(result.data);
						})
						.fail(function(result) {
							logs(result)
						})
                });	
            }
        });
    }   loadTransaction();

	
    function loadForm(data = false) {
		if(!$('#formtransaction').length) return false;
		
		form = "#transaction-form";
		if($(form).validate()) 
		$(form).validate().resetForm();

		//Load new form
		if(data === false) {
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	//set any input to null

			$(form +" [name=id_transaction]").val(""); // set index to null		
		} 
		//load existing data
		else { 
			$(form + " [name=amount]").val(data.amount);
			$(form + " [name=date]").val(data.date);
			$(form + " [name=information]").val(data.information);
			$(form + " [name=id_transaction]").val(data.id_transaction);
		}
		i = 0;
		$("#formtransaction").modal("show").on('shown.bs.modal', function () {
			$(form + " [name=transaction]").focus();
		});
	}

	$(".add-transaction").click(function(e) {	
			
		loadForm(false);
	});	
	
	$("#formtransaction").submit(function (e) {
		saveTransaction(e);
	});

	$(document).on('keypress',function(e) {
		//if press + AND modal is not open AND input is not infocus
    	if(e.which == 43 && !$('#formtransaction').hasClass('in') && !$('input').is(':focus')) {
		loadForm();
		}
	});

	function saveTransaction(e)  {
		formtransaction = $("#transaction-form");
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = formtransaction.serialize();
		
		/* send post data */		
		if(formtransaction.valid())
		$.post("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=transaction/save", data).done(function (result) {
			logs(result);
			$(".modal").modal('hide'); // hide modal
			notice(result); // print alert
			loadTransaction(); // load new table
			
		})
		.fail(function(result) {
			console.log(result)
		});

	}
});
</script>

<!-- Modal -->
<div id="formtransaction" class="modal fade" role="dialog">
<?= Form::open( ['url' => '', 'method' => 'post', 'id' => 'transaction-form']) ?>
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Transaction</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modal-body no-padding">
		<?= Form::hidden('invoice_id', Req::get('id')); // id referensi induk ?>
		<?= Form::hidden('id_transaction');// id referensi utama  ?>
		<div class="box box-modal">
			<div>
				<table>					
					<tr>
						<td>Nominal</td>
						<td>
							<?= Form::currency('amount','',["required"]); ?>
						</td>
					</tr>				
					<tr>		
						<td>Tanggal Transaksi</td>
						<td>
							<?= Form::datetime('date','',["required"]); ?>
						</td>
					</tr>	
					<tr>
						<td>Metode</td>
						<td><?php
							$sql = DB::table(FDBPrefix."payment_method")
								->get();
								
							$method = [];
							foreach($sql as $k=>$v) {
								$method[$v['id_method']] = $v['payment_information'];				
							}
							

							?>
							<?= Form::select('method',$method,'',["style"=>"width: 50px","min"=>"1"]); ?>
						</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td>
							<?= Form::textarea('information','',["rows" => "3"]); ?>
						</td>
					</tr>
				</table>	
			</div>
		</div>	 
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Close; ?></button>
			<?= Form::submit("<i class='icon-check'></i> ". Save,["class"=>"btn btn-success save-transaction"]); ?>
      </div>
    </div>

  </div>
  <?=Form::close();?>
</div>

