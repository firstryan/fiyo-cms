<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
		<header>
			<h5>Terms of Reference (Task)</h5>
			<button type="button" class="btn btn-primary btn-lg right add-item" data-toggle="modal" data-target="#formItem"><i class="icon-plus"></i> <?= Add; ?></button>
		</header>							
		<span class="table-item">				
		</span>  
	</div>
</div>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadData(ref) {
		if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=contract/list&id="+pid+"&act="+act,
            success: function(data){ 
                $(".table-item").html(data);
                
                $(".delete-item").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=contract/delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							$(".modal").modal('hide');
							notice(result);		
							loadData();
							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-item").click(function(e) {
                    i = $(this).data('id');
					e.preventDefault();
					loadParent(i) ;
					$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=contract/data&id=" + i)
						.done(function (result) {
							if(result.status == 'ok') 
							loadForm(result.data);
						})
						.fail(function(result) {
							logs(result)
						})
                });	
            }
        });
    }   loadData();

	
    function loadForm(data = false) {
		if(!$('#formItem').length) return false;
		
		form = "#item-form";
		$(form).validate().resetForm();

		//Load new form
		if(data === false) {
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	//set any input to null

			$(form +" [name=task]").val(""); // set index to null	
			$(form +" [name=id_task]").val(""); // set index to null
		} 
		//load existing data
		else { 

			$(form + " [name=task]").val(data.task);
			$(form + " [name=parent_id]").val(data.parent_id).trigger("chosen:updated");
			$(form + " [name=id_task]").val(data.id_task);
		}
		i = 0;
		$("#formItem").modal("show").on('shown.bs.modal', function () {
			$(form + " [name=item]").focus();
		});
	}

	$(".add-item").click(function(e) {
		loadParent(null);		
		loadForm(false);
	});	
	
	$("#formItem").submit(function (e) {
		saveItem(e);
	});

	$(document).on('keypress',function(e) {
		//if press + AND modal is not open AND input is not infocus
    	if(e.which == 43 && !$('#formItem').hasClass('in') && !$('input').is(':focus')) {
		loadForm();
		}
	});

	function loadParent(id = null) {
		reff = url.searchParams.get("id");
		$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=contract/parent&id=" + i +"&reff="+reff)
		.done(function (result) {

			$(".parent").html(result.data).trigger("chosen:updated");

		})
		.fail(function(result) {
			logs(result)
		})
	}

	function saveItem(e)  {
		formItem = $("#item-form");
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = formItem.serialize();

		/* send post data */		
		if(formItem.valid())
		$.post("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=contract/save", data).done(function (result) {
			$(".modal").modal('hide'); // hide modal
			notice(result); // print alert
			loadData(); // load new table
			
		})
		.fail(function(result) {
			logs(result)
		});

	}
});
</script>

<!-- Modal -->
<div id="formItem" class="modal fade" role="dialog">
<?= Form::open( ['url' => '', 'method' => 'post', 'id' => 'item-form']) ?>
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Item</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modal-body no-padding">
		<?= Form::hidden($app['reff_index'], Req::get('id')); ?>
		<?= Form::hidden('id_task'); ?>
		<div class="box box-modal">
			<div>
				<table>					
					<tr>		
						<td style="width: 30%">Task</td>
						<td>
							<?= Form::text('task','',["required","style" =>"width: 90%"]); ?>
							<br><small><i> *) &nbsp;write summary description</i></small>
							<?= Form::hidden('reff_id'); ?>
						</td>
					</tr>
					
					<tr>
						<td>Parent Task</td>
						<td>
							<?php													
								echo Form::select(
									'parent_id', [], '',
									["class" => "form-control parent w90"]
								);
							
							?>
						</td>
					</tr>	
					<tr>		
						<td style="width: 30%">Sort</td>
						<td>
							<?= Form::number('sort','',["min" => "1","style" =>"width: 40px"]); ?>
						</td>
					</tr>
				</table>	
			</div>
		</div>	 
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Close; ?></button>
			<?= Form::submit("<i class='icon-check'></i> ". Save,["class"=>"btn btn-success save-item"]); ?>
      </div>
    </div>

  </div>
<?=Form::close(); ?>
</div>

