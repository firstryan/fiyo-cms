<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
	<header>
			<h5>Contract</h5>
				<button type="submit" class="btn btn-success right" data-save="edit" value="<?php echo Save; ?>" name="edit"><i class="icon-check"></i> <?php echo Save; ?></button>	
		</header>	
		
		<div>
			<table>
			
				<tr>		
					<td>
						Project Start
					</td>
					<td>						
						<?= Form::date('date_start'); ?>
					</td>
					<td class="text-right">	
						Project Finish
					</td>
					<td>						
						<?= Form::date('date_finish'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Contract Type
					</td>
					<td>
						<?php
							$type = [
								"Monthly"=>"Monthly",
								"Quarterly"=>"Quarterly",
								"Semi-annually"=>"Semi-annually",
								"Annually"=>"Annually",
								"One-time"=>"One-time",
							]
						?>
						<?= Form::select('contract',$type); ?>	   
					</td>
					<td class="text-right">
						End of Contract
					</td>
					<td>						
						<?= Form::date('date_end'); ?>
					</td>
				</tr>
			
				<tr>
					<td>
						Payment Terms
					</td>
					<td>						
						<?php
							$terms = [
								"Cash"=>"Cash",
								"2 Terms"=>"2 Terms",
								"3 Terms"=>"3 Terms",
								"Annually"=>"Annually",
								"Credit"=>"Credit",
							]
						?>
						<?= Form::select('payment_terms',$terms); ?>	     
					</td>
				
					<td class="text-right">
						Taxable
					</td>
					<td>
						<?php
							$tax = [
								"Taxed"=>"Taxed",
								"Untaxed"=>"Untaxed",
							]
						?>
						<?= Form::select('tax',$tax); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Payment Status
					</td>
					<td>						
						<?php
							$status = [
								"Unpaid"=>"Unpaid",
								"Paid"=>"Paid",
								"Colection"=>"Colection",
								"Cancel"=>"Cancel",
								"Draft"=>"Draft",
								"DP"=>"DP",
							]
						?>
						<?= Form::select('payment_status',$status); ?>	     
					</td>
					<td class="text-right">
						<u><b>Contract Value</b></u>
					</td>
					<td>					
						<?= Form::currency('value','',['size' =>  12]); ?>					   
					</td>
				</tr>				
			</table>		
		</div>  
	</div>
</div>



<?= Form::hidden('vendor_id','',["class" => "vendor_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>



<div class="box-left w60">
	<?php 
	if(Req::get('id')) include('form.item.contract.php');
	?>
</div>


<div class="box-right w40">
	<?php 
	if(Req::get('id')) include('form.item.document.php');
	?>
<!-- Akhir box -->
</div>