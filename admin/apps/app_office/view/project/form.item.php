<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
		<header>
			<h5>Item</h5>
			<button type="button" class="btn btn-primary btn-lg right add-item" data-toggle="modal" data-target="#formItem"><i class="icon-plus"></i> <?= Add; ?></button>
		</header>							
		<span class="table-item">				
		</span>  
	</div>
</div>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadData(ref) {
        if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=item/list&id="+pid+"&act="+act,
            success: function(data){ 
                $(".table-item").html(data);
                
                $(".delete-item").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=item/delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							$(".modal").modal('hide');
							notice(result);		
							loadData();
							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-item").click(function(e) {
                    i = $(this).data('id');
                    e.preventDefault();
					$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=item/data&id=" + i)
						.done(function (result) {
							if(result.status == 'ok') 
							loadForm(result.data);
						})
						.fail(function(result) {
							logs(result)
						})
                });	
            }
        });
    }   loadData();

	
    function loadForm(data = false) {		
		if(!$('#formItem').length) return false;
		
		form = "#item-form";
		$(form).validate().resetForm();

		logs(data);
		//Load new form
		if(data === false) {
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	//set any input to null

			$(form +" [name=id]").val(""); // set index to null		
			$(form +" [name=qty]").val(1); 	
		} 
		//load existing data
		else { 
			$(form + " [name=item]").val(data.item);
			$(form + " [name=price]").val(data.price);
			$(form + " [name=qty]").val(data.qty);
			$(form + " [name=description]").val(data.description);
			$(form + " [name=cycle]").val(data.cycle);
			$(form + " [name=id_item]").val(data.id_item);
		}
		i = 0;
		$("#formItem").modal("show").on('shown.bs.modal', function () {
			$(form + " [name=item]").focus();
		});
	}

	$(".add-item").click(function(e) {		
		loadForm(false);
	});	
	
	$("#formItem").submit(function (e) {
		saveItem(e);
	});

	$(document).on('keypress',function(e) {
		//if press + AND modal is not open AND input is not infocus
    	if(e.which == 43 && !$('#formItem').hasClass('in') && !$('input').is(':focus')) {
		loadForm();
		}
	});

	function saveItem(e)  {
		formItem = $("#item-form");
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = formItem.serialize();

		/* send post data */		
		if(formItem.valid())
		$.post("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=item/save", data).done(function (result) {
			$(".modal").modal('hide'); // hide modal
			logs(result);
			notice(result); // print alert
			loadData(); // load new table
			
		})
		.fail(function(result) {
			logs(result)
		});

	}
});
</script>

<!-- Modal -->
<div id="formItem" class="modal fade" role="dialog">
<?= Form::open( ['url' => '', 'method' => 'post', 'id' => 'item-form']) ?>
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Item</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modal-body no-padding">
		<?= Form::hidden($app['reff_index'], Req::get('id')); ?>
		<?= Form::hidden($app['index_item']); ?>
		<div class="box box-modal">
			<div>
				<table>					
					<tr>		
						<td style="width: 30%">Product / Service</td>
						<td>
							<?= Form::text('item','',["required","style" =>"width: 90%"]); ?>
							<?= Form::hidden('reff_id'); ?>
						</td>
					</tr>
					<tr>
						<td>Price</td>
						<td>
							<?= Form::currency('price','',["required","style"=>"width: 100px"]); ?>
						</td>
					</tr>
					<tr>
						<td>Qty</td>
						<td>
							<?= Form::number('qty',1,["style"=>"width: 50px","min"=>"1"]); ?>
						</td>
					</tr>
					<tr>
						<td>Payment Cycle</td>
						<td>
							<?php
								$cycle = [
									"According to Contract" => "According to Contract",
									"One-time"=>"One-time",
									"Monthly"=>"Monthly",
									"Querterly"=>"Querterly",
									"Semi-annually"=>"Semi-annually",
									"Annually"=>"Annually",
								]
							?>			
							<?= Form::select('cycle',$cycle); ?>	
						</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<?= Form::text('description','',["style" =>"width: 90%"]); ?>
						</td>
					</tr>
				</table>	
			</div>
		</div>	 
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Close; ?></button>
			<?= Form::submit("<i class='icon-check'></i> ". Save,["class"=>"btn btn-success save-item"]); ?>
      </div>
    </div>

  </div>
</div>

