<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
$(function () {
	$("form#mainForm").submit(function (e) {

		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			
			logs(result);
			id = parseInt(result.id); // set ID
			
			if(result.status == 'success' && id > 0){

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");

				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
			notice(result);		//print notice alert
		})
		.fail(function(result) {
			logs(result)
		});
	});
	
	
	var searchRequest =  null;
	var targetFile =  '?app=<?=Req::get('app');?>&api=vendor&type=autocomplete';
    
 	$(".vendor").autocomplete({
		source: function(request, response) {
			if (searchRequest !== null) {
				searchRequest.abort();
			}
			searchRequest = $.ajax({
				url: targetFile,
				dataType: "json",
				data: {term: request.term},
				success: function(data) {
					searchRequest = null;					
					response($.map(data, function(item) {
						return {
							id: item.id,
							nik: item.nik,
							telp: item.telp,
							nama: item.nama,
							value: item.value,
							label: item.label,
							shift: item.shift,
							vendor: item.vendor,
							lokasi: item.lokasi,
							fungsi: item.fungsi,
							content: item.content,
							golongan: item.golongan,
						};
					}));
				}
			}).fail(function(data) {
				searchRequest = null;
			});
		},
        minLength: 1,
        select: function(event, data) {
			$(".vendor").val(data.item.id);
			$(".vendor_id").val(data.item.id);
            
            if(data.item.value != '') {
                $(".vendor").attr('readonly','readonly');
                $(".ac_close").show();
            }
			//daftarDoc(data.item.value);
        },
        html: true, 
        open: function(event, ui) {
          $(".ui-autocomplete").css("z-index", 1000);
        }
	}).data("ui-autocomplete")._renderItem = function (ul, item) {
        if(item.label == 'readonly')
            return $('<li/>', {'class': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);	
        else         
            return $('<li/>', {'data-value': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);				
	};
    
    $(".ac_close").click(function () {
        $(".vendor").val('').removeAttr('readonly').focus();  
		$(".vendor_id").val('');
        $(".ac_close").hide();
    });


});


</script>

<div class="box-left">
	<div class="box">								
		<header>
			<h5>Overview</h5>
		</header>	
		
		<div>
			<table>
				<tr>		
					<td>
						Project Title
					</td>
					<td>					
						<?= Form::text('title','',["required" => "required","style" => "width: 90%"]); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Client
					</td>
					<td>
						<?php
                            //set disable if nama not found
                            if(isset($data['vendor']))
                                $disable = "disabled"; else $disable = "" ;

                            echo Form::text("vendor","", ["class" => "vendor", "required", $disable,  "style" => "width: 90%", "placeholder"=>"Vendor Name / Personal"  ]); 
                        ?>     
					   <div class="btn-danger btn ac_close"  <?php if(!isset($data['nama_pegawai'])) : ?> style="display:none" <?php endif; ?>><i class="icon-times"></i></div>
					   
					   
					</td>
				</tr>
				
				<tr>		
					<td>
						Refferal
					</td>
					<td>					
						<?= Form::text('refferal'); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Order Date
					</td>
					<td>					
						<?= Form::date('date_order','',["required" => "required"]); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Budget
					</td>
					<td>					
						<?= Form::currency('budget'); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Status
					</td>
					<td>					
						<?php
							$level = [
								"Offering"=>"Offering",
								"Under Construction"=>"Under Construction",
								"Revision"=>"Revision",
								"Maintenance"=>"Maintenance",
								"Terminated"=>"Terminated",
								"Canceled"=>"Canceled",
								"Active"=>"Active",
								"Release"=>"Release",
							]
						?>			
						<?= Form::select('status',$level); ?>					   
					</td>
				</tr>
				
			</table>		
		</div>  
	</div>

	<div class="box">								
		<header>
			<h5>Technical</h5>
		</header>	
		
		<div>
			<table>
				<tr>		
					<td>
						Workload
					</td>
					<td>					
						<?php
							$level = [
								"Low"=>"Low",
								"Medium"=>"Medium",
								"Hard"=>"Hard",
								"Intense"=>"Intense",
							]
						?>			
						<?= Form::select('workload',$level); ?>		
						&nbsp; &nbsp; 
						&nbsp; &nbsp; 
						Priority
						
						&nbsp; 
						<?php
							$level = [
								"Low"=>"Low",
								"Medium"=>"Medium",
								"High"=>"High",
							]
						?>			
						<?= Form::select('workload',$level); ?>		  
					</td>
				</tr>
				<tr>		
					<td>
						Progress
					</td>
					<td>					
						<span>0<span>%  &nbsp; <?= Form::button("<i class='icon-file-text'></i> Progress Reports",['class' => 'btn btn-primary']); ?>					   
					</td>
				</tr>				
				
				<tr>		
					<td>
						Teams
					</td>
					<td>					
						<?= Form::currency('team'); ?>	
						<?= Form::button("<i class='icon-users'></i>",['class' => 'btn btn-primary']); ?>					   
					</td>
				</tr>
				
			</table>		
		</div>  
	</div>
	
	<div class="box">								
		<header>
			<h5>Documents</h5>  <?= Form::button(Add." <i class='icon-plus'></i>",['class' => 'btn btn-primary right']); ?>		
		</header>	
		
		<div>
			
		<table class="data">
				<tr>		
					<td>
						#1
					</td>
					<td>					
						Judul File				   
					</td>
					
					<td>					
						<?= Form::button("<i class='icon-times'></i>",['class' => 'btn btn-primary right']); ?>						   
					</td>
				</tr>
			</table>			
		</div>  
	</div>

<!-- Akhir box -->
</div>


<div class="box-right">

<div class="box">								
		<header>
			<h5>Contract</h5>
		</header>	
		
		<div>
			<table>
			
				<tr>		
					<td>
						Project Start
					</td>
					<td>						
						<?= Form::date('date_start'); ?>
					</td>
					<td class="text-right">	
						Project Finish
					</td>
					<td>						
						<?= Form::date('date_finish'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Contract Type
					</td>
					<td>
						<?php
							$type = [
								"Monthly"=>"Monthly",
								"Quarterly"=>"Quarterly",
								"Semi-annually"=>"Semi-annually",
								"Annually"=>"Annually",
								"One-time"=>"One-time",
							]
						?>
						<?= Form::select('payment_method',$type); ?>	   
					</td>
					<td class="text-right">
						End of Contract
					</td>
					<td>						
						<?= Form::date('date_end'); ?>
					</td>
				</tr>
			
				<tr>
					<td>
						Payment Terms
					</td>
					<td>						
						<?php
							$terms = [
								"Cash"=>"Cash",
								"2 Terms"=>"2 Terms",
								"3 Terms"=>"3 Terms",
								"Annually"=>"Annually",
								"Credit"=>"Credit",
							]
						?>
						<?= Form::select('payment_terms',$terms); ?>	     
					</td>
				
					<td class="text-right">
						Taxable
					</td>
					<td>
						<?php
							$tax = [
								"Taxed"=>"Taxed",
								"Untaxed"=>"Untaxed",
							]
						?>
						<?= Form::select('tax',$tax); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Payment Status
					</td>
					<td>						
						<?php
							$status = [
								"Unpaid"=>"Unpaid",
								"Paid"=>"Paid",
								"Colection"=>"Colection",
								"Cancel"=>"Cancel",
								"Draft"=>"Draft",
							]
						?>
						<?= Form::select('payment_terms',$status); ?>	     
					</td>
					<td class="text-right">
						<u><b>Contract Value</b></u>
					</td>
					<td>					
						<?= Form::currency('value','',['size' =>  12]); ?>					   
					</td>
				</tr>
				
			</table>		
		</div>  
	</div>

	<div class="box">								
		<header>
			<h5>Billable items</h5>  <?= Form::button(Add." <i class='icon-plus'></i>",['class' => 'btn btn-primary right']); ?>		
		</header>	
		
		<div>
			<table class="data">
				
			<thead>
				<tr>		
					<th>
						No
					</th>
					<th class="text-right">					
					Description		   
					</th>
					<th>					
					Sub-Total				   
					</th>					
					<th>					
											   
							</th>
				</tr>
			</thead>

			<tr>		
					<td>
						#1
					</td>
					<td>					
						Server + Domain				   
					</td>
					<td style="width: 20%" class="text-right">		
						150.000				   
					</td>
					
					<td style="width: 5%">					
						<?= Form::button("<i class='icon-times'></i>",['class' => 'btn btn-primary right']); ?>						   
					</td>
				</tr>
				<tfoot>
				<tr>		
					<td>
						
					</td>
					<td class="text-right">					
						Total			   
					</td>
					<td>					
						150.000				   
					</td>
					
					<td>					
						<?= Form::button("<i class='icon-times'></i>",['class' => 'btn btn-primary right']); ?>						   
					</td>
				</tr>
				</tfoot>
				
				
			</table>		
		</div>  
	</div>

	<div class="box">								
		<header>
			<h5>Task</h5>  <?= Form::button(Add." <i class='icon-plus'></i>",['class' => 'btn btn-primary right']); ?>		
		</header>	
		
		<div>
			<table class="data">
				<tr>		
					<td>
						#1
					</td>
					<td>					
						Membuat Task 1				   
					</td>
					
					<td>					
						<?= Form::button("<i class='icon-times'></i>",['class' => 'btn btn-primary right']); ?>						   
					</td>
				</tr>
			</table>		
		</div>  
	</div>

	
	
</div>


<?= Form::hidden('vendor_id','',["class" => "vendor_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>

<?php 
if(Req::get('id')) include('form.item.php');
?>