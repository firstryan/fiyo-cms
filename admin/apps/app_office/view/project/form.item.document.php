<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadData(ref) {
        if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=document/list&id="+pid+"&act="+act,
            success: function(data){
                $(".table-item2").html(data);
                
                $(".delete-item").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=document/delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							notice(result);		
							loadData();
							$(".modal").modal('hide');							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-item").click(function(e) {
                    i = $(this).data('id');
					e.preventDefault();
					$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=document/data&id=" + i)
						.done(function (result) {
							if(result.status == 'ok') 
							loadForm2(result.data);
						})
						.fail(function(result) {
							logs(result)
						})
                });
            }
        });
    }   loadData();

	
    function loadForm2(data = false) {
		if(!$('#formItem2').length) return false;
		form = "#item-form2";
		$(form).validate().resetForm();

		//Load new form
		if(data === false) {
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	
			$(form +" [name=doc]").val(""); 	
			$(form +" [name=id_doc]").val(""); 
			$(form + " [name=old_path]").val(""); 
			$(form + " [name=path]").val(""); 
		} 
		//load existing data
		else { 
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	
			$(form + " [name=filename]").val(data.filename);
			$(form + " [name=label]").val(data.label).trigger("chosen:updated");
			$(form + " [name=id_doc]").val(data.id_doc);
			$(form + " [name=old_path]").val(data.path);			
			$(form + " [name=path]").val("").attr("data-exists","true").attr("data-old",data.id_doc);

			if(data.extension) {
				$(form + " [name=path-txt]").val(data.filename + "." + data.extension);
				$(".has-file").show();
			}else {
				$(".has-file").hide();

			}
		}
		i = 0;
		$("#formItem2").modal("show").on('shown.bs.modal', function () {
			$(form + " [name=item]").focus();
		});
		
		
		$(".delete-doc").click(function(e) {
			$(this).parents("td").find("input").val("");
			$(this).parents("form").find("[name=old_path]").val("");
		});	
		
	}

		
		

	$(".add-item2").click(function(e) {
		loadForm2(false);
		
	});	
	
	$("#formItem2").submit(function (e) {
		saveItem2(e);
	});


	function saveItem2(e)  {
		/*static event*/
		e.preventDefault();
		
		formItem = $("#item-form2");
		/* initialize editor */
		// $("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		formData = new FormData(formItem[0]);
			
		/* send post data */	
		if(formItem.valid())
		$.ajax({
			type: 'POST',
			// make sure you respect the same origin policy with this url:
			// http://en.wikipedia.org/wiki/Same_origin_policy
			url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=document/save",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,


			success: function(result){	
				$(".modal").modal('hide'); // hide modal
				notice(result); // print alert
				loadData(); // load new table
			},
			error: function(data) {
				logs(data);			
			},
			xhr: function () {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					$('progress').attr({
						value: 0,
						max: 100,
						});
					// For handling the progress of the upload
					myXhr.upload.addEventListener('progress', function (e) {
					if (e.lengthComputable) {
						$('progress').attr({
						value: e.loaded,
						max: e.total,
						});
					}
					}, false);
				}
				return myXhr;
			}
		
		});
		

		return false;		

	}
});
</script>


<div class="full">
	<div class="box">								
		<header>
			<h5>Documentation</h5>
			<button type="button" class="btn btn-primary btn-lg right add-item2" data-toggle="modal" data-target="#formItem2"><i class="icon-upload"></i> Upload</button>
		</header>							
		<span class="table-item2">				
		</span>  
	</div>
</div>

<!-- Modal -->
<div id="formItem2" class="modal fade" role="dialog">
<?= Form::open( ['url' => '', 'method' => 'post', 'id' => 'item-form2', 'enctype'=> "multipart/form-data"]) ?>
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Documentation</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modal-body no-padding">
		<?= Form::hidden($app['reff_index'], Req::get('id')); ?>
		<?= Form::hidden('id_doc'); ?>
		<?= Form::hidden('old_path'); ?>
		<div class="box box-modal">
			<div>
				<table>					
					<tr>		
						<td style="width: 30%">File Name</td>
						<td>
							<?= Form::text('filename','',["required","style" =>"width: 90%"]); ?>
							
						</td>
					</tr>
					
					<tr>
						<td>Label</td>
						<td>
							<?php
								$label = [
										"Main" => "Primary",
										"Additional" => "Additional",
										"Revision" => "Revision",
										"Archived" => "Archived",
									];
								echo Form::select('label', $label);
							?>
						</td>
					</tr>	
					<tr>		
						<td>File</td>
						<td>	
							<?= Form::file('path', '',["class" => "form-control", "max" => "25000
							", "style" =>"width: 400px"]) ?> 
							
							<span class="has-file">
								<a class="btn btn-danger delete-doc"> <i class="icon-trash"></i></a>
								<a class="btn btn-primary"><i class="icon-download"></i> Download File</a>
							</span>
						</td>
					</tr>
				</table>	
			</div>
		</div>	 
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Close; ?></button>
			<?= Form::submit("<i class='icon-check'></i> ". Save,["class"=>"btn btn-success save-item"]); ?>
      </div>
    </div>

  </div>
  <?= Form::close();?>
</div>

