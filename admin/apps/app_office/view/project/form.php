<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
	formNumber();
	$("form#mainForm").submit(function (e) {
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			
			logs(result);
			id = parseInt(result.id); // set ID
			
			if(result.status == 'success' && id > 0){

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");
				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
			notice(result);		//print notice alert
		});		
		reformNumber();

	});

</script>

<?php 


$id  = (Req::get('id') AND Req::get('act') == 'edit') ? '&id='.$id : '';
$tab = Req::get('tab');
switch($tab) {
	default :
		require_once('form.tab.overview.php');
	break;
	case '1':
		require_once('form.tab.contract.php');
	break;
	case '2':
		require_once('form.tab.technical.php');
	break;
	case '3':
		require_once('form.tab.cost.php');
	break;
	case '4':
		require_once('form.tab.invoice.php');
	break;
	case '5':
		require_once('form.tab.event.php');
	break;
}
?>

<div class="app_link tabs tab-top"> 
	<a class="btn apps <?=activeTab();?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?><?=$id;?>"><i class="icon-list-alt"></i> Overview</a> 
	<a class="btn apps <?=activeTab(1);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=1<?=$id;?>"><i class="icon-book"></i> Contract</a> 
	<a class="btn apps <?=activeTab(2);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=2<?=$id;?>"><i class="icon-tasks"></i> Technical</a> 
	<a class="btn apps <?=activeTab(3);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=3<?=$id;?>"><i class="icon-money"></i> Cost</a> 
	<a class="btn apps <?=activeTab(4);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=4<?=$id;?>"><i class="icon-file-text"></i> Invoices</a> 
	<a class="btn apps <?=activeTab(5);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=5<?=$id;?>"><i class="icon-calendar"></i> Events</a> 
</div>