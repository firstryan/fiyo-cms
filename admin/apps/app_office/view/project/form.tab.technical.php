<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
	<header>
			<h5>Technical</h5>
				<button type="submit" class="btn btn-success right" data-save="edit" value="<?php echo Save; ?>" name="edit"><i class="icon-check"></i> <?php echo Save; ?></button>	
		</header>			
		<div>
			<table>			
				<tr>		
					<td>
						Workload
					</td>
					<td>					
						<?php
							$level = [
								"Low"=>"Low",
								"Medium"=>"Medium",
								"High"=>"High",
								"Extreme"=>"Extreme",
							]
						?>			
						<?= Form::select('workload',$level); ?>		
					</td>
					
					<td>		
						Priority
					
					&nbsp; 
					<?php
						$level = [
							"Low"=>"Low",
							"General"=>"General",
							"High"=>"High",
							"Special"=>"Special",
						]
					?>			
					<?= Form::select('priority',$level); ?>		  
				</td>
				</tr>
				<tr>		
					<td>
						Progress
					</td>
					<td>					
						<span>0<span>%  &nbsp; <?= Form::button("<i class='icon-file-text'></i> Progress Reports",['class' => 'btn btn-primary']); ?>					   
					</td>
				</tr>				
				
				<tr>		
					<td>
						Teams
					</td>
					<td>					
						<?= Form::currency('team'); ?>	
						<?= Form::button("<i class='icon-users'></i>",['class' => 'btn btn-primary']); ?>					   
					</td>
				</tr>			
			</table>		
		</div>  
	</div>
</div>



<?= Form::hidden('vendor_id','',["class" => "vendor_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>

<?php 
if(Req::get('id')) include('form.item.technical.php');
?>