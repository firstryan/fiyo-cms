<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>
<script>
$(function () {
	
var searchRequest =  null;
	var targetFile =  '?app=<?=Req::get('app');?>&api=client&type=autocomplete';
    
 	$(".client").autocomplete({
		source: function(request, response) {
			if (searchRequest !== null) {
				searchRequest.abort();
			}
			searchRequest = $.ajax({
				url: targetFile,
				dataType: "json",
				data: {term: request.term},
				success: function(data) {
					searchRequest = null;					
					response($.map(data, function(item) {
						return {
							id: item.id,
							nik: item.nik,
							telp: item.telp,
							nama: item.nama,
							value: item.value,
							label: item.label,
							shift: item.shift,
							client: item.client,
							lokasi: item.lokasi,
							fungsi: item.fungsi,
							content: item.content,
							golongan: item.golongan,
						};
					}));
				}
			}).fail(function(data) {
				searchRequest = null;
			});
		},
        minLength: 1,
        select: function(event, data) {
			$(".client").val(data.item.id);
			$(".client_id").val(data.item.id);
            
            if(data.item.value != '') {
                $(".client").attr('readonly','readonly');
                $(".ac_close").show();
            }
			//daftarDoc(data.item.value);
        },
        html: true, 
        open: function(event, ui) {
          $(".ui-autocomplete").css("z-index", 1000);
        }
	}).data("ui-autocomplete")._renderItem = function (ul, item) {
        if(item.label == 'readonly')
            return $('<li/>', {'class': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);	
        else         
            return $('<li/>', {'data-value': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);				
	};
    
    $(".ac_close").click(function () {
        $(".client").val('').removeAttr('readonly').focus();  
		$(".client_id").val('');
        $(".ac_close").hide();
    });
});
</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Overview</h5>
			
			<button type="submit" class="btn btn-success right" data-save="edit" value="<?php echo Save; ?>" name="edit"><i class="icon-check"></i> <?php echo Save; ?></button>	
		</header>	
		
		<div>
			<table>
				<tr>		
					<td>
						Project Title
					</td>
					<td>					
						<?= Form::text('title','',["required" => "required","style" => "width: 90%"]); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Client
					</td>
					<td>
						<?php
                            //set disable if nama not found
                            if(isset($data['client']))
                                $disable = "disabled"; else $disable = "" ;

                            echo Form::text("client","", ["class" => "client", "required", $disable,  "style" => "width: 90%", "placeholder"=>"Client Name / Personal"  ]); 
                        ?>     
					   <div class="btn-danger btn ac_close"  <?php if(!isset($data['nama_pegawai'])) : ?> style="display:none" <?php endif; ?>><i class="icon-times"></i></div>
					   
					   
					</td>
				</tr>
				
				<tr>		
					<td>
						Refferal
					</td>
					<td>					
						<?= Form::text('referral','',[ "style" => "width: 50%"]); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Order Date
					</td>
					<td>					
						<?= Form::date('date_order','',["required" => "required"]); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Budget
					</td>
					<td>					
						<?= Form::currency('budget'); ?>					   
					</td>
				</tr>
				<tr>		
					<td>
						Status
					</td>
					<td>			
						<?php
							$level = [
								"Offering"=>"Offering",
								"Consideration"=>"Consideration",
								"Analyzing"=>"Analyzing",
								"Designing"=>"Designing",
								"Production"=>"Production",
								"Revision"=>"Revision",
								"Improvement"=>"Improvement",
								"Maintenance"=>"Maintenance",
								"Active"=>"Active",
								"Release"=>"Release",
								"Terminated"=>"Terminated",
								"Canceled"=>"Canceled",
								"Fraud"=>"Fraud",
							];

							
							$level = [
								"Offering"=>"Offering",
								"Analyzing"=>"Analyzing",
								"Production"=>"Production",
								"Correction"=>"Correction",
								"Maintenance"=>"Maintenance",
								"Active"=>"Active",
								"Release"=>"Release",
								"Terminated"=>"Terminated",
								"Canceled"=>"Canceled",
							]
						?>			
						<?= Form::select('status',$level); ?>					   
					</td>
				</tr>
				
			</table>		
		</div>  
	</div>
</div>


<?= Form::hidden('client_id','',["class" => "client_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>

<?php 
if(Req::get('id')) include('form.item.php');
?>