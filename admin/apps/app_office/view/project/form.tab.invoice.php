<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
	<header>
			<h5>Latest Data</h5>
		</header>	
		
		<div>
			<table>
			
				<tr>		
					<td>
						Last Invoice
					</td>
					<td>						
						
					
					</td>
					<td class="text-right">	
						Last Transaction
					</td>
					<td>						
						<?= Form::text('date_finish','',['readonly']); ?>
					</td>
				</tr>
				<tr>
					<td>
						Invoices Count
					</td>
					<td>
						
					<?= Form::text('date_finish','',['readonly']); ?>   
					</td>
					<td class="text-right">
						Transaction Amount
					</td>
					<td>						
						<?= Form::date('date_end'); ?>
					</td>
				</tr>
			
				
				</tr>				
			</table>		
		</div>  
	</div>
</div>

<?= Form::hidden('vendor_id','',["class" => "vendor_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>



<div class="box-left w60">
	<?php 
	if(Req::get('id')) include('form.item.invoice.php');
	?>
</div>


<div class="box-right w40">
	<?php 
	if(Req::get('id')) include('form.item.document.php');
	?>
<!-- Akhir box -->
</div>