<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>


<div class="app_link tabs tab-top" style="text-align: center;width: 90%;"> 
	<a class="btn apps <?=activeTab();?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?><?=$id;?>"><i class="icon-list-alt"></i> Overview</a> 
	<a class="btn apps <?=activeTab(1);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=1<?=$id;?>"><i class="icon-book"></i> Contract</a> 
	<a class="btn apps <?=activeTab(2);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=2<?=$id;?>"><i class="icon-tasks"></i> Technical</a> 
	<a class="btn apps <?=activeTab(3);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=3<?=$id;?>"><i class="icon-money"></i> Cost</a> 
	<a class="btn apps <?=activeTab(4);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=4<?=$id;?>"><i class="icon-file-text"></i> Invoices</a> 
	<a class="btn apps <?=activeTab(5);?>" href="?app=<?=app_param('app');?>&view=<?=app_param('view');?>&act=<?=app_param('act');?>&tab=5<?=$id;?>"><i class="icon-calendar"></i> Events</a> 
</div>