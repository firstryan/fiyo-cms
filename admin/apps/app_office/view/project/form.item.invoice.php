<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
		<header>
			<h5>Invoice(s)</h5>
			<a class="btn btn-primary btn-lg right" href="http://localhost/portofolio/admin/?app=office&view=invoice&act=add&project=<?= Req::get('id');?>"><i class="icon-plus"></i> <?= Add; ?></a>
		</header>							
		<span class="table-item">				
		</span>  
	</div>
</div>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadData(ref) {
		if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=invoice/list&id="+pid+"&act="+act,
            success: function(data){ 
                $(".table-item").html(data);
                
                $(".delete-item").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=<?= Req::get('view')?>&type=invoice/delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							$(".modal").modal('hide');
							notice(result);		
							loadData();
							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-item-a").click(function(e) {
					i = $(this).data('id');
					h = $(this).attr('href');
					e.preventDefault(); 
					loadUrl(h)
                });	
            }
        });
	}   loadData();
	


});
</script>


