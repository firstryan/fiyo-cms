<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

//Set ID Refference
$id = (int)Input::get('id');

//Set value for Form model
$row = Database::table(FDBPrefix.'office_client')->
	where("id_client=".(int)Input::get('id'))->
	get();
if(isset($row[0])) 
	$row = $row[0];
else 
	$row = false;

//Check if id is  valid
if(!$id OR !$row) {
	$view = "client";
	if($view) $view = "&view=".$view;
	notice('error','Data tidak ditemukan!'); //set text for error data
	jsRedirect("?app=$app[root]$view");
	die(); //set die form hidden content
}

//Set Form open model	
echo Form::model($row, ['url' => '', 'method' => 'post', 'id' => 'mainForm']);
?>


<?php 

$url_cetak = url("?app=$app[root]&print=sample&id=". Req::get('id'));
$print = "<a data-url='$url_cetak' type='button'  data-print='' class='btn btn-primary cetak' data-toggle='modal' data-target='#cetak'>Cetak <i class='icon-print'></i></a>";
?>
<div id="cetak" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Print Preview</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
        <div class="frame"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
//JS script
$('.cetak').on('click', function(e){
  	e.preventDefault();  
  	$('#cetak iframe').remove();
  	$('#cetak').modal('show').find('.frame').append("<iframe src=" + $(this).data('url') + " class='full'></iframe> ");
});
</script>



	<div id="app_header">
		<div class="warp_app_header">		
			<div class="app_title">Edit Client</div>			
			<div class="app_link">	
				<a class=" btn btn-default" href="?app=<?=Req::get('app');?>&view=<?=Req::get('view');?>" title="<?php echo Prev; ?>">	
				<i class="icon-arrow-left"></i> <?php echo Prev; ?></a>			
				<button type="submit" class="btn btn-success" data-save="edit" title="<?php echo Save; ?>" value="<?php echo Save; ?>" name="edit"><i class="icon-check"></i> <?php echo Save; ?></button>	
				<?=$print;?>
			</div>
			<?php printAlert(); ?>
		</div>			 
	</div>
		
	<?php 
		require('form.php');
	?>	
</form>

