<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

echo Form::open( ['url' => '', 'method' => 'post', 'id' => 'mainForm']);
?>
<div id="app_header">
	<div class="warp_app_header">		
		<div class="app_title"><?php echo Add; ?> Client</div>			
		<div class="app_link">	
			<a class=" btn btn-default" href="?app=<?=Req::get('app');?>&view=<?=Req::get('view');?>" title="<?php echo Prev; ?>">	
			<i class="icon-arrow-left"></i> <?php echo Prev; ?></a>				
			<button type="submit" class="btn btn-success" data-save="add" title="<?php echo Save; ?>" value="<?php echo Save; ?>" name="edit"><i class="icon-check"></i> <?php echo Save; ?></button>		
		</div>
		<?php notice(); ?>
	</div>			 
</div>
	
<?php 
	require('form.php');
?>	
