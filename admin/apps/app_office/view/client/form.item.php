<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<div class="full">
	<div class="box">								
		<header>
			<h5>Item</h5>
			<button type="button" class="btn btn-primary btn-lg right add-item" data-toggle="modal" data-target="#formItem"><i class="icon-plus"></i> <?= Add; ?></button>
		</header>							
		<span class="table-item">				
		</span>  
	</div>
</div>

<script>
$(function () {	
    var url = new URL(window.location.href);
    var pid = url.searchParams.get("id");
    var act = url.searchParams.get("act");

    function loadData(ref) {
        if(ref != null) pid = ref;
        $.ajax({
            url: "?app=<?=$app['root'];?>&api=item-list&id="+pid+"&act="+act,
            success: function(data){ 
                $(".table-item").html(data);
                
                $(".delete-item").click(function(e) {
                    e.preventDefault();
                    i = $(this).data('id');
                    $('#confirmDelete').modal('show');
                    $('#confirm').on('click', function(){
						$.get("?app=<?=$app['root'];?>&api=item-delete&id=" + i + "&_token=<?=USER_TOKEN;?>").done(function (result) {
							$(".modal").modal('hide');
							notice(result);		
							loadData();
							
						})
						.fail(function(result) {
							logs(result)
						})
                    });		                    
                });	
            
                $(".edit-item").click(function(e) {
                    i = $(this).data('id');
                    e.preventDefault();
					$.get("?app=<?=$app['root'];?>&api=item-data&id=" + i)
						.done(function (result) {
							if(result.status == 'ok') 
							loadForm(result.data);
						})
						.fail(function(result) {
							logs(result)
						})
                });	
            }
        });
    }   loadData();

	
    function loadForm(data = false) {		
		if(!$('#formItem').length) return false;
		
		form = "#item-form";
		$(form).validate().resetForm();

		//Load new form
		if(data === false) {
			$(form+" [type=text], "+form+" [type=date], "+form+" [type=number], "+form+" [type=email], "+form+" [type=password]").val("");	//set any input to null

			$(form +" [name=id]").val(""); // set index to null		
			$(form +" [name=qty]").val(1); 	
		} 
		//load existing data
		else { 
			$(form + " [name=item]").val(data.item);
			$(form + " [name=vendor]").val(data.vendor);
			$(form + " [name=value]").val(data.value);
			$(form + " [name=type]").val(data.type).trigger("chosen:updated");
			$(form + " [name=qty]").val(data.qty);
			$(form + " [name=desc]").val(data.desc);
			$(form + " [name=id]").val(data.id);
		}
		i = 0;
		$("#formItem").modal("show").on('shown.bs.modal', function () {
			$(form + " [name=item]").focus();
		});
	}

	$(".add-item").click(function(e) {		
		loadForm(false);
	});	
	
	$("#formItem").submit(function (e) {
		saveItem(e);
	});

	$(document).on('keypress',function(e) {
		//if press + AND modal is not open AND input is not infocus
    	if(e.which == 43 && !$('#formItem').hasClass('in') && !$('input').is(':focus')) {
		loadForm();
		}
	});

	function saveItem(e)  {
		formItem = $("#item-form");
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = formItem.serialize();

		/* send post data */		
		if(formItem.valid())
		$.post("?app=<?=$app['root'];?>&api=item-save", data).done(function (result) {
			$(".modal").modal('hide'); // hide modal
			notice(result); // print alert
			loadData(); // load new table
			
		})
		.fail(function(result) {
			logs(result)
		});

	}
});
</script>

<!-- Modal -->
<div id="formItem" class="modal fade" role="dialog">
<?= Form::open( ['url' => '', 'method' => 'post', 'id' => 'item-form']) ?>
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Item</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modal-body no-padding">
		<?= Form::hidden('group_id', Req::get('id')); ?>
		<?= Form::hidden('id'); ?>
		<div class="box box-modal">
			<div>
				<table>					
					<tr>		
						<td>Jenis Produk / Jasa</td>
						<td>
							<?= Form::text('item','',["required","id" =>"itemOne"]); ?>
						</td>
					</tr>
					<tr>
						<td>Produsen / Vendor</td>
						<td>
							<?= Form::text('vendor'); ?>
						</td>
					</tr>
					<tr>
						<td>Nominal</td>
						<td>
							<?= Form::currency('value','',["required","style"=>"width: 100px"]); ?>
						</td>
					</tr>
					<tr>
						<td>Jenis</td>
						<td>
							<?php									
								$type = ["Income" => "Pemasukan", "Outcome" => "Pengeluaran"];
								echo Form::select(
									'type',$type
								);
							?>					
						</td>
					</tr>
					<tr>
						<td>Qty</td>
						<td>
							<?= Form::number('qty',1,["style"=>"width: 50px","min"=>"1"]); ?>
						</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td>
							<?= Form::text('desc'); ?>
						</td>
					</tr>
				</table>	
			</div>
		</div>	 
      </div>
      <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal"><?= Close; ?></button>
			<?= Form::submit("<i class='icon-check'></i> ". Save,["class"=>"btn btn-success save-item"]); ?>
      </div>
    </div>

  </div>
</div>

