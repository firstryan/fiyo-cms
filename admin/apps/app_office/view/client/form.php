<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
$(function () {
	$("form#mainForm").submit(function (e) {
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			
			notice(result);		//print notice alert
			id = parseInt(result.id); // set ID
			if(result.status == 'success' && id > 0){

				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
		}).fail(function (e) {
			logs(e);
		});
	});
	

});
</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Data</h5>
		</header>							
			<div>
			<table>
				<tr>		
					<td>
						Nama
					</td>
					<td>
						<?= Form::text('name','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Personal
					</td>
					<td>
						<?= Form::text('personal','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Email
					</td>
					<td>
						<?= Form::email('email','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Phone
					</td>
					<td>
						<?= Form::text('phone'); ?> &nbsp;  Fax.
						<?= Form::text('fax'); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Alamat
					</td>
					<td>
						<?= Form::textarea('address','',["required" => "required", "rows" => "3"]); ?>
					</td>
				</tr>
			</table>		
		</div>  
	</div>
</div>

<?= Form::hidden('id_client'); ?>
<?= Form::close(); ?>

<?php 
// if(Req::get('id')) include('form.item.php');
?>