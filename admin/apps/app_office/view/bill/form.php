<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
$(function () {
	$("form#mainForm").submit(function (e) {

		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			
			logs(result);
			id = parseInt(result.id); // set ID
			
			if(result.status == 'success' && id > 0){

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");

				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
			notice(result);		//print notice alert
		})
		.fail(function(result) {
			logs(result)
		});
	});
	
	
	var searchRequest =  null;
	var targetFile =  '?app=<?=Req::get('app');?>&api=vendor&type=autocomplete';
    
 	$(".vendor").autocomplete({
		source: function(request, response) {
			if (searchRequest !== null) {
				searchRequest.abort();
			}
			searchRequest = $.ajax({
				url: targetFile,
				dataType: "json",
				data: {term: request.term},
				success: function(data) {
					searchRequest = null;					
					response($.map(data, function(item) {
						return {
							id: item.id,
							nik: item.nik,
							telp: item.telp,
							nama: item.nama,
							value: item.value,
							label: item.label,
							shift: item.shift,
							vendor: item.vendor,
							lokasi: item.lokasi,
							fungsi: item.fungsi,
							content: item.content,
							golongan: item.golongan,
						};
					}));
				}
			}).fail(function(data) {
				searchRequest = null;
			});
		},
        minLength: 1,
        select: function(event, data) {
			$(".vendor").val(data.item.id);
			$(".vendor_id").val(data.item.id);
            
            if(data.item.value != '') {
                $(".vendor").attr('readonly','readonly');
                $(".ac_close").show();
            }
			//daftarDoc(data.item.value);
        },
        html: true, 
        open: function(event, ui) {
          $(".ui-autocomplete").css("z-index", 1000);
        }
	}).data("ui-autocomplete")._renderItem = function (ul, item) {
        if(item.label == 'readonly')
            return $('<li/>', {'class': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);	
        else         
            return $('<li/>', {'data-value': item.label}).append($('<a/>')
            .append($('<span>' + item.content + '</span>')).append())
            .appendTo(ul);				
	};
    
    $(".ac_close").click(function () {
        $(".vendor").val('').removeAttr('readonly').focus();  
		$(".vendor_id").val('');
        $(".ac_close").hide();
    });

    $(".vendor").focus();

});


</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Data</h5>
		</header>							
			<div>
			<table class="double-col">
				<tr>		
					<td>
						Vendor
					</td>
					<td>
						<?php
                            //set disable if nama not found
                            if(isset($data['vendor']))
                                $disable = "disabled"; else $disable = "" ;

                            echo Form::text("vendor","", ["class" => "vendor", "required", $disable,  "size" => "30", "placeholder"=>"Vendor Name / Personal"  ]); 
                        ?>     
					   <div class="btn-danger btn ac_close"  <?php if(!isset($data['nama_pegawai'])) : ?> style="display:none" <?php endif; ?>><i class="icon-times"></i></div>
					   
					   
					</td>
					
					<td>
						No.Bill
					</td>
					<td>
						<?= Form::number('no','',["required" => "required"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Date Bill
					</td>
					<td>
						<?= Form::date('created_date','',["required" => "required"]); ?>
					</td>
					<td>
						Status
					</td>
					<td>
						<?php
							$status = [
								"Unpaid"=>"Unpaid",
								"Paid"=>"Paid",
								"Colection"=>"Colection",
								"Cancel"=>"Cancel",
								"Draft"=>"Draft",
							]
						?>
						<?= Form::select('status',$status); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Due Date
					</td>
					<td>
						<?= Form::date('due_date','',["required" => "required"]); ?>
					</td>
				</tr>
			</table>		
		</div>  
	</div>
</div>

<?= Form::hidden('vendor_id','',["class" => "vendor_id"]); ?>
<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>

<?php 
if(Req::get('id')) include('form.item.php');
?>