<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();

$chk = Input::get('check');
$qr = false;
foreach($chk as $id) {
    $qr = DB::table($app['table'])
        ->where("$app[index]= ".$id)
        ->update(array("deleted_at"=> date("Y-m-d H:i:s")));
}
       
if($qr) {
    $return = [
        'status' => 'info',
        'text' => Status_Saved
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Req::$get
    ];
} 

header('Content-Type: application/json');
echo json_encode($return);
