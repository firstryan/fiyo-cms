<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table'];
$table2 = $app['table_group'];

if(Input::get('id'))
$item = DB::table($app['table'])
    ->where("group_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_group = $table1.group_id")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:30% !important; text-align: left">Nama</th>
            <th style="width:20% !important;  text-align: left">Personal</th>
            <th style="width:10% !important;  text-align: left">Alamat</th>
            <th style="width:10% !important;  text-align: left">Email</th>
            <th style="width:10% !important;  text-align: left">Telp.</th>
            <th style="width:10% !important;" class="no-print"></th>
        </tr>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index'];
                $total = 0;
                foreach($item as $row) {
                    $status = [
                        "Outcome" => "Pengeluaran",
                        "Income" => "Pemasukan"
                    ];
                    $status = $status[$row['type']];
                    $value = number($row['value']);
                    $stotal = $row['value'] * $row['qty'];
                    
                    if($row['type'] == 'Outcome')  {  
                        $total -= $stotal;
                        $stotal = "(-) &nbsp; " .number($stotal);  
                    }
                    else {
                        $total += $stotal;
                        $stotal = number($stotal);
                    }


                    echo "<tr>";
                    echo "<td>$row[item]</td>";  
                    echo "<td>$row[vendor]</td>";   
                    echo "<td>$status</td>";
                    echo "<td align='right'> $value </td>"; 
                    echo "<td> $row[qty] </td>";
                    echo "<td align='right'>$stotal</td>";   
                    echo "<td align='right' class='no-print'><span class='btn-primary btn edit-item' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                }
                
                echo "<tfoot>";
                echo "<tr>";
                echo "<td colspan='4' class='no-border'></td>";  
                echo "<td>Total</td>";  
                echo "<td style='text-align: right'>" . number($total). "</td>";   
                echo "<td align='right' class='no-print'></td>";   
                echo "</tr>";
                echo "</tfoot>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='6' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    