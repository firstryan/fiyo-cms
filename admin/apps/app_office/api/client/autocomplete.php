<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$minLen = 0;
$key = Input::get('term');
$key = str_replace("'","`", $key);
$len = $minLen - strlen($key);
if(strlen($key) >= $minLen ) {
    $data = [];
    $tabel = FDBPrefix.'office_client';
    $sql = DB::table( $tabel )
        ->where("deleted_at = '0000-00-00 00:00:00' AND (name LIKE '%$key%' OR personal LIKE '%$key%')")
        ->limit(20)
        ->get();

    if($sql) {
        foreach($sql as $row) {    
            
            $shift = [
                "Non-shit", 
                "3:1",
                "2:1",
            ];
            
            array_push($data, 
            [
                "value" =>  $row['name'],  
                "id" =>  $row['id_client'],  
                "label" => "$row[name] - ($row[personal])", 
                "content" => "<b>$row[name]</b> <br>$row[personal]",
            ]);
        }
    } else {        
        $data = [["label" => "disabled", "value" => "", "content" => Data_not_found]];
    }
} else {
    $data = [["label" => "disabled", "value" => "", "content" => "Masukan $len huruf lagi"]];

}

header('Content-Type: application/json');
echo json_encode($data);

?>