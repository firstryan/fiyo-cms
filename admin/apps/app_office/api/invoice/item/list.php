<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = FDBPrefix.'office_invoice_item';
$table2 = FDBPrefix.'office_invoice';

if(Input::get('id'))
$item = DB::table($table1)
    ->where("invoice_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_invoice = $table1.invoice_id")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:50% !important; text-align: left">Nama Produk / Jasa</th>
            <th style="width:10% !important;  text-align: right">Nominal</th>
            <th style="width:5% !important;  text-align: left">Qty</th>
            <th style="width:10% !important;  text-align: right">Sub-Total</th>
            <th style="width:10% !important; min-width: 90px " class="no-print"></th>

        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = 'id_item';
                $total = 0;
                foreach($item as $row) {
                    

                    $value = number($row['price']);
                    $stotal = $row['price'] * $row['qty'];
                    
                    
                    $total += $stotal;
                    $stotal = number($stotal);

                    $desc = ($row['description']) ? "<br><i>" . str_replace("\n","<br>",$row['description']) ."</i>" : '';

                    echo "<tr>";
                    echo "<td>$row[item] $desc</td>";  
                    echo "<td align='right'> $value </td>"; 
                    echo "<td> $row[qty] </td>";
                    echo "<td align='right'>$stotal</td>";   
                    echo "<td align='right' class='no-print'><span class='btn-primary btn edit-item' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                }
                
                echo "<tfoot>";
                echo "<tr>";
                echo "<td colspan='2' class='no-border'></td>";  
                echo "<td>Total</td>";  
                echo "<td style='text-align: right'>" . number($total). "</td>";   
                echo "<td align='right' class='no-print'></td>";   
                echo "</tr>";
                echo "</tfoot>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    