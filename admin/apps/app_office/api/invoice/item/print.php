<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = FDBPrefix. 'office_invoice_item';
$table2 = FDBPrefix. 'office_invoice';

if(Input::get('id'))
$item = DB::table(FDBPrefix. 'office_invoice_item')
    ->where("invoice_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_invoice = $table1.invoice_id")
    ->get();
    
?>
<table class="data no-border-side">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:60% !important; text-align: left">Product / Service</th>
            <th style="width:15% !important;  text-align: right">Price</th>
            <th style="width:5% !important;  text-align: center">Qty</th>
            <th style="width:15% !important;  text-align: right">Sub-Total</th>
        </tr>
        <?php endif; ?>
        
    </thead>	
    <tbody>
        <?php
        if(isset($saldo)) $saldo  = $saldo; else $saldo = 0;
        
            if($item){ 
                $index = $app['index'];
                $total = 0;
                foreach($item as $row) {                   
                    $value = number($row['price']);
                    $stotal = $row['price'] * $row['qty'];
                    
                    $total += $stotal;
                    $stotal = number($stotal);
                    $desc = ($row['description']) ? "<br><div style='opacity: .9; padding: 5px; line-height: 1.2em'><i>" . str_replace("\n","<br>",$row['description']) ."</i></div>" : '';

                    echo "<tr>";
                    echo "<td>$row[item] $desc</td>";        
                    echo "<td style='text-align:right'> $value </td>"; 
                    echo "<td style='text-align:center'> $row[qty] </td>";
                    echo "<td style='text-align:right'>$stotal</td>";   
                    echo "</tr>";
                }
                
                echo "<tfoot>";
                echo "<tr>";
                echo "<td class='no-border'></td>";  
                echo "<td colspan='2' style='text-align:right; border-left: 1px solid #ccc;'><b>Sub-Total</b></td>";  
                echo "<td style='text-align: right'>" . number(abs($total)). "</td>";   
                echo "</tr>";

                echo "<tr>";
                echo "<td  class='no-border'></td>";  
                echo "<td colspan='2' style='text-align:right; border-left: 1px solid #ccc;'><b>Saldo</b></td>";  
                echo "<td style='text-align: right'>" . number(abs($saldo)). "</td>";   
                echo "</tr>";
                
                echo "<tr>";
                echo "<td  class='no-border'></td>";  
                echo "<td colspan='2' style='text-align:right; border-left: 1px solid #ccc;'><b>Total</b></td>";  
                echo "<td style='text-align: right'> <b>" . number(abs($total - $saldo)). "  </b></td>";   
                echo "</tr>";
                echo "</tfoot>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='6' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    