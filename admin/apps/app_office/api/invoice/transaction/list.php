<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = FDBPrefix.'payment_transaction';
$table2 = FDBPrefix.'office_invoice';
$table3 = FDBPrefix.'payment_method';

if(Input::get('id'))
$item = DB::table($table1)
    ->where("invoice_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_invoice = $table1.invoice_id")
    ->leftJoin($table3, "$table3.id_method = $table1.method_id")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:20% !important; text-align: left">Tanggal Transaksi</th>
            <th style="width:10% !important;  text-align: left">Metode</th>
            <th style="width:10% !important;  text-align: right">Nominal</th>
            <th style="width:20% !important;">Reff</th>
            <th style="width:30% !important;  text-align: left">Keterangan</th>
            <th style="width:10% !important; min-width: 90px " class="no-print"></th>

        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = 'id_transaction';
                $total = 0;
                foreach($item as $row) {
                    

                    $value = number($row['amount']);

                    echo "<tr>";
                    echo "<td>$row[date]</td>";  
                    echo "<td>$row[payment_information]</td>";  
                    echo "<td align='right'> $value </td>"; 
                    echo "<td> $row[reff] </td>";
                    echo "<td> $row[information] </td>";
                    echo "<td align='right' class='no-print'><span class='btn-primary btn edit-transaction' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-transaction' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                }
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    