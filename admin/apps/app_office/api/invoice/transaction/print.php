<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = FDBPrefix.'payment_transaction';
$table2 = FDBPrefix.'office_invoice';
$table3 = FDBPrefix.'payment_method';

if(Input::get('id'))
$item = DB::table($table1)
    ->where("invoice_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_invoice = $table1.invoice_id")
    ->leftJoin($table3, "$table3.id_method = $table1.method_id")
    ->get();
    
?>
<table class="data no-border-side">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:20% !important; ">Transaction ID</th>
            <th style="width:25% !important; text-align: left">Transaction Date</th>
            <th style="width:15% !important;  text-align: center">Method</th>
            <th style="width:25% !important;  text-align: center">Information</th>
            <th style="width:150px !important;  text-align: right">Amount</th>
        </tr>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index'];
                $saldo = 0;
                foreach($item as $row) {                   
                    $value = number($row['amount']);
                    $saldo += $row['amount'];

                    echo "<tr>";
                    echo "<td> $row[reff] </td>"; 
                    echo "<td>$row[date] </td>";        
                    echo "<td style='text-align:center'>$row[payment_information]</td>";  
                    echo "<td style='text-align:center'> $row[information] </td>";
                    echo "<td style='text-align:right'>$value</td>";   
                    echo "</tr>";
                }
            }
            else {
                echo "<tr>";
                    echo "<td colspan='6' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    