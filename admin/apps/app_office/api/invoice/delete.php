<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2017 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');


/****************************************/
/*	    Enable and Disbale Modules		*/
/****************************************/
$chk = Input::get('check');
$qr = false;
foreach($chk as $id) {
    $qr = DB::table(FDBPrefix. 'office_invoice')
        ->where('id_invoice='.$id)
        ->update(array("deleted_at"=> date("Y-m-d H:i:s")));
}
       
if($qr) {
    $return = [
        'status' => 'info',
        'text' => Status_Saved
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Req::$get
    ];
} 

echo json_encode($return);
