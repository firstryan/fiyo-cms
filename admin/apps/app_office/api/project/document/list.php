<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table_item'];
$table2 = $app['table'];

if(Input::get('id'))
$item = DB::table($table1)
    ->where("$app[index] = ". Input::get('id'))
    ->leftJoin($table2, "$table2.$app[index] = $table1.$app[reff_index]")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index_item'];
                $total = 0;
                $no = 1;
                foreach($item as $row) {

                    $download = "";
                    
                    if(!empty($row['path']))
                    $download = "<span class='btn-primary btn preview-item' data-id='$row[$index]' ><i class='icon-download'></i></span>";

                    echo "<tr>";
                    echo "<td style='width:30% !important; text-align: left'>$row[filename]</td>";  
                    echo "<td style='width:20% !important; text-align: right' align='right' class='no-print'> $download <span class='btn-primary btn edit-item' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                    $no++;
                }
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    