<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$post;


if(!$data OR $data['_token'] != USER_TOKEN) die($d);

//setup saving type
if(!empty($data[$app['index_item']])) {
    $app['reff'] = $data[$app['index_item']];
    $app['index'] = $app['index_item'];
    $app['type'] = "edit";
}
else {    
    $app['type'] = "add";
}


$app['fillable'] = [
    "project_id",
    "filename",
    "extension",
    "label",
];

$app['table'] = $app['table_item'];

$app['no_log'] = true;


$path = '../media/files/project';
if(!file_exists($path)) mkdir($path);

$filename = $data['project_id'] . '-' . str_replace(" ","-",($data['filename']));
//setup fillable field

$ext = "";

$file   = Req::file('path');

$oldpath = (Req::post('old_path')) ? $path . '/'. Req::post('old_path') : false;
$old    =  (!$data['id_doc'] AND $oldpath) ? md5_file($oldpath) : false;
$fname  = ($file AND file_exists($file['tmp_name'])) ? md5_file($file['tmp_name']) : true;

if(isset(pathinfo($file['name'])['extension']))
$ext    = pathinfo($file['name'])['extension'];

if(empty($file['name']) AND !$oldpath) { 
    array_push($app['fillable'], "path" );  

    $data['path'] = null;
    $data['extension'] = null;
} else if($old != $fname) {
    if(file_exists($oldpath))
    unlink($oldpath);

    if($fname !== true) {
        $filename = $fname;
        $c = Req::move('path', $path, $filename, true);  
        array_push($app['fillable'], "path" );  
    
        $data['path'] = $path."/".$filename;
        $data['extension'] = $ext;
    }
}
else {

    $filename = $old;
}


$qr = Query::save(array_merge(["data" => $data], $app));
$max = min(ini_get('post_max_size'), ini_get('upload_max_filesize'));

//setup return value
if($qr) {
    $n = (!$data['id_doc']) ? DB::$last_id : $data['id_doc'];
    $return = [
        'status' => 'success',
        'text' => Status_Saved,
        'id' => DB::$last_id
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => "Max upload file size: $max"
    ];
} 


echo json_encode($return);