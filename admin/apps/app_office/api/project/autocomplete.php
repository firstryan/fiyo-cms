<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$minLen = 0;
$key = Input::get('term');
$len = $minLen - strlen($key);
if(strlen($key) >= $minLen ) {
    $data = [];
    $table = $app['table'];
    $sql = DB::table( $table )
        ->where("$table.deleted_at = '' AND title LIKE '%$key%' OR personal LIKE '%$key%' OR name LIKE '%$key%'")
        ->leftJoin(FDBPrefix."office_client","client_id = id_client")
        ->limit(20)
        ->get();
        
    if($sql) {
        foreach($sql as $row) {    
            
            array_push($data, 
            [
                "value" =>  $row['title'],  
                "client" =>  $row['name'],  
                "client_id" =>  $row['id_client'],  
                "id" =>  $row[$app['index']],  
                "label" => "$row[title] - ($row[personal])", 
                "content" => "<b>$row[title]</b> <br>$row[name]",
            ]);
        }
    } else {        
        $data = [["label" => "disabled", "value" => "", "content" => Data_not_found]];
    }
} else {
    $data = [["label" => "disabled", "value" => "", "content" => "Masukan $len huruf lagi"]];

}

header('Content-Type: application/json');
echo json_encode($data);

?>