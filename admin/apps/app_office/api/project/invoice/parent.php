<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

header('Content-Type: application/json');
if(USER_LEVEL > 2) die('Access Denied');

$p = (Input::get('id')) ? " AND id_task != ".Input::get('id') : ""; 
$data = DB::table($app['table_item'])
->where("parent_id = '' AND parent_id = 0 $p AND project_id = ". Input::get('reff'))
->orderBy("sort ASC")
->get();  

if(!isset($data[0])) die('Access Denied'); 

$html = "<option value='0'>No Parent</option>";
$no = 1;
foreach($data as $list) {
    $html .= "<option value='$list[id_task]'>$no. $list[task]</option>";
    $html .= call_subs($list['id_task'], $no);
    $no++;
}
    
if(!empty($html)) {
    $return = [
        'status' => 'ok',
        'data' => $html,
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Status_Fail
    ];
} 


echo json_encode($return);

function call_subs($id, $number = 1) {    
$p = (Input::get('id')) ? "AND id_task != ".Input::get('id') : ""; 

    $data = DB::table(FDBPrefix.'office_task')
    ->where("parent_id = $id $p ")
    ->orderBy("sort ASC")
    ->get();  
    
    $html = "";
    $no = 1;
    foreach($data as $list) {
        $html .= "<option value='$list[id_task]'>$number.$no. $list[task]</option>";
        $html .=call_subs($list['id_task'], "$number.$no");
        $no++;
    }
    return $html;
}