<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table_item'];
$table2 = $app['table'];

if(Input::get('id'))
$item = DB::table($table1)
    ->where("$app[index] = ". Input::get('id'))
    ->leftJoin($table2, "$table2.$app[index] = $table1.$app[reff_index]")
    ->orderBy("$table1.created_at ASC")
    ->get();

?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index_item'];
                $total = 0;
                $no = 1;
                foreach($item as $row) {
                    echo "<tr>";
                    echo "<td style='width:3% !important; text-align: left'>$no</td>";  
                    echo "<td style='width:40% !important; text-align: left'>$row[no]</td>";  
                    echo "<td style='width:10% !important; text-align: right' align='right' class='no-print'><a href='?app=office&view=invoice&act=edit&id=$row[id_invoice]' class='btn-primary btn edit-item-a' ><i class='icon-pencil icon-only'></i></a> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                    $no++;
                }
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    