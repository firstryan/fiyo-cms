<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

if(USER_LEVEL > 2) die('Access Denied');
if(Input::get('id')) {
    //get data from id
    $data = DB::table($app['table_item'])
    ->where("$app[index_item] = ".Input::get('id'))
    ->get();  
    
    if(!isset($data[0])) die('Access Denied'); 
    
    $data = $data[0];    
        
    if($data) {
        $return = [
            'status' => 'ok',
            'data' => $data,
        ];
    }
    else {
        $return = [
            'status' => 'error',
            'text' => Status_Fail
        ];
    } 


    header('Content-Type: application/json');
    echo json_encode($return);
} 