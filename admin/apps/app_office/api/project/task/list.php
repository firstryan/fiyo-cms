<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table_item'];
$table2 = $app['table'];

if(Input::get('id'))
$item = DB::table($table1)
    ->where("parent_id = 0 AND $app[index] = ". Input::get('id'))
    ->leftJoin($table2, "$table2.$app[index] = $table1.$app[reff_index]")
    ->orderBy("sort ASC")
    ->get();
    
function call_subs($id, $number = 1) {  
    $p = (Input::get('id')) ? "AND id_task != ".Input::get('id') : ""; 

    $data = DB::table(FDBPrefix.'office_task')
    ->where("parent_id = $id $p ")
    ->orderBy("sort ASC")
    ->get();  
    
    $html = "";
    $no = 1;

    foreach($data as $row) {
        echo "</tr>";
        echo "<tr> <td> &nbsp; $number.$no</td><td> $row[task] </td>   <td class='no-print' style='text-align: right'><span class='btn-primary btn edit-item' data-id='$row[id_task]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[id_task]'><i class='icon-times icon-only'></i></span></td></tr>";
        call_subs($row['id_task'], "&nbsp;&nbsp;$number.$no");
        $no++;
    }
}

?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index_item'];
                $total = 0;
                $no = 1;
                foreach($item as $row) {
                    echo "<tr>";
                    echo "<td style='width:3% !important; text-align: left'>$no</td>";  
                    echo "<td style='width:40% !important; text-align: left'>$row[task]</td>";  
                    echo "<td style='width:10% !important; text-align: right' align='right' class='no-print'><span class='btn-primary btn edit-item' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                    call_subs($row['id_task'], $no) ;
                    $no++;
                }
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    