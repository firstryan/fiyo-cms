<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$post;
if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

//setup saving type
if(!empty($data[$app['index_item']])) {
    $app['reff'] = $data[$app['index_item']];
    $app['index'] = $app['index_item'];
    $app['type'] = "edit";
}
else {    
    $app['type'] = "add";
}


$app['table'] = $app['table_item'];

$app['no_log'] = true;

//setup fillable field
$app['fillable'] = [
    'item', 
    "reff_id",
    "bill_id",
    "price",
    "qty",
];

//setup query saving
$qr = Query::save(array_merge(["data" => $data], $app));

//setup return value
if($qr) {
    $return = [
        'status' => 'success',
        'text' => Status_Saved,
        'id' => DB::$last_id
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Query::$message
    ];
} 


echo json_encode($return);