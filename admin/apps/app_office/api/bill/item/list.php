<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table_item'];
$table2 = $app['table'];

if(Input::get('id'))
$item = DB::table($table1)
    ->where("$app[index] = ". Input::get('id'))
    ->leftJoin($table2, "$table2.$app[index] = $table1.$app[reff_index]")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:50% !important; text-align: left">Nama Produk / Jasa</th>
            <th style="width:10% !important;  text-align: right">Nominal</th>
            <th style="width:5% !important;  text-align: left">Qty</th>
            <th style="width:10% !important;  text-align: right">Sub-Total</th>
            <th style="width:10% !important; " class="no-print"></th>
        </tr>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index_item'];
                $total = 0;
                foreach($item as $row) {
                    

                    $value = number($row['price']);
                    $stotal = $row['price'] * $row['qty'];
                    
                    
                    $total += $stotal;
                    $stotal = number($stotal);

                    echo "<tr>";
                    echo "<td>$row[item]</td>";  
                    echo "<td align='right'> $value </td>"; 
                    echo "<td> $row[qty] </td>";
                    echo "<td align='right'>$stotal</td>";   
                    echo "<td align='right' class='no-print'><span class='btn-primary btn edit-item' data-id='$row[$index]' ><i class='icon-pencil icon-only'></i></span> <span  class='btn-danger btn delete-item' data-id='$row[$index]'><i class='icon-times icon-only'></i></span></td>";
                    echo "</tr>";
                }
                
                echo "<tfoot>";
                echo "<tr>";
                echo "<td colspan='2' class='no-border'></td>";  
                echo "<td>Total</td>";  
                echo "<td style='text-align: right'>" . number($total). "</td>";   
                echo "<td align='right' class='no-print'></td>";   
                echo "</tr>";
                echo "</tfoot>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='4' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    