<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$minLen = 0;
$key = Input::get('term');
$len = $minLen - strlen($key);
if(strlen($key) >= $minLen ) {
    $data = [];
    $tabel = $app['table'];
    $sql = DB::table( $tabel )
        ->where("deleted_at = '' AND name LIKE '%$key%' OR personal LIKE '%$key%'")
        ->limit(20)
        ->get();

    if($sql) {
        foreach($sql as $row) {    
            
            array_push($data, 
            [
                "value" =>  $row['name'],  
                "id" =>  $row[$app['index']],  
                "label" => "$row[name] - ($row[personal])", 
                "content" => "<b>$row[name]</b> <br>$row[personal]",
            ]);
        }
    } else {        
        $data = [["label" => "disabled", "value" => "", "content" => Data_not_found]];
    }
} else {
    $data = [["label" => "disabled", "value" => "", "content" => "Masukan $len huruf lagi"]];

}

header('Content-Type: application/json');
echo json_encode($data);

?>