<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table'];
$table2 = $app['table_group'];

if(Input::get('id'))
$item = DB::table($app['table'])
    ->where("group_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_group = $table1.group_id")
    ->get();
    
?>
<table class="data">
    <thead>
        <?php                
            if($item) :
        ?>
        <tr>									
            <th style="width:30% !important; text-align: left">Nama Produk / Jasa</th>
            <th style="width:25% !important;  text-align: left">Produsen / Vendor</th>
            <th style="width:18% !important;  text-align: right">Nominal</th>
            <th style="width:10% !important;  text-align: center">Qty</th>
            <th style="width:150px !important;  text-align: right">Sub-Total</th>
        </tr>
        <?php endif; ?>
    </thead>	
    <tbody>
        <?php
            if($item){ 
                $index = $app['index'];
                $total = 0;
                foreach($item as $row) {
                    $status = [
                        "Outcome" => "Pengeluaran",
                        "Income" => "Pemasukan"
                    ];
                    $status = $status[$row['type']];
                    $value = number($row['value']);
                    $stotal = $row['value'] * $row['qty'];
                    
                    if($row['type'] == 'Outcome')  {  
                        $total -= $stotal;
                        $stotal = "(-) &nbsp; " .number($stotal);  
                    }
                    else {
                        $total += $stotal;
                        $stotal = number($stotal);
                    }


                    echo "<tr>";
                    echo "<td>$row[item]</td>";  
                    echo "<td>$row[vendor]</td>";       
                    echo "<td style='text-align:right'> $value </td>"; 
                    echo "<td style='text-align:center'> $row[qty] </td>";
                    echo "<td style='text-align:right'>$stotal</td>";   
                    echo "</tr>";
                }
                
                echo "<tfoot>";
                echo "<tr>";
                echo "<td colspan='3' class='no-border'></td>";  
                echo "<td style='text-align:center'><b>Total</b></td>";  
                echo "<td style='text-align: right'>" . number(abs($total)). "</td>";   
                echo "</tr>";
                echo "</tfoot>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='6' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    