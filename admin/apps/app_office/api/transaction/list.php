<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
// if(Input::get('token') != USER_TOKEN) die ('Access Denied!');

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	$sTable	 = FDBPrefix."payment_transaction";
	$sTable_invoice	 = FDBPrefix."office_invoice";
	$sTable_bill	 = FDBPrefix."office_bill";
	$sTable_client	 = FDBPrefix."office_client";
	$sTable_vendor	 = FDBPrefix."office_vendor";


	$aColumns =[
		"id_transaction", 
		"date",
		'reff', 
		"invoice_id",
		"@CASE WHEN `$sTable_invoice.client_id` > 0 THEN `$sTable_client.name`
		ELSE `$sTable_vendor.name`
		END as name",
		"payment_type",
		"amount",
		"information",
		"date",
		"@CASE WHEN invoice_id > 0 THEN `$sTable_invoice.no`
		ELSE `$sTable_bill.no`
		END as no",
        "$sTable.deleted_at"
        
	];
		
	$aWhereColumns =[
		"id_transaction", 
		"date",
		'reff', 
		"invoice_id",
		"$sTable_vendor.name",
		"$sTable_client.name",
		"payment_type",
		"amount",
		"information",
		"date",
		"$sTable_invoice.no",
		"$sTable_bill.no",
        "$sTable.deleted_at"
	];
	
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "id_transaction";

	/* Indexed column (used for fast and accurate table cardinality) */
	$uniqueColumn = "no";

	$sGroup = " ";

	$sJoin = "LEFT JOIN $sTable_invoice ON $sTable.invoice_id = $sTable_invoice.id_invoice ";
	$sJoin .= "LEFT JOIN $sTable_bill ON $sTable.bill_id = $sTable_bill.id_bill ";	
	$sJoin .= "LEFT JOIN $sTable_client ON $sTable_invoice.client_id = $sTable_client.id_client ";
	$sJoin .= "LEFT JOIN $sTable_vendor ON $sTable_bill.vendor_id = $sTable_vendor.id_vendor ";
	/*
	 * Ordering
	 */
	$sOrder = "ORDER BY $sTable.created_at ASC";


	$sTables = " $sTable ";
	
	/* 
	 * Paging
	 */
	$sLimit = "LIMIT 10";
	if (  Req::get('iDisplayLength') != '-1' )
	{
		$sLimit = "LIMIT ".intval( Req::get('iDisplayStart') ).", ".
			intval( Req::get('iDisplayLength') );
	}
	
	
	
	if (Req::get('iSortCol_0') AND !empty(Req::get('iSortCol_0')))
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( Req::get('iSortingCols') ) ; $i++ )
		{
			if ( Req::get( 'bSortable_'.intval(Req::get('iSortCol_'.$i))) == "true" )
			{
				$data = str_replace(".","`.`",$aColumns[ intval( Req::get('iSortCol_'.$i) ) ]);	
						
				$c = strpos( strtolower($aColumns[ intval( Req::get('iSortCol_'.$i) ) ]), "as");
				if($c) { $c += 3;
					$data = substr($aColumns[ intval( Req::get('iSortCol_'.$i) ) ], $c);
				}

				$data = trim(str_replace(["@","(",")"," as","as "],"", strtolower($data)));
				$sOrder .= "`". $data ."` ".
					(Req::get('sSortDir_'.$i)==='asc' ? 'asc' : 'desc') .", ";
					
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	if(!empty((int)Input::get('cat'))) 
		$cat = " category_id = '".Input::get('cat')."'  AND "; 
	else 
		$cat = ''; 
		
	//Filter Year
	if(!empty((int)Input::get('year'))) {		
		setSession('YEAR',Input::get('year'));
		$year = " YEAR(date) = '".Input::get('year')."'  AND "; 
	}
	else {
		setSession('YEAR', 0);
		$year = ''; 

	}

	//Filter Month
	if(!empty((int)Input::get('month'))) {
		setSession('MONTH',Input::get('month'));
		$month = " MONTH(date) = '".Input::get('month')."'  AND "; 
	}
	else {		
		setSession('MONTH', 0);
		$month = '';
	} 

	
	if(!empty(Req::get('user'))) $user = " author_id = ' ". Req::get('user') ."  AND "; 
	else $user = '';

	if(!empty(Req::get('level'))) $level = "  $sTable.level = 'Req::get([level]'  AND "; 
	else $level = '';
	
	$deleted = "$sTable.deleted_at = '0000-00-00 00:00:00'";
	$sWhere = " WHERE  $cat  $year $month $deleted ";

	if ( Req::get('sSearch') && Req::get('sSearch') != "" )
	{
		$sWhere .= " AND(";
		for ( $i=0 ; $i<count($aWhereColumns) ; $i++ )
		{

			$data = str_replace(".","`.`",$aWhereColumns[$i]);
			$sWhere .= "`".$data."` LIKE '%".addslashes( Req::get('sSearch') )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( Req::get('bSearchable_'.$i) && Req::get('bSearchable_'.$i) == "true" && Req::get('sSearch_'.$i) != '' )
		{
			if ( $sWhere == "AND " )
			{
				$sWhere .= " ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".addslashes(Req::get('sSearch_'.$i))."%' ";
		}
	}	
	
	/*
	 * SQL queries
	 * Get data to display
	 */


	$sColumns = "`".str_replace([" as "," AS "], "` as `", str_replace(".","`.`", str_replace(" , ", " ", implode("`, `", $aColumns))))."`";
	$sColumns = str_replace(["`(",") `","`@", "@ `"], "", $sColumns );
	$sColumns = str_replace(")`", ")", $sColumns );
	$sColumns = str_replace("END`", "END", $sColumns );

	$sQuery = $sRaw = "
		SELECT SQL_CALC_FOUND_ROWS 
		$sColumns
		FROM  $sTables $sJoin
		$sWhere 
		$sGroup
		$sOrder		
		$sLimit
		";

		// echo $sRaw;
	$rResult = Database::query($sQuery);
	
	/* Data set length after filtering */
	
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	
	$rResultFilterTotal = Database::query($sQuery)->fetchColumn();
	$iFilteredTotal = $rResultFilterTotal;
	//echo $iFilteredTotal;
	/* Total data set length */
	
	$sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
	";	

	
	$rResultTotal = Database::query($sQuery)->fetchColumn();
	$iTotal = $rResultTotal;	
	/*
	 * Output
	 */

	$output = array(
		"sEcho" => intval(@Req::get('sEcho')),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);

	header('Content-Type: application/json');
	$saldo = 0;
	foreach ( $rResult as $aRow )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{					
								
			$title = $aRow['date'];            

			if($_SESSION['USER_LEVEL'] == 1) {
				$checkbox ="<label><input type='checkbox' data-name='rad-$aRow[$sIndexColumn]' name='check[]' value='$aRow[$sIndexColumn]' rel='ck' ><span class='input-check'></span></label>";
                $view = "transaction";
                if($view) $view = "&view=".$view;
				$name ="<a class='tips' title='".Edit."' href='?app=" . Req::get('app') . $view . "&act=edit&id=$aRow[$sIndexColumn]' target='_self' data-placement='right'>$title</a>";
				$reff ="<a class='tips' title='".Edit."' href='?app=" . Req::get('app') . $view . "&act=edit&id=$aRow[reff]' target='_self' data-placement='right'>$aRow[reff]</a>";	
									
			}
			else {	
				$name =  $title;
				$reff = $aRow["reff"];	
				$checkbox = "<span class='icon lock'></lock>";
			
									
			}	
			
			$type = $aRow['payment_type'] == 'Income' ? 
			"<lavel class='label label-success'> ".$aRow['payment_type'] . " <i class='icon-arrow-down fa fa-signin'></i> </label>	" :  
			"<lavel class='label label-danger'> ".$aRow['payment_type'] . " <i class='icon-share fa fa-signin'></i> </label>	";

			if ( $i == 0 )
			{			
				if(!Req::get('param'))
				$row[] = $checkbox; 
			}				
			else if ( $i == 1 )
			{					
				$row[] = "<div>".$reff."</div>";				
			}			
			else if ( $i == 2 )
			{			
				$row[] = "<div>". $aRow['no']."</div>";

			}	
			else if ( $i == 3 )
			{			
				$row[] = "<div>". $aRow['name']."</div>";
			}
			else if ( $i == 4 )
			{			
				$row[] = "<div>".  $type ."</div>";
			}
			else if ( $i == 5 )
			{				
				$row[] = "<div>".$aRow['date']."</div>";	
			}
			else if ( $i == 6 )
			{						
				$row[] = "<div>".number($aRow["amount"])."</div>";
			}
			else if ( $i == 7 )
			{						
				$row[] = "<div>".number($aRow["amount"])."</div>";
			}
			else if ( $i == 8 )
			{						
				$row[] = "<div>".number($aRow["amount"])."</div>";
			}
		}
		$output['aaData'][] = $row;
	}
	
	echo json_encode( $output );
?>