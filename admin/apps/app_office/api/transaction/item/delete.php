<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$get;
if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

$id = $data['id'];
if(Database::table(FDBPrefix.'office_invoice_item')->delete("id_item = $id")) {
    $return = [
        'status' => 'info' ,
        'text' => Status_Deleted
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Status_Fail
    ];
}

echo json_encode($return);
