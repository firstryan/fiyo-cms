<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$post;
if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

//setup saving type
if(!empty($data[$app['index']]))
    $app['type'] = "edit";
else
    $app['type'] = "add";

//set refference index
$app['reff'] = $data[$app['index']];

//setup fillable field
$app['fillable'] = [
    'no', 
    'client_id', 
    'vendor_id', 
    'project_id', 
    "client", 
    "project",
    "status",
    "template",
    "created_at",
    "created_date",
    "template",
    "due_date",
];

//setup query saving
$qr = Query::save(array_merge(["data" => $data], $app));

//setup return value
if($qr) {
    $return = [
        'status' => 'success',
        'text' => Status_Saved,
        'id' => DB::$last_id
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => DB::$query 
    ];
} 


echo json_encode($return);