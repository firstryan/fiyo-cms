<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$app = [
	"name" 	=> "Vendor",
	"table" 	=> FDBPrefix."office_vendor",
	"type"		=> "create",
	"index"		=> "id_vendor",
	"reff"		=> "",
	"fillable"	=> [],
	"validator"	=> [],
];