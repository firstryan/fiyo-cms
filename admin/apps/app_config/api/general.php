<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 2) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$post;
if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

foreach($data as $k => $v) { 
	$status = true;
	$qr = DB::table(FDBPrefix.'setting')->where("name='$k'")->update(array("value"=>$v));
}

$folder = str_replace("\apps\app_config\api","",__dir__);
$new_folder = Input::post('folder_new');
$old_folder = Input::post('folder_old');


$n_old_folder =  str_replace(Input::post('folder_old'),Input::post('folder_new'), $folder);

$redirect = false;
if($old_folder != $new_folder) {
	$ok = rename($folder,$n_old_folder);
	if($ok) {
		DB::table(FDBPrefix.'setting')
			->where("name='backend_folder'")
			->update(array("value"=>$new_folder));				
		$status = true;		
		$redirect = $new_folder;
	}
	else {			
		$status = false;		
		$redirect = null;	
	}
}


$_SESSION['media_theme'] = $_POST['media_theme'];

if($status AND $redirect) {
    $return = [
        'status' => 'success',
        'text' =>Status_Updated,
        'url' => $new_folder
    ];
}
else if($status === true) {
    $return = [
        'status' => 'success',
        'text' => Status_Updated
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Status_Fail
    ];
}


echo json_encode($return);