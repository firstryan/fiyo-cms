<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');


$api 	= Input::get('api');
$act 	= Input::get('act');
$view 	= Input::get('view');
$print 	= Input::get('print');

if(!$api AND !$print) :
	
switch($view)
{
	case 'test':
		require('view/test/test.php');
	break;
	case 'form':
		require('view/frm.php');
	break;
	default :
		switch($act)
		{
			case 'add':	 
			require('view/add.php');
			break;
			case 'edit':
			require('view/edit.php');
			break;
			default :
			require('view/default.php');
			break;
		}
	break;
}
	

else :
	if($api)
	switch($api)
	{
		case 'list':	 
		require('api/list.php');
		break;
		case 'save':	 
		require('api/save.php');
		break;	
		case 'delete':	 
		require('api/delete.soft.php');
		break;		
		case 'item-list':	 
		require('api/item.list.php');
		break;		
		case 'item-data':	 
		require('api/item.data.php');
		break;			
		case 'item-save':	 
		require('api/item.save.php');
		break;	
		case 'item-delete':	 
		require('api/item.delete.php');
		break;


		default :
			page('404');
		break;
	}

	if($print)
	switch($print)
	{
		case $print:
			$p = "print/$print.php"; 	 
			if(file_exists(__dir__.'/'.$p))
				require($p);
			else 
				page('404');
		break;
		default :
			page('404');
		break;
	}
endif;