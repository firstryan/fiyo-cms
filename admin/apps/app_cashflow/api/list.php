<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
// if(Input::get('token') != USER_TOKEN) die ('Access Denied!');

	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	$sTable	 = FDBPrefix."cashflow";
	$sTable1 = FDBPrefix."cashflow_group";
	$sTable2 = FDBPrefix."cashflow_category";


	$aColumns =[
		"id", 
		'category', 
		'place', 
		"date",
		"@SUM(IF(type = 'Income', value, 0) * qty) AS debit",
		"@SUM(IF(type = 'Outcome', value, 0) * qty) AS kredit",
		"@SUM(IF(type = 'Income', value, 0)) - SUM(IF(type = 'Outcome', value, 0))@  AS total",
		"@SUM(IF(type = 'Income', value, 0)) - SUM(IF(type = 'Outcome', value, 0))@  AS saldo",
		// "@SUM(SUM(IF(type = 'Income', value, 0) * qty) - SUM(IF(type = 'Outcome', value, 0) * qty)) 
		// 	OVER (PARTITION BY date ORDER BY  date,id_group ASC)@  AS saldo",
		"author",
		"id_group",
	];
		
	$aWhereColumns =[
		"id", 
		'category', 
		'place', 
		"date",
		"author",
		"id_group",
	];
	
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "id";

	/* Indexed column (used for fast and accurate table cardinality) */
	$uniqueColumn = "date";
	
	/* DB table to use */
	$sJoin = "LEFT JOIN $sTable1 ON $sTable.group_id = $sTable1.id_group";
	$sJoin .= " LEFT JOIN $sTable2 ON $sTable1.category_id = $sTable2.id_cat";

	$sGroup = " GROUP BY id_group";

	
	/*
	 * Ordering
	 */
	$sOrder = "ORDER BY date,id_group ASC";


	$sTables = " $sTable ";
	
	/* 
	 * Paging
	 */
	$sLimit = "LIMIT 10";
	if (  Req::get('iDisplayLength') != '-1' )
	{
		$sLimit = "LIMIT ".intval( Req::get('iDisplayStart') ).", ".
			intval( Req::get('iDisplayLength') );
	}
	
	
	
	if (Req::get('iSortCol_0') AND !empty(Req::get('iSortCol_0')))
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( Req::get('iSortingCols') ) ; $i++ )
		{
			if ( Req::get( 'bSortable_'.intval(Req::get('iSortCol_'.$i))) == "true" )
			{
				$data = str_replace(".","`.`",$aColumns[ intval( Req::get('iSortCol_'.$i) ) ]);	
						
				$c = strpos( strtolower($aColumns[ intval( Req::get('iSortCol_'.$i) ) ]), "as");
				if($c) { $c += 3;
					$data = substr($aColumns[ intval( Req::get('iSortCol_'.$i) ) ], $c);
				}

				$data = trim(str_replace(["@","(",")"," as","as "],"", strtolower($data)));
				$sOrder .= "`". $data ."` ".
					(Req::get('sSortDir_'.$i)==='asc' ? 'asc' : 'desc') .", ";
					
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	if(!empty((int)Input::get('cat'))) 
		$cat = " category_id = '".Input::get('cat')."'  AND "; 
	else 
		$cat = ''; 
		
	//Filter Year
	if(!empty((int)Input::get('year'))) {		
		setSession('YEAR',Input::get('year'));
		$year = " YEAR(date) = '".Input::get('year')."'  AND "; 
	}
	else {
		setSession('YEAR', 0);
		$year = ''; 

	}

	//Filter Month
	if(!empty((int)Input::get('month'))) {
		setSession('MONTH',Input::get('month'));
		$month = " MONTH(date) = '".Input::get('month')."'  AND "; 
	}
	else {		
		setSession('MONTH', 0);
		$month = '';
	} 

	
	if(!empty(Req::get('user'))) $user = " author_id = ' ". Req::get('user') ."  AND "; 
	else $user = '';

	if(!empty(Req::get('level'))) $level = "  $sTable.level = 'Req::get([level]'  AND "; 
	else $level = '';
	
	$deleted = "$sTable1.deleted_at = '0000-00-00 00:00:00'";
	$sWhere = " WHERE  $cat  $year $month $deleted ";

	if ( Req::get('sSearch') && Req::get('sSearch') != "" )
	{
		$sWhere .= " AND(";
		for ( $i=0 ; $i<count($aWhereColumns) ; $i++ )
		{

			$data = str_replace(".","`.`",$aWhereColumns[$i]);
			$sWhere .= "`".$data."` LIKE '%".addslashes( Req::get('sSearch') )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( Req::get('bSearchable_'.$i) && Req::get('bSearchable_'.$i) == "true" && Req::get('sSearch_'.$i) != '' )
		{
			if ( $sWhere == "AND " )
			{
				$sWhere .= " ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= "`".$aColumns[$i]."` LIKE '%".addslashes(Req::get('sSearch_'.$i))."%' ";
		}
	}	
	
	/*
	 * SQL queries
	 * Get data to display
	 */


	$sColumns = "`".str_replace([" as "," AS "], "` as `", str_replace(".","`.`", str_replace(" , ", " ", implode("`, `", $aColumns))))."`";
	$sColumns = str_replace(["`(",") `","`@", "@ `"], "", $sColumns );
	$sColumns = str_replace(")`", ")", $sColumns );

	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS 
		$sColumns
		FROM  $sTables $sJoin
		$sWhere 
		$sGroup
		$sOrder		
		$sLimit
		";
		
	$rResult = Database::query($sQuery);
	
	// echo $sQuery;
	/* Data set length after filtering */
	
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	
	$rResultFilterTotal = Database::query($sQuery)->fetchColumn();
	$iFilteredTotal = $rResultFilterTotal;
	//echo $iFilteredTotal;
	/* Total data set length */
	
	$sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
	";	

	
	$rResultTotal = Database::query($sQuery)->fetchColumn();
	$iTotal = $rResultTotal;	
	/*
	 * Output
	 */

	$output = array(
		"sEcho" => intval(@Req::get('sEcho')),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);

	header('Content-Type: application/json');
	$saldo = 0;
	foreach ( $rResult as $aRow )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{					
			if($saldo == 0)
			$saldo = $aRow['saldo'];
								
			$title = $aRow['category'];


			if($_SESSION['USER_LEVEL'] == 1) {
				$checkbox ="<label><input type='checkbox' data-name='rad-$aRow[id_group]' name='check[]' value='$aRow[id_group]' rel='ck' ><span class='input-check'></span></label>";
				
				$name ="<a class='tips' title='".Edit."' href='?app=$app[root]&act=edit&id=$aRow[id_group]' target='_self' data-placement='right'>$title</a>";	
									
			}
			else {	
				$name =  $title;
				$checkbox = "<span class='icon lock'></lock>";
			
									
			}					
			if ( $i == 0 )
			{			
				if(!Req::get('param'))
				$row[] = $checkbox; 
			}				
			else if ( $i == 1 )
			{					
				$row[] = "<div>".$name."</div>";				
			}			
			else if ( $i == 2 )
			{			
				$row[] = "<div>".$aRow["place"]."</div>";
			}	
			else if ( $i == 3 )
			{			
				$row[] = "<div>".$aRow["date"]."</div>";
			}
			else if ( $i == 4 )
			{				
				$debit = number($aRow['debit']);
				$row[] = "<div class='right'>$debit</div>";
			}
			else if ( $i == 5 )
			{						
				$credit = number($aRow['kredit']);
				$row[] = "<div class='right'>$credit</div>";
			}
			else if ( $i == 6 )
			{		
				$total = number($aRow['total']);
				$row[] = "<div class='right'>$total</div>";
			}
			else if ( $i == 7 )
			{		
				$saldo = $aRow['saldo'];
				
				if($saldo < 0)  {
					$saldo = "(-)&nbsp;" .number(abs($saldo));  
				} else {					
					$saldo = number($saldo);  
				}

				$row[] = "<div class='right'>$saldo</div>";
			}
		}
		$output['aaData'][] = $row;
	}
	
	echo json_encode( $output );
?>