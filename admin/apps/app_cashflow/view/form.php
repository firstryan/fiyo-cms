<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
$(function () {

	$("form#mainForm").submit(function (e) {
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?=$app['root'];?>&api=save", data).done(function (result) {
			notice(result);		//print notice alert
			id = parseInt(result.id); // set ID
			if(result.status == 'success' && id > 0){
				loadUrl("?app=<?=$app['root'];?>&act=edit&id=" + id);
			}
		});
	});
	

});
</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Data</h5>
		</header>							
			<div>
			<table class="double-col">
				<tr>		
					<td>
						 Judul / Tempat / Sumber Transaksi
					</td>
					<td>
						<?= Form::text('place','',["required" => "required"]); ?>
					</td>
					<td>
						Jenis
					</td>
					<td>
						<?php
							$sql = Database::table(FDBPrefix.'cashflow_category')->get();
									
							foreach($sql as $k=>$v) {
								$qrs[$v['id_cat']] = $v['category'];				
							}
							echo Form::select(
								'category_id',$qrs, '',
								["required" => "required"], 'Please select one...'
							);
						?>					
					</td>
						
				</tr>
					
				
				<tr>
				<td>
						Tanggal
					</td>
					<td>
						<?= Form::date('date','',["required"]); ?>
					</td>
				</tr>
			</table>		
		</div>  
	</div>
</div>

<?= Form::hidden('id_group'); ?>
<?= Form::close(); ?>

<?php if(Req::get('id')) include('form.item.php'); ?>