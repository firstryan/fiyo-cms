<?php
/**
* @version		v 1.2.1
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.php
* @description	
**/

defined('_FINDEX_') or die('Access Denied');


$view 	= Input::get('view');
$act 	= Input::get('act');
$api 	= Input::get('api');
$print	= Input::get('print');

if(!$api AND !$print)
switch($view)
{	
	default :
	 switch($act) {	
		default :		
		 require('view/default.php');
		break;
		case 'add':	 
		 require('view/add.php');
		break;
		case 'edit':
		 require('view/edit.php');
		break;	
	}
	break;
	case 'group':			 
	 switch($act) {	
		default :
	 require('view/group/view.php');
	break;
	case 'edit':	 
	 require('view/group/edit.php');
	break;
	case 'add':	 
	 require('view/group/add.php');
	 }
	break;		
}



if(!$view AND !$print)
switch($api)
{	
	case 'contact_list':	
	 require('api/contact_list.php');
	break;
	case 'status':	 
	 require('api/status.php');
	break;	
}