<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

header('Content-Type: application/json');
defined('_FINDEX_') or die('Access Denied');
if(Input::get('token') != USER_TOKEN) die ('Access Denied!');

/****************************************/
/*	    Enable and Disbale Contact		*/
/****************************************/
if(isset($_GET['stat'])) {
	if($_GET['stat']=='1'){
		DB::table(FDBPrefix.'contact')->where('id='.Input::get('id'))->update(array("status"=>1));
		alert('success',Status_Applied,1);
	}
	if($_GET['stat']=='0'){
		DB::table(FDBPrefix.'contact')->where('id='.Input::get('id'))->update(array("status"=>0));
		alert('success',Status_Applied,1);
	}
}