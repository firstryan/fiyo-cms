<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$line = ["",
    "<div class='kop-style-1'></div>",
    "<div class='kop-style-2a'></div><div class='kop-style-2b'></div>",
    "<div class='kop-style-3a'></div><div class='kop-style-3b'></div>",
];

$isi = "";
   

?>
<!-- FIRST PAGE -->
<div class="container <?=  Printer::$config['class']; ?>" style="padding: <?=  Printer::$config['padding']; ?>"> 
    <div class="top"><?php  
        if(checkLocalhost()) {
            echo str_replace("media/",FLocal."media/",$data[0]);			
        } 
        else 
            echo   $data[0]; 
        ?>
        <?php if(isset($line[$data[1]])) echo $line[$data[1]]; ?>
    </div>
    <div class="main">
        <?php  
        if(checkLocalhost()) {
            echo str_replace("media/",FLocal."media/",$data[2]);			
        } 
        else 
            echo   $data[2]; 
        ?>
    </div>

    <div class="footer">
    </div>
</div>
<!-- END OF PAGE -->

            
