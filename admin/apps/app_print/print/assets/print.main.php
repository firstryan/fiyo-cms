<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');


?>
<html>
    <head>
        <title>
            <?=Printer::get('title');?>
        </title>

        <link rel="stylesheet" href="<?php echo  "apps/app_print/print/" .basename(__dir__); ?>/print.template.css">
	    <link href="<?php echo AdminPath; ?>/css/font/font-awesome.css" rel="stylesheet" />
        <style>

        </style>
        
        <script src="<?php echo AdminPath; ?>/js/jquery.min.js"></script>        
        <script src="<?php echo AdminPath; ?>/js/jquery-print.js"></script>
        <script>

            $(function() {
                
                $(".print-now").click(function () {


    
                    $("#print-it").print(/*options*/);
                });
                $(".zoom-1").click(function () {
                    $(".wrapper").animate({ 'zoom': 1.2 }, 200);
                });
                $(".zoom-2").click(function () {
                    
                    $(".wrapper").animate({ 'zoom': 1.5 }, 200);
                });
                $(".zoom-0").click(function () {
                    $(".wrapper").animate({ 'zoom': 1 }, 200);
                });

            });

        </script>
    </head>
    <body>
        <div id="controller">
            <span class="zoomer"><a class="zoom-0">Normal</a>
            <a class="zoom-1">Zoom (x1)</a>
            <a class="zoom-2">Zoom (x2)</a>
        </span>
            <a class="print-now right"><i class="icon icon-print"></i>
Print</a>

        </div>
        <div class="wrapper"  style="width: <?=  Printer::$config['width']; ?>">
        <?php  if(Printer::$config['editable']) $editable = 'contenteditable="true"'; else $editable = ''; ?>
            <div id="print-it" <?=$editable;?>> 
                <?php foreach( $pages as $data) if(!empty($data[2])) include("print.page.php"); ?>
            </div>
        </div>
    </body>
</html>