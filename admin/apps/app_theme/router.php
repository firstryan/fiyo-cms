<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');


$view = Input::get('view');
$folder = Input::get('folder');
$act 	= Input::get('act');
$api 	= Input::get('api');

			
if(!$api)
if($view == 'layout') {
    switch ($act) {
        case "add" :
	    require('layout/add_layout.php');
    break;
        case "edit" :
	 require('layout/edit_layout.php');
            break;
        default : 
	 require('layout/view_layout.php');
            break;
        
    }         
}
else if($view == 'admin')
	 require('admin_theme.php');
else if($folder AND $folder != 'blank') 
	 require('edit_theme.php');
else
	 require('site_theme.php');

if($api)
switch($api)
{
    case 'module':	 
        $module = app_param('module');
	 require("themes/" .siteConfig('admin_theme') . "/module/$module.php");
	break;
    case 'modules':	 
        $module = app_param('modules');
	 require("themes/" .siteConfig('admin_theme') . "/modules/$module.php");
	break;
    case 'plugins':	 
        $module = app_param('plugins');
	 require("themes/" .siteConfig('admin_theme') . "/plugins/$module.php");
	break;
    case 'plugin':	 
        $module = app_param('plugin');
	 require("themes/" .siteConfig('admin_theme') . "/plugin/$module.php");
	break;
    case 'api':	 
        $module = app_param('api');
	 require("themes/" .siteConfig('admin_theme') . "/api/$module.php");
	break;
}