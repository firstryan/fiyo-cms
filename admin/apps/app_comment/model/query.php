<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2017 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

class Query {
	public static $message = null;
	protected static $status = false;
	
	function comment_update($data, $id) {
			$data = array_query($data,['comment','name','website' ,'email','status']);
			
		//validation
		$valid = Validator::is_valid($data, [
			'name' => 'required',
			'email' => 'required',
		]);
		
		//query		
		if($valid === true) {
			if(Database::table(FDBPrefix.'comment')->where("id=$id")->update($data)) {
				self::$message = Comment_Updated;	
				notice('success', Comment_Updated);	
				return true;
			}
			else {
				self::$message = Status_Fail;
				notice('error',Status_Fail);				
				return false;
			}
			
		}	
		else {	
			self::$message = $valid; 			
			notice('error', $valid);		
			return false;
		}
	}

		
}
