<?php
/**
* @version		1.4.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');

$view 	= Input::get('view');
$act 	= Input::get('act');
$api 	= Input::get('api');
$print	= Input::get('print');
	
if(!$api) 	
switch($act)
{	
	default :
	 require('default.php');
	break;
	case 'config':	 
	 require('config.php');
	break;
	case 'edit':
	 require('edit.php');
	break;
}

if(!$print AND $api) {		
	switch($api) {		   
	   case 'comment-list':	 
		   require('api/comment_list.php');
	   break;
	   case 'comment-status':	 
		   require('api/comment_status.php');
	   break;
   }	
}
