<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see license.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');

require_once('model/query.php');

/****************************************/
/*		      Delete Comment			*/
/****************************************/ 	
if(isset($_POST['delete_comment']) or isset($_POST['check_comment'])){
	$source = @$_POST['check_comment'];
	$source = multipleSelect($source);
	$delete = multipleDelete('comment',$source);	
	if(isset($delete)){
		notice('info',Comment_Deleted);
		redirect(getUrl());
	}
	else {
		notice('error',Please_Select_Item);
		redirect(getUrl());
	}
}

/****************************************/
/*	 Redirect when comment-Id not found	*/
/****************************************/

if(Input::post('save_comment')) {	
	$data 	= Input::$post;	
	$id 	= Input::get('id');
	if(Query::comment_update($data, $id)) refresh();	
}



function update($a,$b) {	
	$db = new FQuery();  
	$db->connect(); 
	$qr=DB::table(FDBPrefix."comment_setting")
		->where("name='$a'")
		->update(['value' => "$b"]);
	if($qr) return true;

}

if(isset($_POST['save_config'])) { 	
	$qr = update('auto_submit',$_POST['auto']);
	$qr = update('name_filter',"$_POST[name]");
	$qr = update('email_filter',"$_POST[email]");
	$qr = update('word_filter',"$_POST[word]");			
	$qr = update('recaptcha_publickey',"$_POST[public]");			
	$qr = update('recaptcha_privatekey',"$_POST[private]");			
	if($qr)
	{					
		notice('info',Status_Applied);
	}
}
	