<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

if(USER_LEVEL > 5 or !Input::get('token')) die ('Access Denied!');
defined('_FINDEX_') or die('Access Denied');

$g = date("Y-m-d h:i:s");
$qr = DB::table(FDBPrefix."sitemap")->where("id = ". Input::post('id')) 
        ->update([Input::post('type')=>Input::post('val'),"time"=>"$g"]);
if($qr) echo alert("success",Status_Applied);

?>