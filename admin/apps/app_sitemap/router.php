<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');
$view 	= Input::get('view');
$act 	= Input::get('act');
$api 	= Input::get('api');
$print	= Input::get('print');
	
if(!$api) 	
switch($act)
{	
	default :
	 require('view/default.php');
	break;
}

if(!$print AND $api) {		
	switch($api) {		   
	   case 'create':	 
		   require('api/create.php');
	   break;
	   case 'update':	 
		   require('api/update.php');
	   break;
	   case 'crawl':	 
	   require('api/crawl.php');
	   break;
   }	
}

