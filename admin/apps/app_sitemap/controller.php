<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

// Access only for Administrator
if($_SESSION['USER_LEVEL'] > 2)
	redirect('index.php');
	
$db = new FQuery();
if(isset($_POST['config_save'])) {
	if(empty($_POST['web']) AND empty($_POST['timeout']) AND empty($_POST['ex_dir']) AND empty($_POST['ex_file']) AND empty($_POST['xml']) AND empty($_POST['txt'])) 
	{	
		notice('error','invalid');
	}			
	else	
	{	 		
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='root_url'")->update(['value'=>Input::post('web')]);
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='timeout'")->update(['value'=>Input::post('timeout')]);
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='ex_dir'")->update(['value'=>Input::post('ex_dir')]);
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='ex_file'")->update(['value'=>Input::post('ex_file')]);
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='xml'")->update(['value'=>Input::post('xml')]);
		$qr=DB::table(FDBPrefix."sitemap_setting")->where("name='txt'")->update(['value'=>Input::post('txt')]);
		
		notice('success',Status_Applied);
		refresh();
	}
}

if(isset($_POST['save_crawler']) AND isset($_SESSION['CRAWLER_DATA'])) {
    $c = $_SESSION['CRAWLER_DATA'];
    if(!empty($c)) {
	$g = date("Y-m-d h:i:s");
	foreach($c as $h) { 
		if(oneQuery("sitemap","url","$h")) {
			$f = "UPDATE `".FDBPrefix."sitemap` SET `time`= '$g' WHERE `url` = '$h'";	
			$qr= DB::query($f);
		}
		else {
			$f = "INSERT INTO `".FDBPrefix."sitemap` (`id`,`url`,`time`) VALUES ('','$h','$g');";
                      
			$qr= DB::query($f);
		}
	}	
        if(isset($f)) {
            $qr=DB::table(FDBPrefix."sitemap")->delete("time < '$g'");
            notice('success',Status_Saved);
            redirect("?app=sitemap");
        }
    }
}
                            