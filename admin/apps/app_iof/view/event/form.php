<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');


$event = Query::getEventData();
$klub = Query::getKlubData($event['klub_id']);

?>

<script> 
$(function () {
	$("form#mainForm").submit(function (e) {
		/*static event*/
		e.preventDefault();
		
		/* initialize editor */
		$("textarea[name='editor']").val(getEditor('editor'));		
		
		/* get Form data */
		data = $(this).serialize();
		
		/* send post data */
		if($(this).valid())
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=save", data).done(function (result) {
			notice(result);		//print notice alert
			id = parseInt(result.id); // set ID
			if(result.status == 'success' && id > 0){

				loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>");

				// loadUrl("?app=<?= Req::get('app')?>&view=<?= Req::get('view')?>&act=edit&id=" + id);
			}
		});
	});


	$(".btn-status").click(function () {
		status = $(this).data('status');
		
		$.post("?app=<?= Req::get('app')?>&api=<?= Req::get('view')?>&type=status&status=1", '_token='+ $('[name=_token').val() + '&id_event=<?=Req::get('id');?>&status='+ status)
		.done(function (result) {
				notice(result);
			id = parseInt(result.id); 
		})
		.complete(function(e) {
			refresh();
			logs(e.responseText);
		});	
	});

});
</script>
<div class="full">
	<div class="box">								
		<header>
			<h5>Data</h5>

			<?php if($data['status'] == 0 AND userAllow([2]) OR userAllow(1)) : ?>
				<a href="#" class="btn-success btn setujui btn-status" data-status="1"> &nbsp;<i class="icon-legal" ></i> Setujui Tanggal Booking</a>
			<?php endif; ?>

			<?php if($data['status'] == 1 AND userAllow([2]) OR userAllow(1)) : ?>
				<a href="#" class="btn-danger btn status btn-status" data-status="0"> &nbsp;<i class="icon-legal"></i> Batalkan Persetujuan Tanggal Booking</a>
				<a href="#" class="btn-success btn status btn-status" data-status="2"> &nbsp;<i class="icon-legal"></i> Registrasi Klub Event</a>
			<?php endif; ?>
			
			<?php if($data['status'] == 2 AND userAllow([3]) OR userAllow(1) ) : ?>
				<a href="#" class="btn-success btn status btn-status" data-status="4"> &nbsp;<i class="icon-legal"></i> Setujui Registrasi Event</a>
				<a href="#" class="btn-danger btn cancel-status btn-status" data-status="3"> &nbsp;<i class="icon-legal"></i> Tidak Setujui Registrasi Event</a>


				<!-- Beri alasan -->
			<?php endif; ?>
		

			<?php if($data['status'] == 4 AND userAllow([2]) OR userAllow(1)) : ?>
				<a href="#" class="btn-success btn setujui btn-status" data-status="5"> &nbsp;<i class="icon-legal"></i> Verifikasi Kelengkapan Event</a>
			<?php endif; ?>

			
		</header>									
		<header>
		</header>							
			<div>
			<table>
				<tr>		
					<td>
						Nama
					</td>
					<td>
						<?= Form::text('nama_event','',["required" => "required", "size" => "50"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Tanggal Booking
					</td>
					<td>
						<?= Form::date('tanggal_booking','',["required" => "required", "size" => "50"]); ?>
					</td>
				</tr>
				<tr>		
					<td>
						Tanggal Pelaksanaan
					</td>
					<td>
						<?= Form::date('tanggal_pelaksanaan','',["required" => "required", "size" => "50"]); ?>
					</td>
				</tr>
				
				<tr>		
					<td>
						Status
					</td>
					<td>
						<?php 

							$s = $data['status'];
							if($s == 0) {
								$status = '<label class="label label-default"> menunggu persetujuan dari PENGDA</label>';
							}
							if($s == 1) {
								$status = '<label class="label label-primary"> PENGDA menyutujui Tanggal Booking</label>';
							}
							elseif($s == 2) {
								$status = '<label class="label label-warning"> KLUB pengajuan Registrasi ke PENGCAB </label>';
							}
							elseif($s == 3) {
								$status = '<label class="label label-danger"> PENGCAB tidak menyetujui Registrasi</label>';
							}
							elseif($s == 4) {
								$status = '<label class="label label-primary"> PENGCAB menyetujui Registrasi</label>';
							}
							elseif($s == 5) {
								$status = '<label class="label label-success"> Persyaratan Event Lengkap</label>';
							}

						?>
						<?= $status ?>
					</td>
				</tr>
				
				
				<tr>		
					<td>
						Kendaraan
					</td>
					<td><?php

						$kelas = ['Mobil' => "Mobil",
								'Motor' => "Motor"]

						?>
						<?= Form::select('kelas', $kelas, '', ["class" => "form-control kendaraan", "label-class" => "bmd-label-floating" ,"required"], "Jenis Kendaraan"); ?>
					</td>
				</tr>
				
				<tr>		
					<td>
						Kelas
					</td>
					<td> <?php
							$kelas = []
						?>
						<?= Form::select('jenis', $kelas, '', ["class" => "form-control jenis", "label-class" => "bmd-label-floating" ,"required", "readonly"], "Jenis Event"); ?>
					</td>
				</tr>
				
				<tr>		
					<td>
						Personal
					</td>
					<td>
						<?php
							$sql = DB::table("regencies")->where("province_id = $klub[pengda]")->get();
							
							foreach($sql as $k=>$v) {
								if($v['id'] != $klub['pengcab'])
									$reg[$v['id']] = $v['name']."{disabled='disabled'}";	
								else
									$reg[$v['id']] = $v['name']."{selected='selected'}";				
							}
						?>    
						<?php

						$kelas = []

						?>
						<?= Form::select('kota', $reg, '', ["class" => "form-control kota", "label-class" => "bmd-label-floating" ,"required" ,"disasbled"], "Pilih Kabupaten/Kota"); ?>
					</td>
				</tr>
				
				
				<tr>		
					<td>
						Alamat
					</td>
					<td>
						<?= Form::textarea('alamat','',["required" => "required", "rows" => "2"]); ?>
					</td>
				</tr>
			</table>		
		</div>  
	</div>
</div>

<script>
    $(function () {
        $(".kendaraan").change(function () {
            v = $(this).val();
            
            if(v == 'Mobil')
                var newOption = $('<option value="Event">Event</option><option value="Latian Bersama">Latian Bersama</option><option value="Bakti Sosial">Bakti Sosial</option>');
            else
                var newOption = $('<option value="Kelas A">Kelas A</option><option value="Kelas B">Kelas B</option><option value="Kelas C">Kelas C</option>');
           
            $('.jenis').html(newOption).val("<?=$event['jenis'];?>").trigger("chosen:updated");
        }).change();
    })

</script>

<?= Form::hidden($app['index']); ?>
<?= Form::close(); ?>
