<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

$api 	= Input::get('api');
$act 	= Input::get('act');
$view 	= Input::get('view');
$type 	= Input::get('type');
$print 	= Input::get('print');

if(!$api AND !$print) :
	
switch($view)
{
	// Client View
	case 'client':
		switch($act)
		{
			case 'add':	 
				require('view/'. $view .'/add.php');
			break;
			case 'edit':
				require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	
	// Project View
	case 'project':
		switch($act)
		{
			case 'add':	 
			require('view/'. $view .'/add.php');
			break;
			case 'edit':
			require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	// Invoice View
	case 'invoice':
		switch($act)
		{
			case 'add':	 
			require('view/'. $view .'/add.php');
			break;
			case 'edit':
			require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	// Vendor View
	case 'vendor':
		switch($act)
		{
			case 'add':	 
				require('view/'. $view .'/add.php');
			break;
			case 'edit':
				require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	// Vendor View
	case 'bill':
		switch($act)
		{
			case 'add':	 
				require('view/'. $view .'/add.php');
			break;
			case 'edit':
				require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	// Vendor View	
	case 'project':
		switch($act)
		{
			case 'add':	 
				require('view/'. $view .'/add.php');
			break;
			case 'edit':
				require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	
	case 'event':
		switch($act)
		{
			case 'add':	 
				require('view/'. $view .'/add.php');
			break;
			case 'edit':
				require('view/'. $view .'/edit.php');
			break;
			default:
				require('view/'. $view .'/table.php');
			break;	
		}
	break;

	default :
		switch($act)
		{
			case 'add':	 
			require('view/add.php');
			break;
			case 'edit':
			require('view/edit.php');
			break;
			case 'table':
			require('view/table.php');
			break;
			default :
			require('view/dashboard.php');
			break;	
		}
	break;
}
	

else :
	if($api)
	switch($api)
	{	
		case 'clientlist':	 
		require('api/client.list.php');
		break;	
		case 'clientautocomplete':	 
		require('api/client.autocomplete.php');
		break;		
		case 'clientdata':	 
		require('api/client.data.php');
		break;			
		case 'clientsave':	 
		require('api/client.save.php');
		break;	
		case 'clientdelete':	 
		require('api/client.delete.php');
		break;
		

		
		// Invoice Item API
		case 'invoiceitemlist':	 
		require('api/invoice/item.list.php');
		break;		
		case 'invoiceitemdata':	 
		require('api/invoice/item.data.php');
		break;			
		case 'invoiceitemsave':	 
		require('api/invoice/item.save.php');
		break;	
		case 'invoiceitemdelete':	 
		require('api/invoice/item.delete.php');
		break;

		case 'invoice':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;

		case 'client':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;

		case 'vendor':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;
		
		case 'bill':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;
				
		case 'event':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;
		
		case 'transaction':
			$file = __dir__ ."/api/$api/$type.php";
			if(file_exists($file)) require_once($file);
		break;


		case 'list':	 
		require('api/list.php');
		break;
		case 'save':	 
		require('api/save.php');
		break;	
		case 'delete':	 
		require('api/delete.soft.php');
		break;		
		case 'item-list':	 
		require('api/item.list.php');
		break;		
		case 'item-data':	 
		require('api/item.data.php');
		break;			
		case 'item-save':	 
		require('api/item.save.php');
		break;	
		case 'item-delete':	 
		require('api/item.delete.php');
		break;


		default :
			page('404');
		break;
	}

	if($print)
	switch($print)
	{
		case $print:
			$p = "print/$print.php"; 	 
			if(file_exists(__dir__.'/'.$p))
				require($p);
			else 
				page('404');
		break;
		default :
			page('404');
		break;
	}
endif;