<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

//if(!Input::get('iSortCol_0')) die ('Access Denied!');

defined('_FINDEX_') or die('Access Denied');

// Field Data Item
$item = false;

$table1 = $app['table'];
$table2 = $app['table_group'];

if(Input::get('id'))
$item = DB::table($app['table'])
    ->where("group_id = ". Input::get('id'))
    ->leftJoin($table2, "$table2.id_group = $table1.group_id")
    ->get();
    
?>
<table class="data nota paper58">
    <tbody>
        <?php
        $i = 1;
            if($item){ 
                $index = $app['index'];
                $total = 0;
                foreach($item as $row) {
                    $status = [
                        "Outcome" => "Pengeluaran",
                        "Income" => "Pemasukan"
                    ];
                    $status = $status[$row['type']];
                    $value = number($row['value']);
                    $stotal = $row['value'] * $row['qty'];
                    
                        $total += $stotal;
                        $stotal = number($stotal);


                    echo "<tr>";
                    echo "<td colspan='4'>$row[item]</td>";  
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td style='text-align:center'> $row[qty] x</td>";
                    echo "<td style='text-align:right'> $value </td>"; 
                    echo "<td style='text-align:right'  colspan='3'>$stotal</td>";   
                    echo "</tr>";
                }
                
                echo "<tr>";
                echo "<td colspan='4' class='no-border' style='text-align:center'>==============================</td>";   
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan='2' class='no-border'></td>";  
                echo "<td style='text-align:center'><b>Total</b></td>";  
                echo "<td style='text-align: right'>" . number(abs($total)). "</td>";   
                echo "</tr>";
            }
            else {
                echo "<tr>";
                    echo "<td colspan='6' class='data-empty'>". Data_is_empty;
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </tbody>			
</table>    