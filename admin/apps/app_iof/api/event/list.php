<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(Input::get('token') != USER_TOKEN) die ('Access Denied!');

$sTable	 = FDBPrefix."iof_event";
$sTableKlub  = FDBPrefix."iof_klub";
$sTableKota	 = "regencies";

$aColumns =[
	'id_event' , 	
	'nama_event', 
	'nama_klub as klub',
	'tanggal_booking', 
	"$sTableKota.name as kota", 
	'status',
	'tanggal_pelaksanaan', 
	'jenis', 
	'kelas', 
	'alamat', 
	'koordinat',  
	
];

$aWhereColumns =[
	'id_event',
	'nama_event', 
	'tanggal_booking', 
	'tanggal_pelaksanaan', 
	'jenis', 
	'kelas', 
	'nama_klub',
	'alamat', 
	'koordinat', 
	'kota',  
	'status'
];


/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = 'id_event';

/* Indexed column (used for fast and accurate table cardinality) */
$uniqueColumn = 'id_event';

$sGroup = " ";

$sJoin = "LEFT JOIN $sTableKota ON $sTable.kota = $sTableKota.id ";
$sJoin .= "LEFT JOIN $sTableKlub ON $sTable.klub_id = $sTableKlub.id_klub ";

/*
	* Ordering
	*/
$sOrder = "ORDER BY created_at ASC";


$sTables = " $sTable ";

/* 
	* Paging
	*/
$sLimit = "LIMIT 10";
if (  Req::get('iDisplayLength') != '-1' )
{
	$sLimit = "LIMIT ".intval( Req::get('iDisplayStart') ).", ".
		intval( Req::get('iDisplayLength') );
}



if (Req::get('iSortCol_0') AND !empty(Req::get('iSortCol_0')))
{
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<intval( Req::get('iSortingCols') ) ; $i++ )
	{
		if ( Req::get( 'bSortable_'.intval(Req::get('iSortCol_'.$i))) == "true" )
		{
			$data = str_replace(".","`.`",$aColumns[ intval( Req::get('iSortCol_'.$i) ) ]);	
					
			$c = strpos( strtolower($aColumns[ intval( Req::get('iSortCol_'.$i) ) ]), "as");
			if($c) { $c += 3;
				$data = substr($aColumns[ intval( Req::get('iSortCol_'.$i) ) ], $c);
			}

			$data = trim(str_replace(["@","(",")"," as","as "],"", strtolower($data)));
			$sOrder .= "`". $data ."` ".
				(Req::get('sSortDir_'.$i)==='asc' ? 'asc' : 'desc') .", ";
				
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if ( $sOrder == "ORDER BY" )
	{
		$sOrder = "";
	}
}

/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
if(!empty((int)Input::get('cat'))) 
	$cat = " category_id = '".Input::get('cat')."'  AND "; 
else 
	$cat = ''; 
	
//Filter Year
if(!empty((int)Input::get('year'))) {		
	setSession('YEAR',Input::get('year'));
	$year = " YEAR(date) = '".Input::get('year')."'  AND "; 
}
else {
	setSession('YEAR', 0);
	$year = ''; 

}

//Filter Month
if(!empty((int)Input::get('month'))) {
	setSession('MONTH',Input::get('month'));
	$month = " MONTH(date) = '".Input::get('month')."'  AND "; 
}
else {		
	setSession('MONTH', 0);
	$month = '';
} 


if(!empty(Req::get('user'))) $user = " author_id = ' ". Req::get('user') ."  AND "; 
else $user = '';

if(!empty(Req::get('level'))) $level = "  $sTable.level = 'Req::get([level]'  AND "; 
else $level = '';

$deleted = "$sTable.deleted_at = '0000-00-00 00:00:00'";
$sWhere = " WHERE  $cat  $year $month $deleted ";

if ( Req::get('sSearch') && Req::get('sSearch') != "" )
{
	$sWhere .= " AND(";
	for ( $i=0 ; $i<count($aWhereColumns) ; $i++ )
	{

		$data = str_replace(".","`.`",$aWhereColumns[$i]);
		$sWhere .= "`".$data."` LIKE '%".addslashes( Req::get('sSearch') )."%' OR ";
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
}

/* Individual column filtering */
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
	if ( Req::get('bSearchable_'.$i) && Req::get('bSearchable_'.$i) == "true" && Req::get('sSearch_'.$i) != '' )
	{
		if ( $sWhere == "AND " )
		{
			$sWhere .= " ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		$sWhere .= "`".$aColumns[$i]."` LIKE '%".addslashes(Req::get('sSearch_'.$i))."%' ";
	}
}	

/*
	* SQL queries
	* Get data to display
	*/


$sColumns = "`".str_replace([" as "," AS "], "` as `", str_replace(".","`.`", str_replace(" , ", " ", implode("`, `", $aColumns))))."`";
$sColumns = str_replace(["`(",") `","`@", "@ `"], "", $sColumns );
$sColumns = str_replace(")`", ")", $sColumns );

$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS 
	$sColumns
	FROM  $sTables $sJoin
	$sWhere 
	$sGroup
	$sOrder		
	$sLimit
	";
	
$rResult = Database::query($sQuery);

// echo $sQuery;
/* Data set length after filtering */

$sQuery = "
	SELECT FOUND_ROWS()
";

$rResultFilterTotal = Database::query($sQuery)->fetchColumn();
$iFilteredTotal = $rResultFilterTotal;
//echo $iFilteredTotal;
/* Total data set length */

$sQuery = "
	SELECT COUNT(`".$sIndexColumn."`)
	FROM   $sTable
";	


$rResultTotal = Database::query($sQuery)->fetchColumn();
$iTotal = $rResultTotal;	
/*
	* Output
	*/

$output = array(
	"sEcho" => intval(@Req::get('sEcho')),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

header('Content-Type: application/json');
$saldo = 0;
foreach ( $rResult as $aRow )
{
	$row = array();
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{					
		$data = $aRow;
		//title 					
		$title = $aRow['nama_event'];

		if($_SESSION['USER_LEVEL'] <= 3) {
			//get view parameter
			$view = Req::get('api');
			if($view) $view = "&view=".$view;
			
			$checkbox ="<label><input type='checkbox' data-name='rad-$aRow[$sIndexColumn]' name='check[]' value='$aRow[$sIndexColumn]' rel='ck' ><span class='input-check'></span></label>";
			
			$name ="<a class='tips' title='".Edit."' href='?app=" . Req::get('app') . $view . "&act=edit&id=$aRow[$sIndexColumn]' target='_self' data-placement='right'>$title </a>";	
								
		}
		else {	
			$name =  $title;
			$checkbox = "<span class='icon lock'></lock>";
		}

		

		$s = $data['status'];
		if($s == 0) {
			$status = '<label class="label label-default"> menunggu persetujuan dari PENGDA</label>';
		}
		if($s == 1) {
			$status = '<label class="label label-primary"> PENGDA menyutujui Tanggal Booking</label>';
		}
		elseif($s == 2) {
			$status = '<label class="label label-warning"> PKLUB pengajuan Registrasi ke PENGCAB </label>';
		}
		elseif($s == 3) {
			$status = '<label class="label label-danger"> PENGCAB tidak menyetujui Registrasi</label>';
		}
		elseif($s == 4) {
			$status = '<label class="label label-primary"> PENGCAB menyetujui Registrasi</label>';
		}
		elseif($s == 5) {
			$status = '<label class="label label-success"> Persyaratan Event Lengkap</label>';
		}
		
		//
		if ( $i == 0 )
		{			
			if(!Req::get('param'))
			$row[] = $checkbox; 
		}				
		else if ( $i == 1 )
		{					
			$row[] = "<div>".$name."</div>";				
		}			
		else if ( $i == 2 )
		{			
			$row[] = "<div>".$aRow["klub"]."</div>";
		}	
		else if ( $i == 3 )
		{			
			$row[] = "<div>".$aRow["tanggal_booking"]."</div>";
		}
		else if ( $i == 4 )
		{				
			$row[] = "<div>".$aRow["kota"]."</div>";
		}
		else if ( $i == 5 )
		{						
			$row[] = "<div>$status</div>";
		}
		else if ( $i == 6 )
		{		
			$row[] = "<div>".$aRow["kelas"]."</div>";
		}
		else if ( $i == 7 )
		{		
			$row[] = "<div>".$aRow["alamat"]."</div>";
		}
		else if ( $i == 8 )
		{		
			$row[] = "<div>".$aRow["tanggal_pelaksanaan"]."</div>";
		}
	}
	$output['aaData'][] = $row;
}
	
echo json_encode( $output );