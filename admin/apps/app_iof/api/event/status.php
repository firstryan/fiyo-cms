<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');
if(USER_LEVEL > 3) die ();
header('Content-Type: application/json');

//Check CSRF Token AND $data
$data = Input::$post;
if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denised');

//setup saving type
$app['type'] = "edit";

//set refference index
if(isset($data[$app['index']]))
$app['reff'] = $data[$app['index']];

//setup fillable field
$app['fillable'] = [
    'status' ,
];


//setup query saving
$qr = Query::save(array_merge(["data" => $data], $app));

//setup return value
if($qr) {

    //skrip kirim email disini

    
    $return = [
        'status' => 'success',
        'text' => Status_Saved,
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => Query::$message
    ];
} 


echo json_encode($return);