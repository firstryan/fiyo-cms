<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');



ob_start(); 
require(__dir__.'../../api/invoice/transaction/print.php');
$trans = ob_get_contents();
ob_end_clean();


ob_start(); 
require(__dir__.'../../api/invoice/item/print.php');
$table = ob_get_contents();
ob_end_clean();




 
$data = DB::table(FDBPrefix.'office_invoice_item')
        ->where(" invoice_id = ". Req::get('id'))
        ->leftJoin(FDBPrefix.'office_invoice',"id_invoice = invoice_id")
        ->get();
        
if($data) $data = $data[0];
else die("Data Kosong");

$statex = strtolower($data['status']);
$staupx = strtoupper($data['status']);
$status = "<div class='status $statex'>$staupx</div>";

$client = $data['client'] ;
if($data['client_id']) {        
    $c = DB::table(FDBPrefix.'office_client')
    ->where(" id_client = ". $data['client_id'])
    ->get();

    if(isset($c[0])) {
        $c = $c[0];
        $client = "$c[name]";
        if(!empty($c['personal'])) {            
            $client = "$c[name] ($c[personal])";
        }
        $client .= "<br>".str_replace("\n","<br>",$c['address']);
    }
}
$kamus = [
    "{client}" => "$client",
    "{no}" => "$data[no]",
    "{date}" => date_format(date_create($data['created_date']), "d F Y"),
    "{duedate}" => date_format(date_create($data['due_date']), "d F Y"),
    "{status}" => "$status",
    "{item}" => $table,
    "{transaction}" => $trans,
];



$template = ($data['template']) ? $data['template'] : 'invoice';
$cetak = new Printer($kamus, $template, ["class" => "invoice"]);

$cetak->cetak();

?>