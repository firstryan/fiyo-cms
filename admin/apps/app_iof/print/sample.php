<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');



ob_start(); 
require(__dir__.'../../api/item.print.nota.php');
$table = ob_get_contents();
ob_end_clean();
 
$data = DB::table($app['table_group'])
        ->where(" id_group = ". Req::get('id'))
        ->leftJoin(FDBPrefix.'cashflow_category',"id_cat = category_id")
        ->get();

if($data) $data = $data[0];
else die("Data Kosong");

$kamus = [
    "{type}" => "$data[category]",
    "{date}" => date_format(date_create($data['date']), "d F Y"),
    "{place}" => $data['place'],
    "{item}" => $table,
];


$cetak = new Printer($kamus, 'nota', ["width" => "180", "padding" => 0, "paper"=>"58", "class"=>"nota"]);
$cetak->cetak();

?>