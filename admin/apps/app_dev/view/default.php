<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>	
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$(".activator label").click(function(){ 
		var parent = $(this).parents('.switch');
		var id = $('.number',parent).val();	
		var value = $('.type',parent).val();
		if(value == 1) value = 0; else value = 1;
		$.ajax({
			url: "?app=module&api=status",
			data: "stat="+value+"&id="+id,
			success: function(data){	
				if(value == 1) {
					$('.type',parent).val("1");
				}
				else 
					$('.type',parent).val("0");				
				notice(data);		
			}
		});
	});
	$(".home label").click(function(){ 
		var parent = $(this).parents('.switch');
		var id = $('.number',parent).attr('value');	
		var value = $('.type',parent).attr('value');
		if(value == 1) value = 0; else value = 1;
		$.ajax({
			url: "?app=module&api=status",
			data: "name="+value+"&id="+id,
			success: function(data){
				if(value == 1)
					$('.type',parent).val("1");
				else 
					$('.type',parent).val("0");				
				notice(data);		
			}
		});
	});
	
	$(".cb-enable").click(function(){		
		var parent = $(this).parents('.switch');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', false);	
	});
	
	$(".cb-disable").click(function(){		
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', false);	
	});
	
	$("#form").submit(function(e){
		e.preventDefault();
		var ff = this;
		var checked = $('input[name="check[]"]:checked').length > 0;
		if(checked) {	
			$('#confirmDelete').modal('show');	
			$('#confirm').on('click', function(){
				ff.submit();
			});		
		} else {
			noticeabs("<?php echo alert('error',Please_Select_Delete); ?>");
			$('input[name="check[]"]').next().addClass('input-error');
			return false;
		}
	});		
	loadTable();
});
</script>
<form method="post" id="form">
	<div id="app_header">
		<div class="warp_app_header">		
			<div class="app_title">Data Tabel</div>
			<div class="app_link">			
				<a class="add btn btn-primary" href="?app=module&act=add" title="<?php echo Add_new_module; ?>"><i class="icon-plus"></i> <?php echo Add; ?></a>
				<button type="submit" class="delete btn btn-danger btn-grad" title="<?php echo Delete; ?>" value="<?php echo Delete; ?>" name="delete"><i class="icon-trash"></i> &nbsp;<?php echo Delete; ?></button>
				<input type="hidden" value="true" name="delete_confirm"  style="display:none" />
				<?php printAlert(); ?>
				<span class="filter-table visible-lg-inline-block">
					<span>
						<?php echo Category; ?>
						<select name="cat" class="category filter"  data-placeholder="<?php echo Choose_category; ?>" style="min-width:120px;">
						<option>All</option>
						<?php	
							$_GET['id']=0;
							$sql =DB::table(FDBPrefix.'article_category')->where('parent_id=0')->get(); 
							foreach($sql as $rowc){
								if($rowc['level'] >= $_SESSION['USER_LEVEL']){
									if(Req::get('cat')==$rowc['id']) $s="selected";else$s="";
									echo "<option value='$rowc[id]' $s>$rowc[name]</option>";
								}
							}						
						?>
						</select>
					</span>
					
					<span>
						<?php echo Author; ?>
						<select name="user"  class="user filter"  placeholder="">
						<option value="">All</option>
							<?php 
								$sql = Database::table(FDBPrefix.'user')->select('*')->get();
								foreach($sql as $rowa){
									if($rowa['level']==Req::get('user')){
										echo "<option value='$rowa[id]' selected>$rowa[name]</option>";}
									else {
										echo "<option value='$rowa[id]'>$rowa[name]</option>";
									}
								}
							?>
						</select>					
					</span>
					<span>
						<?php echo Access_Level; ?>
						
							<?php
						$sql = Database::table(FDBPrefix.'user_group')
								->select('level, group_name')->get();
								
						$qr = [0 => 'All' ];
								//if($rowg['level']==99 AND !$_GET['level'] == '99') $s="selected"; else $s="";
								//echo "<option value='99' $s>"._Public."</option>"
						foreach($sql as $k=>$v) {
							$qr[$v['level']] = $v['group_name'];				
						}

						$qr += [99 => _Public];
						echo Form::select(
							'level',$qr, '',
							["class" => "form-control level filter", "name"=>"level"]
						); 
					?>
					</span>
				</span>
			</div>
		</div>		 
	</div>
	<table class="data">
		<thead>
			<tr>								  
				<th style="width:1% !important;" class="no" colspan="0" id="ck">  
					<input type="checkbox" id="checkall" target="check[]">
				</th>
				<th style="width:25% !important;" hidden-xs><?php echo Title; ?></th>
				<th style="width:15% !important;" class='no hidden-xs' align="center">Status</th>
				<th style="width:20% !important;"><?php echo Position; ?></th>
				<th style="width:25% !important;" class='hidden-xs'><?php echo Type; ?></th>
				<th style="width:10% !important; text-align:center;" class='hidden-xs'><?php echo Short; ?></th>
				<th style="width:15% !important; text-align:center;" class='hidden-xs'><?php echo _Print; ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$tb1 = FDBPrefix.'module';
			$tb2 = FDBPrefix.'user_group'; 
			$sql = DB::table($tb1)->select('*,'.$tb1.'.id as mid')->orderBy('`position` DESC, `short` ASC, `name` ASC')
				->leftJoin($tb2,$tb1.'.level = '.$tb2.'.level')->get();
				
			$no=1;
			foreach($sql as $row){	
				if($row['status']==1)
				{ $stat1 ="selected"; $stat2 =""; $enable = ' enable';}							
				else
				{ $stat2 ="selected";$stat1 =""; $enable = 'disable';}
				
				$status ="<span class='invisible'>$enable</span>
				<div class='switch s-icon activator'>
					<label class='cb-enable $stat1 tips' data-placement='right' title='".Disable."'><span>
					<i class='icon-remove-sign'></i></span></label>
					<label class='cb-disable $stat2 tips' data-placement='right' title='".Enable."'><span>
					<i class='icon-check-circle'></i></span></label>
					<input type='hidden' value='$row[mid]' class='number invisible'>
					<input type='hidden' value='$row[status]'  class='type invisible'>
				</div>";	
				
				//switch status
				if($row['show_title']==1)
				{ $sname1 ="selected"; $sname2 =""; $stitle = ' show';}							
				else
				{ $sname2 ="selected"; $sname1 =""; $stitle ='hide';}
				$sname ="
				<div class='switch s-icon home'><span class='invisible'>$stitle</span>
					<label class='cb-enable $sname1 tips' data-placement='left' title='".Hidden_title."'><span>
					<i class='icon-font'></i>
					</span></label>
					<label class='cb-disable $sname2 tips' data-placement='left' title='".Visible_title."'><span>
					<i class='icon-font'></i></span></label>
					<input type='hidden' value='$row[mid]'  class='number invisible'>
					<input type='hidden' value='$row[show_title]' class='type invisible'>
				</div>";			
				
				//module name
				$name = "<a href='?app=module&act=edit&id=$row[mid]' class='tips' data-placement='right' title='".Edit."'>$row[name]</a>";
				
				//checkbox
				$check = "<input type='checkbox' name='check[]' value='$row[mid]' rel='ck'>";						
				//creat user group values	
				$level = $row['group_name'];
				if(empty($level)) $level = _Public;
				
				$url_cetak = url("?app=office&print=sample&id=$row[position]");

				$print = "<a data-url='$url_cetak' type='button'  data-print='' class='btn btn-primary cetak' data-toggle='modal' data-target='#cetak'>Cetak <i class='icon-print'></i></a>";
				
				echo "<tr><td align='center'>$check</td><td>$name</td><td class='hidden-xs'><div class='switch-group'>$sname$status</div></td><td class='hidden-xs'>$row[position]</td><td>$row[folder]</td><td align='center' class='hidden-xs'>$row[short]</td><td align='center' class='hidden-xs'>$print</td></tr>";
				$no++;	
			}
			?>
        </tbody>			
	</table>
</form>




test ini adalah test enak enak 





<?php 
$print = "<a data-url='$url_cetak' type='button'  data-print='' class='btn btn-primary cetak' data-toggle='modal' data-target='#cetak'>Cetak <i class='icon-print'></i></a>";
?>
<div id="cetak" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Print Preview</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
        <div class="frame"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>
//JS script
$('.cetak').on('click', function(e){
  	.preventDefault();  
  	$('#cetak iframe').remove();
  	$('#cetak').modal('show').find('.frame').append("<iframe src=" + $(this).data('url') + " class='full'></iframe> ");
});
</script>