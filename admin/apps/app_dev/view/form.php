<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

?>

<script> 
	
	
</script>
<div class="full">
	<div class="box">								
		<header class="dark">
			<h5>Login Data</h5>
		</header>								
		<div>
			<table>
				<tr>
					<td>
						Switch Button
					</td>
					<td>
					<label>
						<span class="switch">  
							<input id="rad1" value="1" name="status" type="radio" selected="" checked="" class="invisible form-control wrapped">
							
							<input id="rad2" value="0" name="status" type="radio" class="invisible form-control wrapped">
							
							<label for="rad1" class="cb-enable checked selected"><span>Aktif </span></label>  <label for="rad2" class="cb-disable"><span>Non&nbsp;aktif </span></label>
						</span>
					</label>


						<span class="switch"> 
						<?= Form::switchs('switch','1', true, ['class' => 'cb-disable'],'Radio Button 1'); ?>
						
						
						<?= Form::switchs('switch','2', false,'','Radio Button 1'); ?>

						

						
						<?= Form::switchs('switch','3', true,'','Radio Button 1'); ?>

						
						</span>
					</td>
				</tr>
				<tr>
					<td>
						Input Text
					</td>
					<td>
						<?= Form::text('text','',['required']); ?>
					</td>
				</tr>
				<tr>
					<td>
						Email Input
					</td>
					<td>
						<?= Form::email('email'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Input Number
					</td>
					<td>
						<?= Form::number('nomor'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Input Date
					</td>
					<td>
						<?= Form::date('date'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Input Datetime
					</td>
					<td>
						<?= Form::datetime('datetime','',["required2"]); ?>
					</td>
				</tr>
				
				<tr>
					<td>
						Input Time
					</td>
					<td>
						<?= Form::time('time'); ?>
					</td>
				</tr>

				
				<tr>
					<td>
						Currency
					</td>
					<td>
						<?= Form::currency('number'); ?>
					</td>
				</tr>

				<tr>
					<td>
						Input List
					</td>
					<td>
						<?php
							$sql = Database::table(FDBPrefix.'user_group')
									->where("level >= ". USER_LEVEL)
									->select('level, group_name')->get();
									
							$qrs = [];
							foreach($sql as $k=>$v) {
								$qrs[$v['level']] = $v['group_name'];				
							}
							$qrs += [];
							echo Form::select(
								'level',$qrs, '',
								["class" => "form-control", "required2"]
							);
						?>					
					</td>
				</tr>
					
				<tr>
					<td>
						Radio Button
					</td>
					<td>
						<?= Form::radio('radio','1', true,'','Radio Button 1'); ?>
						<?= Form::radio('radio','2', true,'','Radio Button 1'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Checkbox
					</td>
					<td>
						<?= Form::checkbox('checkcheck','1','1',''); ?>
						<?= Form::checkbox('checkcheck2','2','0','','Label 1'); ?>
						<br>Nama, value, checked, option, label
					</td><span>
				</tr>
					
					<tr>
						<td>
							Input File
						</td>
						<td>
							<?= Form::file('inis', '',["class" => "form-control", "max" => "1900","requw`ired" , "extention" => "pdf,jpg,jpeg"]) ?>
						</td>
					</tr>
					<tr>
						<td>
							Input File
						</td>
						<td>
							<?= Form::file('inis', '',["class" => "form-control", "max" => "1900","rewquired" , "extention" => "pdf,jpg,jpeg"]) ?>
						</td>
					</tr>

					<tr>
					<td>
						Input Image
					</td>
					<td>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>						
					</td>
				</tr>

				<tr>
					<td>
						Input Image
					</td>
					<td>
						<?= Form::fileimage('ini', '',["class" => "form-control", ""]); ?>
					</td>
				</tr>

				
				<tr>
					<td>
						Color Picker
					</td>
					<td>
						<?= Form::color('ini'); ?>
					</td>
				</tr>

			</table>			
		</div>  
	</div>
</div>
<div class="col-lg-6 box-right">
	<div class="box">								
		<header class="dark">
			<h5>Personal Data</h5>
		</header>								
		<div>
			<table>
				
			</table>	
		</div>  
	</div> 
</div>  
