<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2020 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/


// header('Content-Type: application/json');     
defined('_FINDEX_') or die('Access Denied');

$id     = app_param('id');
$type   = app_param('type');
$parent = app_param('parent');
$filter = app_param('filter');
$data   = false;


$filter = str_replace("%20", ' ', $filter);

switch($type) {
    
    case 'province' :
        
        if($id)
            $data = DB::table("provinces")->where("id = '$id'")->get();
        else if($parent)
            $data = DB::table("provinces")->where("id = '$parent'")->get();
        else
            $data = DB::table("provinces")->where("name LIKE '%$filter%'")->get();

    break;
    
    case 'city' :

        if($id)
            $data = DB::table("regencies")->where("id = '$id'")->get();
        else if($parent)
            $data = DB::table("regencies")->where("province_id = '$parent'")->get();
        else
            $data = DB::table("regencies")->where("name LIKE '%$filter%'")->get();

    break;

    case 'distric' :
        
        if($id)
            $data = DB::table("kecamatan")->where("id = '$id'")->get();
        else if($parent)
            $data = DB::table("kecamatan")->where("regency_id = '$parent'")->get();
        else
            $data = DB::table("kecamatan")->where("name LIKE '%$filter%'")->get();

    break;
    
    case 'village' :
        
        if($id)
            $data = DB::table("kelurahan")->where("id = '$id'")->get();
        else if($parent)
            $data = DB::table("kelurahan")->where("kecamatan_id = '$parent'")->get();
        else
            $data = DB::table("kelurahan")->limit(100)->where("name LIKE '%$filter%'")->get();

    break;
} 

Api::render($data);