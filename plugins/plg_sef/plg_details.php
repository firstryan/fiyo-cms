<?php
/**
* @name			Plugin SEF
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2017 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/

defined('_FINDEX_') or die('Access Denied');

$plg_name   = 'SEF';
$plg_desc   = 'Plugins untuk melakukan kontrol terhadap setiap fungsi SEF';
$plg_author = 'Fiyo CMS';