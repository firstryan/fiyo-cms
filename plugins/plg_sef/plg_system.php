<?php
/**
* @name			Plugin SEF
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2017 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
*/

defined('_FINDEX_') or die('Access Denied');

if(strpos(getLink(),"?app=") !== 0 AND !isHomePage()) {
	$link = getLink();
	$links = array_reverse(explode("/", $link));
	foreach($links as $a => $b) {
		$url =  substr($link, 0, strrpos($link, $b));
		$check = DB::table(FDBPrefix.'permalink')->where("permalink LIKE '%$url%'");
		if($check->count()) {
			$sef = (object) $check->get()[0];
		break;
		}
	}


	$c = getLink();
	
	$linkArray = explode("/", $c);
	$permalink = explode("/", $sef->permalink);

	$i = 0;
	$break = false;
	foreach($permalink as $a){
		if($linkArray[$i] == $a or  strpos($a,"{") !== false) {
		} else {
			$break = true;
			break;
		}
		$i++;
	}
	
	if(count($permalink) == count($links) AND strpos($sef->permalink,"{") !== false AND !$break) {
		$link = $sef->link;
		foreach($permalink as $key => $value) {
			if(strpos($value, "{") == 0 AND  strpos($value, "}") == strlen($value)-1) {
				$val = str_replace(["{", "}"], "", $value);		
				$link = str_replace("{{$val}}", $linkArray[$key], $link);
			}
		}
		$get = explode("&", $link);
		foreach($get as $g) {
			$p = explode("=", $g);
			if(isset($p[1])) {
				$key = str_replace("?", "", $p[0]);
				$val = $p[1];
				Input::setGet($key, $val);Input::setRequest($key, $val);
			}
		}
		Input::setRequest('link', $sef->permalink);
		new Input();
	} else {	

		header("HTTP/1.1 404 Not Found", true, 404); 
	}
}


/************* SEF PAGINATION FUNCTION *****************/
function redirect_www() {
	if($_SERVER['SERVER_ADDR'] != '127.0.0.1' AND $_SERVER['SERVER_ADDR'] != '::1' AND $_SERVER['SERVER_ADDR'] != $_SERVER['HTTP_HOST'] ) {
		if(isset($_SERVER['HTTPS']))  $http = "https"; else $http = "http";
		if(siteConfig('sef_www')) {
			if(!strpos(getUrl(),"//www.")) {
				$link = getUrl();
				$link = str_replace("$http://","$http"."://www.",$link);
				redirect($link);
			}
		}
		else {
			if(strpos(getUrl(),"//www.")) {
				$link = getUrl();
				$link = str_replace("$http"."://www.","$http"."://",$link);
				redirect($link);
			}
		}
	}
}

function redirect_https() {
	if(siteConfig('https')) {
		if(!isset($_SERVER['HTTPS'])) {
			$link = getUrl();
			$link = str_replace("http://","https://",$link);
			redirect($link);			
		}  
	}
}


function redirect_link(){	
	
	$c = getLink();
	$c = str_replace("//","/", $c);
	$spos =  strrpos($c,"/");
	if(strlen($c)-1 == strrpos($c,"/")) {
		$link = substr($c, 0, $spos);
		$link = url($link);
		redirect($link);
	}

	return false;
	$url = getUrl();
	if(substr($url, -1) == '&') {	
		redirect(substr($url,0, strlen($url) -1));
	} 

	if(!isHomePage()){
		$url = getUrl();
		if(isset($_SERVER['HTTPS'])) $http = "https"; else $http = "http";
		$app = app_param('app');	
		if(strpos($c = str_replace("$http://","",getUrl()),"//")) {
                    while(strpos($c,"//")) {
			$c = str_replace("//","/",$c);
                    }
                    redirect("$c");
		}
		
		if(SEF_URL) {	
			$got = false;
			if(_Page == 1) {
				if(strpos(getUrl(),"&page="))
					redirect(str_replace("&page="._Page,"",getUrl()));
			}
			if(empty($app)) {
                header("HTTP/1.1 404 Not Found", true, 404); 
				//redirect for 404 page
				$link = str_replace(FUrl,"",getUrl());
				$linl = strlen($link);
                       
				$max = 0;
				while($linl) {	
					if(substr($link,$linl-1) == "/") {					
						$link = substr($link,0, $linl-1);
					}					
					$got = FQuery("permalink","permalink LIKE '$link%'");
					$linl = strlen($link);
					if($linl < 4 or $max == 20 ){
						$fail_url = getUrl();
						break;
					}
					else if($got) {
						$link = FQuery("permalink","permalink LIKE '$link%'","permalink");
						break;
					}
					$link = substr($link,0, $linl-1);
					$max++;
				}
				if($got AND !isset($fail_url))
					redirect(FUrl.$link);
				else if(_FINDEX_ !== 'BACK' or strpos(getUrl(),"?")) {
        				//redirect(FUrl);
        				//die();
     				    }
				
			}
			else if(!empty($app)) {
				if( strlen($url) -  strpos($url,"?") < 4)
        			redirect(substr($url,0,strpos($url,"?")));
			}
			else if(SEF_EXT == 0 or SEF_EXT == '') {
				$sum = strlen(getUrl());
				$rev = strrev(getUrl());
				if(!strpos(getUrl(),'?')) {
					$pos = substr(getUrl(),$sum-1);
					if($pos == "/")
						redirect(substr(getUrl(),0,$sum-1));
				}
			}
		}
		else {
			$direct = check_permalink('link',$_SERVER['REQUEST_URI'],'permalink');
			$xurl =  "$http://".$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];
			$gurl = str_replace(FUrl,'',$xurl);
			$strt = strlen($gurl);
			$x = 1;
			while($strt) {
				$val =  substr($gurl,0,$strt);
				if(check_permalink('permalink',$val,'link')) {
					if(str_replace($val.SEF_EXT,'',$xurl) != FUrl)
						if(_Page == 0)
					break;				
				}				
				$strt -= 1;
				$x++;			
			}
			$url =  check_permalink('permalink',$val,'link');
			$pid =  check_permalink('permalink',$val,'pid');
			//echo FUrl.$url;//redirect(FUrl.$url.SEF_EXT);
		}
	}
	else {
		if(isset($_GET['page']) AND _Page == 1)
		redirect(FUrl);
	}	
}

function add_permalink($title, $cat = NULL, $pid = null, $ext = null, $next = null) {
	$page = _Page;
	if(!preg_match("/[0-9]/",$page))
		$page = null;
		
	if(SEF_URL AND !isHomePage() AND !$page)
	{ 		
		$eqpos = strpos($_SERVER['REQUEST_URI'],"=");
		$tapos = strpos($_SERVER['REQUEST_URI'],"?");
		if($eqpos >0 AND $tapos>0 AND empty($_GET['page'])){		
			$permalink = str_replace(" ","-",strtolower($title));		
			if(app_param('app') == 'article' AND app_param('view') == 'item' ) {	
				while(substr_count($permalink,'/'))
				{
					$permalink = str_replace("/","-",$permalink);
				}
			}
			
			$category = str_replace(" ","-",strtolower($cat));
			
			if(!empty($cat))
				$permalink = strtolower($category)."/".$permalink;
			else
				$permalink = $permalink;				
		
			
			while(substr_count($permalink,"["))
			{
				$permalink = str_replace("[","",$permalink);
			}
			
			while(substr_count($permalink,"]"))
			{
				$permalink = str_replace("]","",$permalink);
			}
			
			while(substr_count($permalink,"("))
			{
				$permalink = str_replace("(","",$permalink);
			}
			
			while(substr_count($permalink,")"))
			{
				$permalink = str_replace(")","",$permalink);
			}
			
			while(substr_count($permalink,"{"))
			{
				$permalink = str_replace("{","",$permalink);
			}
			
			while(substr_count($permalink,"}"))
			{
				$permalink = str_replace("}","",$permalink);
			}
			
			while(substr_count($permalink,"&amp;"))
			{
				$permalink = str_replace("&amp;","",$permalink);
			}
			
			while(substr_count($permalink,"&"))
			{
				$permalink = str_replace("&","",$permalink);
			}
			
			/************ ? removal **************/
			while(substr_count($permalink,"?"))
			{
				$permalink = str_replace("?","",$permalink);
			}
			
			/************ + removal **************/
			while(substr_count($permalink,"+"))
			{
				$permalink = str_replace("+","",$permalink);
			}/************ # removal **************/
			while(substr_count($permalink,"#"))
			{
				$permalink = str_replace("#","",$permalink);
			}
			/************ & removal **************/
			while(substr_count($permalink,"\&"))
			{
				$permalink = str_replace("\&","",$permalink);
			}
			
			/************ . removal **************/
			while(substr_count($permalink,"."))
			{
				$permalink = str_replace(".","-",$permalink);
			}
			
			/************ ! removal **************/
			while(substr_count($permalink,"!"))
			{
				$permalink = str_replace("!","",$permalink);
			}
			
			/************ ` removal **************/
			while(substr_count($permalink,"`"))
			{
				$permalink = str_replace("`","",$permalink);
			}
			
			/************ ' removal **************/
			while(substr_count($permalink,"'"))
			{
				$permalink = str_replace("'","",$permalink);
			}
			
			/************ " removal **************/
			while(substr_count($permalink,"\""))
			{
				$permalink = str_replace('"',"",$permalink);
			}
			
			/************ ; removal **************/
			while(substr_count($permalink,";"))
			{
				$permalink = str_replace(';',"",$permalink);
			}
			/************ " removal **************/
			while(substr_count($permalink,'|'))
			{
				$permalink = str_replace('|',"",$permalink);
			}
			/************ % removal **************/
			while(substr_count($permalink,'%'))
			{
				$permalink = str_replace('%',"",$permalink);
			}
			/************ * removal **************/
			while(substr_count($permalink,'*'))
			{
				$permalink = str_replace('*',"",$permalink);
			}/************ ^ removal **************/
			while(substr_count($permalink,'^'))
			{
				$permalink = str_replace('^',"",$permalink);
			}	
			/************ \ removal **************/
			while(substr_count($permalink,'\\'))
			{
				$permalink = str_replace("\\","",$permalink);
			}/************ \ removal **************/
				
			/************ , removal **************/
			while(substr_count($permalink,','))
			{
				$permalink = str_replace(",","",$permalink);
			}
			
			/************ $ removal **************/
			while(substr_count($permalink,'$'))
			{
				$permalink = str_replace("$","",$permalink);
			}
			
			/************ @ removal **************/
			while(substr_count($permalink,'@'))
			{
				$permalink = str_replace("@","",$permalink);
			}
			while(substr_count($permalink,"--"))
			{
				$permalink = str_replace("--","-",$permalink);
			}
										
			if(empty($pid)) $pid = Page_ID;
			$link = getLink();
			
			
			if(!empty($category) AND empty($ext)) 
				$permalink = $permalink.SEF_EXT;
			else if(!empty($ext)) {
				$ext = str_replace(".","",$ext);
				$permalink = "$permalink.$ext";
			}
					
			$link = str_replace("&amp;","&",$link);

			if(strpos($link,"api=")) return false;
			if($pml = check_permalink('link',$link,'permalink')) {
				redirect(FUrl.$pml);
            }
			else if(!empty($permalink)  AND http_response_code() !== 404){
				if($c = check_permalink('permalink',$permalink)) {	
					/*if(app_param('app') == 'article' AND app_param('view') == 'item' ) {
						if(check_permalink('link',$link,'permalink'))
							redirect(FUrl.check_permalink('link',$link,'permalink'));	
					} else {*/
						$x = 2;
						$permalink = str_replace(SEF_EXT,"",$permalink);
						while($c) {
							$p = "$permalink-$x";
							$c = check_permalink('permalink',$p.SEF_EXT);
							$x++;	
						}
						$permalink = $p.SEF_EXT;
					/*}*/
				}


				if(!empty($permalink) AND $permalink != "-" AND !empty($link)) {					
					if(DB::table(FDBPrefix.'permalink')->insert(['link'=>$link,'permalink'=>$permalink,'pid'=>$pid,'status'=>1])) 
					redirect(FUrl.$permalink);
				}
			}			
		}	
	}	
}

function delete_permalink($link) {
	DB::table(FDBPrefix.'permalink')->delete("permalink='$link'");
}


function add_link($sef, $link = null) {
	if(empty($link)) $link = getLink();
	if(SEF_URL AND !isHomePage())
	{ 	
		$eqpos = strpos($_SERVER['REQUEST_URI'],"=");
		$tapos = strpos($_SERVER['REQUEST_URI'],"?");
		if($eqpos >0 AND $tapos>0 AND empty($_GET['page'])){

			$permalink = str_replace(" ","-",strtolower($sef));	
			$link = str_replace(" ","-",strtolower($link));	
							
			while(substr_count($permalink,"["))
			{
				$permalink = str_replace("[","",$permalink);
			}
			
			while(substr_count($permalink,"]"))
			{
				$permalink = str_replace("]","",$permalink);
			}
			
			while(substr_count($permalink,"("))
			{
				$permalink = str_replace("(","",$permalink);
			}
			
			while(substr_count($permalink,")"))
			{
				$permalink = str_replace(")","",$permalink);
			}
			
			
			while(substr_count($permalink,"&amp;"))
			{
				$permalink = str_replace("&amp;","",$permalink);
			}
			
			while(substr_count($permalink,"&"))
			{
				$permalink = str_replace("&","",$permalink);
			}
			
			/************ ? removal **************/
			while(substr_count($permalink,"?"))
			{
				$permalink = str_replace("?","",$permalink);
			}
			
			/************ + removal **************/
			while(substr_count($permalink,"+"))
			{
				$permalink = str_replace("+","",$permalink);
			}/************ # removal **************/
			while(substr_count($permalink,"#"))
			{
				$permalink = str_replace("#","",$permalink);
			}
			/************ & removal **************/
			while(substr_count($permalink,"\&"))
			{
				$permalink = str_replace("\&","",$permalink);
			}
			
			/************ . removal **************/
			while(substr_count($permalink,"."))
			{
				$permalink = str_replace(".","-",$permalink);
			}
			
			/************ ! removal **************/
			while(substr_count($permalink,"!"))
			{
				$permalink = str_replace("!","",$permalink);
			}
			
			/************ ` removal **************/
			while(substr_count($permalink,"`"))
			{
				$permalink = str_replace("`","",$permalink);
			}
			
			/************ ' removal **************/
			while(substr_count($permalink,"'"))
			{
				$permalink = str_replace("'","",$permalink);
			}
			
			/************ " removal **************/
			while(substr_count($permalink,"\""))
			{
				$permalink = str_replace('"',"",$permalink);
			}
			
			/************ ; removal **************/
			while(substr_count($permalink,";"))
			{
				$permalink = str_replace(';',"",$permalink);
			}
			/************ " removal **************/
			while(substr_count($permalink,'|'))
			{
				$permalink = str_replace('|',"",$permalink);
			}
			/************ % removal **************/
			while(substr_count($permalink,'%'))
			{
				$permalink = str_replace('%',"",$permalink);
			}
			/************ * removal **************/
			while(substr_count($permalink,'*'))
			{
				$permalink = str_replace('*',"",$permalink);
			}/************ ^ removal **************/
			while(substr_count($permalink,'^'))
			{
				$permalink = str_replace('^',"",$permalink);
			}	
			/************ \ removal **************/
			while(substr_count($permalink,'\\'))
			{
				$permalink = str_replace("\\","",$permalink);
			}/************ \ removal **************/
				
			/************ , removal **************/
			while(substr_count($permalink,','))
			{
				$permalink = str_replace(",","",$permalink);
			}
			
			/************ $ removal **************/
			while(substr_count($permalink,'$'))
			{
				$permalink = str_replace("$","",$permalink);
			}
			
			/************ @ removal **************/
			while(substr_count($permalink,'@'))
			{
				$permalink = str_replace("@","",$permalink);
			}
			while(substr_count($permalink,"--"))
			{
				$permalink = str_replace("--","-",$permalink);
			}
										
			if(empty($pid)) 
				$pid = Page_ID;
			
			$link = str_replace("&amp;","&",$link);

			if(strpos($link,"api=")) 
				return false;
				
			if($pml = check_permalink('link',$link,'permalink')) {
				redirect(FUrl.$pml);
            }
			else if(!empty($permalink)  AND http_response_code() !== 404){
				if($c = check_permalink('permalink',$permalink)) {	
					/*if(app_param('app') == 'article' AND app_param('view') == 'item' ) {
						if(check_permalink('link',$link,'permalink'))
							redirect(FUrl.check_permalink('link',$link,'permalink'));	
					} else {*/
						$x = 2;
						$permalink = str_replace(SEF_EXT,"",$permalink);
						while($c) {
							$p = "$permalink-$x";
							$c = check_permalink('permalink',$p.SEF_EXT);
							$x++;	
						}
						$permalink = $p.SEF_EXT;
					/*}*/
				}


				if(!empty($permalink) AND $permalink != "-" AND !empty($link)) {					
					if(DB::table(FDBPrefix.'permalink')->insert(['link'=>$link,'permalink'=>$permalink,'pid'=>$pid,'status'=>1])) 
					redirect(FUrl.$permalink);
				}
			}			
		}	
	}	
}