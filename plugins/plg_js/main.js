
var xhr = new XMLHttpRequest();
xhr.open('INIT', '', false);
xhr.send(null);
FTheme = xhr.getResponseHeader('FTheme');
FUrl = xhr.getResponseHeader('FUrl');


function checkFirstVisit() {
	$(window).bind('onbeforeunload',function(){
		return false;
		window.location.replace(document.URL);

	});  
	$(document).bind('keydown keyup', function(e) {
		if(e.which === 116) {
		   console.log('blocked');
			e.preventDefault();	
		   window.location.replace(document.URL);
		}
		if(e.which === 82 && e.ctrlKey) {
		   console.log('blocked');
			e.preventDefault();	
		   window.location.replace(document.URL);
		}
	});
}

function bname(path) {
	if(path)
		return path.split('/').reverse()[0];
	else	
		return "";
}

function basename(path, text = "Choose file...") {
	if(path) {
		path = bname(path);
		return path.split('\\').reverse()[0];
	} 
	else 
		return text;
}

function stringify(s) {		
	return s.replace(/"/g, "'");
}

function getEditor(editor) {
	try{
		return stringify(CKEDITOR.instances['editor'].getData());
	} catch (e) {

	}
}

function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires ;
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
	  var c = ca[i];
	  while (c.charAt(0) == ' ') {
		c = c.substring(1);
	  }
	  if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
	  }
	}
	return "";
  }

function URLToArray(url) {
    var request = {};
    var pairs = url.substring(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < pairs.length; i++) {
        if(!pairs[i])
            continue;
        var pair = pairs[i].split('=');
        request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
     }
     return request;
}

function tmpUrl(url) {
	if(url == '') {
		return tmpUrl.url;
	} else {
		if ( typeof tmpUrl.url == 'undefined' ) {
			tmpUrl.url = url;
		}
	}
}

function refresh(){
	return true;
}

function loadUrl(url, param) {
	$("#loadingbar").remove();
	dataurl = url;
	$("body").append("<div id='loadingbar'></div>").find("#loadingbar").animate({width:'90%'},3000);
	
	if (typeof pingVisitor !== 'undefined') clearInterval(pingVisitor);
	if(dataurl) {
		var gurl = url;
		var w = $("#loadingbar");
		dataurl = dataurl + "";
		if(dataurl.indexOf("?") === 0)
			dataurl = FUrl + dataurl;

	  	$.ajax({
			url: dataurl,
			type: 'GET',
			timeout:30000, 
			beforeSend: function(xhr){
				xhr.setRequestHeader('LoadHTMLData', true);
				xhr.setRequestHeader('FTheme', getCookie('FTheme'));
			},
			error:function(response){
				if(response.status == 404)
					window.location.href = response.responseJSON.url;
					
				$("#mods").modal("show");
				w.stop();
				w.animate({width:'101%'},100).fadeOut('fast');
			},
			success: function(response, text, xhr){	
				themePast = getCookie('FTheme')
				themeNow = xhr.getResponseHeader('FTheme');
				themeNow = xhr.getResponseHeader('FTheme');
				FUrl = xhr.getResponseHeader('FUrl');
				changeTheme = false;

				if(themePast != themeNow) {
					changeTheme = true;
					setCookie('FTheme', themeNow);
				}

				w.stop();
				$('.mCSB_container').css('top',0);
				
				if(response == 'Redirecting...' || response == 'Access Denied!'){
					window.location.replace(location.href);
				}
				else {
 					breadcrumb(gurl);
					if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {							
						tabs = $(".drawer").scrollLeft();
					}
					
					if(param != 'noredirect') {
						if(response.redirect == 'refresh') {	
							loadUrl(window.location.href, {notice: response});
						}
						else if(response.redirect !== 'undefined') {							
							loadUrl(response.redirect, {notice: response});
						}

						if(changeTheme) {
							$("html").hide();
							$("html").delay(600).fadeIn(1000).html(response.html);
						}
						else {
							$("main").html(response.html);
						}

						$("script:not([data-static])").remove();
						$(response.js).appendTo("body");

						if(param != 'nopush') 
						window.history.pushState(response.url, "Fiyo CMS", response.url);
					}

					//remove Animate
					w.animate({width:'101%'},100).fadeOut('fast');
					setTimeout(function() {
					  w.remove();
					}, 1100);
					
					//load time to footer
					var z = $(".load-time").val();
					$("#load-time").html(z);
						
					//remove unused class
					if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {						
						$('body').removeClass('hide-sidebar user-sidebar no-transform');
						$('.scrolling').removeClass('hide-sidebar');
						$("#left").css("left","");
						$('.changeSidebarPos').removeClass('removeSidebar');
						$(".navbar-logo").html($(".app_title").html());	
						
						
						$(".tabs").scrollLeft(tabs);
					} 

					loader();					
					$("a[href], a[href]:not([target='_blank']").click(function(e) {	
						if (!$(this).attr('target') ){
							if ($(this).attr('href')!== window.location.hash){
								e.preventDefault();	
								loadUrl(this);
							}
						}						
					});
				}
				if(typeof param !== "undefined"){
					notice(param.notice)
				}
			}
		});
	}
}

window.onpopstate = function(e){
    if(e.state){
		var url = e.state;		
			loadUrl(url, 'nopush');
    }
};

$(function() {
	$("a[href], a[data-delete], a[href]:not([target='_blank']").click(function(e) {			
	   if (!$(this).attr('target')){
		   if ($(this).attr('href')!==window.location.hash){
				e.preventDefault();	
				if($(this).data('noredirect') == 'true' ||  $(this).data('delete')){
					loadUrl(this, 'noredirect');
				} else {
					loadUrl(this);
				}
			}		
	   }
	});
});

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {	
	title = $(".app_title").html();
	$(".navbar-logo").html(title);						
} 

$(function() {
	$("body").fadeIn(500);
	//checkFirstVisit();
    $('a[href*=\\#]').on('click', function(e){
      e.preventDefault();
    });    
	
    $('.minimize-box').on('click', function(e) {
        e.preventDefault();
        var $icon = $(this).children('i');
        if ($icon.hasClass('icon-chevron-down')) {
            $icon.removeClass('icon-chevron-down').addClass('icon-chevron-up');
        } else if ($icon.hasClass('icon-chevron-up')) {
            $icon.removeClass('icon-chevron-up').addClass('icon-chevron-down');
        }
    });
	
    $('.close-box').click(function() {
        $(this).closest('.box').hide('slow');
    });

    $('.changeSidebarPos').on('click', function(e) {
        $('body').toggleClass('hide-sidebar');
		$('body').removeClass('user-sidebar no-transform');
		$("#left").css("left","");
		$('.changeSidebarPos').toggleClass('removeSidebar');
		$.ajax({
			type: 'POST',
			url: "themes/fiyo/module/view.php",
			data: "view=true",
			success: function(data){
			}
		});	
	});
	


    $('.userSideBar').on('click', function(e) {
        $('body').toggleClass('user-sidebar');
        $('body').removeClass('hide-sidebar');
    });
    
    $('.removeSidebar').on('click', function(e) {
		removeSidebar();
	});

	function removeSidebar() {
        $('body').removeClass('hide-sidebar');
        $('body').removeClass('user-sidebar');
		$('body').removeClass('no-transform');
		$("#left").css("left",'');

	}
    $('li.accordion-group > a').on('click',function(e){
        $(this).children('span').children('i').toggleClass('icon-angle-down');
    });		
	
	$('.spinner').change(function () {
		$(this).next('label').hide() ;
	});
	
	loader();


	
});

window.__defineGetter__('__FILE__', function() {
	return (new Error).stack.split('/').slice(-1).join().split('.')[0];
});

function logs(data) {
	console.log(data);
}
function formNumber() {
	$('form').submit(function() {
		$("input.currency, input.decimal", $(this)).val(function () {return $(this).autoNumeric('get') });
		$('.error').parents('.panel-collapse').height('auto').removeClass('collapsed').addClass('in').before().find('a').removeClass('collapsed');		
	});
}

function reformNumber() {
	$("input.currency, input.decimal").each(function () {
		dec = $(this).data('decimal');
		if(!dec) dec = ',';
		
		sep = $(this).data('separator');
		if(!sep) sep = '.';
		
		max = $(this).attr('max');
		if(!max) max = null;
		
		decl = $(this).data('declong');
		if(!decl) decl = 0;

		val =  $(this).val();
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');

		if(dec != '.') {
			val = val.replace(dec,'.');
		}

		$(this).autoNumeric('init', {
			aSep: sep,
			aDec: dec,
			mDec: decl				
		}).autoNumeric('set', val);		
	});	
}

function selectAjax(formthis, u, m, id, text) {	
	t = formthis = $(formthis);
	holder = $(formthis).attr('placeholder');

	if(t.data('list')) {
		d = t.data('list').split("|");
		if(!u)
		u = d[0]; //url
		m = d[1]; //method	
	
		if(!id)
		id = d[2]; //id
	
		if(!text)
		text = d[3]; //text	
	}
	
	def = t.data("default");
	sel = t.data("selected");
	if(sel) def = sel;

	dis = t.data("disable")+ ''
	ena = t.data("enable") + '';

	if(!Array.isArray(dis))			
		dis = dis.split(",");
	if(!Array.isArray(ena))			
		ena = ena.split(",");

	if(u.indexOf("?") === 0)
		u = FUrl + u;

    $.ajax({
      url: u,
      method: m,
      complete: function (r) {
		disable = defaulted = '';	
		formthis.html('');	
		formthis.append('<option value=></option>');
		if($.each(r.responseJSON['data'],function(key, value) {
			
			if(!id) ids = value.id;
			else ids = value[id]

			if(!text) texts = value.name;
			else texts = value[text]
			
			if(dis[0] == '@all') {
				disable = " disabled";
			}
			else if(dis.includes(ids)) {
				disable = " disabled";
			}

			if(ena.includes(ids)) {
				disable = '';
			}

			if(def == ids) {
				defaulted = ' selected checked';
			} else {
				defaulted =  '';
			}
			formthis.append('<option value="' + ids +'" ' +   disable + defaulted  + '>' +  texts + '</option>')
		}

		)) {
			if(holder && !def) {
				// $(this).next().find('a').find('span').html($(this).attr('placeholder'));
				formthis.trigger("chosen:updated").next().find('a').find('span').html(holder);	
			}
			else 			
				formthis.trigger("chosen:updated");
			
			if(sel)
				formthis.change();			
		}
	
	  }		
	});
}

function loader() {	
	URL = $("meta[name='url']").attr('content');
	v = $(".input-append.time, .input-append.datetime,.input-append.date").length;	
	$(".bootstrap-datetimepicker-widget").slice(1, 4).remove();
	$("input, select, textarea").addClass('form-control');	
	$("form button").addClass('btn');	
	$('label input[type="checkbox"]:not(.wrapped)').addClass('wrapped form-check-input');
	$('input[type="checkbox"]:not(.wrapped)').addClass('wrapped form-check-input').wrap("<label class='form-check-label'>");
	$('input[type="radio"]:not(.wrapped)').addClass('wrapped').parent().wrap("<label>");
	$('input[type="number"]').attr("type","text").addClass('spinner').addClass('numeric');
	$('input[type="currency"]').attr("type","text").addClass('numeric currency');
	$('input[type="decimal"]').attr("type","text").addClass('numeric decimal');
	$('input[type="checkbox"],input[type="radio"]:not(.switch-radio)').after("<span class='input-check form-check-sign'><span class='check'>");	
	
	$('.switch-radio').each(function () { $(this).attr("id",($(this).attr('name')+$(this).val())) });	

	$("input.form-control[type=password]:not(.wrapped)").addClass('wrapped').parent().wrapInner("<div>");
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {	
		$(".btn").find("[class^='icon-'], [class*='icon-']").parent().addClass("btn-icon-only");
		$('body').on( 'change keyup keydown paste cut', 'textarea', function (){
			$(this).height(0).height(this.scrollHeight - 10);
		}).find( 'textarea' ).change();
		$("input").removeAttr("size");
		$(".dataTables_filter input").prependTo(".dataTables_filter").attr("placeholder","Search...");
		$(".dataTables_filter label").remove();

		$(".app_link.tabs").prependTo("#mainApp");

	}else {
		
		var beforePrint = function(e) {  
			target = $("iframe").contents().find("#print-it");
			height = target.height();

			target.css({"width": "100vw", "position":"fixed", "left":0, "right": "0" , "top" : 0, "z-index": 20});
			$(".modal .modal-dialog").css({"width": "100%", "left":0, "right": "0" , "top" : 0,"margin": 0});
			$(".modal-content").css({"border-radius" : 0}).height(height);
			$(".modal .frame iframe").height(height);
		};


		var afterPrint = function() {		
			target = $("iframe").contents().find("#print-it");
			target.removeAttr("style");
			
			$(".modal-content, .modal .modal-dialog, #print-it").removeAttr("style");
			$(".modal-header, .modal-footer").show();
			
		};

		if (window.matchMedia) {
			var mediaQueryList = window.matchMedia('print');
			mediaQueryList.addListener(function(mql) {
				if (mql.matches) {
					beforePrint();
				} else {
					afterPrint();
				}
			});
		}

		window.onbeforeprint = beforePrint;
		window.onafterprint = afterPrint;

		$('input[type="file"]:not(.wrapped,.file-image-preview):not([disabled])').addClass('wrapped').wrap("<label class='input-append file input-group'>").wrap("<div class='form-file form-control'>").change(function () {	t = $(this); 		
			//check size		
			text = [];
			var fileExtension = t.attr("extention");
			fileExtension = $.trim(fileExtension).split(",");

			if(Array.isArray() && fileExtension.length > 0)
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				text.push("Only formats are allowed : "+fileExtension.join(', '));
			}

			var size = this.files[0].size;
			max = $(this).attr('max');
			if(typeof max !== "undefined")
			{	
				if(size > max * 1000) {
					text.push("File size is to large!");	t.val(null);	
				}
			}	
			
			if(text.length) {
				if(text.length == 1) text = text[0];
				notice("error", text);
			}
			return t.parent().parent().find(".form-file-text").val(t.val());
		}).after(function () {t = $(this);v = t.attr("value"); required = t.attr("required");  return "<input type='text' name='" + t.attr("name") + "-txt' readonly " + required + " placeholder= '" + basename(v) + "' class='form-file-text'>";}).parent().before('<span class="add-on input-group-addon"><i class="icon-file-text-o"></i></span>').wrapInner("<div>");
		
		
		$('input[type="file"].file-image-preview:not([disabled])').addClass('wrapped').wrap("<label class='input-append file input-group'>").wrap("<div class='form-file file-image form-control'>").before("<div class=\"preview-image\"></div>").change(function () {	
			t = $(this); 				
			var size = this.files[0].size;
			max = $(this).attr('max');
			if(typeof max !== "undefined")
			{if(size > max * 1000) {	notice("error","File size is to large!");	t.val(null);	}} 				
			return t.parent().parent().find(".form-file-text").val(t.val());
		
		}).after(function () {t = $(this);v = t.attr("value");p= t.attr("placeholder");  required = t.attr("required");  return "<input type='text' name='" + t.attr("name") + "-txt' readonly " + required + " placeholder= '" + basename(v, p) + "' class='form-file-text'>";}).parent().wrapInner("<div>");
		
		$(".form-file-text").click(function () {t = $(this);t.parent().trigger("click");});

		$('input[type="file"]').change(function () {
			v = $(this);
			v.parent().find(".form-file-text").val(v.val());
		});
	
		if ($.isFunction($.fn.datetimepicker) ) {
			$('input[type=date]:not(.wrapped):not([disabled])').addClass('wrapped date datepicker').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append date input-group'>").datetimepicker({
				format: "yyyy-MM-dd",
				pickTime: false
			}).find(".picker-switch").remove();	
		
			$('input[type=time]:not(.wrapped):not([disabled])').addClass('wrapped time').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-time"></i></span>').parent().wrapInner("<div class='input-append time input-group'>")
			.datetimepicker({
				format: 'hh:mm',
				pickTime: true,
				pickDate: false,		
				pickSeconds: false,
			});
			
			$('input[type=datetime]:not(.wrapped):not([disabled])').addClass('wrapped datetime datetimepicker').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append datetime input-group'>")
			.datetimepicker({
				format: 'yyyy-MM-dd hh:mm:ss',
				pickTime: true,
				pickDate: true,		
			});
			
			
			$("*").scroll(function() {$(".bootstrap-datetimepicker-widget").hide()});
			
			$("#weeklyDatePicker").datetimepicker({
				format: 'MM-DD-YYYY'
			});
			
			if ($.isFunction($.fn.multiDatesPicker) ) {
				$('input[type=multidate]:not(.wrapped):not([disabled])').multiDatesPicker({
					dateFormat: "yy-mm-dd",	
				}).change(function () {
					var dates = $(this).multiDatesPicker('value');
					if($(this).val() == '')	{	
						$(this).val('');
					}
					else{$(this).val(dates);}
					v = dates.split(",");
				}).addClass('wrapped multidate multidatepicker').attr('type','text').attr('autocomplete','OFF').after('<span class="add-on input-group-addon"><i class="icon-calendar"></i></span>').parent().wrapInner("<div class='input-append multidate input-group'>").find('div span').click(function () {$(this).parent().find('input').focus()});
			}
			
			$(".input-group input").focusin(function () {
				$(this).parent().next().find('i').click();
				if($(this).parent().parent().hasClass('date'))
					$(".bootstrap-datetimepicker-widget").find(".picker-switch").hide();
				else
					$(".bootstrap-datetimepicker-widget").find(".picker-switch").show();	
			
			}).focusout(function () {
				$(".bootstrap-datetimepicker-widget").hide();
			});
		
			$('.input-group.date i,.input-group.datetime i').click(function () {
				if($(this).parent().parent().parent().hasClass("date") || $(this).parent().parent().hasClass("date"))	
					$('.bootstrap-datetimepicker-widget').find(".picker-switch").hide();
				else
					$('.bootstrap-datetimepicker-widget').find(".picker-switch").show();
				
				t = $(this).offset().top;
				hl = $(document).height();
				
				l = $(this).offset().left;
				wl = $(document).width();
				
				h =  $('.bootstrap-datetimepicker-widget').height();


				$('.bootstrap-datetimepicker-widget').css("margin-top" , "");
				if((hl - t) < 320) {			
					$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "-"+ parseInt(h+ 10) +"px"}).addClass("drop-top");
				}
				if((wl - l) < 320) {
					if((hl - t) < 320) {			
						$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "-"+ parseInt(h+ 45) +"px"}).addClass("drop-top");
					} else {
						$('.bootstrap-datetimepicker-widget').css({ "margin-top" : "2px"}).addClass("drop-top");
					}		
				}
			});

			$(".bootstrap-datetimepicker-widget .datepicker").click(function() {
				$(".is-focused").addClass("is-filled");
			})
		}
		
	loadSpinner();
		
	}

	$("input[type=email]").addClass("email").wrap("<span class='form-control-wrap'>");

	
	$("select").addClass("select").wrap("<span class='form-control-wrap'>");

	$("input[required]:not(.required)").wrap("<span class='form-control-wrap'>").addClass('required').after('<div class="required-input"><span title="Required" data-placement="top">*</span></div>');

	$("input[type=text]:not(.required)").wrap("<span class='form-control-wrap'>");

	$("textarea[required]:not(.required)").addClass('required').after('<div class="required-input"><span title="Required" data-placement="top">*</span></div>').parent().wrapInner("<div>");
		
	$("select[required]:not(.wrapped)").addClass('wrapped').addClass('required').parent().append('<div class="required-input"><span title="Required" data-placement="top">*</span></div>').wrapInner("<div>");


	$("select[data-list").each(function () {
		t = formthis = $(this);
		d = t.data('list').split("|");
		u = d[0]; //url
		m = d[1]; //method	
		id = d[2]; //url
		text = d[3]; //method	
		
		def = t.data("default");
		dis = t.data("disable")+ ''
		ena = t.data("enable") + '';

		if(!Array.isArray(dis))			
			dis = dis.split(",");

		if(!Array.isArray(ena))			
			ena = ena.split(",");
		
		selectAjax(formthis, u, m, id, text)
	});

	
	$("select[data-trigger]").change(function () {
		t = formthis = $(this);
		v = t.val();
		d = t.data("trigger");
		d = d.split("|");
		c = d[0].split(",");; //url
		u = d[1]; //method	
		u = u.replace('{val}', v);

			
		if($.each(c,function(key, value) {
			if($(value).length) {
				selectAjax(value, u, )
			}
		}));
	});

	$('.required-input i').tooltip();
	
	$("#editor").attr("required","required");

	/* FORM AJAX */
	if ($.isFunction($.fn.validate)) {	
		$("form[ajax=true]").validate({
			submitHandler: function(form) {
				/*static event*/
				form = $(form);
				action = form.attr('action');
				if(!action) action = window.location.href;
				/* initialize editor */
				$("textarea[name='editor']").val(getEditor('editor'));
				var formData = new FormData(form[0]);
				
				/* send post data */
				if(form.valid())
				$.ajax({ 
					type : "POST", 
					url : action, 
					data : formData, 
					contentType: false,
					processData: false,
					beforeSend: function(xhr){
						xhr.setRequestHeader('Authorization', $("input[name='_token']").val());
						xhr.setRequestHeader('API', true);
					},
					success : function(result) { 
						id = parseInt(result.id); // set ID		
						if(result.status == 'success' && id > 0){
							notice(result);
							loadUrl(result.redirect, {notice: result});
						}
						else if(result.status == 'success' && result.redirect){
							loadUrl(result.redirect, {method: 'PUT'});
						} else {
							notice(result);
						}
					}, 
					error : function(result) { 
						logs(result);
						logs(result.responseText);
					} 
				  }); 
	
				reformNumber();
				
			}
		});

		$("form").validate({ 
			ignore: ":hidden:not(select)",			
			errorPlacement: function(error, element) {
				if (element.prop("tagName") == "SELECT" ) {
					error.insertAfter(element.next());				
				}
				else
					error.insertAfter(element);
			}
		  });
		  
		$("body").find("#content form").validate({ ignore: ":hidden:not(select)" });		

	}
	
	$(".cb-enable").click(function(){
		if($(this).has('span').length == 1 ) {
			var parent = $(this).parents('.switch');
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}		
	});

	$(".cb-disable").click(function(){
		if($(this).has('span').length == 1) {
			var parent = $(this).parents('.switch');
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}
	});	
	
	$(".cb").click(function(){
		if($(this).parents('.switch-group').length == 0 ) {
			var parent = $(this).parents('.switch');
			parent.find("input").removeAttr("checked");
			$('label',parent).removeClass('selected');
			$(this).addClass('selected');
		}
	});	
	
	$('.selectbox li').click(function(){
		$(this).toggleClass('active');
		var checkBoxes = $("input[type='checkbox']", this);
		checkBoxes.prop("checked", !checkBoxes.prop("checked"));
	});
	$('.selections-all').click(function(){
		var t = $('.selectbox li').addClass('active');
		var checkBoxes = $("input[type='checkbox']", t);
		checkBoxes.prop("checked",true);
	});
	$('.selections-reset').click(function(){
		var t = $('.selectbox li').removeClass('active');
		var checkBoxes = $("input[type='checkbox']", t);
		checkBoxes.prop("checked",false);
	});

 
	if ($.isFunction($.fn.popover)) {
		$('[data-toggle=popover]').popover();
		$('[data-popover=tooltip]').popover();
	}

	if ($.isFunction($.fn.tooltip)) {
		$('[data-toggle=tooltip]').tooltip();
		$('[data-tooltip=tooltip]').tooltip();
		$('.tips').tooltip();
	}
	
	if ($.isFunction($.fn.alphanumeric)) {
		$('.alphanumeric').alphanumeric();
		$('.alphadot').alphanumeric({allow:"."});
		$('.nocaps').alpha({nocaps:true});
		$('.numeric').numeric();
		$('.numericdot').numeric({allow:"."});
		$('.selainchar').alphanumeric({ichars:'.1a'});
		$('.web').alphanumeric({allow:':/.-_'});
		$('.email').alphanumeric({allow:':.-_@'});

	} 
	
	

	
	$('.accordion-toggle').click(function () {
		$(this).parent().next().css("display", "")
	});
	$('.accordion-toggle collapsed').bind(function () {});
	

	 //Get the value of Start and End of Week
	$('#weeklyDatePicker').on('dp.change', function (e) {
		var value = $("#weeklyDatePicker").val();
		var firstDate = moment(value, "MM-DD-YYYY").day(0).format("MM-DD-YYYY");
		var lastDate =  moment(value, "MM-DD-YYYY").day(6).format("MM-DD-YYYY");
		$("#weeklyDatePicker").val(firstDate + " - " + lastDate);
	});
	

	$(".table-scroll").scroll(function() {
		v = $(this).scrollLeft();
		if(v > 1) $(this).addClass('moving_scroll');
		else $(this).removeClass('moving_scroll');
	});

	
	$(".inner").scroll(function() {
		v = $(this).scrollTop();
		if(v > 1) $(this).addClass('moving_scroll');
		else $(this).removeClass('moving_scroll');
	});

	  $(".file-image-preview").change(function() {
		readURL(this);
	  });

	
	$(".bootstrap-datetimepicker-widget").hide();		

	$("input.currency, input.decimal").each(function () {
		dec = $(this).data('decimal');
		if(!dec) dec = ','
		
		sep = $(this).data('separator');
		if(!sep) sep = '.'
		
		max = $(this).attr('max');
		if(!max) max = null
		
		decl = $(this).data('declong');
		if(!decl) decl = 0

		val =  $(this).val();
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');
		val = val.replace(sep,'');

		if(dec != '.') {
			val = val.replace(dec,'.');
		}

		$(this).autoNumeric('init', {
			aSep: sep,
			aDec: dec,
			mDec: decl				
		}).autoNumeric('set', val);		
	});		


	loadChoosen();
	noticeabs();

	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		

	}else {
		fc = $("#mainApps form input.form-control:visible:not([readonly],disabled,.date,.datetime,.time)").get(0);
		if(!$(".add").length)
		$(fc).focus();
		
		
	}
}


$(document).on('keypress',function(e) {
	//if add exists
	if($(".add").length)
	//if press + AND modal is not open AND input is not infocus
	if(e.which == 43 && !$('#formItem').hasClass('in') && !$('input').is(':focus')) {
		loadUrl($(".add[href]", $("#mainApps")).first().attr('href'));
	}
});

document.addEventListener("keydown", KeyCheck); 
function KeyCheck(event)
{
   var KeyID = event.keyCode;
   switch(KeyID)
   {
      case 8: //backspace
		if(!$('input').is(':focus'))
	  	$(".app_link .key-backspace").click();
      break;
      case 46: //delete
		  $("input.delete, submit.delete").parents('form').submit();
		  $("[type=submit].delete").parents('form').submit();
      break;
	  case 13: //delete
	  	if($("#confirmDelete").hasClass('in'))
		  $("#confirmDelete [name=delete]").click();
      break;
	  case 27: //delete
	  
      break;
      default:
		  
      break;
   }
}


function readURL(input, image = false) {
	if (input.files && input.files[0]) {	
	  var reader = new FileReader();
	  reader.onload = function(e) {
		var size = input.files[0].size;
		max = $(input).attr('max');
		if(typeof max !== "undefined")
		{
			if(size > max)
			return false;
		} 

		  $(input).parent().find('.preview-image').html("<img src='" +  e.target.result + "' style='max-width:100%'> </img>").after("<span class='icon-times delete-image'>x</span>");

		  $(".icon-times").click(function(z) {
			  $(this).parent().find(".preview-image").html("");
			  $(this).parent().find("input").val("");
			  $(this).parent().find(".form-file-text").val("");
			  $(this).remove();
			z.preventDefault();
		});
	  }
	  
	  reader.readAsDataURL(input.files[0]);
	}
  }
  

function selectCheck() {
	$('input[type="checkbox"]').click(function(){	
		var $checked = $(this).is(':checked');
		var $target = $(this).attr('target');
		var $subs = $(this).attr('sub-target');
		if($subs) {
			$target = $(this).attr('value');
			var $checkbox = $('input[data-parent="'+$target+'"]');
		} else {
			var $checkbox = $('input[name="'+$target+'"]');
		}
		$('input[type="checkbox"]').next().removeClass('input-error');
		$('input[type="radio"]').next().removeClass('input-error');		
		if($checked) {			
			$checkbox.prop('checked', 1);					
			$($checkbox).parents('.data tr').addClass('active');					
		}
		else {
			$checkbox.prop('checked', 0);	
			$($checkbox).parents('.data tr').removeClass('active');				
		}
	});
	
	$('[target-radio], label[target], radio-name[target]').click(function(e){			
		var $target = $(this).attr('target-radio');
		var $type = $(this).attr('target-type');
		var $checkbox = $('input[data-name="'+$target+'"]');
		var $checked = $($checkbox).is(':checked');
		if($type == 'multiple')
		var $checkbox = $('input[name="'+$target+'"]');
		$('input[type="checkbox"]').next().removeClass('input-error');
		$('input[type="radio"]').next().removeClass('input-error');
		if($(e.target).is('.switch *, a[href]')) {
		} else {
			$('tr').removeClass('active');	
			if($checked) {
				$checkbox.prop('checked', 0);					
			}
			else {
				$checkbox.prop('checked', 1);	
				if($('tr')) $(this).addClass('active');
			}
		}
	});	
	
}

function loadScrollbar() {
	
}
function loadChoosen() {
	if ($.isFunction($.fn.chosen) ) {
		$("select.deselect").chosen({disable_search_threshold: 10,allow_single_deselect: true});
		$("select").chosen({disable_search_threshold: 10}).change(function(event) {
			var ini = $(this).val();
			$(this).removeClass(function (index, classNames) {
				var current_classes = classNames.split(" "), // change the list into an array
				classes_to_remove = []; // array of classes which are to be removed		
				$.each(current_classes, function (index, class_name) {
				// if the classname begins with bg add it to the classes_to_remove array
					if (/s-.*/.test(class_name)) {
						classes_to_remove.push(class_name);
					}
				});
				// turn the array back into a string
				return classes_to_remove.join(" ");
			}).addClass(function () {
				$(this).removeClass($(this).attr("delete-class"));
				t = $('option:selected', this).attr('class');
				$(this).attr("delete-class",t);
				if(t)
				return t;
			});
			
		}).addClass(function () {
			$(this).next().find('a').find('span').html($(this).attr('placeholder'));
			$(this).removeClass($(this).attr("delete-class"));
			t = $('option:selected', this).attr('class');
			$(this).attr("delete-class",t);
			if(t)
			return t;
		});
	

		$("select").ready(function(){	
			var cl = $(this).val();
			$(this).next('.chosen-container').attr('rel','selected-'+cl);
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
				title = $(".app_title").html();
				$(".navbar-logo").html(title);
				$(".app_title").html("");
				
			} else {
				
			}
		});

		
		$(".chosen-container").before(function () {
			$(this).prev().addClass("chosen-select");
			s = $(this).parent().find("select");
			s.css({ position: "absolute", visibility: "hidden", display: "block", width: "auto" });
			panjang = s.width();

			jumlah = $(this).parent().find("select option").length;
			if(jumlah > 12 ) jumlah = 12;
			posisi = $(this).offset().top;

			butuh = 25 * jumlah;
			tinggiDokumen 	= $(document).height() || 0;
			tinggiMain 		= parseInt($("#mainApps").height()) || 0;
			tinggiKotak 	= parseInt($("#mainApps #app_header").next().height()) || 0;
			tinggi = Math.max(tinggiDokumen, tinggiKotak, tinggiMain, 600);
			p = panjang + 15;
			$(this).attr('style', 'min-width: ' + 	p + 'px !important');
			if(parseInt(tinggi - posisi)-80 >= butuh) {	
			} else {	
				$(this).prev().addClass("drop-top");
			}			
		}).addClass("form-control");

		
		$(".select").change(function () { 
			s = $(this);
			s.css({ position: "absolute", visibility: "hidden", display: "block" });
			panjang = s.width();

			p = panjang + 15;
			$(this).next().attr('style', 'min-width: ' + 	p + 'px !important');
			
		});
	}
}
function loadTable(url,display) {
	if(url) {		
        var tr = true;
        var file = url;
	} else  {
		var tr = false;
        var file = null;
	}
	$('table.data').show();
	var i = 1;
	
	if ($.isFunction($.fn.dataTable)) {
		oTable = $('table.data').dataTable({
			"iDisplayLength": display,
			"bProcessing": tr,
			"bServerSide": tr,
			"sAjaxSource": file,
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				selectCheck();
				$('[data-toggle=tooltip]').tooltip();
				$('[data-tooltip=tooltip]').tooltip();
				$('.tips').tooltip();				
				loadChoosen();
				$("tr").click(function(e){
					var i =$("td:first-child",this).find("input[type='checkbox']");					
					var c = i.is(':checked');
					if($(e.target).is('.switch *, a, button, submit, .btn, .button, a *')) {

					} else if(i.length) {
						if(c) {
							i.prop('checked', 0);		
							$(this).removeClass('active');			
						}
						else {
							i.prop('checked', 1);
							$(this).addClass('active');
							$('.input-error').removeClass('input-error');
							
						}
					}
				});		
				$('input[type="checkbox"],input[type="radio"]').wrap(function() {
					if($(this).closest("label").length == 0)
					return "<label>";
				});
				$('input[type="checkbox"],input[type="radio"]:not(.switch-radio)').after("<span class='input-check'>");
				$('a[href]').on('click', function(e){
				   if ($(this).attr('target') !== '_blank'){
					e.preventDefault();	
					loadUrl(this);
				   }				
				});
				i = 0;
			}
		});
		$('table.data th input[type="checkbox"]').parents('th').unbind('click.DT');
	}
}
function loadSpinner() {
	if ($.isFunction($.fn.spinner)) {
		$('.spinner').spinner();
		$('.spinner min-nol').spinner({ min: 0});
		$('#spinnerfast').spinner({ min: -1000, max: 1000, increment: 'fast' });
		$('#spinnerhide').spinner({ min: 0, max: 100, showOn: 'both' });
		$('#spinnernull').spinner({ min: -100, max: 100, allowNull: true });
		$('#spinnerdisable').spinner({ min: -100, max: 100 });
		$('#spinnermaxlen').spinner();
		$('#spinner5').spinner();	
	}
}
function noticeabs(data, text) {	
	if(!data) return false;
	$(".alert-success, .alert.success").remove();
	$("#alert").html("").show();
	if(text != null) {		
		abs = "";
		if(data == 'success' || data == 'info')
			abs = " alert-fixed";
			
		var a = $("<div class='alert "+data+" alert-"+data+abs+"'>"+text+"</div>");	
		if(data) {
			$(".inner .alert").remove();
		} else {
			a = $('.alert');
		}
		a.hide().appendTo("#alert_top");	
		a.fadeIn('slow');
		var a = a.css({margin:'0 auto'});	
		
		$(".close,.btn-close", a).on('click', function(e) {
			$(this).fadeOut();
		});
	}
	 else {
		var a = $(data);
		if(data) {
			$(".inner .alert").remove();
		} else {
			a = $('.alert').addClass('alert-big');
		}
		a.hide().appendTo("#alert_top");	
		a.fadeIn('slow');
		var a = a.css({margin:'0 auto'});	
		
		$(".close,.btn-close", a).on('click', function(e) {
			$(this).parent().fadeOut();
		});	

	}
}

function notice(data, text) {
	$("#alert").html("").show();
	
	if(text != null) {		
		if(Array.isArray(text)) {
			var str = '<ul>'
			text.forEach(function(tx) {
				str += '<li>'+ tx + '</li>';
			}); 
			str += '</ul>';
			text = str;
		}
		
		abs = "";
		if(data == 'success' || data == 'info')
			abs = " alert-fixed";
		var a = $("<div class='alert "+data+" alert-"+data+abs+"'>"+text+"</div>").hide().appendTo("#alert").fadeIn().css({display:'block'});		
	

	} 
	else if(typeof data === 'object') {		
		abs = "";
		if(data.status == 'success' || data.status == 'info')
			abs = " alert-fixed";

		$("#alert").html("");
		if(data.status == 'error') data.status = "error alert-danger danger";		
		var a = $("<div class='alert "+data.status+" alert-"+data.status+abs+"'>"+data.text+"</div>").hide().appendTo("#alert").fadeIn().css({display:'block'});	
	}
	else {
		var a = $(data).hide().appendTo("#alert").fadeIn().css({display:'block'});
	}
	
	$("#alert script").removeAttr('style');	
	
	$(".alert-fixed").on('click', function(e) {
		$(this).parent().fadeOut();
	});
		
	$(".close,.btn-close", a).on('click', function(e) {
		$(this).parent().fadeOut();
	});
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

(function($) {    
    $.LoadingDot = function(el, options) {        
        var base = this;        
        base.$el = $(el);                
        base.$el.data("LoadingDot", base);        
        base.dotItUp = function($element, maxDots) {
            if ($element.text().length == maxDots) {
                $element.text("");
            } else {
                $element.append(".");
            }
        };
        
        base.stopInterval = function() {    
            clearInterval(base.theInterval);
        };
        
        base.init = function() {
        
            if ( typeof( speed ) === "undefined" || speed === null ) speed = 300;
            if ( typeof( maxDots ) === "undefined" || maxDots === null ) maxDots = 3;            
            base.speed = speed;
            base.maxDots = maxDots;                                    
            base.options = $.extend({},$.LoadingDot.defaultOptions, options);                        
            base.$el.html("<span>" + base.options.word + "<em></em></span>");            
            base.$dots = base.$el.find("em");
            base.$loadingText = base.$el.find("span");
            
            base.$el.css("position", "relative");
            base.$dots.css({"position": "absolute"});
                          
            base.theInterval = setInterval(base.dotItUp, base.options.speed, base.$dots, base.options.maxDots);
            
        };        
        base.init();    
    };
    
    $.LoadingDot.defaultOptions = {
        speed: 300,
        maxDots: 3,
        word: "Loading"
    };
    
    $.fn.LoadingDot = function(options) {        
        if (typeof(options) == "string") {
            var safeGuard = $(this).data('LoadingDot');
			if (safeGuard) {
				safeGuard.stopInterval();
			}
        } else { 
            return this.each(function(){
                (new $.LoadingDot(this, options));
            });
        }         
    };
    
})(jQuery);
