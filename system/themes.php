<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

if(!defined('MetaDesc'))
define('MetaDesc', 	siteConfig('site_desc'));
if(!defined('MetaKeys'))
define('MetaKeys', 	siteConfig('site_keys'));
if(!defined('TitleValue'))
define('TitleValue', app_param('name'));
if(!defined('MetaAuthor'))
define('MetaAuthor',siteConfig('site_name'));
if(!defined('MetaRobots')) {
	if(app_param('app') == null)
		define('MetaRobots', 'noindex');
	else if(siteConfig('follow_link'))
		define('MetaRobots', 'index, follow');
	else
		define('MetaRobots', 'index, nofollow');
}


if(!isset($GLOBALS['print'])) 
	$GLOBALS['print']= false;

if(isset($GLOBALS['REQUEST_HEADER']['API']) AND $GLOBALS['REQUEST_HEADER']['API'] == true) 
	define('_API_',	true);
else
	define('_API_',	app_param('api'));

	
if(isset($GLOBALS['REQUEST_HEADER']['LoadHTMLData']) AND $GLOBALS['REQUEST_HEADER']['LoadHTMLData'] == true) 
	define('_HTML_DATA_',	true);
else
	define('_HTML_DATA_',	app_param('blank'));

define('_APP_',	 app_param('app'));
define('_FEED_', app_param('feed'));
define('_VIEW_', app_param('view'));
define('_PRINT_',app_param('print'));

/********************************************/
/*         Define Type & load Apps	       */
/********************************************/
$themes = FLayout("theme");
header('FTheme: ' . $themes);
header('FUrl: ' . FUrl);
header('FLayout: ' . FLayout("name"));
if(empty($themes)) $themes = siteConfig('site_theme');

define("FThemeFolder", $themes); 
define("FThemePath",FUrl."themes/".FThemeFolder."");
$loadApps = loadApps(true);

/********************************************/
/*  		Define Type & Site Title	  	*/
/********************************************/
if(isset($_GET['theme']) AND $_GET['theme'] =='module')
	define('PageTitle','Module_Position'); 
else if(!defined('PageTitle') AND http_response_code() == '404')
	define('PageTitle','404 - ' . SiteTitle);
else if(!defined('PageTitle') AND http_response_code() == '200') 
	define('PageTitle',SiteTitle);
	
if(TitleType==1)
	define('FTitle',PageTitle.TitleDiv.SiteTitle);
else if(TitleType==2) 
	define('FTitle',SiteTitle.TitleDiv.PageTitle);
else if(TitleType==3) 
	define('FTitle',PageTitle);
else if(TitleType==0) 
	define('FTitle',SiteTitle);	
	


/********************************************/
/*  		  setup FThemes File		  	*/
/********************************************/
if(_APP_ =='user' and _VIEW_ == 'login') {
	define("FThemes","themes/".FThemeFolder."/login.php");
} else {
	if(http_response_code() == '404') {
		define("FThemes","themes/".FThemeFolder."/404.php");
	}
	else
		define("FThemes","themes/".FThemeFolder."/index.php");
}


/********************************************/
/*  		  Load default theme		  	*/
/********************************************/
if(_FINDEX_ == 'blank' OR _PRINT_ OR _API_ OR _FEED_ == 'rss')  {	
	echo $loadApps;
}
else if(!file_exists(FThemes)) {	
	$file = str_replace(".php",".html",FThemes);
	if(file_exists($file))
	if(@rename($file,FThemes)) refresh();
	echo alert("error",FThemes."Theme is not found!",true,true);
	die();
}
else {
	include(FThemes); 
}

$output = ob_get_contents();
ob_end_clean();
if(_FINDEX_ !== 'blank' AND !_PRINT_ AND !_API_) {
	
	ob_start(); 
		loadAppsJs();	
		loadPluginJs();
		$jsasset = ob_get_contents();
		$jsasset = preg_replace("/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\)\/\/.*))/", "", $jsasset);
	ob_end_clean();

	ob_start(); 
		loadAppsCss();
		if(function_exists('loadModuleCss')) loadModuleCss();
		$cssasset = ob_get_contents();
	ob_end_clean();
	
	
	$output = str_replace(array("{loadApps}"),$loadApps,$output);
	$output = str_replace("?&", "?{app=}{view=}{type=}{category=}&",$output);	
	$output = str_replace('&"', '"', $output);	

	
	$tlx = strpos($output,"<link");
	$ntx = substr($output , 0, $tlx );
	$output = str_replace($ntx, $ntx.$cssasset,$output);
		
	preg_match_all('/\{chkmod:(.*?)\}/',$output,$position); 
	if(!empty($position[1])) {
		foreach($position[1] as $val) {
			$valc = $val;
			$coma = strpos($val,",");
			if($coma) {
				$c = explode(",",$val);
				if(is_array($c)) $valc = $c;	
			}
			$posm = strpos($output,"{chkmod:$val}");
			$posn = strpos($output,"{/chkmod}")+9;
			if(checkModule($valc)){
				$strpos = substr($output,0,$posn);
				$strpos = str_replace(array("{chkmod:$val}"),"",$strpos);
				$strpos = str_replace(array("{/chkmod}"),"",$strpos);	
				$else = strpos($strpos,"{else:}");
				if($else) {
					$strpos = substr($strpos,0,$else);
					$output = $strpos.substr($output, $posn);
				} else {
					$output = $strpos.substr($output,$posn);
				}	
			
			} else {		
				$strpos = substr($output,0,$posn);
				$else = strpos($strpos,"{else:}");
				if($else) {
					$strpos = substr($strpos,0,$posm);
					$posn2 = strpos(substr($output, $else+7),"{/chkmod}");
					$outpuk = $strpos.substr($output, $else+7,$posn2);
					$output = $outpuk.substr($output,$posn);
				} else {
					$strpos = substr($output,0,$posm);
					$output = $strpos.substr($output,$posn);
				}							
				
			}
		}
	}
	
	preg_match_all('/\{module:(.*?)\}/',$output,$position); 
	if(!empty($position[1])) {
		$no = 1;
		foreach($position[1] as $val) {
			$output = str_replace(array("{module:$val}"),loadModule($val,true),$output);
			$no++;
		}
	}	
	
	preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $output, $result);
	if(!empty($result[1])) {
		$no = 1;
		foreach($result['href'] as $val) {
			if(strpos($val, "?") !== false) {	
				$output = str_replace($val, url($val),$output);						
			}	
			$no++;
		}
	}	

	ob_start();
	if(!_AUTO_HTML_CONSTRUCT_) {
		
		$output = str_replace(array("\"./" ), "\"/",$output);
		$output = str_replace(array("href=\"/vendor", "href=\"vendor"), "href=\"".FThemePath."/vendor",$output);
		$output = str_replace(array("src=\"/vendor", "src=\"vendor"), "src=\"".FThemePath."/vendor",$output);
		$output = str_replace(array("href=\"/dist", "href=\"dist"), "href=\"".FThemePath."/dist",$output);
		$output = str_replace(array("src=\"/dist", "src=\"dist"), "src=\"".FThemePath."/dist",$output);
		$output = str_replace(array("href=\"css","href=\"/css"), "href=\"".FThemePath."/css",$output);
		$output = str_replace(array("href=\"/asset", "href=\"asset"), "href=\"".FThemePath."/asset",$output);
		$output = str_replace(array("src=\"/asset", "src=\"asset"), "src=\"".FThemePath."/asset",$output);
		$output = str_replace(array("href=\"/assets", "href=\"assets"), "href=\"".FThemePath."/asset",$output);
		$output = str_replace(array("src=\"/assets", "src=\"assets"), "src=\"".FThemePath."/asset",$output);
		$output = str_replace(array("src=\"/js","src=\"js"), "src=\"".FThemePath."/js",$output);
		$output = str_replace(array("src=\"/image", "src=\"image"), "src=\"".FThemePath."/image",$output);
		$output = str_replace(array("href=\"/image", "href=\"image"), "href=\"".FThemePath."/image",$output);
		$output = str_replace(array("src=\"/img","src=\"img"), "src=\"".FThemePath."/img",$output);		
		$output = str_replace(array("src=\"/media","src=\"media"), "src=\"".FUrl."/media",$output);		
		$output = str_replace(array("href=\"/media","href=\"media"), "href=\"".FUrl."/media",$output);
		$output = str_replace(array("url('/assets/","url('./assets"), "url('".FThemePath."/assets//",$output);
		$output = str_replace(array("url('./"), "url('".FThemePath."/",$output);
		
		$output = str_replace(array("=\"/plugins","=\"plugins"), "=\"".FUrl."plugins",$output);		
		$output = str_replace(array("{siteHome}","{siteUrl}","{homeurl}"),FUrl,$output);
		$output = str_replace(array("{sitename}","{siteName}"),SiteName,$output);
		$output = str_replace(array("{siteKeys}","{sitekeys}"),SiteKeys,$output);
		$output = str_replace(array("{siteDesc}","{sitedesc}"),SiteDesc,$output);
		$output = str_replace(array("{pagetitle}","{pageTitle}"),PageTitle,$output);
		$output = str_replace(array("{sitetitle}","{siteTitle}","{title}","{titlebar}"),FTitle,$output);
		$output = str_replace(array("{metadesc}","{metaDescription}"),MetaDesc,$output);
		$output = str_replace(array("{metakeys}","{metaKeywords}"),MetaKeys,$output);
		$output = str_replace(array("{metaauthor}","{metaAuthor}"),MetaAuthor,$output);
		$output = str_replace(array("{metarobots}","{metaRobots}"),MetaRobots,$output);
		$output = str_replace(array("{layout}"),FLayout('name'),$output);
		$output = str_replace(array("{layoutDesc}","{layoutdesc}"),FLayout('desc'),$output);
		$output = str_replace(array("{notice}","{alert}","<alert/>","<alert></alert>","<notice/>","<notice></notice>"),notice(),$output);
		$output = str_replace(array("{metarobots}","{metaRobots}"),MetaRobots,$output);
		$output = str_replace("{lang}",SiteLang,$output);
		$output = str_replace("{lang}",SiteLang,$output);
		if(checkMobile()) $m = "m-"; else $m = "";
		$output = str_replace("{m-}",$m,$output);
		if(checkMobile()) $m = ".mobile"; else $m = "";
		$output = str_replace("{mobile}",$m,$output);
		$output = str_replace(array("{pid}","{PID}"),Page_ID,$output);		
		if(isHomePage()) $h = "home"; else $h = "default";
		$output = str_replace("{home}",$h,$output);
		if(USER_ID) {
			$output = str_replace("{userid}",USER_ID,$output);
			$output = str_replace("{username}",USER_NAME,$output);
			$output = str_replace("{userlevel}",USER_LEVEL,$output);		
		} else {		
			$output = str_replace("{userid}",'',$output);
			$output = str_replace("{username}",'',$output);
			$output = str_replace("{userlevel}",'',$output);	
		}
	}
	

	$output = preg_replace('#^\s*//.+$#m', "", $output);
	$output = preg_replace('/<!--(.*)-->/Uis', "", $output);
	$output = preg_replace(array('(( )+\))','(\)( )+)'), ')', $output);
	$output = str_replace(array("\t","\n"), ' ', $output);
	$output = str_replace(array("  ","   "), ' ', $output);
	$output = str_replace("  ", ' ', $output);


	$html = str_get_html($output);
	$jsMain = "";
	foreach($html->find('script') as $e) {
		$jsMain .= $e->outertext ;
		$e->src= null;
	}

	//$jsMain = minimizeJavascriptSimple($jsMain);
	
	$body = $html->getElementsByTagName('body');
	$bpos = strpos($body, ">");
	$apos = substr($body, 0, $bpos +1);
	$cpos = substr($body, $bpos +1);
	$body = str_replace($cpos, "<main>" . $cpos , $body);
	$html->getElementsByTagName('body')->outertext = $body;	
	
	
	foreach($html ->find('script') as $item) {
		$item->outertext = '';		
	}
	$output = $html->save();


	$output = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $output);

	$tlb = strpos($output,"</body>");
	$ntb = substr($output ,$tlb );
	$output = str_replace($ntb, "</main>" .$jsMain .$jsasset.$ntb,$output);	
	
	// Create a DOM object from a URL
	$html = str_get_html($output);
	
	ob_end_clean();
}

/* timer */
$et = microtime(TRUE) - _START_TIME_;
$et = substr($et,0,6)."s";

$output = str_replace(array("{loadtime}","{loadTime}"),$et,$output);
/* timer */

if(_HTML_DATA_) {	
	$html->getElementsByTagName('body');	
	$jsasset = "";
	
	foreach($html->find('script') as $item) {
		if(isset($item->{'data-static'}))
			$item->outertext = '';
		$jsasset .= $item->outertext;
	}
		
	$output = $html->getElementsByTagName('main')->innertext;

	if(Cookie::get('FTheme') != FThemeFolder) {
		$output = $html->getElementsByTagName('html')->innertext;
	}
				
	header('Content-Type: application/json');
	echo json_encode(["status" => "success", "url" => $_SERVER['REQUEST_URI'],"html" => $output, "js" => $jsasset]);
	ob_end_flush();
} else {
	setcookie('FTheme', $themes);
	if(Input::server('https') == 'on') $output = str_replace("http://", "https://", $output);
	echo $output;
	ob_end_flush();
}