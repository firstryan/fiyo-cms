<?php 
/**
* version   2.5
* package   Fiyo CMS
* copyright Copyright (C) 2012 Fiyo CMS.
* license   GNU/GPL, see LICENSE.txt
**/

/*
* Define database variables
*/ 
define('FDBUser', $DBUser);
define('FDBPass', $DBPass);
define('FDBHost', $DBHost);
define('FDBName', $DBName);
define('FDBPort', $DBPort);
define('FDBPrefix', $DBPrefix);

class Database extends Controller {   

    private static $db_host = FDBHost;	// Database Host
    private static $db_user = FDBUser;	// Username
    private static $db_pass = FDBPass;	// Password
    private static $db_name = FDBName; 	
    private static $db_port = FDBPort; 	
	
	public static $table;	
    public static $rows = '*';
    public static $join = '';
    public static $where;
    public static $orderBy;
    public static $groupBy;
    public static $limit;
    public static $_instance = null;
    public static $self_query = false;
    public static $as_object  = false;     

    private static $newconn = null;
	
    public static $query;    // variabel Last Query
    public static $error;    // variabel Last Error
    public static $last_id;    // variabel Last Query
    public static $db  = false;     
    public static $count = 0;     
    public $result = null;   // Cek untuk melihat apakah sambungan aktif

    public function __construct () { 
		$this->middleware;	
	}
	
	public static function connect($dbname = null, $host = null, $user = null, $pass = null)
	{        
        static $conn = false;
        
        if(isset($dbname)) {
            self::$db_host = $host;	// Database Host
            self::$db_user = $user;	// Username
            self::$db_pass = $pass;	// Password
            self::$db_name = $dbname; 	
        }   else {
            self::$db_host = FDBHost;	// Database Host
            self::$db_user = FDBUser;	// Username
            self::$db_pass = FDBPass;	// Password
            self::$db_name = FDBName; 
        }
        if(!$conn or !empty($host)){ 
            try{
                $options = array(
                PDO::ATTR_PERSISTENT    => false,
                PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION);
                self::$db = $conn = new PDO("mysql:host=".self::$db_host.
                ";port=".self::$db_port.
                ";dbname=".self::$db_name.
                ";charset=utf8",self::$db_user, 
                self::$db_pass, $options);
            }
            catch(PDOException $e){				
                alert('error',self::$db_host.' Unable to connect database!',true,true);
                die();
                }
        } else self::$db = $conn;	
    }

    
	public static function reConnect()
	{
          return self::connect(FDBName, FDBHost, FDBUser, FDBPass);     
    }

    public static function table($value)
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        self::$self_query = false;
        self::$where = null;
        self::$orderBy = null;
        self::$limit = null;
        self::$join = null;
        self::$rows = '*';
		
        self::$table = $value;
        return self::$_instance;
    }

    public function select($rows) {
        self::$rows = $rows;
        return $this;
    }
	
    public function where($where = '*') {
        self::$where = $where;
        return $this;
    }
	
    public function wheres($where = '*') {
        self::$where = $where;
        return $this;
    }
	
    public function rows(array $rows) {
        self::$rows = $rows;
        return $this;
    }

    public function orderBy($orderBy = null) {
        self::$orderBy = $orderBy;
        return $this;
    }

    public function groupBy($groupBy = null) {
        self::$groupBy = $groupBy;
        return $this;
    }

    public function limit($limit = null) {
        self::$limit = $limit;
        return $this;
    }


    public function leftJoin($joinTable = null, $join = null) {
        self::$join .= " LEFT JOIN $joinTable ON $join";
        return $this;	
    }
		
	public static function query($query, $fetch = false, $error = true){
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        static $cons = false;		        
		try{
            $result = self::connect();   
            $result = self::$db->prepare($query);                
            self::$query = $query;           
            self::$self_query = true;
            $result ->execute();
			if($fetch){
                if(self::$as_object)
                    return $result->fetchAll(PDO::FETCH_OBJ);
                else
                    return $result->fetchAll(PDO::FETCH_ASSOC);
            }
			else {                
                return $result;
            }
		}
		catch(PDOException $e){
		    self::$error = $e;
			if(!$cons) {
                if(_SHOW_ERROR_)
                    echo($e);		
                $cons = true;
				return false;	
			}
		}
    }
    
    
    public static function row()
    {
        if(!self::$self_query) {
            if(_SHOW_ERROR_)
            alert("error", "Can't call <b>DB::table</b> with <b>row()</b> function.", true);
            return false;
        }

        try{            
            $query = self::$query;        
            self::connect();   
            $result = self::$db->prepare($query);   
            $result->execute();
            if(self::$as_object)
                return $result->fetchAll(PDO::FETCH_OBJ);
            else
                return $result->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e){
		    self::$error = $e;
            if(_SHOW_ERROR_)
                echo($e);		
		}
        
    }
	
    /*
    * Cek apakah tabel setting ada
	* Sebelum melakukan query lanjutan
    */
    public static function tableExists($table)
    {
        $sql = 'SHOW TABLES FROM '.self::$db_name.' LIKE "'.$table.'"';
        $result = self::query($sql, true);
        self::$query = $sql;
        
        if($result)
        {
            if(count($result))
            {
                return true;
            }
        }
        return false;
        
    }

    public function get($obj = false) {	
		$sql = "SELECT ". self::$rows . " FROM `". self::$table . "`";
		
        if(self::$join != null)
            $sql .= self::$join;	
		if(self::$where != null)
            $sql .= ' WHERE '.self::$where;
        if(self::$groupBy != null)
            $sql .= ' GROUP BY '.self::$groupBy;	
        if(self::$orderBy != null)
            $sql .= ' ORDER BY '.self::$orderBy;	
        if(self::$limit != null)
            $sql .= ' LIMIT '.self::$limit;	
            
		self::$query = $sql;
		static $cons = false;
		try{
            $result = self::connect();           
            $result = self::$db->prepare($sql);            
            $result -> execute();
            
            self::$join     = null;
            self::$where    = null;
            self::$groupBy  = null;	
            self::$orderBy  = null;	
            self::$limit    = null;
            
            self::$count =  $result->rowCount();	
            if(self::$as_object)
                return $result->fetchAll(PDO::FETCH_OBJ);
            else
                return $result->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e){
            self::errorHandler($e);
		}		
    }


    public function first() {	
		$sql = "SELECT ". self::$rows . " FROM `". self::$table . "`";
		
        if(self::$join != null)
            $sql .= self::$join;	
		if(self::$where != null)
            $sql .= ' WHERE '.self::$where;
        if(self::$groupBy != null)
            $sql .= ' GROUP BY '.self::$groupBy;	
        if(self::$orderBy != null)
            $sql .= ' ORDER BY '.self::$orderBy;	
        if(self::$limit != null)
            $sql .= ' LIMIT 1';	
            
		self::$query = $sql;
		static $cons = false;
		try{
            $result = self::connect();           
            $result = self::$db->prepare($sql);            
            $result -> execute();
            
            self::$join     = null;
            self::$where    = null;
            self::$groupBy  = null;	
            self::$orderBy  = null;	
            self::$limit    = null;
            
            self::$count =  1;	
			return $result->fetchAll(PDO::FETCH_ASSOC)[0];
		}
		catch(PDOException $e){
		    errorHandler($e);
		}		
    }

    public static function transaction() {        
        self::$db->beginTransaction();
    }

    public static function commit() {        
        self::$db->commit();
    }
    
    public static function rollback() {        
        self::$db->rollback();
    }

    
    public function count() {	
		$sql = "SELECT ". self::$rows . " FROM `". self::$table . "`";
		
        if(self::$join != null)
            $sql .= self::$join;	
		if(self::$where != null)
            $sql .= ' WHERE '.self::$where;
        if(self::$groupBy != null)
            $sql .= ' GROUP BY '.self::$groupBy;	
        if(self::$orderBy != null)
            $sql .= ' ORDER BY '.self::$orderBy;	
        if(self::$limit != null)
            $sql .= ' LIMIT '.self::$limit;	
            
		self::$query = $sql;
		static $cons = false;
		try{
            $result = self::connect();           
            $result = self::$db->prepare($sql);            
            $result -> execute();
            
			return $result->rowCount();
		}
		catch(PDOException $e){
		    self::errorHandler($e);
		}		
    }
	
	public function insert($values, $rows = null)
    {
        $insert = 'INSERT INTO '. self::$table ;
        $kuot = $arkey = false;
		
        if(isset($values[0])) {
            for($i = 0; $i < count($values); $i++)
            {
                if(is_string($values[$i]))
                    $values[$i] = "'".$values[$i]."'";
            }
        } else {
            $rows = array_keys($values);
            foreach ($rows as $k => $r)
                $rows[$k] = '`'.$r.'`';
            $rows = implode(',',$rows);
            $insert .= ' ('.$rows. ')';
			$arkey = true;
        }
		

        if(is_array($values)) {
            foreach ($values as $k => $r) {
                if(!empty($r)) {
                    if($arkey)
                        $kuot = true;
                    else if(strpos('"',"$r") === 0 or strpos("'","$r") === 0 )
                        $kuot = true;
                    
                }
                if(empty($r)) {
                    $kuot = true;     
                }			
                
                if($kuot) 
                $values[$k] = "'". addslashes($r)."'";
            }
             $values = implode(',',$values);
        }
        
        $insert .= ' VALUES ('.$values.')';
		
		self::$query = $insert;	
		static $cons = false;
		try{
			$result = self::connect();        
			$result = self::$db->prepare($insert);
			$query = $result ->execute();
            self::$last_id = self::$db->lastInsertId();
        }
		catch(PDOException $e){
		    self::$error = str_replace("for key","for data",$e->errorInfo[2]);
			if(!$cons) {	
				Controller::setError();		
				Controller::pushError($e);
			    $cons = true;
				return false;
			}
		}       
		
		if(isset($query)) {
            return true;
        }
    }
	
	
    public function update(array $rows = [])
    {        
        $update = 'UPDATE '.self::$table.' SET ';
        $keys = array_keys($rows);
		
        for($i = 0; $i < count($rows); $i++){
            if(is_string($rows[$keys[$i]]) AND $rows[$keys[$i]] !== '+hits')
            {
                $update .= '`'.$keys[$i].'`="'.$rows[$keys[$i]].'"';
            }
            else
            {
				if($rows[$keys[$i]] == '+hits') $rows[$keys[$i]] = $keys[$i] . '+'. 1;
                 $update .= '`'.$keys[$i].'`='.$rows[$keys[$i]];
            }

            // Parse to add commas
            if($i != count($rows)-1)
            {
                $update .= ',';
            }
        }
		
		if(self::$where != null) {
            $update .= ' WHERE '.self::$where;
		}
		
		self::$query = $update;	
		static $cons = false;
		try{
			$result = self::connect();      
			$result = self::$db->prepare($update);
			$query = $result ->execute();
        }
		catch(PDOException $e){
		    self::$error = $e;
			if(!$cons) {	
				Core::setError();		
                Core::pushError($e);	
                
			    $cons = true;
				return false;
			}
		}       
		
		if(isset($query)) {
            return true;
        }
		
		
	}
	
	public static function delete($where = null)
    {
        $table = self::$table;
		
        if($where == null)
            {
            $delete = 'DELETE FROM '.$table;
        }
        else
        {
            $delete = 'DELETE FROM '.$table.' WHERE '.$where;
        }
			
		self::$query =  $delete ;	
		static $cons = false;
		try{
			$result = self::connect();        
			$result = self::$db->prepare($delete);
			$query = $result->execute();
            self::$count =  $result->rowCount();	
        }
		catch(PDOException $e){
		    self::$error = $e;
			if(!$cons) {		
				Core::setError();		
				Core::pushError($e);	
			    $cons = true;
				return false;
			}
		}
        
		if(isset($query)) {
            return true;
        }
    }
    

    public static $message = null;
	protected static $status = false;
	
	public static function save($preset = ['no_log' => false]) {	
		if(isset($preset['table']))
			$table = $preset['table'];
		else {
			self::$message = Status_Fail;	
			return false;
		}

		$fillable = $preset['fillable'];
		$data = $preset['data'];

		$fillme =  $data;
		$data = array_query($data, $fillable);
		
		$data =  array_intersect_key($data, $fillme);
		
		
		//validationmode
		$valid = true;
		if(isset($preset['validator'])) {
			$valid = false;
			$valid = Validator::is_valid($data, $preset['validator']);
		}
		

		//query		
		if($valid === true) {			
			if(isset($preset['type'])) {

				//Set flag query as false
				$qr = false;
				if(in_array($preset['type'], ['insert','add','create'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["created_at" => date("Y-m-d H:i:s")]);

					$qr = Database::table($table)->insert($data);
				return true;
				} else if(in_array($preset['type'], ['edit','update'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["updated_at" => date("Y-m-d H:i:s")]);

					$qr = Database::table($table)->where("$preset[index] = $preset[reff]")->update($data);
				}

				//if $qr is true
				if($qr) {
					self::$message = Status_Saved;	
					return true;		
				}
			}
		}	
		else {		
			self::$message = $valid; 		
			return false;
		}

		self::$message = Status_Fail;	
		return false;
    }
    
    
    
    private static function errorHandler($e) {        
        $msg = $e->getMessage();
        $pos = strpos($e->getMessage(), ":");
        $err = substr($msg, $pos + 1);  

        Controller::setError();		
        Controller::pushError($e);

        self::$error = $err;
        if(_SHOW_ERROR_ AND !app_param('api')) 
            alert('error',trim($err),true,true);

        return false;        
    }

}

class_alias('Database', 'DB');
class_alias('Database', 'FQuery');

