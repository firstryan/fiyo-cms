<?php 
/**
* version   2.5
* package   Fiyo CMS
* copyright Copyright (C) 2012 Fiyo CMS.
* license   GNU/GPL, see LICENSE.txt
**/

/*
* Define database variables
*/ 


class Model extends DB {   
    public static $table;	
    public static $index;	
    public static $field;	
    public static $fillable;	
    public static $validator;	
    public static $data;	
    public static $reff;	

    public static $rows = '*';
    public static $join = '';
    public static $where;
    public static $orderBy;
    public static $groupBy;
    public static $limit;
    public static $_instance = null;
    public static $self_query = false;
    public static $query = null;
    public static $preset = [];
    public static $as_object = false;
    public static $redirect = null;

    

    public static $message = null;
	protected static $status = false;

    public function __construct() {
        self::$table  = parent::$table;
    }
    
    private static function getPreset() {
        self::$preset = array_merge(
            ['validator'    =>  self::$validator],
            ['redirect'    =>  self::$redirect],
            ['fillable' =>  self::$fillable], 
            ['field'    =>  self::$field],
            ['index'    =>  self::$index],
            ['reff'    =>  self::$reff],
            ['data'    =>  self::$data],
        );
    }

    private static function getData($preset) {       
        parent::$as_object = self::$as_object;     
        if(isset($preset['data']) AND count($preset['data']))
            self::$data = $preset['data'];
        else
            self::$data = $preset;     
    }

    
    public static function setTable($table) {  
            self::$table = $table;     
    }
    
    public function single() {
        self::$query  = parent::$query;   
        return $this->first();
    }

    public function first() {         
        parent::$as_object = self::$as_object;
        $qr = DB::table(self::$table)
                ->select($select)
                ->where($key);			
        self::$query  = parent::$query;   
    }

    
    public function item($value, $param = null) {   
        if(!$param) $param = self::$index;             
        parent::$as_object = self::$as_object; 
        $qr = DB::table(self::$table)
                ->where("$param = '$value'");                
        $count = $qr->count();
        self::$query  = parent::$query;   
        
        if($count > 1)
            return $qr->get();
        else if($count == 1)
            return $qr->get()[0];
        else    {
            http_response_code(404);
            return false;	

        }		
    }

    
    public static function find($key, $select = '*') {        
        parent::$as_object = self::$as_object;    
        $qr = DB::table(self::$table)
                ->select($select)
                ->where("$key");

        $count = $qr->count();
        
        if($count > 1)
            return $qr->get();
        else if($count == 1)
            return $qr->get()[0];
        else    
            return false;
        
    }

	
    public static function list($select = '*') {      
        $qr = DB::table(self::$table)
                ->select($select)
                ->limit(self::$limit)
                ->orderBy(self::$orderBy)
                ->where(self::$where)
                ->get();
                
        self::$query  = parent::$query;
        return $qr;
    }

    
    public static function itemObject($value, $param = 'id') {    
        self::$query  = parent::$query;       
        return arrayToObject(self::item($value, $param = 'id'));
    }

    public static function findObject($key, $select = '*') { 
        self::$query  = parent::$query;          
        return arrayToObject(self::find($key, $select = '*'));
    }

    
    public static function listObject($select = '*') {  
        self::$query  = parent::$query;         
        return arrayToObject(self::list($select));
    }


    public static function listJson($select = '*') { 
        parent::$as_object = self::$as_object;               
        $qr = DB::table(self::$table)
                ->select($select)
                ->get();

                self::$query  = parent::$query;   
        return json_encode($qr);
    }
   
    public static function create() {
        echo self::$index;
        return true;
    }

	public static function add($preset = ['no_log' => false]) {
        self::$redirect = null;
        if(!Input::$post) return false;        
        if(!isset($preset['data'])) $data = Input::$post;  

        if(isset($preset['redirect'])) 
        self::$redirect = $preset['redirect'];

        self::getData($data);
        self::getPreset();
        self::$query  = parent::$query;
        
        return self::save(self::$preset, 'add' );
    }
    
	public static function patch($preset = ['no_log' => false], $reff = null) {
        if($reff == null) 
            $reff = app_param('id'); 
        else    
            $reff = app_param($reff);
            
        if(!Input::$post OR empty($reff)) return false;   
        
        if(!isset($preset['data'])) $preset = Input::$post;
        self::getData($preset);
        self::$reff = $reff;
        self::getPreset();
        self::$query  = parent::$query;   
        return self::save(self::$preset, 'patch');
    }
    
	public static function save($preset = ['no_log' => false], $type = 'add') {
        parent::$last_id = 0;
        $preset['type'] = $type;
        self::$query  = parent::$query;   
		if(isset(self::$table))
			$table = self::$table;
		else {
			self::$message = self::error_massage();		
			return false;
        }
        

		$fillable = $preset['fillable'];
		$data = $preset['data'];

		$fillme =  $data;
		$data = array_query($data, $fillable);
		
		$data =  array_intersect_key($data, $fillme);
        
		
		//validationmode
		$valid = true;
		if(isset($preset['validator'])) {
			$valid = Validator::is_valid($data, $preset['validator']);
		}
		
		//query		
		if($valid === true) {
			if(isset($preset['type'])) {
				//Set flag query as false
                $qr = false;

                if(_API_ AND isset($preset['data'][$preset['reff']]))
                    $preset['reff'] = $preset['data'][$preset['reff']];
                    
				if(in_array($preset['type'], ['insert','add','create'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["created_at" => date("Y-m-d H:i:s")]);
					$qr = Database::table($table)->insert($data);
				} else if(in_array($preset['type'], ['edit','update','patch'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["updated_at" => date("Y-m-d H:i:s")]);
					$qr = Database::table($table)->where("$preset[index] = '$preset[reff]'")->update($data);
				}

				//if $qr is true
				if($qr) {
                    Session::set('NOTICE_CREATED', true);
                    if(isset($preset['redirect']))
                        self::$message = self::success_massage(['last_id' => DB::$last_id, 'redirect' => $preset['redirect']]);
                    else
                        self::$message = self::success_massage(['last_id' => DB::$last_id]);	
					return true;		
				} else {
                    self::$message = self::error_massage();	
                    return false;
                }
			}
		}	
		else {		
            self::$query  = parent::$query;   	
            self::$message = self::error_massage($valid[0]);	
			return false;
		}

        self::$query  = parent::$query;   
		self::$message = self::error_massage();	
		return false;
    }

    
	public static function delete($reff = 'id', $index = null) {  
        if($reff == null) 
            $id = app_param('id'); 
        else    
            $id = app_param($reff); 
            self::$message = self::error_massage();	

        if(Req::post('delete') OR app_param('delete') OR app_param('action') == 'delete' AND !empty($reff)) { 
            if(!$index) $index = self::$index;  
            self::$message = self::deleted_massage(); 
            return  DB::table(self::$table)->delete("$index = '$id'");
        } else {
            self::$message = self::error_massage();	
            return false;
        }
    }


    private static function success_massage($param = null) {  
        if(isset($param['last_id']) AND  DB::$last_id) {
                $url ="?app=". app_param('app');
            if(app_param('view')) 
                $url .="&view=". app_param('view');
            if(app_param('type')) 
                $url .="&type=". app_param('type');
            
            if(self::$redirect)
                $redirect = self::$redirect;
            else
                $redirect = $url . "&action=edit&id=" . DB::$last_id;
            
            return [
                'status' => 'success',
                'text' => Status_Saved,
                'id' => DB::$last_id,
                'redirect' => url($redirect)
            ];
        }  
        else         
            return [
                'status' => 'success',
                'text' => Status_Saved,
            ];        
    }

    private static function error_massage($valid = Status_Fail) {
        return [
            'status' => 'error',
            'text' => $valid,
        ];        
    }
    private static function deleted_massage() {
        $GLOBALS['REQUEST_HEADER']['API'] = true;
        $redirect = self::$redirect;

        if($redirect) {
            return [
                'status' => 'info',
                'text' => Status_Deleted,
                'redirect' => url($redirect)
            ]; 

        }
        else  {
            return [
                'status' => 'info',
                'text' => Status_Deleted,
                'redirect' => 'refresh'
            ]; 

        }     
    }
    
}