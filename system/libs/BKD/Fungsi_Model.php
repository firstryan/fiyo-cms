<?php
	class Fungsi_Model extends CI_Model {
		
		public $dblocal = NULL;
		public $dbeps = NULL;
		public $dbpresensi = NULL;

		public function __construct() {
			parent::__construct();

			$this->dblocal = $this->load->database('default', TRUE);
			$this->dbeps = $this->load->database('eps', TRUE);
			$this->dbpresensi= $this->load->database('presensi', TRUE);
		}

		function format_tanggal_indo($tanggal) {
			return substr($tanggal,8,2)."-".substr($tanggal,5,2)."-".substr($tanggal,0,4);
		}

		function agama1($kode) {
			switch(intval($kode)) {
				  case 1: $agama="ISLAM"; break;
				  case 2: $agama="KRISTEN"; break;
				  case 3: $agama="KATHOLIK"; break;
				  case 4: $agama="HINDU"; break;
				  case 5: $agama="BUDHA"; break;
				  default: $agama="-";
			}
			return $agama;
		}

		function jenisKelamin($kode) {
			$jenisKelamin="";
			switch(intval($kode)) {
				case 1: $jenisKelamin="LAKI-LAKI";break;
				case 2: $jenisKelamin="PEREMPUAN";break;
			}
			return $jenisKelamin;
		}

		function namaPNS($B_03A,$B_03,$B_03B) {
			if ($B_03A!='') $dpn=$B_03A.". ";else $dpn="";
				if ($B_03B!='') $nama= $dpn.stripslashes($B_03).", ".$B_03B; else $nama= $dpn.stripslashes($B_03);
			return $nama;
		}

		function getNaJab($nip) {
			$q="select JAB from V_JABATAN where B_02='$nip'";
			$result = $this->dbeps->query($q)->row();
			return $result->JAB;
		}

		function getJabatan($nip){
			$result = $this->dbeps->query("SELECT I_JB FROM MASTFIP08 WHERE B_02B = '$nip'")->row();
			return $result->I_JB;
		}

		

		function namaPkt($pangkat){
			$q="select NAMAY from TABPKT where KODE ='$pangkat' LIMIT 1";
			$result = $this->dbeps->query($q)->row();
			return $result->NAMAY;
		}

		function pktH($pkt){
			switch($pkt){
				case "11" : $hasil='I/a';break;
				case "12" : $hasil='I/b';break;
				case "13" : $hasil='I/c';break;
				case "14" : $hasil='I/d';break;
				case "21" : $hasil='II/a';break;
				case "22" : $hasil='II/b';break;
				case "23" : $hasil='II/c';break;
				case "24" : $hasil='II/d';break;
				case "31" : $hasil='III/a';break;
				case "32" : $hasil='III/b';break;
				case "33" : $hasil='III/c';break;
				case "34" : $hasil='III/d';break;
				case "41" : $hasil='IV/a';break;
				case "42" : $hasil='IV/b';break;
				case "43" : $hasil='IV/c';break;
				case "44" : $hasil='IV/d';break;
				case "45" : $hasil='IV/e';break;
				default : $hasil='-';break;
			}
			return $hasil;
		}

		function tktDidik($tk) {
			switch($tk) {
				case "10" : $didik="SD";break;
				case "20" : $didik="SLTP";break;
				case "30" : $didik="SLTA";break;
				case "41" : $didik="DIPLOMA I";break;
				case "42" : $didik="DIPLOMA II";break;
				case "43" : $didik="DIPLOMA III";break;
				case "44" : $didik="DIPLOMA IV";break;
				case "50" : $didik="SARMUD NON AKADEMI";break;
				case "60" : $didik="SARMUD AKADEMI";break;
				case "70" : $didik="STRATA 1 (S1)";break;
				case "80" : $didik="STRATA 2 (S2)";break;
				case "90" : $didik="STRATA 3 (S3)";break;
				default : $didik="-";
			}
			return $didik;
		}

		function lokasiKerjaB($A_01) {
			$q="select nm from TABLOK08 where kd='$A_01' LIMIT 1";
			$result = $this->dbeps->query($q)->row();
			return $result->nm;
		}

		function jurusan($didik,$jur) {
			$hasil="";
			if ($didik!="-") {
				$q="select KET from TABDIK where TKP='$didik' and KOD='$jur'";
				$result = $this->dbeps->query($q)->row();
				$hasil = $result->KET;
			}
			return $hasil;
		}

		function dikStru($tk) {
			switch($tk) {
				case "1" : $stru="LEMHANAS";break;
				case "2" : $stru="SESPA/SEPAMEN";break;
				case "3" : $stru="SEPADYA/SPAMA";break;
				case "4" : $stru="SEPALA/ADUMLA";break;
				case "5" : $stru="SEPADA/ADUM";break;
				case "6" : $stru="DIKLATPIM Tk.I";break;
				case "7" : $stru="DIKLATPIM Tk.II";break;
				case "8" : $stru="DIKLATPIM Tk.III";break;
				case "9" : $stru="DIKLATPIM Tk.IV";break;
				case "10" : $stru="DIKLATPIM PEMDA";break;
				default: $stru="-";
			}
			return $stru;
		}

		function jenisCuti($id) {
			$q = "SELECT kategori FROM jenis_cuti WHERE id = '$id';";
			$result = $this->dblocal->query($q)->row();
			return $result->kategori;
		}

		function status_acc($st) {
			switch($st) {
				case "0" : $status="BELUM";break;
				case "1" : $status="DISETUJUI";break;
				case "2" : $status="DITOLAK";break;
				default: $status="-";
			}
			return $status;
		}

		function rating($rt) {
			switch($rt) {
				case "1" : $status="KURANG BAIK";break;
				case "2" : $status="CUKUP BAIK";break;
				case "3" : $status="BAIK";break;
				case "4" : $status="SANGAT BAIK";break;
				default: $status="-";
			}
			return $status;
		}

		function kekata($x) {
		    $x = abs($x);
		    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
		    "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		    $temp = "";
		    if ($x <12) {
		        $temp = " ". $angka[$x];
		    } else if ($x <20) {
		        $temp = $this->kekata($x - 10). " belas";
		    } else if ($x <100) {
		        $temp = $this->kekata($x/10)." puluh". $this->kekata($x % 10);
		    } else if ($x <200) {
		        $temp = " seratus" . $this->kekata($x - 100);
		    } else if ($x <1000) {
		        $temp = $this->kekata($x/100) . " ratus" . $this->kekata($x % 100);
		    } else if ($x <2000) {
		        $temp = " seribu" . $this->kekata($x - 1000);
		    } else if ($x <1000000) {
		        $temp = $this->kekata($x/1000) . " ribu" . $this->kekata($x % 1000);
		    } else if ($x <1000000000) {
		        $temp = $this->kekata($x/1000000) . " juta" . $this->kekata($x % 1000000);
		    } else if ($x <1000000000000) {
		        $temp = $this->kekata($x/1000000000) . " milyar" . $this->kekata(fmod($x,1000000000));
		    } else if ($x <1000000000000000) {
		        $temp = $this->kekata($x/1000000000000) . " trilyun" . $this->kekata(fmod($x,1000000000000));
		    }     
		        return $temp;
		}

		function terbilang($x, $style=4) {
		    if($x<0) {
		        $hasil = "minus ". trim($this->kekata($x));
		    } else {
		        $hasil = trim($this->kekata($x));
		    }     
		    switch ($style) {
		        case 1:
		            $hasil = strtoupper($hasil);
		            break;
		        case 2:
		            $hasil = strtolower($hasil);
		            break;
		        case 3:
		            $hasil = ucwords($hasil);
		            break;
		        default:
		            $hasil = ucfirst($hasil);
		            break;
		    }     
		    return $hasil;
		}

		function DateToIndo_OLD($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
		   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
			$BulanIndo = array("Januari", "Februari", "Maret",
							   "April", "Mei", "Juni",
							   "Juli", "Agustus", "September",
							   "Oktober", "November", "Desember");

			$tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
			$bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
			$tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
			
			$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
			return($result);
		}

		function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
			// variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
			 $BulanIndo = array(1=>"Januari", "Februari", "Maret",
								"April", "Mei", "Juni",
								"Juli", "Agustus", "September",
								"Oktober", "November", "Desember");
 
			 $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
			 $bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
			 $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
			 
			 $result = $tgl . " " . $BulanIndo[(int)$bulan] . " ". $tahun;
			 return($result);
		 }

		function getLevel1JPT($A_02) {
			$concat = "B2" . $A_02."000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel2JPT($A_02) {
			$concat = "A2" . $A_02."300000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel3JPT($A_02) {
			$concat = "A2" .$A_02."000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getAtasanEsel4($A_01, $A_02, $A_03, $A_04, $A_05) {
			$concat = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getAtasanEsel3($A_01, $A_02, $A_03, $A_04='00') {
			if ($A_01 == 'A2') $concat = $A_01 . $A_02 . $A_03 . substr($A_04,0,1)."000";
			else $concat = $A_01 . $A_02 . $A_03 . "0000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getAtasanEsel2($A_01,$A_03='00') {
			if ($A_01 != 'A2') $A_03='00';
			$concat = $A_01 . "00".$A_03."0000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		//ambil atasan untuk biro di setda (khusus struktural ya tapi)
		function getLevel1Biro($A_02, $A_03, $A_04) {
			$concat = "A2" . $A_02 . $A_03 . substr($A_04,0,1) . "000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel2Biro($A_03) {
			
			$concat = "A2". "00" . $A_03 . "0000";
			/*$d = $A_04 - 1;
			$concat = $A_01 . $A_02 . $A_03 . $d . "00";*/

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel3Biro($A_02) {

			$concat = "A2" . $A_02."300000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}


		function getLevel1BADANPENGHUBUNG($A_04) {//TIDAK DIGUNAKAN

			$concat = "C1" . "00" . "00" . $A_04 . "00";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel2BADANPENGHUBUNG($A_04) {

			$concat = "C1" . "00" . "00" . $A_04 . "00";
			/*$d = $A_04 - 1;
			$concat = $A_01 . $A_02 . $A_03 . $d . "00";*/

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel3BADANPENGHUBUNG() {

			$concat = "C1" . "00000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel1RSJDSOEDJARWADI($A_04) {//TIDAK DIGUNAKAN

			$concat = "86" . "00" . "00" . $A_04 . "00";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel2RSJDSOEDJARWADI($A_03) {

			$concat = "86" . "00" . $A_03 . "00" . "00";
			/*$d = $A_04 - 1;
			$concat = $A_01 . $A_02 . $A_03 . $d . "00";*/

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel3RSJDSOEDJARWADI() {

			$concat = "86" . "00000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel1BPKAD($A_02, $A_04) {

			$concat = "B5" . $A_02 . "00" . $A_04 . "00";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel2BPKAD($A_02) {

			$concat = "B5" . $A_02 . "00" . "00" . "00";
			/*$d = $A_04 - 1;
			$concat = $A_01 . $A_02 . $A_03 . $d . "00";*/

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getLevel3BPKAD() {

			$concat = "B5" . "00000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}


		//ambil atasan untuk UPT/Balai/Panti

		function getAtasanEsel4UPT($A_01, $A_02, $A_04) {
			
			$concat = $A_01 . $A_02 . "00". $A_04 ."00";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getAtasanEsel3UPT($A_01,$A_02) {
			
			$concat = $A_01 .$A_02."000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}

		function getAtasanEsel2UPT($A_01,$A_02) {
			
			$concat = $A_01 .$A_02."000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}


		//ambil atasan untuk guru dan pegawai sekolah
		function getLevel1Guru($A_01, $A_02, $A_03, $A_04) {
			$concat = $A_01 . $A_02 . $A_03 . $A_04 . "10";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				
					return '-';
				
			}
		}

		function getLevel2Guru($A_01, $A_02, $A_03, $A_04) {
			$concat = $A_01 . $A_02 . $A_03 . "1000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return '-';
			}
		}

		function getLevel3Guru($A_01,$A_02) {
			
			$concat = $A_01 .$A_02."000000";

			$qAtasan = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rAtasan = $this->dbeps->query($qAtasan)->row();
			if (isset($rAtasan)) {
				return ($rAtasan->nip_pengganti != '' ? $rAtasan->nip_pengganti : $rAtasan->B_02B);
			} else {
				return "-";
			}
		}


		function getNIPUmpeg($A_01) {
			switch ($A_01) {
				case 'A2'	: $kumpeg="A200312300";break;
				case 'A3'	: $kumpeg="A300101000";break;
				case '29'	: $kumpeg="2900100000";break;
				case 'C1'	: $kumpeg="C100001000";break;
				case '80'	: $kumpeg="8000332000";break;
				case '81'	: $kumpeg="8100332000";break;
				case '82'	: $kumpeg="8200232000";break;
				case '83'	: $kumpeg="8300302000";break;
				case '84'	: $kumpeg="8400231000";break;
				case '85'	: $kumpeg="8500231000";break;
				case '86'	: $kumpeg="8600100000";break;
				
				default		: $kumpeg=$A_01."00103000";break;
			}
			//$rUmpeg = $this->dblocal->query("SELECT nip_umpeg FROM umpeg WHERE lokasi_id = '$A_01';")->row();
			//return $rUmpeg->nip_umpeg;
			$qUmpeg = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$kumpeg'";
			$rUmpeg = $this->dbeps->query($qUmpeg)->row();
			if (isset($rUmpeg)) {
				return ($rUmpeg->nip_pengganti != '' ? $rUmpeg->nip_pengganti : $rUmpeg->B_02B);
			} else {
				return "-";
			}
		}

		function getNIPUmpegBiro($A_01, $A_02, $A_03) {
			switch ($A_03) {

				case '31'	: $kumpeg=$A_01.$A_02.$A_03."2300";break;
				case '12'	: $kumpeg=$A_01.$A_02.$A_03."2300";break;
				case '11'	: $kumpeg=$A_01.$A_02.$A_03."1400";break;
				case '24'	: $kumpeg=$A_01.$A_02.$A_03."3300";break;
				case '13'	: $kumpeg=$A_01.$A_02.$A_03."4300";break;
				case '22'	: $kumpeg=$A_01.$A_02.$A_03."1300";break;
				case '21'	: $kumpeg=$A_01.$A_02.$A_03."1300";break;
				case '32'	: $kumpeg=$A_01.$A_02.$A_03."1400";break;
				case '34'	: $kumpeg=$A_01.$A_02.$A_03."2300";break;
				default		: "-";break;
			}
			//$rUmpeg = $this->dblocal->query("SELECT nip_umpeg FROM umpeg WHERE lokasi_id = '$A_01';")->row();
			//return $rUmpeg->nip_umpeg;
			$qUmpeg = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$kumpeg'";
			$rUmpeg = $this->dbeps->query($qUmpeg)->row();
			if (isset($rUmpeg)) {
				return ($rUmpeg->nip_pengganti != '' ? $rUmpeg->nip_pengganti : $rUmpeg->B_02B);
			} else {
				return "-";
			}
		}

		function getNIPUmpegUPT($A_01, $A_02) {
			switch ($A_01) {

				case 'B4'	: $kumpeg="B4".$A_02."001000";break;
				case 'B5'	: $kumpeg="B5".$A_02."001000";break;
				case 'D0'	: $kumpeg="D0".$A_02."001000";break;
				case 'D1'	: $kumpeg="D1"."00103000";break;
				case 'D2'	: $kumpeg="D2".$A_02."001000";break;
				case 'D3'	: $kumpeg="D3".$A_02."001000";break;
				case 'D5'	: $kumpeg="D5".$A_02."001000";break;
				case 'D6'	: $kumpeg="D6"."00103000";break;
				case 'D7'	: $kumpeg="D7".$A_02."001000";break;
				case 'D8'	: $kumpeg="D8"."00103000";break;
				case 'D9'	: $kumpeg="D9".$A_02."001000";break;
				case 'E1'	: $kumpeg="E1".$A_02."001000";break;
				case 'E2'	: $kumpeg="E2".$A_02."001000";break;
				case 'E3'	: $kumpeg="E3".$A_02."001000";break;
				case 'E4'	: $kumpeg="E4".$A_02."001000";break;
				case 'E5'	: $kumpeg="E5".$A_02."001000";break;
				case 'E6'	: $kumpeg="E6".$A_02."001000";break;
				case 'E7'	: $kumpeg="E7".$A_02."001000";break;
				case 'F1'	: $kumpeg="F1".$A_02."001000";break;
				
				
				default		: "-";break;
			}
			//$rUmpeg = $this->dblocal->query("SELECT nip_umpeg FROM umpeg WHERE lokasi_id = '$A_01';")->row();
			//return $rUmpeg->nip_umpeg;
			$qUmpeg = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$kumpeg'";
			$rUmpeg = $this->dbeps->query($qUmpeg)->row();
			if (isset($rUmpeg)) {
				return ($rUmpeg->nip_pengganti != '' ? $rUmpeg->nip_pengganti : $rUmpeg->B_02B);
			} else {
				return "-";
			}
		}

		function getNIPUmpegBKD($A_02) {
			$concat = "B2" .$A_02."103000";
			
			$qUmpeg = "SELECT B_02B,nip_pengganti FROM TABLOKB08 t left join MASTFIP08 m on t.KOLOK=m.I_05 AND I_5A = '1' AND m.A_01 <> '99' left join bkd_sinaga_new.pejabat_pengganti s on t.KOLOK=s.unitkerja WHERE KOLOK = '$concat'";
			$rUmpeg = $this->dbeps->query($qUmpeg)->row();
			if (isset($rUmpeg)) {
				return ($rUmpeg->nip_pengganti != '' ? $rUmpeg->nip_pengganti : $rUmpeg->B_02B);
			} else {
				return "-";
			}
		}		

		function getKopSuratOPD($A_01) {
			$rOPD = $this->dblocal->query("SELECT nm, alamat, kota, telp, fax FROM opd WHERE kd = '$A_01';")->row();
			return $rOPD;
		}

		function getKopSuratSETDA($A_01) {		

			$rOPD = $this->dblocal->query("SELECT nm, alamat, kota, telp, fax FROM opd WHERE kd = 'A2';")->row();
			return $rOPD;
		}

		function getnamaLOK($A_01, $A_02) {
			$rOPD = $this->dbeps->query("SELECT NALOKP FROM TABLOKB08 WHERE A_01 = '$A_01' AND A_02 = $A_02;")->row();
			return $rOPD->NALOKP;
		}


		function infoPengganti($nip_pengganti) {
			$rPengganti = $this->dblocal->query("SELECT nip_pengganti FROM pejabat_pengganti 
				WHERE nip_pengganti = '$nip_pengganti';")->row();
			return $rPengganti;
		}

		function infoPenambah($penambah) {
			$rPenambah = $this->dblocal->query("SELECT penambah FROM pejabat_pengganti 
				WHERE penambah = '$penambah';")->row();
			return $rPenambah;
		}

		function getPktPimpinanttd($nip_A_02){
			$pkt = "select TABPKT.NAMAY as namapkt from TABPKT inner join MASTFIP08 ON MASTFIP08.F_03 = TABPKT.KODE WHERE MASTFIP08.B_02B = $nip_A_02";
			$rpkt = $this->dbeps->query($pkt)->row();
			return $rpkt->namapkt;
		}

		function getSignKepalaOPD($A_01,$nip_A_02='') {
			$concat = $A_01 . "00000000";
			$nip = $this->getAtasanEsel2($A_01);
			
			$qjab = "select NAJAB from TABLOKB08 where KOLOK='$concat'";
			$rJab = $this->dbeps->query($qjab)->row();
			$jab = $rJab->NAJAB;

			$ub = '';
			if ($nip_A_02 != '') {
				$qub = "select IF(NAJAB!='',NAJAB,I_JB) jabatan from eps.MASTFIP08 left join pejabat_pengganti pp on B_02B=nip_pengganti left join eps.TABLOKB08 l on pp.unitkerja=l.KOLOK where B_02B='$nip_A_02'";
				$rub = $this->db->query($qub)->row();
				$ub = $rub->jabatan;
			}

			$qKepala = "SELECT B_02B, B_03A, B_03, B_03B, F_03,'$jab' jabatan,if(I_05='$concat',0,1) plt FROM MASTFIP08 WHERE B_02B = '$nip'";
			$rKepala = $this->dbeps->query($qKepala)->row();
			if (isset($rKepala)) {
				//if ($ub!= '' && $ub != $rKepala) $rKepala = $rKepala."<br />u.b. ".$ub;
				return $rKepala;
			} else {
				return "-";
			}
		}

		function getSignSEKDA($A_01,$nip_A_02='') {
			$concat = $A_01 . "00000000";
			$nip = $this->getLevel3JPT($A_01);
			
			$qjab = "select NAJAB from TABLOKB08 where KOLOK='$concat'";
			$rJab = $this->dbeps->query($qjab)->row();
			$jab = $rJab->NAJAB;

			$ub = '';
			if ($nip_A_02 != '') {
				$qub = "select IF(NAJAB!='',NAJAB,I_JB) jabatan from eps.MASTFIP08 left join pejabat_pengganti pp on B_02B=nip_pengganti left join eps.TABLOKB08 l on pp.unitkerja=l.KOLOK where B_02B='$nip_A_02'";
				$rub = $this->db->query($qub)->row();
				$ub = $rub->jabatan;
			}

			$qKepala = "SELECT B_02B, B_03A, B_03, B_03B, F_03,'$jab' jabatan,if(I_05='$concat',0,1) plt FROM MASTFIP08 WHERE B_02B = '$nip'";
			$rKepala = $this->dbeps->query($qKepala)->row();
			if (isset($rKepala)) {
				//if ($ub!= '' && $ub != $rKepala) $rKepala = $rKepala."<br />u.b. ".$ub;
				return $rKepala;
			} else {
				return "-";
			}
		}


		function getNamaPNS($B_02B) {
			$rPNS = $this->dbeps->query("SELECT B_03A, B_03, B_03B FROM MASTFIP08 WHERE B_02B = '$B_02B';")->row();
			if (isset($rPNS)) {
				return $this->namaPNS($rPNS->B_03A, $rPNS->B_03, $rPNS->B_03B);
			} else {
				return "-";
			}
		}



		// function get_tot_cuti_tahunan($B_02B)
		// {
		// 	$tahunini = date("Y");
		// 	$tCuti = $this->dblocal->query("SELECT count(B_02B) as c FROM cuti_tahunan 
		// 		WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1 AND YEAR(tgl_awal) LIKE '$tahunini';")->row();
		// 	return $tCuti->c;
		// }

		function get_tot_cuti_tahunan2($B_02B){
			$tahunini = date("Y");
			$tCuti = $this->dblocal->query("SELECT SUM(DATEDIFF(tgl_akhir, tgl_awal)) AS selisih FROM `cuti_tahunan` WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1 AND YEAR(tgl_awal) = YEAR(CURDATE())
				AND (DAYNAME(tgl_akhir) != 'Saturday' OR DAYNAME(tgl_akhir) != 'Sunday') 
				AND (DAYNAME(tgl_awal) != 'Saturday' OR DAYNAME(tgl_awal) != 'Sunday');")->row();
			return $tCuti->selisih;
		}

		

		function get_tot_cuti_tahunan_tahun_lalu2($B_02B)
		{
			$tahunini = date("Y");
			$tCuti = $this->dblocal->query("SELECT SUM(DATEDIFF(tgl_akhir, tgl_awal)) AS selisih FROM `cuti_tahunan` 
			WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1
			AND YEAR(tgl_akhir) != YEAR(CURDATE())
			AND (DAYNAME(tgl_akhir) != 'Saturday' OR DAYNAME(tgl_akhir) != 'Sunday') 
			AND (DAYNAME(tgl_awal) != 'Saturday' OR DAYNAME(tgl_awal) != 'Sunday');")->row();
			return $tCuti->selisih;
		}

		function get_tot_cuti_tahunan($B_02B){
			$tahunini = date("Y");
			$tCuti = $this->dblocal->query("SELECT SUM(lama) AS total FROM `cuti_tahunan` WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1 AND YEAR(tgl_awal) = YEAR(CURDATE());")->row();
			return $tCuti->total;
		}

		function get_tot_cuti_tahunan_tahun_lalu($B_02B)
		{
			$tahunini = date("Y");
			$tCuti = $this->dblocal->query("SELECT SUM(lama) AS total FROM `cuti_tahunan` 
			WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1
			AND YEAR(tgl_akhir) != YEAR(CURDATE());")->row();
			return $tCuti->total;
		}

		function get_cuti_besar_tahun_ini($B_02B)
		{
			$tahunini = date("Y");
			$sCuti = $this->dblocal->query("SELECT count(B_02B) as c FROM cuti_besar 
				WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1 AND YEAR(tgl_awal) = '$tahunini';")->row();
			return $sCuti->c;
		}

		function get_cuti_besar_back_5($B_02B)
		{
			$tahunini = date("Y");
			$tahun = $tahunini - 5;
			$sCuti = $this->dblocal->query("SELECT count(B_02B) as c FROM cuti_besar 
				WHERE B_02B = '$B_02B' AND acc_A_04 = 1 AND acc_A_03 = 1 AND acc_A_02 = 1 AND YEAR(tgl_awal)
				NOT BETWEEN '$tahunini'
				AND $tahun  ;")->row();
			return $sCuti->c;
		}

		
		
	}
?>