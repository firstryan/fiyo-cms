<?php
	class Cuti_Tahunan_Model extends CI_Model {
		
		public $dblocal = NULL;
		public $model_fungsi = NULL;

		function fill_data2($act,$id='',$nip,$unit_kerja,$tanggal,$awal,$akhir,$deskripsi)
		{
			$this->data = array(
				'tanggal_awal' => $awal,
				'tanggal_akhir' => $akhir,
				'tanggal' => $tanggal,
				'status' => '1',
				'tipe' => 'CT',
				'deskripsi' => $deskripsi,
				'update_datetime' => date("Y-m-d H:i:s"),
				'update_user' => $this->session->B_02B,
				'validation_user' => $this->session->B_02B,
				'validation_datetime' => date("Y-m-d H:i:s")
				
			);
			if ($act == 'insert')
			{
				$this->data['id'] = $id;
				$this->data['unit_kerja'] = $unit_kerja;
				$this->data['nip'] = $nip;
				$this->data['create_user'] = $this->session->B_02B;
				$this->data['create_datetime'] =  date("Y-m-d H:i:s");
			}
		}

		public function hapus($id) {
			return $this->db->delete('cuti_tahunan', array('id' => $id));
		}

		function insert_data()
		{
			$insert = $this->db->insert('presensi.izin', $this->data);
			return $insert;
		}

		function insert_detail($id,$parent_id,$tanggal)
		{
			$this->data = array(
				'id' => $id,
				'parent_id' => $parent_id,
				'tanggal' => $tanggal
			);
			$insert = $this->db->insert('presensi.izin_detail', $this->data);
			return $insert;
		}

		function delete_data_detail($parent_id){
			$this->db->where('parent_id', $parent_id);
			$delete = $this->db->delete('presensi.izin_detail');
			return $delete;
		}

		public function __construct() {
			parent::__construct();

			$this->dblocal = $this->load->database('default', TRUE);
			$this->model_fungsi = $this->Fungsi_Model;
		}

		function id_izin($nip,$in)
		{
			$bln = substr($in,5,2);
			$thn = substr($in,0,4);
			$prefix = $nip.'_'.$thn.''.$bln;
			
			$this->db->select_max('id','id');
			$this->db->like('id', $prefix, 'after');
			$query =  $this->db->get('presensi.izin')->row_array();
			
			$id = sprintf("%02d",(substr($query['id'], -2)+1));
			$out =	strtoupper($prefix.''.$id);
			return $out;	
		}

		function get_pegawai_nip($nip)
		{
			$this->db->select('b.nm as  instansi,
			a.A_01 as opd,
			a.I_JB as jabatan,
			CONCAT(a.A_01,a.A_02) as upt,
			CONCAT(a.A_01,a.A_02,a.A_03,a.A_04) as satker,
			CONCAT(a.A_01,a.A_02,a.A_03,a.A_04,a.A_05) as unit_kerja,
			a.B_02B as nip,CONCAT(a.B_03A," ",a.B_03," ",a.B_03B) as nama');
			$this->db->JOIN('eps.TABLOK08 b','a.A_01 = b.kd','INNER');
			$this->db->WHERE('a.B_02B',$nip);
			
			 return $this->db->get('eps.MASTFIP08 a');
		}

		function getDatesFromRange($start, $end, $output, $format = 'Y-m-d') 
		{
			$array = array();
			$interval = new DateInterval('P1D');
		
			$realEnd = new DateTime($end);
			$ends = $realEnd->add($interval);
		
			$period = new DatePeriod(new DateTime($start), $interval, $ends);
		
			foreach($period as $date) { 
				$array[] = $date->format($format); 
			}
			
			switch($output)
			{
				case 'array':
					return $array;
				break;
				
				case 'implode':
					return implode(",", $array);
				break;
				
				default:
					return $array;
				break;
			}			
		}

		public function simpan_pengajuan($jenis, $lama, $tujuan, $tgl_awal, $tgl_akhir, $alamat, $rating) {
			$time = time();
			$id = md5('CTHN'.$time.$this->session->userdata('B_02B'));

			if ($this->session->userdata('I_05') == 'A200000000') {
				//Jika Pak Sekda Cuti
				$id_jenis_pns_tahunan = 3;				
				$nip_esel_4 = '-';
				$acc_esel_4 = '1';
				$nip_esel_3 = '-';
				$acc_esel_3 = '1';
				$nip_esel_2 = '-';
				$acc_esel_2 = '1';
				$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
				$acc_umpeg = '0';
			} elseif ($this->session->userdata('I_05') == $this->session->userdata('A_01').'00000000' || $this->session->userdata('I_06') == '21' || $this->session->userdata('I_06') == '22') {
				//Jika Pimpinan OPD Cuti
				$id_jenis_pns_tahunan = 2;
				
				//$level2=$this->model_fungsi->getLevel2JPT($this->session->userdata('A_02'));
				//$level3=$this->model_fungsi->getLevel3JPT($this->session->userdata('A_02'));
							
				$nip_esel_4 = $this->model_fungsi->getLevel1JPT($this->session->userdata('A_02'));
				$acc_esel_4 = '0';
							
				$nip_esel_3 = $this->model_fungsi->getLevel2JPT($this->session->userdata('A_02'));
				$acc_esel_3 = '0';
					
				$nip_esel_2 = $this->model_fungsi->getLevel3JPT($this->session->userdata('A_02'));
				$acc_esel_2 = '0';
				$nip_umpeg = $this->model_fungsi->getNIPUmpegBKD($this->session->userdata('A_02'));
				$acc_umpeg = '0';				
			} else {
				//Jika PNS Cuti
				$id_jenis_pns_tahunan = 1;
				if(($this->session->userdata('A_01') == 'D0') && ($this->session->userdata('A_02') != '00') &&
				($this->session->userdata('A_04') > '30') || ($this->session->userdata('A_04') == '4%')
				|| ($this->session->userdata('A_04') == '5%')
				|| ($this->session->userdata('A_04') == '6%')
				|| ($this->session->userdata('A_04') == '7%')
				|| ($this->session->userdata('A_04') == '8%')
				){ // get nip atasan2 untuk PNS Yang kerja di sekolahan	
					
					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
				if ($this->session->userdata('I_5A') != '1') {
					if ($this->session->userdata('K_01') == '1' && $this->session->userdata('I_05') == '00018'){ //jika kepala sekolah
							
						$level2 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$level3 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));

						$nip_esel_4 = '-';
						$acc_esel_4 = '1';
							
						$nip_esel_3 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$acc_esel_3 = '0';
							
						$nip_esel_2 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
						$acc_esel_2 = '0';

						$nip_umpeg = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$acc_umpeg = '0';
							
								
					} elseif ($this->session->userdata('I_05') == '00053'){

						$level2 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$level3 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));

						$nip_esel_4 = '-';
						$acc_esel_4 = '1';
									
						$nip_esel_3 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$acc_esel_3 = '0';
								
						$nip_esel_2 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
						$acc_esel_2 = '0';
	
						$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
						$acc_umpeg = '0';

						} else {
								
							$level1 = $this->model_fungsi->getLevel1Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'), $this->session->userdata('A_05'));
							$level2 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$level3 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
								
							if ($level1 != NULL){
								$nip_esel_4 = $level1;
								$acc_esel_4 = '0';
							} else {
								$nip_esel_4 = '-';
								$acc_esel_4 = '1';
							}
							
								
							$nip_esel_3 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_esel_3 = '0';
							
							$nip_esel_2 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_umpeg = '0';
							}

					} else {	//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 

						if ($this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52) {	//Jika pengguna adalah struktural eselon 4
							$level2 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$level3 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
								
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_esel_3 = '0';
								
								$nip_esel_2 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
								$acc_esel_2 = '0';

								$nip_umpeg = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
								$acc_umpeg = '0';
								
						} elseif ($this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) { //Jika pengguna adalah struktural eselon 3
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
							$nip_esel_2 = $this->model_fungsi->getLevel3Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'));
							$acc_esel_2 = '0';
							$nip_umpeg = $this->model_fungsi->getLevel2Guru($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_umpeg = '0';
						}
					}

				} elseif ($this->session->userdata('A_01') == 'A2'){ //jika pns setda
					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
					if ($this->session->userdata('I_5A') != '1') {
						$level1 = $this->model_fungsi->getAtasanEsel4($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'), $this->session->userdata('A_05'));
						$level2 = $this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$level3 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
						
						$nip_esel_4 = $this->model_fungsi->getAtasanEsel4($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'), $this->session->userdata('A_05'));
						$acc_esel_4 = '0';
						
						
						$nip_esel_3 = $this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$acc_esel_3 = '0';
					
						$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
						$acc_esel_2 = '0';

						$nip_umpeg = $this->model_fungsi->getNIPUmpegBiro($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'));
						$acc_umpeg = '0';
						
					} else {//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 
						if ($this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52) {	//Jika pengguna adalah struktural eselon 4
							$level1=$this->model_fungsi->getLevel1Biro($this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$level2=$this->model_fungsi->getLevel2Biro($this->session->userdata('A_03'));
							$level3=$this->model_fungsi->getLevel3Biro($this->session->userdata('A_02'));
							
							$nip_esel_4 = $this->model_fungsi->getLevel1Biro($this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_esel_4 = '0';
							
							$nip_esel_3 = $this->model_fungsi->getLevel2Biro($this->session->userdata('A_03'));
							$acc_esel_3 = '0';
					
							$nip_esel_2 = $this->model_fungsi->getLevel3Biro($this->session->userdata('A_02'));
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getNIPUmpegBiro($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'));
						    $acc_umpeg = '0';
							
						} elseif ($this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) { //Jika pengguna adalah struktural eselon 3
							$level2=$this->model_fungsi->getLevel2Biro($this->session->userdata('A_03'));
							$level3=$this->model_fungsi->getLevel3Biro($this->session->userdata('A_02'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = $this->model_fungsi->getLevel2Biro($this->session->userdata('A_03'));
							$acc_esel_3 = '0';
						
							$nip_esel_2 = $this->model_fungsi->getLevel3Biro( $this->session->userdata('A_02'));
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getNIPUmpegBiro($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'));
						    $acc_umpeg = '0';
						}
					}

				} elseif ($this->session->userdata('A_01') == 'C1'){ //jika pns BADAN PENGHUBUNG
					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
					if ($this->session->userdata('I_5A') != '1') {
						
						$level2 = $this->model_fungsi->getLevel2BADANPENGHUBUNG($this->session->userdata('A_04'));
						$level3 = $this->model_fungsi->getLevel3BADANPENGHUBUNG();
						
						$nip_esel_4 = '-';
						$acc_esel_4 = '1';
												
						$nip_esel_3 = $this->model_fungsi->getLevel2BADANPENGHUBUNG($this->session->userdata('A_04'));
						$acc_esel_3 = '0';
					
						$nip_esel_2 = $this->model_fungsi->getLevel3BADANPENGHUBUNG();
						$acc_esel_2 = '0';

						$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						$acc_umpeg = '0';
						
					} else {//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 
						if ($this->session->userdata('I_06') == 41 || $this->session->userdata('I_06') == 42 || $this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) {	//Jika pengguna adalah struktural eselon 4
														
							$level3 = $this->model_fungsi->getLevel3BADANPENGHUBUNG();
							
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
					
							$nip_esel_2 = $this->model_fungsi->getLevel3BADANPENGHUBUNG();
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						    $acc_umpeg = '0';
						}
					}

				} elseif ($this->session->userdata('A_01') == '86'){ //jika pns RSJD SOEDJARWADI
					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
					if ($this->session->userdata('I_5A') != '1') {
						
						$level2 = $this->model_fungsi->getLevel2RSJDSOEDJARWADI($this->session->userdata('A_03'));
						$level3 = $this->model_fungsi->getLevel3RSJDSOEDJARWADI();
						
						$nip_esel_4 = '-';
						$acc_esel_4 = '1';
												
						$nip_esel_3 = $this->model_fungsi->getLevel2RSJDSOEDJARWADI($this->session->userdata('A_03'));
						$acc_esel_3 = '0';
					
						$nip_esel_2 = $this->model_fungsi->getLevel3RSJDSOEDJARWADI();
						$acc_esel_2 = '0';

						$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						$acc_umpeg = '0';
						
					} else {	//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 
						if ($this->session->userdata('I_06') == 41 || $this->session->userdata('I_06') == 42 || $this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) {	//Jika pengguna adalah struktural eselon 4
														
							$level3 = $this->model_fungsi->getLevel3RSJDSOEDJARWADI();
							
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
					
							$nip_esel_2 = $this->model_fungsi->getLevel3RSJDSOEDJARWADI();
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						    $acc_umpeg = '0';
						}
					}

				} elseif ($this->session->userdata('A_02') == '00'){ 
					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
					if ($this->session->userdata('I_5A') != '1') {
						$level1=$this->model_fungsi->getAtasanEsel4($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'), $this->session->userdata('A_05'));
						$level2=$this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$level3=$this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
						
						$nip_esel_4 = $this->model_fungsi->getAtasanEsel4($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'), $this->session->userdata('A_05'));
						$acc_esel_4 = '0';
						
						
						$nip_esel_3 = $this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
						$acc_esel_3 = '0';
						
						$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
						$acc_esel_2 = '0';
						$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						$acc_umpeg = '0';			

					} else {	//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 
						if ($this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52) {	//Jika pengguna adalah struktural eselon 4
							$level2=$this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$level3=$this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = $this->model_fungsi->getAtasanEsel3($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_03'), $this->session->userdata('A_04'));
							$acc_esel_3 = '0';
							
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
							$acc_esel_2 = '0';
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						    $acc_umpeg = '0';
							
						} elseif ($this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) { //Jika pengguna adalah struktural eselon 3
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
							$acc_esel_2 = '0';
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
						    $acc_umpeg = '0';
						}
					}

				} else { // get nip atasan2 untuk PNS PENGHUNI balai/panti/upt/satker

					//Ambil NIP Atasan-atasan, jika pengguna bukan PNS Struktural
					if ($this->session->userdata('I_5A') != '1') {
						if ($this->session->userdata('A_01') == 'D1' || $this->session->userdata('A_01') == 'D6' || $this->session->userdata('A_01') == 'D8'){ //untuk balai, upt , satker dinkes & dinpora
														
							$nip_esel_4 = $this->model_fungsi->getAtasanEsel4UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_04'));
							$acc_esel_4 = '0';

							$nip_esel_3 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_esel_3 = '0';
							
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));
							$acc_esel_2 = '0';
							
							$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_umpeg = '0';
						
						} elseif ($this->session->userdata('A_01') == 'B2' ){ //untuk balai, upt , satker disnaker (INI KALAU LEVEL4NYA KE UMPEG DULU)
							
								
							$nip_esel_4 = $this->model_fungsi->getAtasanEsel4UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_04'));
							$acc_esel_4 = '0';
	
							$nip_esel_3 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_esel_3 = '0';
								
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));
							$acc_esel_2 = '0';
								
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
							$acc_umpeg = '0';

						} elseif ($this->session->userdata('A_01') == 'D5' && $this->session->userdata('A_04') > '30'){ //untuk RUPELSOS DINAS SOSIAL)
							
								
							$nip_esel_4 = '-';
							$acc_esel_4 = '1';

							$nip_esel_3 = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_esel_3 = '0';
								
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_esel_2 = '0';
								
							$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_umpeg = '0';							
							
						} else { // untuk balai, upt, satker selain bkd, disnaker, dinpora, dan dinkes

							if ($this->session->userdata('I_5A') == '2' && $this->session->userdata('I_05') <= 00053){
								
								$nip_esel_4 = '-';
								$acc_esel_4 = '1';
							
								$nip_esel_3 = $this->model_fungsi->getAtasanEsel4UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'), $this->session->userdata('A_04'));
								$acc_esel_3 = '0';
								
								$nip_esel_2 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'));
								$acc_esel_2 = '0';
								
								$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
								$acc_umpeg = '0';

							} else {
								
								$nip_esel_4 = '-';
								$acc_esel_4 = '1';
									
								$nip_esel_3 = $this->model_fungsi->getAtasanEsel4UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'),$this->session->userdata('A_04'));
								$acc_esel_3 = '0';
								
								$nip_esel_2 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'));
								$acc_esel_2 = '0';
								
								$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
								$acc_umpeg = '0';
							}

						}
						
					} else {//Ambil NIP Atasan-atasan jika pengguna adalah PNS struktural 
						if ($this->session->userdata('A_01') == 'B2' && $this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52){ //untuk BKD)							
							$level2=$this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));;
							$level3=$this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
	
							$nip_esel_3 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));
							$acc_esel_3 = '0';
								
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));
							$acc_esel_2 = '0';
							
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
							$acc_umpeg = '0';

						} elseif ($this->session->userdata('A_01') == 'B2' && $this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32){ //untuk BKD)
							
							$level3=$this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
	
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
								
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));
							$acc_esel_2 = '0';
							
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
							$acc_umpeg = '0';

						} elseif ($this->session->userdata('I_06') == 41 && substr($this->session->userdata('I_05'),4,6) == '000000') { //Jika pengguna adalah struktural kepala balai kelas B eselon

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';

							$nip_esel_3 = '-';
							$acc_esel_3 = '1';

							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'));
							$acc_esel_2 = '0';

							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
							$acc_umpeg = '0';

						} elseif ($this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52) {//Jika pengguna adalah struktural eselon 4
							
							$level3=$this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';
							
							$nip_esel_3 = '-';
							$acc_esel_3 = '1';
							
							
							$nip_esel_2 = $this->model_fungsi->getAtasanEsel3UPT($this->session->userdata('A_01'), $this->session->userdata('A_02'));
							$acc_esel_2 = '0';
							
							$nip_umpeg = $this->model_fungsi->getNIPUmpegUPT($this->session->userdata('A_01'),$this->session->userdata('A_02'));

							$acc_umpeg = '0';

						} elseif ($this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) { //Jika pengguna adalah struktural eselon 3
							
							$level3=$this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));

							$nip_esel_4 = '-';
							$acc_esel_4 = '1';

							$nip_esel_3 = '-';
							$acc_esel_3 = '1';

							$nip_esel_2 = $this->model_fungsi->getAtasanEsel2($this->session->userdata('A_01'), $this->session->userdata('A_03'));
							$acc_esel_2 = '0';
							
							$nip_umpeg = $this->model_fungsi->getNIPUmpeg($this->session->userdata('A_01'));
							$acc_umpeg = '0';
						}
					}

				}
			}

			

			$jabatan = $this->model_fungsi->getNaJab($this->session->userdata('B_02'));
			$data = array(
				'id' => $id,
				'id_jenis_cuti' => $jenis,
				'id_jenis_pns_cuti_tahunan' => $id_jenis_pns_tahunan,
				'B_02B' => $this->session->userdata('B_02B'),
				'B_03A' => $this->session->userdata('B_03A'),
				'B_03' => $this->session->userdata('B_03'),
				'B_03B' => $this->session->userdata('B_03B'),
				'F_03' => $this->session->userdata('F_03'),
				'jabatan' => $jabatan,
				'A_01' => $this->session->userdata('A_01'),
				'lama' => $lama,
				'tujuan' => $tujuan,
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
				'jumlah_tgl_cuti' => $this->getDatesFromRange($tgl_awal, $tgl_akhir,'implode',$format = 'Y-m-d'),
				'alamat' => $alamat,
				'tgl_pengajuan' => $time,
				'nip_A_04' => $nip_esel_4,
				'nip_A_03' => $nip_esel_3,
				'nip_A_02' => $nip_esel_2,
				'nip_umpeg' => $nip_umpeg,
				'acc_A_04' => $acc_esel_4,
				'acc_A_03' => $acc_esel_3,
				'acc_A_02' => $acc_esel_2,
				'acc_umpeg' => $acc_umpeg,
				'rating' => $rating

				
				);
			$sql = $this->dblocal->insert_string('cuti_tahunan', $data);
			if ($this->dblocal->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function simpan_aksi_atasan4($id, $status_acc, $alasan){
			$data = array(
					'acc_A_04' => $status_acc,
					'alasan_A_04' => $alasan,
					'tgl_acc_A_04' => time()
					);
				$where = "id = '$id'";

				$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function simpan_aksi_atasan3($id, $status_acc, $alasan){
			$data = array(
					'acc_A_03' => $status_acc,
					'alasan_A_03' => $alasan,
					'tgl_acc_A_03' => time()
					);
				$where = "id = '$id'";

				$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function simpan_aksi_atasan2($id, $status_acc, $alasan){
			$data = array(
					'acc_A_02' => $status_acc,
					'alasan_A_02' => $alasan,
					'tgl_acc_A_02' => time()
					);
				$where = "id = '$id'";

				$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function simpan_aksi_atasan($id, $status_acc, $alasan) {
			if ($this->session->userdata('I_06') >= 41 && $this->session->userdata('I_06') <= 52) {	//Jika pengguna adalah struktural eselon 4
				$data = array(
					'acc_A_04' => $status_acc,
					'alasan_A_04' => $alasan,
					'tgl_acc_A_04' => time()
					);
				$where = "id = '$id'";
			} elseif ($this->session->userdata('I_06') == 31 || $this->session->userdata('I_06') == 32) { //Jika pengguna adalah struktural eselon 3
				$data = array(
					'acc_A_03' => $status_acc,
					'alasan_A_03' => $alasan,
					'tgl_acc_A_03' => time()
					);
				$where = "id = '$id'";
			} elseif ($this->session->userdata('I_06') == 21 || $this->session->userdata('I_06') == 22) { //Jika pengguna adalah struktural eselon 2
				$data = array(
					'acc_A_02' => $status_acc,
					'alasan_A_02' => $alasan,
					'tgl_acc_A_02' => time()
					);
				$where = "id = '$id'";
			}
						
			$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function cetak_sk($id, $nomor, $tgl) {
			$data = array(
				'acc_umpeg' => '1',
				'tgl_acc_umpeg' => time(),
				'nomor_sk_cuti' => $nomor,
				'tgl_sk_cuti' => $tgl
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			
			if ($this->db->simple_query($sql)) {
				//----------- masukkan ke presensi----------------------------------
				$r = $this->db->where('id',$id)->get('cuti_tahunan')->row();
				$tanggal_awal = $r->tgl_awal;
				$tanggal_akhir = $r->tgl_akhir;
				$nip = $r->B_02B;
				$act = 'insert';
				
				$tanggal_h = $this->getDatesFromRange($tanggal_awal,$tanggal_akhir,'implode');
				$tanggal_d = $this->getDatesFromRange($tanggal_awal,$tanggal_akhir,'array');
				
				
				if(count($tanggal_d) > 0)
				{
					$profile_pegawai = $this->get_pegawai_nip($nip)->row_array();
					$unit_kerja = $profile_pegawai['unit_kerja'];
					$parent = $this->id_izin($nip,$tanggal_awal);
			
					$this->fill_data2($act,$parent,$nip,$unit_kerja,$tanggal_h,$tanggal_awal,$tanggal_akhir,$r->tujuan);
					$this->insert_data();
					$this->delete_data_detail($parent);
					
					$k=0;
					foreach($tanggal_d as $tgl)
					{
						$k++;
						$id = $parent.'_'.sprintf("%02d",$k);
						$this->insert_detail($id,$parent,$tgl);
						
					}
					return TRUE;
				}
				else
				{
					return FALSE;
				}

				//-----------------------------------------------------------------//
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function cetak_sk_ulang($id, $nomor, $tgl) {
			$data = array(
				'acc_umpeg' => '1',
				'tgl_acc_umpeg' => time(),
				'nomor_sk_cuti' => $nomor,
				'tgl_sk_cuti' => $tgl
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('cuti_tahunan', $data, $where);
			
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
	}
?>