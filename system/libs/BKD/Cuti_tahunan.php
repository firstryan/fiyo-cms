<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2017 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/

defined('_FINDEX_') or die('Access Denied');

class Cuti_Tahunan extends CI_Controller {

	public $model_tahunan = NULL;
	public $model_fungsi = NULL;
	public  $userdata = null;

	public function __construct($userdata = null) {
		parent::__construct();

		$this->userdata = $userdata;

		//$this->load->model('Cuti_Tahunan_Model');
		//$this->model_tahunan = $this->Cuti_Tahunan_Model;

		//$this->load->model('Fungsi_Model');
		// $this->model_fungsi = $this->Fungsi_Model;
	}

	public function cetak() {
		echo $this->userdata;
	}
	public function notice() {
		$this->load->view('cuti_tahunan/notice');
	}

	# fungsi untuk mengecek status username dari db
	function cek_status_tgl_awal(){
		# ambil username dari form
		$tgl_awal = $_POST['tgl_awal'];
		 
						# select ke model member username yang diinput user
		$hasil_tgl_awal = $this->model_tahunan->cek_tgl_awal($tgl_awal);
		 
						# pengecekan value $hasil_username
		if(count($hasil_tgl_awal)!=0){
			# kalu value $hasil_username tidak 0
							# echo 1 untuk pertanda username sudah ada pada db    
										echo "1"; 
		}else{
							# kalu value $hasil_username = 0
							# echo 2 untuk pertanda username belum ada pada db
				echo "2";
		}
		 
}

	public function pengajuan2() {
		if ($this->session->userdata('B_02B') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/pengajuan2', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('cuti_tahunan/pengajuan2');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function pengajuan() {
		if ($this->session->userdata('B_02B') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/pengajuan', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('cuti_tahunan/pengajuan');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function simpan_pengajuan2() {

		if ($this->session->userdata('B_02B') !== null) {
			
			$this->form_validation->set_rules('tgl_awal', 'Tanggal Awal Cuti', 'trim|required');
			$this->form_validation->set_rules('tgl_akhir', 'Tanggal Akhir Cuti', 'trim|required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/pengajuan2/");
				exit;
			} else {
				$sisa = $_POST['sisa'];
				$start_date = new DateTime($_POST['tgl_awal']);
				$end_date = new DateTime($_POST['tgl_akhir']);
				$interval = $start_date->diff($end_date);
				$lama = $interval->days;
				if ($lama <= $sisa){
				$simpan = $this->model_tahunan->simpan_pengajuan(

					$_POST['jenis'], 
					$lama, 
					$_POST['tujuan'], 
					$_POST['tgl_awal'], 
					$_POST['tgl_akhir'], 
					$_POST['alamat'],
					$_POST['rating']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Pengajuan cuti tahunan berhasil dilakukan. Anda dapat mengecek progress setiap saat di menu Lihat Pengajuan - Cuti Tahunan");
					header("Location: ".base_url()."cuti_tahunan/pengajuan2/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Pengajuan cuti tahunan gagal.");
					header("Location: ".base_url()."cuti_tahunan/pengajuan2/");
					exit;
				}
			}else{
				    $this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Sisa Cuti Anda Sudah Habis.");
					header("Location: ".base_url()."cuti_tahunan/pengajuan2/");
					exit;
			}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function simpan_pengajuan() {
		if ($this->session->userdata('B_02B') !== null) {
			$this->form_validation->set_rules('lama', 'Lama Cuti', 'trim|required');
			$this->form_validation->set_rules('tgl_awal', 'Tanggal Awal Cuti', 'trim|required');
			$this->form_validation->set_rules('tgl_akhir', 'Tanggal Akhir Cuti', 'trim|required');
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/pengajuan/");
				exit;
			} else {
				
				$simpan = $this->model_tahunan->simpan_pengajuan(

					$_POST['jenis'], 
					$_POST['lama'],
					$_POST['tujuan'], 
					$_POST['tgl_awal'], 
					$_POST['tgl_akhir'], 
					$_POST['alamat'],
					$_POST['rating']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Pengajuan cuti tahunan berhasil dilakukan. Anda dapat mengecek progress setiap saat di menu Lihat Pengajuan - Cuti Tahunan");
					header("Location: ".base_url()."cuti_tahunan/pengajuan/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Pengajuan cuti tahunan gagal.");
					header("Location: ".base_url()."cuti_tahunan/pengajuan/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function history_saya() {
		if ($this->session->userdata('B_02B') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/history_saya', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('cuti_tahunan/history_saya');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus() {
		if ($this->session->userdata('B_02B') !== null) {
				$hapus = $this->model_tahunan->hapus($_GET['id']);
				if($hapus){
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus Berhasil");
					// header("Location: ".base_url()."cuti_tahunan/history_saya/");
					redirect('cuti_tahunan/history_saya', 'refresh');

				}else{
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus Gagal");
					// header("Location: ".base_url()."cuti_tahunan/history_saya/");
					redirect('cuti_tahunan/history_saya', 'refresh');
				}	
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus2() {
		if ($this->session->userdata('B_02B') !== null) {
				
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Maaf Hapus Gagal, Semua Pengajuan yang sudah di setujui hingga eselon 2 dan umpeg tidak dapat hapus");
					// header("Location: ".base_url()."cuti_tahunan/history_saya/");
					redirect('cuti_tahunan/history_saya', 'refresh');

		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function detail_history_saya() {
		$data['id'] = $this->uri->segment(3, 0);
    	$data['row'] = $this->db->query("SELECT * FROM cuti_tahunan WHERE id = '".$data['id']."';")->row();

		if ($this->session->userdata('B_02B') !== null) {
			$this->load->view('cuti_tahunan/detail_history_saya', $data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('pns/login');
		}
	}

	public function acc_pengajuan() {
		if ($this->session->userdata('B_02B') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/acc_pengajuan', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('cuti_tahunan/acc_pengajuan');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function detail_acc_pengajuan() {
		$id = $this->uri->segment(3, 0);

		if ($this->session->userdata('B_02B') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/detail_acc_pengajuan', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan'),
						"id" => $id
					]);
			} else {
				$this->load->view('cuti_tahunan/detail_acc_pengajuan', ["id"=>$id]);
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function aksi_acc_pengajuan() {
		if ($this->session->userdata('B_02B') !== null) {
			$this->form_validation->set_rules('id', 'ID', 'trim|required');
			$this->form_validation->set_rules('aksi', 'Aksi', 'trim|required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->simpan_aksi_atasan(
					$_POST['id'], 
					$_POST['aksi'], 
					$_POST['alasan']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function aksi_acc_pengajuan4() {
		if ($this->session->userdata('B_02B') !== null) {
			$this->form_validation->set_rules('id', 'ID', 'trim|required');
			$this->form_validation->set_rules('aksi', 'Aksi', 'trim|required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->simpan_aksi_atasan4(
					$_POST['id'], 
					$_POST['aksi'], 
					$_POST['alasan']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function aksi_acc_pengajuan3() {
		if ($this->session->userdata('B_02B') !== null) {
			$this->form_validation->set_rules('id', 'ID', 'trim|required');
			$this->form_validation->set_rules('aksi', 'Aksi', 'trim|required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->simpan_aksi_atasan3(
					$_POST['id'], 
					$_POST['aksi'], 
					$_POST['alasan']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function aksi_acc_pengajuan2() {
		if ($this->session->userdata('B_02B') !== null) {
			$this->form_validation->set_rules('id', 'ID', 'trim|required');
			$this->form_validation->set_rules('aksi', 'Aksi', 'trim|required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->simpan_aksi_atasan2(
					$_POST['id'], 
					$_POST['aksi'], 
					$_POST['alasan']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_acc_pengajuan/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	//untuk mengisi jika acc jabatan eselon 4 kosong
	public function ganti_eselon4(){
		$simpan = $this->model_tahunan->simpan_ganti_eselon4(
					$_POST['id'], 
					$_POST['ganti04']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				}
	}

	//untuk mengisi jika acc jabatan eselon 3 kosong
	public function ganti_eselon3(){
		$simpan = $this->model_tahunan->simpan_ganti_eselon3(
					$_POST['id'], 
					$_POST['ganti03']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				}
	}

	//untuk mengisi jika acc jabatan eselon 3 kosong
	public function ganti_eselon2(){
		$simpan = $this->model_tahunan->simpan_ganti_eselon2(
					$_POST['id'], 
					$_POST['ganti02']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Simpan aksi berhasil");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Simpan aksi gagal.");
					header("Location: ".base_url()."cuti_tahunan/detail_history_saya/".$_POST['id']);
					exit;
				}
	}

	public function cetak_umpeg() {
		if ($this->session->userdata('sinaga-umpeg') == '1') {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/cetak_umpeg', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('cuti_tahunan/cetak_umpeg');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function detail_cetak_umpeg() {
		$id = $this->uri->segment(3, 0);

		if ($this->session->userdata('sinaga-umpeg') == '1') {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('cuti_tahunan/detail_cetak_umpeg', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan'),
						"id" => $id
					]);
			} else {
				$this->load->view('cuti_tahunan/detail_cetak_umpeg', ["id"=>$id]);
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function cetak_sk() {
		$this->load->library('pdf');
		$this->load->library('ciqrcode');

		if ($this->session->userdata('sinaga-umpeg') == '1') {
			$this->form_validation->set_rules('nosk', 'Nomor SK', 'trim|required');
			$this->form_validation->set_rules('tglsk', 'Tgl SK', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form cetak SK gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_cetak_umpeg/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->cetak_sk(
					$_POST['id'], 
					$_POST['nosk'], 
					$_POST['tglsk']
				);
				if ($simpan) {
					
					$params['data'] = base_url().'cuti_tahunan/cek_sk/'.$_POST['id'];
					$params['level'] = 'H';
					$params['size'] = 2;
					$params['savename'] = FCPATH.'qrcode/'.$_POST['id'].'.png';
					$this->ciqrcode->generate($params);

					//$this->load->view('cuti_tahunan/render_sk', ["id"=>$_POST['id']]);

					$this->pdf->load_view('cuti_tahunan/render_sk', ["id" => $_POST['id']], true);
					$this->pdf->render();
					$this->pdf->stream($_POST['id'].".pdf", array("Attachment"=>0));

				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Aksi anda gagal disimpan.");
					header("Location: ".base_url()."cuti_tahunan/detail_cetak_umpeg/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function cetak_sk_ulang() {
		$this->load->library('pdf');
		$this->load->library('ciqrcode');

		if ($this->session->userdata('sinaga-umpeg') == '1') {
			$this->form_validation->set_rules('nosk', 'Nomor SK', 'trim|required');
			$this->form_validation->set_rules('tglsk', 'Tgl SK', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form cetak SK gagal. Cek kembali inputan anda.");
				header("Location: ".base_url()."cuti_tahunan/detail_cetak_umpeg/".$_POST['id']);
				exit;
			} else {
				$simpan = $this->model_tahunan->cetak_sk_ulang(
					$_POST['id'], 
					$_POST['nosk'], 
					$_POST['tglsk']
				);
				if ($simpan) {
					
					$params['data'] = base_url().'cuti_tahunan/cek_sk/'.$_POST['id'];
					$params['level'] = 'H';
					$params['size'] = 2;
					$params['savename'] = FCPATH.'qrcode/'.$_POST['id'].'.png';
					$this->ciqrcode->generate($params);

					//$this->load->view('cuti_tahunan/render_sk', ["id"=>$_POST['id']]);

					$this->pdf->load_view('cuti_tahunan/render_sk', ["id" => $_POST['id']], true);
					$this->pdf->render();
					$this->pdf->stream($_POST['id'].".pdf", array("Attachment"=>0));

				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Aksi anda gagal disimpan.");
					header("Location: ".base_url()."cuti_tahunan/detail_cetak_umpeg/".$_POST['id']);
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function cek_sk() {
		$id = $this->uri->segment(3, 0);
		$this->load->view('cuti_tahunan/hasil_cek_sk', ["id"=>$id]);
	}

}
