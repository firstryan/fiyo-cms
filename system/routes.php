<?php
/** 
 * Adds a custom validation rule using a callback function.
 * @version	2.5 / 3.0
 * @package	Fiyo CMS
 * @copyright	Copyright (C) 2016 Fiyo CMS.
 * @license	GNU/GPL, see LICENSE.
 */




class Route  {    
    public static $req;
    public static $post;
    public static $get;
    public static $request;
    public static $session;
    public static $cookie;
    public static $file;
    public static $called = 0;

    public static $getAll = false;
    public static $lingked = false;
    public static $view = false;
    public static $type = false;
    public static $action = false;
    public static $param = false;

    private static $_instance = null;
    public static $link = null;
    public static $sef = [];
		
	public function __construct(){     
        if(!isset($GLOBALS['REQUEST_HEADER']))
            $GLOBALS['REQUEST_HEADER'] = apache_request_headers(); 
        else         
            $GLOBALS['REQUEST_HEADER'] = array_merge($GLOBALS['REQUEST_HEADER'], apache_request_headers());	
        self::$post = (isset($_POST)) ? $_POST : null;	
        
        self::$get = (isset($_GET)) ? $_GET : null;	
        self::$session =(isset($_SESSION)) ? $_SESSION : null;
        self::$file = (isset($_FILES)) ? $_FILES : null;
        self::$cookie = (isset($_COOKIE)) ? $_COOKIE : null;	
        
        self::$file = false;
    }
    
    public function link($method) {
        if(!self::$getAll ) 
            return false;
        else if(!self::$lingked) {
            $url = url_rematch($method);
            $url = str_replace("$", "", $url);
            $gurl = getBetweenString("{","}", str_replace("$", "", $url));
            $link =  getLink();
            foreach($gurl as $key) {
                $link   = preg_replace('/&?'.$key.'=[^&]*/', "&$key={{$key}}", getLink());
            }             
            
            add_link($url, $link);  
            self::$lingked = true;
        } else {
            return false;
        }
        
    }
    
    static public function get($var,$strip) { 
        $strip('h');        
    }  
   
    
    static public function view($view, $func = false) {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        if(app_param('view') == $view AND !app_param('api')) {
            self::$called++;
            self::$getAll = true;
            self::$view = true;
            array_push(self::$sef, app_param('view'));
            if($func)
                $func();
        }  else {
            self::$getAll = false;
        }
        return self::$_instance;
    }
    
    
    static public function action($view, $func = false) {        
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        if(app_param('action') == $view) {
            self::$called++;
            self::$getAll = true;
            self::$action = true;
            array_push(self::$sef, app_param('action'));
            if($func)
                $func();
        }  else {
            self::$getAll = false;
        }
        return self::$_instance;
    }

    
    static public function type($view, $func = false) { 
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        if(app_param('type') == $view) {
            self::$called++;
            self::$getAll = true;
            self::$action = true;
            array_push(self::$sef, app_param('action'));
            if($func)
                $func();
        }  else {
            self::$getAll = false;
        }
        return self::$_instance;
    }
    
    static public function category($view, $func = false) {
        self::$called++;
        if(app_param('category') == $view) {
            array_push(self::$sef, app_param('category'));
            if($func)
                $func();
            return true;
        }
    }
    


    static public function post($field, $func = false) {
        if(!Req::$post)
            return false;
        if(is_array($field)) {
            foreach($field as $input) {
                in_array($input, Reg::$post);
            }
        }
        if(app_param('action') == $view) {
            if($func)
                $func();
            return true;
        }
    }
    
    static public function param($view, $func = false) {
        if(app_param('param') == $view) {
            if($func)
                $func();
            return true;
        }
    }
    
    
    static public function custom($view, $func = false) {
        if(!is_array($view)) 
            return false;
        foreach($view as $key => $value) {
            if(app_param($key) == $value) {
                $func($a = null);

            }
        }
    }


    
    static public function api($view, $func = true) {
        if(app_param('api') == $view) {
            if($func)
                $func();
            return true;
        }
    }
}

new Route();