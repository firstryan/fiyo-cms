<?php
/** 
 * Adds a custom validation rule using a callback function.
 * @version	2.5 / 3.0
 * @package	Fiyo CMS
 * @copyright	Copyright (C) 2016 Fiyo CMS.
 * @license	GNU/GPL, see LICENSE.
 */


class View {
    public static $called = false;
    
    private static function loadFile($file = null, $status = true) {  
        if(app_param('action') AND !Route::$action)
            return false;
        if(app_param('type') AND !Route::$type)
            return false;
        if(app_param('category') AND !Route::$category)
            return false;
        //check if API is true
        if(!_API_)
            $view = '/view/';
        else
            $view = '/api/';
        
        $folder = app_param('view');
        $type = app_param('type');


        $dir = dirname(debug_backtrace()[1]['file']);
        $inc = $dir . $view . $file . ".php";     
        $app = app_param('app');
        if(file_exists($inc) AND $status == true) {
            self::$called = true;
            include_once($inc);
        } 
        else if(strpos($dir, 'themes') !== true AND file_exists("apps/app_$app/view/" . $file .".php")) {
            self::$called = true;
            include_once("apps/app_$app/view/" . $file .".php");
        }
        else if(file_exists("apps/app_$app/view/$folder/" . $file .".php")) {
            self::$called = true;
            include_once("apps/app_$app/view/$folder/" . $file .".php");
        }
        else if(file_exists("apps/app_$app/view/$folder/" . $file .".php")) {
            self::$called = true;
            include_once("apps/app_$app/view/$folder/" . $file .".php");
        }
        else if(!empty($type) AND file_exists("apps/app_$app/view/type/" . $file .".php")) {
            self::$called = true;
            include_once("apps/app_$app/view/type/" . $file .".php");
        }
        else {            
            page('404', _404_);
        }        
        
    }

    public static function form($file = null, $status = true) {
        self::loadFile($file, $status);
    }

    public static function create($file = null, $status = true) {
        self::loadFile($file, $status);
    }

    public static function edit($file = null, $status = true) {
        self::loadFile($file, $status);
    }
    
    public static function tree() {
        self::loadFile($file, $status);
    }

    public static function list($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }

    public static function default($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }
    
    public static function file($file = null, $status = true) { 
        self::loadFile($file, $status);        
    }
    
    public static function load($file = null, $status = true) { 
        self::loadFile($file, $status);        
    }
    
    public static function item() {
        self::loadFile($file, $status);        
    }
    
    public static function web() {
        self::loadFile($file, $status);        
    }

}



class Api {    
    public static $called = false;
    private static function loadFile($file = null, $status = true) {  
        $dir = dirname(debug_backtrace()[1]['file']);
        $file = $dir . "/api/" . $file . ".php";   
        if(file_exists($file) AND $status == true) {  
            header('Content-Type: application/json');
            self::$called = true;
            include_once($file);
        } else {            
            page('404', _404_);
        }        
        
    }

    public static function form($file = null, $status = true) {
        self::loadFile($file, $status);
    }

    public static function create($file = null, $status = true) {
        self::loadFile($file, $status);
    }

    public static function edit($file = null, $status = true) {
        self::loadFile($file, $status);
    }
    
    public static function tree() {
        self::loadFile($file, $status);
    }

    public static function list($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }

    public static function default($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }
    
    public static function file($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }
    
    public static function load($file = null, $status = true) {  
        self::loadFile($file, $status);        
    }
    
    public static function item() {
        self::loadFile($file, $status);        
    }
    
    public static function web() {
        self::loadFile($file, $status);        
    }
    public static function render($value = null, $param = []) {
        header('Content-Type: application/json');
        if($value) {            
			http_response_code(200);
            $return = ["status" => "success",
                "data" => $value];
            $return =  array_merge($return, $param);
        }
        else    {         
			http_response_code(203);
            $return = ["status" => "error",
                'text' => $value];   	
        }
        echo json_encode($return);    
        die();   
    }
    
    public static function redirect($value = null) {
        header('Content-Type: application/json');
        if($value) {            
			http_response_code(200);
            $return = ["status" => "success",
                "data" => true,
                "message" => "asd",
                "redirect" => url($value)];
        }
        else    {         
			http_response_code(204);
            $return = ["status" => "error",
                "data" => $value];   	
        }
        echo json_encode($return);    
        die();   
    }

}
