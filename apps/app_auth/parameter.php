<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');

$app = [
	"name" 		=> "IOF",
	"table" 	=> FDBPrefix."office",
	"table_item"=> FDBPrefix."office_invoice_item",
	"type"		=> "create",
	"index"		=> "id_invoice",
	"index_item"=> "id_item",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index"=> "invoice_id", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];