<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


// header('Content-Type: application/json');     
defined('_FINDEX_') or die('Access Denied');

$req = Req::$get;

if(!$req) die('Access Denied');

 if($req) {
    
    $prov = Req::get('province');
    $query = DB::table("regencies")
        ->where("province_id = '" .$prov . "'")
        ->get();
        
}

if($query) {
    $return = [
        'status' => 'success',
        'data' => $query,
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => "Yout parameters are invalid!"
    ];
} 

$json = json_encode($return);
$list = json_decode($json);


if($list->status == 'error') die();
$list = $list->data;

echo "<option value=''></option>\n";
foreach($list as $data) {
    echo "<option value='$data->id'>$data->name</option>\n";
}