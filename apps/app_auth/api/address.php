<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


// header('Content-Type: application/json');     
defined('_FINDEX_') or die('Access Denied');


$id     = app_param('id');
$type   = app_param('type');
$parent = app_param('parent');
$filter = app_param('filter');


switch($type) {
    
    case 'province' :
        
        if($id)
            $data = Address::table("provinces")->where("id = '$id'")->get();
        else if($parent)
            $data = Address::table("provinces")->where("id = '$parent'")->get();
        else
            $data = Address::table("provinces")->where("name LIKE '%$filter%'")->get();

    break;
    
    case 'city' :

        if($id)
            $data = Address::table("regencies")->where("id = '$id'")->get();
        else if($parent)
            $data = Address::table("regencies")->where("province_id = '$parent'")->get();
        else
            $data = Address::table("regencies")->where("name LIKE '%$filter%'")->get();

    break;

    case 'distric' :
        
        if($id)
            $data = Address::table("kecamatan")->where("id = '$id'")->get();
        else if($parent)
            $data = Address::table("kecamatan")->where("regency_id = '$parent'")->get();
        else
            $data = Address::table("kecamatan")->where("name LIKE '%$filter%'")->get();

    break;
    
    case 'village' :
        
        if($id)
            $data = Address::table("kelurahan")->where("id = '$id'")->get();
        else if($parent)
            $data = Address::table("kelurahan")->where("kecamatan_id = '$parent'")->get();
        else
            $data = Address::table("kelurahan")->limit(100)->where("name LIKE '%$filter%'")->get();

    break;
    default:
        page(503,'Error');
        die();
    break;
}


if($data) {
    $return = [
        'status' => 'success',
        'data' => $data,
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => "Yout parameters are invalid!"
    ];
} 

echo json_encode($return);