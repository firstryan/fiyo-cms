<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

Route::view('', function() {	
	Route::action('', function() {
		View::load('list');
	});	
	Route::action('create', function() {
		View::load('form');
	});	
	Route::action('edit', function() {
		View::load('form');
	});	
	Route::action('delete', function() {
		Auth::delete(); 
	});	
});

Route::api('address', function() {		
	Api::load('address');
});


