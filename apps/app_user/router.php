<?php 
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');


// Route::view('', function() {
// 	Route::action('', function() {
// 		View::list('default');
// 	});
	
// 	Route::action('create', function() {
// 		View::create('form');
// 	});
	
// 	Route::action('edit', function() {
// 		View::edit('form');
// 	});
	
// 	Route::action('delete', function() {
// 		View::delete('klub');
// 	});
// });



$view = app_param('view');
$key = @$_GET['key'];
$res = @$_GET['res'];
switch($view)
{
	case 'logout':
		View::default("logout");
	break;
	case 'register':		
		View::default("register");
	break;
	case 'login':			
		View::default("login");
	break;
	case 'edit':
		View::default("edit");
	break;
	case 'lost_password':
		View::default("forgot");
	break;
	default :
		if(!empty($res))
			View::default("reset");
		elseif(!empty($key))
			View::default("activation");
		else 
			View::default("profile");
	break;	
}
