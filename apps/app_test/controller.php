<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


defined('_FINDEX_') or die('Access Denied');

Class IOF {

	public static function list() {					
		return Model::listObject();

	}
	
	public static function default() {
		$list = Model::item(1);		
		return $list;
	}

	public static function form() {
		new Address();
		if(Model::patch()) {
			notice(Model::$message);
			refresh();
		} 
		else if(Model::add()) {
			notice(Model::$message);
			redirect("?app={app}&view={view}&type={type}&action=edit&id=". Model::$last_id);
		} 
		else {
			notice(Model::$message);
		}

		
		//set model if requirement is valid
		if(app_param('id') AND app_param('action') == 'edit') {
			return Model::item(app_param('id'));
		}

		//set model for crate action
		else if(app_param('action') == 'create' AND app_param('id') === null) {
			return true;
		} 
		//block for any
		else {
			http_response_code(404);	
		}
			
	}

}



