<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


$post = Req::$post;
if($post) {
    //Check CSRF Token AND $data
    $data = Input::$post;
    if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

    //setup saving type
    if(!empty($data[$app['index']]))
        $app['type'] = "edit";
    else
        $app['type'] = "add";

    $user = [
        "user" => $data['email_anggota'],
        "name" => $data['nama_anggota'],
        "email" => $data['email_anggota'],
        "level" => 5,
        "status" => 1,
    ];
    
    $notice = Status_Fail;
    $query  = false;
    if(User::register($user)) {
        $query = DB::save(array_merge(["data" => $data], $app));  
    } else {
        $notice = "Data Anggota sudah pernah didaftarkan";
    }

    //setup return value
    if($query) {
        $return = [
            'status' => 'success',
            'text' => Status_Saved,
            'id' => DB::$last_id
        ];
    }
    else {
        $return = [
            'status' => 'error',
            'text' => DB::$error
        ];
    } 
}


$post = Req::$post;
if($post)
    if($return['status'] == 'success') {
        notice($return);
        Req::setSession('TMP_NOTICE_WELCOME', $post);
        redirect(url('?app={app}&view={view}&type={type}&welcome=true'));

    } else {
        $return['close'] = true;
        notice($return);

    }