<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');

$post = Req::$post;
if($post)
    if($return['status'] == 'success') {
        notice($return);
        Req::setSession('TMP_NOTICE_WELCOME', $post);
        redirect(url('?app={app}&view={view}&type={type}&welcome=true'));

    } else {
        $return['close'] = true;
        notice($return);

    }