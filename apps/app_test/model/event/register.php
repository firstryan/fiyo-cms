<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');


//setup fillable field
$app = [
	"name" 		=> "IOF",
	"table" 	=> FDBPrefix."iof_event",
	"index"		=> "id",
	"validator"	=> [],
];

//setup fillable field
$app['fillable'] = [
    'nama_event', 
    'tanggal_booking', 
    'tanggal_pelaksanaan', 
    'jenis', 
    'kelas', 
    'klub_id', 
    'alamat', 
    'koordinat', 
    'kota', 
    'provinsi', 
    'bukti_bayar', 
    'ttd', 
    'file', 
    'status', 
];

