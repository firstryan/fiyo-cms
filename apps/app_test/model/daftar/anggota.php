<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');


//setup fillable field
$app = [
	"name" 		=> "IOF",
	"table" 	=> FDBPrefix."iof_anggota",
	"index"		=> "id_anggota",
	"validator"	=> [],
];

//setup fillable field
$app['fillable'] = [
    'id_anggota', 
    'nama_anggota', 
    'email_anggota', 
    'no_ktp', 
    'tanggal_lahir', 
    'tempat_lahir', 
    'jenis_kelamin', 
    'golongan_darah', 
    'alamat_anggota', 
    'foto_tkp', 
    'foto_kta', 
    'klub_id', 
    'status' => 1, 
    'created_at' => date("Y-m-d H:i:s"), 

];

$password = "MemberIOFJateng";

$post = Req::$post;
if($post) {
    //Check CSRF Token AND $data
    $data = Input::$post;
    if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

    //setup saving type
    if(!empty($data[$app['index']]))
        $app['type'] = "edit";
    else
        $app['type'] = "add";

    $user = [
        "user" => $data['email_anggota'],
        "name" => $data['nama_anggota'],
        "email" => $data['email_anggota'],
        "password" => $password,
        "level" => 5,
        "status" => 1,
    ];
    
    $notice = Status_Fail;
    $query  = false;
    if(User::register($user)) {
        $query = DB::save(array_merge(["data" => $data], $app));  
    } else {
        $notice = "Data Anggota sudah pernah didaftarkan";
    }

    //setup return value
    if($query) {
        $return = [
            'status' => 'success',
            'text' => Status_Saved,
            'id' => DB::$last_id
        ];
    }
    else {
        $return = [
            'status' => 'error',
            'text' => DB::$error
        ];
    } 
}