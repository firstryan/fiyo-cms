<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');


//setup fillable field
$app = [
	"name" 		=> "IOF",
	"table" 	=> FDBPrefix."iof_klub",
	"index"		=> "id_klub",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index"=> "", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];

//setup fillable field
$app['fillable'] = [
    'alamat_klub', 
    'email_klub', 
    'telp_klub', 
    'nama_klub', 
    'pengda', 
    'pengcab', 
    'tanggal_berdiri', 
    'bukti_registrasi', 
];

$password = "MemberIOFJateng";

$post   = Req::$post;
$query  = false;
if($post) {

    //Check CSRF Token AND $data
    $data = Input::$post;
    if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

    //setup saving type
    if(!empty($data[$app['index']]))
        $app['type'] = "edit";
    else
        $app['type'] = "add";

    $user = [
        "user" => $data['email_klub'],
        "name" => $data['nama_klub'],
        "email" => $data['email_klub'],
        "password" => $password,
        "level" => 4,
        "status" => 1,
    ];
    
    $notice = Status_Fail;
    if(User::register($user)) {
        $query = IOF::save(array_merge(["data" => $data], $app));        
    } else {
        $notice = "Data Klub atau penanggung jawab sudah pernah didaftarkan";
    }

    //setup query saving

    //setup return value
    if($query) {
        $return = [
            'status' => 'success',
            'text' => Status_Saved,
            'id' => DB::$last_id
        ];
    }
    else {
        $return = [
            'status' => 'error',
            'text' => DB::$error
        ];
    } 

}