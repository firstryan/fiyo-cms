<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/


class IOFModel {
	public static $message = null;
	protected static $status = false;
	
	public static function save($preset = ['no_log' => false]) {	
		if(isset($preset['table']))
			$table = $preset['table'];
		else {
			self::$message = Status_Fail;	
			return false;
		}

		$fillable = $preset['fillable'];
		$data = $preset['data'];

		$fillme =  $data;
		$data = array_query($data, $fillable);
		
		$data =  array_intersect_key($data, $fillme);
		
		
		//validationmode
		$valid = true;
		if(isset($preset['validator'])) {
			$valid = false;
			$valid = Validator::is_valid($data, $preset['validator']);
		}
		

		//query		
		if($valid === true) {			
			if(isset($preset['type'])) {

				//Set flag query as false
				$qr = false;
				if(in_array($preset['type'], ['insert','add','create'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["created_at" => date("Y-m-d H:i:s")]);

					$qr = Database::table($table)->insert($data);
				return true;
				} else if(in_array($preset['type'], ['edit','update'])) {
					if(isset($preset['no_log']) AND !$preset['no_log'])
					$data = array_merge($data, ["updated_at" => date("Y-m-d H:i:s")]);

					$qr = Database::table($table)->where("$preset[index] = $preset[reff]")->update($data);
				}

				//if $qr is true
				if($qr) {
					self::$message = Status_Saved;	
					return true;		
				}
			}
		}	
		else {		
			self::$message = $valid; 		
			return false;
		}

		self::$message = Status_Fail;	
		return false;
	}

	public static function getKlubData($id = null) {
		$table = FDBPrefix."iof_klub";

		if($id) {
			$qr = DB::table($table)
				->where("id_klub = $id")
				->limit(1)
				->get();
		}
		else {
			$email = USER_EMAIL;
			
			$qr = DB::table($table)
				->where("email_klub= '$email'")
				->limit(1)
				->get();
		}
			

		//query			
		if($qr) {
			$qr = $qr[0];
			self::$message = Status_Saved;	
			return $qr;		
		}
		else {
			self::$message = Status_Fail;	
			return false;
		}
	}
}