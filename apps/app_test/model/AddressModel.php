<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/


class Address extends Model {

    public static $table = 'dummy';
    public static $index = 'id';
    public static $fillable = [
        'name',
        'date'
    ];
    public static $validator = [
        'name' => 'required',
        'date'
    ];


    public function __construct () {
        parent::$table = self::$table;   
        parent::$index = self::$index; 
        parent::$field = self::$field; 
        parent::$fillable = self::$fillable;
        parent::$validator = self::$validator;
    }

    public static function field() {        
        parent::$table = self::$table;  
        return self::$fillable;
    }
    

    public static function view() {            
        parent::$limit = 1;    
        parent::$orderBy = 'id ASC';  
        parent::$where = "name = 'site_name'";
        
        return Model::listJson();
    }    
    
    public static function data($key, $value = 'value') {        
        $model =  Model::find("name = '$key'");
        return $model;        
    }
    
    public static function patch($preset = [], $reff = 'id') { 
        return Model::patch($preset, $reff);
    }
    
    public static function add($preset = []) {
        return Model::add($preset);
    }

    public static function delete($preset = []) { 
        return Model::add($preset);
    }


}

