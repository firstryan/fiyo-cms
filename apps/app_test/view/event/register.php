<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');

if(app_param('welcome') AND Req::session('TMP_NOTICE_WELCOME')) :
    include_once('register_booked.php');
else :
    notice();
?>

<div class="row">
    <div class="col-lg-9  ">
        <h2 style="margin: 0">Lembar Pengajuan Event 
        
        </h2>
    </div>
</div>

<hr>
<?php

$klub = IOF::getKlubData();

?>
<?=Form::model(false,["class"=>"validate"]);?>

<div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Event</h3>

        
        <div class="form-group">            
            <?= Form::text('nama_event', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama Acara"); ?>
        </div>
        
      
        <div class="form-group">            
            <?= Form::date('tanggal_booking', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Tanggal Booking"); ?>
        </div>

        <div class="form-group">  
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Jenis Kendaraan</label>
                <?php

                   $kelas = ['Mobil' => "Mobil",
                            'Motor' => "Motor"]

                ?>
                <?= Form::select('kelas', $kelas, '', ["class" => "form-control kendaraan", "label-class" => "bmd-label-floating" ,"required"], "Jenis Kendaraan"); ?>
            
        </div>


        <div class="form-group">  
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Jenis Event</label>
                <?php
                   $kelas = []
                ?>
                <?= Form::select('jenis', $kelas, '', ["class" => "form-control jenis", "label-class" => "bmd-label-floating" ,"required", "readonly"], "Jenis Event"); ?>
        </div>


        <p> &nbsp;</p>
        <h3>Lokasi</h3>


        <?php
            $sql = DB::table("regencies")->where("province_id = $klub[pengda]")->get();

            foreach($sql as $k=>$v) {
                if($v['id'] != $klub['pengcab'])
                    $reg[$v['id']] = $v['name']."{disabled='disabled'}";	
                else
                    $reg[$v['id']] = $v['name']."{selected='selected'}";				
            }
        ?>   
        <div class="form-group">  
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Kabupaten / Kota</label>
                <?php

                   $kelas = []

                ?>
                <?= Form::select('kota', $reg, '', ["class" => "form-control kota", "label-class" => "bmd-label-floating" ,"required" ,"disasbled"], "Pilih Kabupaten/Kota"); ?>
        </div>
        

        <?php
            $reg = [];
            $sql = DB::table("kecamatan")->where("regency_id = 0")->get();
            foreach($sql as $k=>$v) {
                if($v['id'] != $klub['pengcab'])
                    $reg[$v['id']] = $v['name']."{disabled='disabled'}";	
                else
                    $reg[$v['id']] = $v['name']."{selected='selected'}";				
            }
        ?>   
        
        <div class="form-group">  
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Kecamatan</label>
                <?= Form::select('kecamatan', $reg, '', ["class" => "form-control kecamatan", "label-class" => "bmd-label-floating" ,"required" ,"disasbled"], "Pilih Kecamatan"); ?>
        </div>

        

        <?php
            $reg = [];
            $sql = DB::table("kecamatan")->where("regency_id = 0")->get();
            foreach($sql as $k=>$v) {
                if($v['id'] != $klub['pengcab'])
                    $reg[$v['id']] = $v['name']."{disabled='disabled'}";	
                else
                    $reg[$v['id']] = $v['name']."{selected='selected'}";				
            }
        ?>   
        <div class="form-group">  
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Kelurahan</label>
                <?= Form::select('kelurhan', $reg, '', ["class" => "form-control kelurahan", "label-class" => "bmd-label-floating" ,"required" ,"disasbled"], "Pilih Kelurahan"); ?>
        </div>



        

        <div class="form-group">
            <?=Form::submit('Booking Event', ["class" => "btn-success align-right"]); ?>
        </div>
    </div>

</div>
<?=Form::close();?>
<script>
    $(function () {
        $(".kendaraan").change(function () {
            v = $(this).val();
            
            if(v == 'Mobil')
                var newOption = $('<option value="Event">Event</option><option value="Latian Bersama">Latian Bersama</option><option value="Bakti Sosial">Bakti Sosial</option>');
            else
                var newOption = $('<option value="Kelas A">Kelas A</option><option value="Kelas B">Kelas B</option><option value="Kelas C">Kelas C</option><option value="Kelas D">Kelas D</option>');
           
            $('.jenis').html(newOption).val("<?=Req::post('jenis');?>").trigger("chosen:updated");
        }).change();

        $(".kota").change(function () {
            v = $(this).val();
            logs("asd");
            if(v == 'Mobil')
                var newOption = $('<option value="Event">Event</option><option value="Latian Bersama">Latian Bersama</option><option value="Bakti Sosial">Bakti Sosial</option>');
            else
                var newOption = $('<option value="Kelas A">Kelas A</option><option value="Kelas B">Kelas B</option><option value="Kelas C">Kelas C</option><option value="Kelas D">Kelas D</option>');
           
            $('.kecamatan').html(newOption).val("<?=Req::post('kota');?>").trigger("chosen:updated");
        })

        
        $(".kecamatan").change(function () {
            v = $(this).val();
            logs("asd");
            if(v == 'Mobil')
                var newOption = $('<option value="Event">Event</option><option value="Latian Bersama">Latian Bersama</option><option value="Bakti Sosial">Bakti Sosial</option>');
            else
                var newOption = $('<option value="Kelas A">Kelas A</option><option value="Kelas B">Kelas B</option><option value="Kelas C">Kelas C</option><option value="Kelas D">Kelas D</option>');
           
            $('.kelurahan').html(newOption).val("<?=Req::post('kecamatan');?>").trigger("chosen:updated");
        })
    });

</script>








<?php  endif; ?>