<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');

?>

<h3>Selamat, Booking tanggal event telah berhasil!</h3>
<p>Silahkan cek email untuk mendapatkan informasi lebih lengkap.<br>
</p>
<p><a href="?app=user" class="btn btn-default">Kembali ke profil Klub</a></p>