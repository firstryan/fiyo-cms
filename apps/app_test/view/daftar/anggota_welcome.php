<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');

?>

<h3>Selamat, pendaftaran Anggota telah berhasil!</h3>
<p>Silahkan cek email untuk mendapatkan informasi lebih lengkap.<br>
atau anda mencoba login melalui <u><a href="<?=url('?app=user&view=login');?>">link ini</a></u>.
</p>