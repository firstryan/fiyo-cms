<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');


if(app_param('welcome') AND Req::session('TMP_NOTICE_WELCOME')) :
    include_once('klub_welcome.php');
else :
    notice();
?>

<h2>Form Pendaftaran Klub 
       <a href="<?=url('?app=iof&view=daftar&type=anggota');?>" class="btn btn-danger float-right">  <i class="fa fa-file-text"></i> &nbsp; Pendaftaran Anggota  </a></h2>
<hr>


<?=Form::model(false,["class"=>"validate"]);?>
<div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Klub</h3>



        <div class="form-group">            
            <?= Form::text('nama_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama Klub"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('email_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Email Klub"); ?>
        </div>
        


        <div class="form-group">            
                <?= Form::text('telp_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "No. Telp / No. WA"); ?>
        </div>

        <div class="form-group">            
                <?= Form::text('alamat_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "Alamat"); ?>
        </div>

        <div class="form-group">        
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Pengda</label>
            <?php

                $sql = DB::table("provinces")->get();

                foreach($sql as $k=>$v) {
                    if($v['id'] != 33)
                        $prov[$v['id']] = $v['name']."{disabled='disabled'}";	
                    else
                        $prov[$v['id']] = $v['name'];				
                }
                
            ?>    
            <?= Form::select('pengda', $prov, '',  
                [
                    "class" => "form-control pengda", 
                    "label-class" => "bmd-label-floating", 
                    "required", 
                    "onChange" => "relatedSelectRegency(this, '.pengcab')"
                ], 
                "Pilih Pengurus Daerah ... "); ?>
        </div>
        
        <div class="form-group">    
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Pengcab</label>        
            <?= Form::select('pengcab', [], '', ["class" => "form-control pengcab", "label-class" => "bmd-label-floating", "required"], "Pilih Pengurus Cabang ..."); ?>
        </div>


        <div class="form-group">
            <?=Form::submit('Daftar Sekarang', ["class" => "btn-success align-right"]); ?>
        </div>
    </div>

</div>
<?=Form::close();?>


<script>
    function relatedSelectRegency(e, target) {
        t = $(e);         
        $.post("?app=iof&api=regency&province=" + t.val()).done(function (result) {  
            $(target).html(result).trigger("chosen:updated");
        });
    }
</script>

<?php  endif; ?>