<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');



$post = Req::$post;
if($post) {
    $notice = json_encode($post);
    notice("success",$notice);
    refresh();
    // dd($post);
}

notice();


$list = ["1" => "Pilihan Pertama","2" => "Pilihan Kedua"];


?>

<h2>Form Pendaftaran Klub 
       <a href="<?=url('?app=iof&view=daftar&type=anggota');?>" class="btn btn-danger float-right">  <i class="fa fa-file-text"></i> &nbsp; Pendaftaran Anggota  </a></h2>
<hr>
<?=Form::model(false,["class"=>"validate"]);?>








<div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Penanggung Jawab</h3>

        
        <div class="form-group">            
            <?= Form::text('email', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama sesuai KTP"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('no_ktp', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nomor KTP"); ?>
        </div>
        
        
        <div class="row">
            <div class="col-lg-4">
                
                <div class="form-group">            
                    <?= Form::date('tgl_lahir', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Tanggal Lahir"); ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">            
                    <?= Form::text('tmp_lahir', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Tempat Lahir"); ?>
                </div>
            </div>
        </div>

        
        
        <div class="form-group">        
            <label for="nama" class="bmd-label-static bmd-label-floating">Jenis Kelamin</label>
            <select class="selectpicker " data-style="select-with-transition" title="Single Select" data-size="7">
                <option disabled selected>Pilih jenis kelamin</option>
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        
        <div class="form-group">  
            <label for="nama" class="bmd-label-static bmd-label-floating">Golongan Darah</label>
                <?php
                    $gol = [
                        "A" => "Gol. A",
                        "B" => "Gol. B",
                        "AB" => "Gol. AB",
                        "O" => "Gol. O",
                        "-" => "Belum tahu",
                    ]
                
                ?>
                <?= Form::select('gol_darah', $gol, '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pilih golongan darah"); ?>
            
        </div>


        <div class="form-group">            
                <?= Form::text('alamat', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "Alamat Penanggung jawab"); ?>
        </div>


        
        <div class="row">
            <div class="col-lg-6">               
                <div class="form-group">
                    <?= Form::fileimage('foto_ktp', '',["class" => "form-control full-image fileimage-75", "placeholder" => "Foto KTP"]); ?>
                </div>
            </div>
            <div class="col-lg-6">        
                <div class="form-group">
                    <?= Form::fileimage('foto_kta', '',["class" => "form-control full-image fileimage-75", "placeholder" => "Foto KTA"]); ?>
                </div>
            </div>
        </div>



    </div>
</div>
<br><br>

 <div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Klub</h3>


        <div class="form-group">            
            <?= Form::text('nama', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama Klub"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('email', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Email Klub"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('pengda', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pengda "); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('pengcab', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pengcab "); ?>
        </div>

        <div class="form-group">            
            <?= Form::checkbox('pengcab', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pengcab "); ?>
        </div>

        
      



        <div class="form-group">
            <?=Form::submit('Daftar Sekarang', ["class" => "btn-success align-right"]); ?>
        </div>
    </div>

</div>
<?=Form::close();?>