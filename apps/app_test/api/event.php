<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


// header('Content-Type: application/json');     
defined('_FINDEX_') or die('Access Denied');

$id = Req::get('id');
$query = $id;


if($query) {
    $return = [
        'status' => 'success',
        'data' => $query,
    ];
}
else {
    $return = [
        'status' => 'error',
        'text' => "Yout parameters are invalid!"
    ];
} 

echo json_encode($return);