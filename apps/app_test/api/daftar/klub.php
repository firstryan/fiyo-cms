<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

   
defined('_FINDEX_') or die('Access Denied');


//setup fillable field
$app = [
	"name" 		=> "IOF",
	"table" 	=> FDBPrefix."iof_klub",
	"index"		=> "id_klub",
	"reff"		=> "",
	"reff_item"	=> "",
	"reff_index"=> "", //for relation
	"fillable"	=> [],
	"validator"	=> [],
];

//setup fillable field
$app['fillable'] = [
    'nama_personal', 
    'ktp_personal', 
    'tempat_lahir', 
    'tanggal_lahir', 
    'kelamin_personal', 
    'golongan_darah', 
    'alamat_personal', 
    'provinsi_personal', 
    'telp_personal', 
    'pengda', 
    'pengcab', 
    'nama', 
    'email', 
    'tanggal_daftar', 
    'tanggal_berdiri', 
    'foto_ktp', 
    'foto_kta', 
    'bukti_registrasi', 

];

$password = "MemberIOFJateng";

$post   = Req::$post;
$query  = false;
if($post) {

    //Check CSRF Token AND $data
    $data = Input::$post;
    if(!$data OR $data['_token'] != USER_TOKEN) die('Access Denied');

    //setup saving type
    if(!empty($data[$app['index']]))
        $app['type'] = "edit";
    else
        $app['type'] = "add";

    $user = [
        "user" => $data['email'],
        "name" => $data['nama'],
        "email" => $data['email'],
        "password" => $password,
        "level" => 4,
        "status" => 1,
    ];
    
    $notice = Status_Fail;
    if(User::register($user)) {
        $query = IOF::save(array_merge(["data" => $data], $app));        
    } else {
        $notice = "Data Klub atau penanggung jawab sudah pernah didaftarkan";
    }

    //setup query saving

    //setup return value
    if($query) {
        $return = [
            'status' => 'success',
            'text' => Status_Saved,
            'id' => DB::$last_id
        ];
    }
    else {
        $return = [
            'status' => 'error',
            'text' => $notice
        ];
    } 


    // refresh();
    // echo json_encode($return);

}