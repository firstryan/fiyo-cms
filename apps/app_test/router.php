<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/



Route::view('', function() {	
	redirect('?app={apps}&view=klub&action=daftar');
});

Route::view('klub', function() {	
	View::list('daftar/klub');

});




Route::view('daftar', function() {	
	/* type route */
	Route::type('klub', function() {
		View::list('daftar/klub');
	});

	Route::type('anggota', function() {
		View::list('daftar/anggota');
	});



	/* action route */
	Route::action('create', function() {
		View::create('form');
	});
	
	Route::action('edit', function() {
		View::edit('form');
	});
	
	Route::action('delete', function() {
		View::delete('klub');
	});
});



Route::view('event', function() {	
	Route::type('register', function() {
		View::load('event/register');
	});	
	Route::type('test', function() {
		Route::action('create', function() {
			View::load('event/test');
		});	
		Route::action('edit', function() {
			View::load('event/test');
		});	
		Route::type('test', function() {
			View::load('event/list');
		});	
	});	
});


Route::api('address', function() {	
	Route::post(function () {		
		Api::load('address');
	});
});


