<?php 
/**
* @version		1.5.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

defined('_FINDEX_') or die('Access Denied');
?>



<script>
	$(function() {
		$('a.view-cart').click(function(e) {		
			// hide scrollbars!
			$('<div id="overlay"></div>')
			.css('top', '0')
			.css('opacity', '0')
			.animate({'opacity': '0.7'})
			.appendTo('body');
			var height = ($(window).height() - $('#lightbox').height()) / 4;
			var width = ($(window).width() - 850) / 2;
			$('<div id="lightbox"></div>')
			.css({
			'top': '25%',
			'left': width
			})
			.appendTo('body');
			
			$('<span class="closelight" title="Close">x</span><div class="cartbox">Loading...</div>')
			.load(function() {
				positionLightboxImage();
			})
			.appendTo('#lightbox');
			
			$.ajax({
				type: 'POST',
				url: '<?php echo FUrl; ?>/apps/app_store/controller/cart.php',
				data: '&do=view',
				cache: false,
				async: false,
				success: function(result) {
					$('.cartbox').html(result);
				},
				error: function(result) {
				
				}
			});
			
			$('.closelight')
			.click(function() {
				removeLightbox();
			})
			return false;
		});
		
			
	function removeLightbox() {
		$('#overlay, #lightbox')
		.fadeOut('slow', function() {
		$(this).remove();
		$('body').css('overflow-y', 'auto'); // show scrollbars!
		});
	}
	});
</script>