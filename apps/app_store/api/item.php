<?php
/**
* @version		1.5.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	Article Rating
**/

session_start();
define('_FINDEX_',1);

if(!isset($_POST['id']) AND $_POST['do'] !== 'view')
	header('location:../../../');
else {
	require_once ('../../../system/jscore.php'); 
	require_once ('../sys_store.php'); 
	
	$id = $_POST['id'];
	$alert = '';


$product = new Store;
$product -> product($id);
$title = @$product -> title;
if(isset($title)) :
	$category 	= $product -> category;
	$catlink	= $product -> catlink;
	$brand 		= $product -> brand;
	$title		= $product -> title;
	$stock		= $product -> stock;
	$price		= $product -> price;
	$discount	= $product -> discount;
	$save		= $product -> save;
	$hits 		= $product -> hits;
	$image		= $product -> images;
	$thumb		= $product -> thumbs;
	$desc		= $product -> desc;
	$spec		= $product -> spec;
	$review		= $product -> review;
	$model		= $product -> model;
?>	
<div class="product-item">
	<div>
	<a href='#' class="add right add-to-cart2" rel="<?php echo $_POST['id']; ?>"> Add to Cart </a>
	<h1 class="title"><?php echo "$title"; ?></h1>
	<i class="title">Price : <?php echo "$price"; ?></i>
	</div>
	<div id="product-image" class="product-image">
	
		<!-- $product-slides -->
		<div id="product-slides">
		<?php for($i = 1; $i <= count($image); $i++) : ?>
			<div class="item-slide img<?php echo $i; ?>" <?php if(!isset($active)) { $active = true;  echo "style='display:block'"; }?>>
				<a rel="<?php echo "$image[$i]"; ?>" class="lightbox">
					<img src="<?php echo "$image[$i]"; ?>" alt="Image <?php echo $model; ?>"><span class="overlay"></span>
			   </a>			   
			</div> 
		<?php endfor; ?>
		</div> <!-- #product-slides -->
		
		
		<!-- $product-thumbs -->
		<div id="product-thumbs">		
		<?php for($i = 1; $i <= count($thumb); $i++) : ?>
			<a rel="<?php echo $i; ?>">			
				<img src="<?php echo $thumb[$i]; ?>" alt="">
				<span class="overlay"></span>
			</a>
		<?php endfor; ?>
		</div> <!-- #product-thumbs -->
		
	</div> <!-- #product-slider -->

	<div class="product-detail">
		<div>
			<span class="post-meta"><?php echo "$brand"; ?></span>
			<div class="clearfix">
				<?php echo $desc; ?>
				
			</div>		
		</div>
	</div>
</div>
<div class="clearfix"></div>
<?php
elseif(!isset($product -> memberonly)) :
	echo "<h3>Produk tidak ditemukan atau tidak dapat diperkenankan.</h3>Hubungi administrator jika anda ingin melihat produk yang anda maksud!"; 
endif;

?>

<script>
	$(function() {
		$('#product-thumbs a').click(function() {
			$('#product-thumbs a').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('rel');
			$('.item-slide').hide();
			$('.img'+target).show();
		});
		
		$('.product-info h3').click(function() {
			$('.product-info h3').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('tab-target');
			$('.target-tab').hide();
			$('#'+target).show();
		});	
		
		$('.delete-item-cart').click(function(e) {
			return false;
		});
		$('a.add-to-cart2').click(function(e) {	
			var id = $(this).attr('rel');
			$.ajax({
				type: 'POST',
				url: '<?php echo FUrl; ?>apps/app_store/controller/cart2.php',
				data: 'id='+id+'&do=add&url=<?php echo FUrl; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.cartbox .chart').html(result);
				},
				error: function(result) {				
				}
			});

			
			$('.closelight')
			.click(function() {
				removeLightbox();
			})
			return false;
		});
		
		
	function positionLightboxImage() {
		var top = ($(window).height() - $('#lightbox').height()) / 2.5;
		var left = ($(window).width() - $('#lightbox').width()) / 2;
		$('#lightbox')
		.css({
		'top': top,
		'left': left
		})
		.fadeIn();
	}
			
	function removeLightbox() {
		$('#overlay, #lightbox')
		.fadeOut('slow', function() {
		$(this).remove();
		$('body').css('overflow-y', 'auto'); // show scrollbars!
		});
	}
	
	
	
	
		function removeNotice() {
			setTimeout(function(){
				$('.notif .notice').fadeOut(1000, function() {
				});				
			}, 5000);	
		}
		removeNotice();
		$('.delete-item-cart').click(function(e) {
			var id = $(this).attr('rel');
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart2.php',
				data: 'id='+id +'&do=remove&&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.cartbox .chart').html(result);
				},
				error: function(result) {				
				}
			});
			return false;
		});
		
		$('.item-plus').click(function(e) {
			var ini = $(this);
			var id = $(this).prev().attr('rel');
			var st = $(this).prev().attr('data-stock');
			
			var qty = ini.prev('.item-qty');	
			var qts = parseInt(qty.html());		
			var min = $(qty).prev();
			min.show();
			if(qts >= st-1) {
				$(this).hide();
			}
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart2.php',
				data: 'id='+id +'&do=add&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.notice').remove();
					var qty = ini.prev('.item-qty');
					var qts = parseInt(qty.html());
					if(qts < st) {
						var val = $(this).parent().next();
						$(qty).html(qts + 1);
						
					} else {					
						ini.hide();
						$('.cartbox .notif').prepend('<div class="notice error">Stok barang tidak tersedia.</div>').fadeIn();	
					}				
					removeNotice(); 
					$('.cartbox').html(result);	
				},
				error: function(result) {
				
				}
			});
			return false;
		});
		
		$('.item-min').click(function(e) {
			var ini = this;
			var id = $(this).next().attr('rel');
			var st = $(this).next().attr('data-stock');
			var qty = $(ini).next('.item-qty');
			var qts = parseInt(qty.html());
			if (qts < 3) $(this).hide();
			$('.item-plus').show();		
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart.php',
				data: 'id='+id +'&do=delete&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.notice').remove();
					if(qts < 2) {					
						$('.cartbox .notif').prepend('<div class="notice error">Minimal satu item.</div>').fadeIn();					
					} else {
						$(qty).html(qts - 1);		
					removeNotice(); 
					$('.cartbox').html(result);		
						
					}							
				},
				error: function(result) {				
				}
			});
			return false;
		});
	});
</script>
<?php

}
?>