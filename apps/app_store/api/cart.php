<?php
/**
* @version		1.5.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	Article Rating
**/

session_start();
define('_FINDEX_',1);

if(!isset($_POST['id']) AND $_POST['do'] !== 'view')
	header('location:../../../');
else {
	require_once ('../../../system/jscore.php'); 
	if($_POST['do'] == 'view') $_POST['id'] = $_POST['url']= null;
	$id = $_POST['id'];
	$alert = '';
	function productInfo($value, $id = null) {
		if(empty($id) AND app_param('view') == 'product')
		$id = app_param('id');
		return oneQuery('store_product','id',"$id","$value");
	}
		
	function brandInfo($value, $id = null) {
		if(empty($id) AND app_param('view') == 'brand')
		$id = app_param('id');
		return oneQuery('store_brand','id',"$id","$value");
	}
		
	function storeConfig($value) {
		return oneQuery('store_config','name',"'$value'",'value');
	}
					
	if(isset($_POST['do'])) {
		if($_POST['do']=='delete'){
			$flag = null;
			$count = count(@$_SESSION['STORE_CART']);
			$qts = 0;
			for($i = 0; $i < $count; $i++) {
				if(@$_SESSION['STORE_CART'][$i][0] == $id AND @$_SESSION['STORE_CART'][$i][1] > 1) {
					if($_SESSION['STORE_CART'][$i][1] < 2) {
						$_SESSION['STORE_CART'][$i] = null;
					}
					@$_SESSION['STORE_CART'][$i][1]--;
					$alert = alert('info','Keranjang belanja berhasil dikurangi.',true);
				}				
				$qty = @$_SESSION['STORE_CART'][$i][1];
				if($qty != 0) $flag = true;
			}
			if(!$flag)				
				unset($_SESSION['STORE_CART']);
		}
		else if($_POST['do']=='remove'){
			$flag = null;
			$count = count(@$_SESSION['STORE_CART']);
			$qts = 0;
			for($i = 0; $i < $count; $i++) {
				if(@$_SESSION['STORE_CART'][$i][0] == $id) { 
                                        $name = productInfo('name',$id);
					$_SESSION['STORE_CART'][$i] = null;
					$alert = alert('info',"<b>$name</b> berhasil dihapus.",true);
				}				
				$qty = @$_SESSION['STORE_CART'][$i][1];
				if($qty != 0) $flag = true;
			}
			if(!$flag)				
				unset($_SESSION['STORE_CART']);
		}
		else if($_POST['do']=='add'){
			// get rating			
			$flag  = false;
			$qts = productInfo('qty',$id);
			$name = productInfo('name',$id);
			if($qts == -1) $qts = 99999;
			if(!isset($_SESSION['STORE_CART'])) {
				if($qts != 0) {
					
					$_SESSION['STORE_CART'] = array(array($id,1));
				} else {				
					$alert = alert('error',"Stok barang tidak terssedia.",true);
				}			
			} else {
				$count = count($_SESSION['STORE_CART']);
				for($i = 0; $i < $count; $i++) {
					if(@$_SESSION['STORE_CART'][$i][0] == $id) { 
						$flag = true;
						break;
					}
				}
				if($flag) {
					if($qts != 0 AND $_SESSION['STORE_CART'][$i][1] < $qts) {
						$qty = $_SESSION['STORE_CART'][$i][1]++;
						$qty += 1;
						$alert = alert('info',"<b>$name</b> berhasil ditambahkan ke keranjang belanja.",true);
					} else {
						$_SESSION['STORE_CART'][$i][1] = $qts;
						if($_SESSION['STORE_CART'][$i][1] == 0) {
							$_SESSION['STORE_CART'][$i] = null;
						}
						$alert = alert('error',"Maaf, stok barang tidak mencukupi.",true);
					}
				}
				else if($qts != 0) {
					$alert = alert('info',"Keranjang belanja berhasil diupdate.",true);
					array_push($_SESSION['STORE_CART'],array($id,1));
				} else {				
					$alert = alert('error',"Maaf, stok barang tidak tersedia.",true);
				}
			} 
		}
		
		
		if(isset($_SESSION['STORE_CART'])) {
			$count = count($_SESSION['STORE_CART']);
			$flag  = false;
			echo "<h3>Shopping Cart</h3>";
			echo "<table style='width: 100%'>";
			echo "<thead><th style='width: 130px'>Image</th><th style='width: 320px'>Name</th><th style='width: 105px'>Price</th><th style='width: 90px'>QTY</th><th style='width: 112px'>Sub-total</th></thead>";
			echo "</table>";
			echo "<div class='chart-list'>";
			echo "<table style='width: 100%'>";
			echo "<tbody>";			
			$qts = $qti = $ttotal = 0;
			for($i = 0; $i < $count; $i++) {
				if(isset($_SESSION['STORE_CART'][$i][0])) {
					$pid = $_SESSION['STORE_CART'][$i][0];
					$qty = $_SESSION['STORE_CART'][$i][1];
					$plus = $min = 0;
					$qts = $qts + $qty;
					if($qty > 0) $qti++;
					$img = productInfo('img1',$pid);
					if(empty($img)) $img = productInfo('img2',$pid);
					if(empty($img)) $img = productInfo('img3',$pid);
					if(empty($img)) $img = productInfo('img4',$pid);
					if(empty($img)) $img = productInfo('img5',$pid);
					$img = str_replace('media/','media/.thumbs/',$img);
					$name = productInfo('name',$pid);
					$link = $_POST['url']."?app=store&view=product&id=$pid";
					$brand = brandInfo('name',productInfo('brand',$pid));
					$price = productInfo('price',$pid);
					$stock = productInfo('qty',$pid);
					if($stock == -1) $stock = 9999;
					$stotal = $qty * $price;
					$ttotal = $ttotal + $stotal;
					$price	= storeConfig('currency')." ".angka($price);
					$stotal	= storeConfig('currency')." ".angka($stotal);
					$img  = "<img src='$img'>";
					$dlink = $_POST['url']."?app=store&view=cart&act=delete&id=$pid";
					if($qty < $stock) $plus = " <span class='item-plus'>+</span>"; else $plus = "&nbsp;&nbsp;&nbsp;&nbsp;";
					if($qty > 1) $min = "<span class='item-min'>-</span>"; else $min = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					
					echo "<tr>";
					echo "<td align='center' style='width: 130px'>$img</td><td style='width: 300px'><a href='$link'>$name</a><br>$brand <br/><a href='$dlink' rel='$pid' class='delete-item-cart'>Hapus item</a></td><td style='width: 125px' align='right'>$price</td><td style='width: 70px'  align='right'>$min <span class='item-qty' rel='$pid' data-stock='$stock'>$qty</span> $plus</td><td style='width: 125px' align='right'>$stotal</td>";
					echo "</tr>";					
				}
			}			
			$i--;
			echo "</tbody></table></div>";
			echo "<div class='notif'>$alert</div>";
			$ttotal = storeConfig('currency')." ".angka($ttotal);
			$checkout = $_POST['url']."?app=store&view=checkout";
			$cart = $_POST['url']."?app=store&view=cart";
			echo "<div class='cart-total'><span>Total</span> $ttotal</div>";
			echo "<div class='cart-total-qts'>Total item <span>$qts</span> ($qti)</div>";
			echo "<div class='cart-checkout'><a href='$checkout'> Checkout </a></div>";
		}
		else {		
			alert('error',"Keranjang belanja kosong.");
		}		
	} 		
}
?>

<script>
	$(function() {
		function removeNotice() {
			setTimeout(function(){
				$('.notif .notice').fadeOut(1000, function() {
				});				
			}, 5000);	
		}
		removeNotice();
		$('.delete-item-cart').click(function(e) {
			var id = $(this).attr('rel');
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart.php',
				data: 'id='+id +'&do=remove&&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.cartbox').html(result);					
					removeNotice();
					$('.cartbox').html(result);	
				},
				error: function(result) {
				
				}
			});
			return false;
		});
		
		$('.item-plus').click(function(e) {
			var ini = $(this);
			var id = $(this).prev().attr('rel');
			var st = $(this).prev().attr('data-stock');
			
			var qty = ini.prev('.item-qty');	
			var qts = parseInt(qty.html());		
			var min = $(qty).prev();
			min.show();
			if(qts >= st-1) {
				$(this).hide();
			}
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart.php',
				data: 'id='+id +'&do=add&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.notice').remove();
					var qty = ini.prev('.item-qty');
					var qts = parseInt(qty.html());
					if(qts < st) {
						var val = $(this).parent().next();
						$(qty).html(qts + 1);
						
					} else {					
						ini.hide();
						$('.cartbox .notif').prepend('<div class="notice error">Stok barang tidak tersedia.</div>').fadeIn();	
					}				
					removeNotice(); 
					$('.cartbox').html(result);	
				},
				error: function(result) {
				
				}
			});
			return false;
		});
		
		$('.item-min').click(function(e) {
			var ini = this;
			var id = $(this).next().attr('rel');
			var st = $(this).next().attr('data-stock');
			var qty = $(ini).next('.item-qty');
			var qts = parseInt(qty.html());
			if (qts < 3) $(this).hide();
			$('.item-plus').show();		
			$.ajax({
				type: 'POST',
				url: '<?php echo $_POST['url']; ?>/apps/app_store/controller/cart.php',
				data: 'id='+id +'&do=delete&url=<?php echo $_POST['url']; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.notice').remove();
					if(qts < 2) {					
						$('.cartbox .notif').prepend('<div class="notice error">Minimal satu item.</div>').fadeIn();					
					} else {
						$(qty).html(qts - 1);		
					removeNotice(); 
					$('.cartbox').html(result);		
						
					}							
				},
				error: function(result) {				
				}
			});
			return false;
		});
	});
</script>