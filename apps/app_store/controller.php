<?php
/**
* @version		2.0
* @package		Fi Store
* @copyright	Copyright (C) 2016 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/

defined('_FINDEX_') or die('Access Denied');
/*
$l = getLink();
if(!userInfo()) {	
	if($l == '?app=store&view=checkout')
	redirect(make_permalink('?app=store&view=login'));
} else { 
	if($l == '?app=store&view=login')
	redirect(make_permalink('?app=store&view=profile'));
}
*/
if(app_param('view') == "checkout") {
	$step = app_param('step');
	if(empty($_SESSION['STORE_CART']) AND $step != 5 AND !empty($step))
		redirect(make_permalink('?app=store&view=checkout'));	
	if($step == 1 AND isset($_POST['submit'])) {
	redirect(make_permalink('?app=store&view=checkout&step=2'));
        }
        
        if(isset($_POST['shipping'])) {
            if(!isset($_POST['shipper'])) {
                
            } else {
                $_SESSION['STORE_SHIPPER'] = $_POST['shipper'];
                $_SESSION['STORE_SHIPPING_COST'] = $_POST['shipper-cost'][$_POST['shipper']];
                redirect(make_permalink('?app=store&view=checkout&step=3'));
            }
        }
	if($step == 3 AND isset($_POST['submit']) AND isset($_POST['pays'])) {
                $_SESSION['STORE_PAYMENT_METHOD'] = $_POST['pays'];
		redirect(make_permalink('?app=store&view=checkout&step=4'));
                die();
        }
	if($step == 4 AND isset($_POST['submit'])) {
		//$_SESSION['STORE_CART'] = null;
		redirect(make_permalink('?app=store&view=checkout&step=5'));	
	}
}	

if(app_param('view') == "cart") {
	
}	

function storeConfig($value) {
	return oneQuery('store_config','name',"'$value'",'value');
}

function productInfo($value, $id = null) {
	if(empty($id) AND app_param('view') == 'product')
	$id = app_param('id');
	return oneQuery('product','id',"$id","$value");
}

function categoryInfo($value, $id = null) {
	if(empty($id) AND app_param('view') == 'category')
	$id = app_param('id');
	return oneQuery('product_category','id',"$id","$value");
}

function categoryLink($value) {
	$link = make_permalink("?app=store&view=category&id=$value","","",true);
	return $link ;
}

function brandInfo($value, $id = null) {
	if(empty($id) AND app_param('view') == 'brand')
	$id = app_param('id');
	return oneQuery('store_brand','id',"$id","$value");
}

function brandLink($value) {
	$link = make_permalink("?app=store&view=brand&id=$value","","",true);
	return $link ;
}

function productHits($vid) {
	
	$hits=$vid+1;
	$id = app_param('id');
	DB::table(FDBPrefix.'product') -> where("id=$id")->update(array('hits'=>"$hits"));	
}

class Store {	
	function product($id) {			
		if(productInfo('id',$id)) {		
			
			$sql =DB::table(FDBPrefix."product")->where("id=$id AND status=1")->get();
			$qr = $sql[0];				
			if($qr) {		
				$category 	= categoryInfo('name',$qr['category']);
				$catLevel 	= categoryInfo('level',$qr['category']);
				$catLink	= categoryLink($qr['category']);
				
				productHits($qr['hits']);							
				$catLinks	= categoryLink($qr['category']);	
				$catHref	= "<a href='$catLinks'>$category</a>";
						
				$brand 	= brandInfo('name',$qr['brand']);
				$brandLinks	= brandLink($qr['brand']);
				$brand = "<a href='$brandLinks'>$brand</a>";
				$store = $qr['description'];
				if(checkLocalhost()) {
					$store = str_replace(FLocal."media/","media/",$store);
					$store = str_replace("/media/",FUrl."media/",$store);				
				}
				
				$images = $thumbs = null;
				$x = 1;
				for($i = 0; $i < 5; $i++) {
					$n = $i+1;
					if(!empty($qr["img$n"])) {
						$images[$x] = $qr["img$n"];
						$thumbs[$x] = str_replace("media/","media/.thumbs/",$qr["img$n"]);
						$x++;
					}				
				}
				
				$stock = $qr['qty'];
				if($stock != 0)	{		
					$stockz = '';
					$stocks = null;
					if($stock > 0)
					$stocks = " ($stock)";
					$stocks = "<span class='ready'>Stok Tersedia $stocks</span>";
				} else {
					$stocks = "<span class='outstock'>Stok Habis!</span>";				
				}
				
				$save = '';
				if(storeConfig('cents')) $price = @angka2($qr['price']); else $price = @angka($qr['price']);
				$price = $qr['price'];
				$disc = $discount = $qr['discount'];
				if(!empty($discount))
					if($qr['discount_type'] == 1) { 
						$save	= $discount;
						$save 	= substr($save ,0,strlen($save));
						//$save 	= substr($save ,0,strlen($save)-2);
						if(storeConfig('cents')) $save = angka2($save); else $save = angka($save);
						$save 	= storeConfig('currency')." ". $save;
						$price 	= $price - $discount;
					} else {			
						$disc 	= $price * ($discount / 100);
						$save 	= $discount. "%";
						$price 	= $price - $disc;
						
					}
				else {
					$discount = null;
				}
				$price = substr($price,0,strlen($price));
				if(storeConfig('cents')) $price = @angka2($price); else $price = @angka($price);
				$price	= storeConfig('currency'). " " . $price;
					
				$disc = substr($qr['price'] ,0,strlen($qr['price']));
				if(storeConfig('cents')) $disc = @angka2($disc); else $disc = @angka($disc);
				$disc	= "<strike> ". storeConfig('currency')." "."$disc </strike>";
				
				$buyLink = make_permalink("?app=store&view=product&id=$id")."#buyitem";
				$buyText = "Add to cart";
				if($stock != 0)
				$buy = "<a href='$buyLink' class='buy add-to-cart' rel='$id' qty='$stock'>$buyText</a><br/>";
				else $buy = '';
				
				$review = ''; //loadReview(,'rate');	
				/* perijinan akses artikel */				
				if(USER_LEVEL > $catLevel AND USER_LEVEL > $qr['level']) {
					echo "<h2>Maaf, anda tidak diperkanankan mengakses produk ini.</h2>Harap login terlebih dahulu sebelum membeli karena produk ini khusus untuk member terdaftar."; 
					$this -> memberonly	= true;
					
					}
				else {
					$this -> store	= $store;
					$this -> category	= $category;
					$this -> catlink	= $catLink;
					$this -> name		= $qr['name'];
					$this -> title		= $qr['name'];
					$this -> model		= $qr['model'];
					$this -> hits 		= $qr['hits'];	
					$this -> stock 		= $stocks;	
					$this -> brand 		= $brand;	
					$this -> buy 		= $buy;	
					$this -> images 	= $images;	
					$this -> thumbs		= $thumbs;	
					$this -> price		= $price;
					$this -> discount	= $disc;
					$this -> save		= $save;
					$this -> desc		= $qr['description'];
					$this -> model		= $qr['model'];
					$this -> spec		= $qr['spesification'];
					$this -> review		= $review;
				}		
			}
		}
	}

	function category($id = null,$type = null) {
		$link = null;
		/* Set global parameter */
		$show_panel	= menu_param('show_panel');
		$show_rss	= menu_param('show_rss');
		$read_more      = menu_param('read_more');
		$per_page	= menu_param('per_page');
		$intro		= menu_param('intro');
		$type 		= app_param('view');
		
		/* Set Access_Level */
		$accessLevel = Level_Access;
		
		if($type == 'archives')  {	
			$where = "status=1";
		} 
		else if($type == 'category')  { 
			$catName = categoryInfo('name',$id);
			$catDesc = categoryInfo('description',$id);
			$catLink  = categoryLink($id);	
			$where = "status=1 AND category = $id";
		}
		else if($type == 'featured') {
			$where = "status=1 AND featured = 1";
		
		}
		else if($type == 'brand') {
                        if(empty($per_page)) $per_page = 20;
			$catName = categoryInfo('name',$id);
			$catDesc = categoryInfo('description',$id);
			$catLink  = categoryLink($id);	
			$where = "status=1 AND brand = $id";
		}
		else if($type == 'tag') {
                        if(empty($per_page)) $per_page = 20;
			$tag = app_param('tag');
			$tag = str_replace("-"," ",$tag);
			$where = "status=1 AND tag LIKE '%".$tag."%'";
		}
		else { 
			$where = "status=1";
		}
		
		if(_FEED_ == 'rss') {
                        if(empty($per_page)) $per_page = 20;
			$pages = url_param('page');
			if($pages != null) {
				$link = str_replace("?page=$pages","",getUrl());
				redirect("$link?feed=rss");					
			}
		}		
			
		if(!$per_page) $per_page = 20;

		loadPaging();		
		$paging = new paging();
		
		$result = $paging->pagerQuery(FDBPrefix.'product',"*,
		DATE_FORMAT(date,'%d %M %Y') as date,
		DATE_FORMAT(date,'%Y-%m-%d %H:%i:%s') as order_date","$where $accessLevel",'id DESC',$per_page);
		
		$no = 0;
		$perrows = count($result);		
		foreach($result as $qr){
		
			/* Category Details */		
			$catLinks	= categoryLink($qr['category']);					
			$category	= categoryInfo('name',$qr['category']);
			$catHref	= "<a href='$catLinks'>$category</a>";
			
			/* Author */			
			if(empty($qr['author'])) 
				$author = userInfo('name',1);
			else  {
				$author = $qr['author'];
			}
			
			/* Article Links */
			$link	= "?app=store&amp;view=product&amp;id=$qr[id]";	
			$vlink  = str_replace("&amp;","&",$link);
			$link  	= make_permalink($vlink);			
				
			
			
			$save = '';
			if(storeConfig('cents')) $price = @angka2($qr['price']); else $price = @angka($qr['price']);
			$price = $qr['price'];
			$disc = $discount = $qr['discount'];
			if(!empty($discount))
				if($qr['discount_type'] == 1) { 
					$save	= $discount;
					$save 	= substr($save ,0,strlen($save));
					//$save 	= substr($save ,0,strlen($save)-2);
					if(storeConfig('cents')) $save = angka2($save); else $save = angka($save);
					$save 	= storeConfig('currency')." ". $save;
					$price 	= $price - $discount;
				} else {			
					$disc 	= $price * ($discount / 100);
					$save 	= $discount. "%";
					$price 	= $price - $disc;
				}
			else {
				$discount = null;
			}
			
			$images = $thumbs = null;
			$x = 1;
			for($i = 0; $i < 5; $i++) {
				$n = $i+1;
				if(!empty($qr["img$n"])) {
					$images = $qr["img$n"];
					$local = '';
					if(checkLocalhost()) $local = FLocal;
					$thumbs = str_replace("media/","media/.thumbs/",$qr["img$n"]);
					$thumbs2 = str_replace("/$local","",$thumbs);
					if(!file_exists($thumbs2)) $thumbs = $images ;
					break;
				}				
			}
			$price = substr($price,0,strlen($price));
			if(storeConfig('cents')) $price = @angka2($price); else $price = @angka($price);
			$price	= storeConfig('currency'). " " . $price;
					
			$disc = substr($qr['price'] ,0,strlen($qr['price']));
			if(storeConfig('cents')) $disc = @angka2($disc); else $disc = @angka($disc);
			$disc	= "<strike> ". storeConfig('currency')." "."$disc </strike>";
			
			$stock = $qr['qty'];
			if($qr['qty'] != 0) {
				$buyLink = make_permalink("?app=store&view=product&id=$qr[id]")."#buyitem";
				$buy 	= "<a href='$buyLink' class='button btn buy add-to-cart' data-id='$qr[id]' rel='$qr[id]' title='Buy $qr[name]'><span>Buy</span></a>";
			}
			else {
				$buy  = "<span class='button btn outstock' rel='$qr[id]' title='Out of stock!'><span>Out Stock</span></span>";
			}
				
			$title 	= "<a href='$link' title='$qr[name]'>$qr[name]</a>";	
			$brand 	= brandInfo('name',$qr['brand']);
			$brandLinks	= brandLink($qr['category']);
			$brand = "<a href='$brandLinks'>$brand</a>";
			$store = $qr['description'];
			$detail	= "<a href='$link' class='button btn detail' title=' See detail $qr[name]'><span>Detail</span></a>";
			$detailLink = make_permalink("?app=store&view=cart&act=add&id=$id");
				
			$this -> perrows 		= $perrows;
			$this -> show_rss		= $show_rss;
			$this -> show_panel		= $show_panel;
			$this -> name[$no]		= $qr['name'];
			$this -> stock[$no]		= $stock;	
			$this -> brand[$no]		= $brand;	
			$this -> discount[$no] 	= $discount;
			$this -> thumbs[$no]	= $thumbs;
			$this -> buy[$no]		= $buy;
			$this -> detail[$no]	= $detail;
			$this -> price[$no]		= $price;
			$this -> category[$no]	= $category;
			$this -> catlink[$no]	= $catLinks;
			$this -> title[$no] 	= $title;
			$this -> link[$no] 		= $link;
			$this -> desc[$no]		= clearXMLString($qr['description']);
			$this -> ftitle[$no] 	= clearXMLString($qr['name']);

			if(defined('SEF_URL')) {		
				$link = link_paging('?');
				if (strpos(getUrl(),'&') > 0)  {			
					$link = link_paging('&');
				}				
			}
			else if(isHomePage())  {
				$link = "?";
			}
			else if(!url_param('id'))  {			
				$tag  = app_param('tag');
				$link = "?app=store&tag=$tag";	
				$link = make_permalink($link,Page_ID);
				$link = $link."&amp;";
			}
			else {		
				$link="?app=store&view=category&id=$categoryId";	
				$link = make_permalink($link,Page_ID);
				$link = $link."&amp;";		
			}				
			$no++;
		}
		
		// pageLink	
		$this -> pglink	 = $paging->createPaging($link);
		
		// rssLink
		if($type == 'tag')		{	
			$tag = str_replace(" ","-",$tag);	
			$rssLink = "?app=store&tag=$tag&feed=rss";	
		}
		else if($type == 'category')	{
			$rssLink = "?app=store&view=category&id=$id&feed=rss";	
		}
		else {
			$rssLink = "?app=store&view=archives&feed=rss";	
		}
		
		if(_FEED_ == 'rss') {
			$rssLink = make_permalink($rssLink);
			$this -> rssTitle = SiteTitle;			
			$categoryLink = @clearXMLString($rssLink);
			$categoryLink = str_replace(".xml","",$categoryLink);
			$this -> rssLink  	= $categoryLink;
			$this -> rssDesc  	= @$categoryDesc;	
		}
		else {
			
			$this -> rssLink  	= make_permalink($rssLink);
		}
		
		
	}	
}


/****************************************/
/*			   SEF Article				*/
/****************************************/
$view = app_param('view');
$id = app_param('id');

if($id > 0) {
	$a = FQuery("product_category","id=$id",'',1); 
	if(!$a)
	$a = FQuery("product","id=$id",'',1); 
	if(!$a)
	$a = FQuery("store_brand","id=$id",'',1); 
}
else if ((app_param('tag') != null)) {
	$a = app_param('tag');
}
else{
	$a = 'store';
}
if($a){ 
	if(defined('SEF_URL')){
			$page = menuInfo('id',"?app=store");
		$root_link = check_permalink('link','?app=store','permalink');
		if(empty($root_link)) $root_link = 'store';
		if($view == 'product') {
			$item = productInfo('name')."-".productInfo('id');
			$page = menuInfo('id',"?app=store");
			add_permalink($item,"$root_link",$page);
		}
		else if($view == 'category' or $view == 'catlist') {	
			$page = menuInfo('id',"?app=store");
			$ncat = oneQuery('product_category','id',$id,'name');		
			if(_FEED_ == 'rss')
				add_permalink("$root_link/$ncat","","","xml");
			else 
				add_permalink("$root_link/$ncat","",$page);
		}
		else if($view == 'brand') {	
			$page = menuInfo('id',"?app=store");
			$ncat = oneQuery('store_brand','id',$id,'name');		
			if(_FEED_ == 'rss')
				add_permalink("$root_link/brand/$ncat","","","xml");
			else 
				add_permalink("$root_link/brand/$ncat","",$page);
		}
		else if($view == "user") {
			add_permalink("$root_link/user");		
		}	
		else if($view == "cart") {
			$page = menuInfo('id',"?app=store");
                        $add = url_param('add');
                       if(empty($add))
                            add_permalink("$root_link/cart","",$page);
		}	
		else if($view == "login") {
			add_permalink("$root_link/user/login","",$page);	
		}	
		else if($view == "checkout") {
                        $t = url_param('token');
			$step = app_param('step');
			if($step == 2 or $step == 3 or $step == 4 or $step == 5 AND empty($t))	
                            add_permalink("$root_link/checkout/$step","",$page);
			else if(empty($t))	
                            add_permalink("$root_link/checkout","",$page);
		}			
		else if (app_param('tag') != null) {	
			$tag = app_param('tag');
			if(_FEED_ == 'rss')
				add_permalink("tag/$tag","","","xml");		
			else add_permalink("tag/$tag");
		}			
		else if (app_param('tag') == null AND app_param('view') == null) {	
			add_permalink("store");
		}	
	}
}

if($a){
	if(!isHomePage()) {	
		if ($view=="product") {
			define('PageTitle', productInfo('name'));			
			$desc = productInfo('description');
			if(!empty($desc)) 	
				define('MetaDesc', productInfo('description'));
			else
				define('MetaDesc', generateDesc(productInfo('description')));
			
			$keys = productInfo('keywords');		
			if(!empty($keys)) 	
				define('MetaKeys', productInfo('keywords'));
			else if(!empty($desc))
				define('MetaKeys', generateKeywords(productInfo('description')));
			else
				define('MetaKeys', generateKeywords( productInfo('name')));
				
			if(!isset($follow))
				$follow = 'noindex';
			else if(siteConfig('follow_link'))
				$follow = 'index, follow';
			else
				$follow = 'index, nofollow';
			
			define('MetaRobots',"$follow");
			
			$author = oneQuery('brand','id',productInfo('brand'),'name');
			if(define('MetaAuthor',$author));
			
		}
		else if($view=="category" or $view=="catlist") {
			if(pageInfo(Page_ID,'title'))
				define('PageTitle', pageInfo(Page_ID,'title'));
			else
				define('PageTitle', categoryInfo('name'));
			$desc = categoryInfo('description');
			if(!empty($desc )) 
				define('MetaDesc', $desc);
			else
				
			$keys = categoryInfo('keywords');
			if(!empty($keys)) 
				define('MetaKeys', $keys );
			
			
			$cat = app_param('id');
			$qry = oneQuery("menu","link","'?app=article&view=category&id=$cat'");
			if(!$qry)
				$qry = oneQuery("menu","link","'?app=article&view=catlist&id=$cat'");
			if($qry) {
				if(siteConfig('follow_link'))
					$follow = 'index, follow';
				else
					$follow = 'index, nofollow';
			}
			else
				$follow = 'noindex';
			define('MetaRobots',"$follow");
			
		}		
		else if($view=='archives')
			define('PageTitle', "Store");
		else if($view=='checkout')
			define('PageTitle', "Checkout");
		else if($view=='featured')
			define('PageTitle', "Store");
		else if (app_param('tag') != null)		{	
			define('PageTitle', $p = str_replace("-"," ",ucfirst(app_param('tag'))));
			define('Apps_Title', $p);
			
			}
	}
}