<?php
/**
* @version		1.5.0
* @package		Fi Store
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/

defined('_FINDEX_') or die('Access Denied');

$product = new Store;

$product -> product($id = app_param('id'));

if(isset($product -> title)) :
	$category 	= $product -> category;
	$catlink	= $product -> catlink;
	$brand 		= $product -> brand;
	$title		= $product -> title;
	$stock		= $product -> stock;
	$price		= $product -> price;
	$discount	= $product -> discount;
	$save		= $product -> save;
	$hits 		= $product -> hits;
	$image		= $product -> images;
	$thumb		= $product -> thumbs;
	$desc		= $product -> desc;
	$spec		= $product -> spec;
	$review		= $product -> review;
	$model		= $product -> model;
?>	
<div class="product-item">
	<span class="band onsale"></span>

	<div id="product-image" class="product-image">
	
		<!-- $product-slides -->
		<div id="product-slides">
		<?php for($i = 1; $i <= count($image); $i++) : ?>
			<div class="item-slide img<?php echo $i; ?>" <?php if(!isset($active)) { $active = true;  echo "style='display:block'"; }?>>
				<a rel="<?php echo "$image[$i]"; ?>" class="lightbox">
					<img src="<?php echo "$image[$i]"; ?>" alt="Image <?php echo $model; ?>"><span class="overlay"></span>
			   </a>			   
			</div> 
		<?php endfor; ?>
		</div> <!-- #product-slides -->
		
		
		<!-- $product-thumbs -->
		<div id="product-thumbs">		
		<?php for($i = 1; $i <= count($thumb); $i++) : ?>
			<a rel="<?php echo $i; ?>">			
				<img src="<?php echo $thumb[$i]; ?>" alt="">
				<span class="overlay"></span>
			</a>
		<?php endfor; ?>
		</div> <!-- #product-thumbs -->
		
	</div> <!-- #product-slider -->

	<div class="product-detail">
		<div>
			<h1 class="title"><?php echo "$title"; ?></h1>
			<span class="post-meta"><?php echo "$brand"; ?></span>
			<p class="stock"><?php echo "$stock"; ?></p>		
			<div class="clearfix">
				<div class="price-single"><?php echo $price; ?></div>
				<?php if(!empty($save)) : ?>
					<span class="discount"><?php echo $discount; ?></span>
					<span class="discount">Hemat: <?php echo $save; ?></span>
				<?php endif; ?>
				

				<div style="margin-top: 10px">Pilihan Warna:</div>
				<ul class="color-box option-box">
					<li >Merah</li>
					<li class="active">Hijau</li>
					<li>Biru</li>
					<li>Hitam</li>
				</ul>
					


				

				<div>Pilihan Ukuran:</div>
				<ul class="color-box option-box">
					<li>S</li>
					<li>M</li>
					<li>L</li>
					<li>XL</li>
				</ul>


				
				<?php echo $product -> buy; ?>		
				<a class="add-to-whis" rel="<?php echo $id; ?>"><span>Add to whis</span></a> or 
				<a class="add-to-compare" rel="<?php echo $id; ?>"><span>Compare</span></a>
				
			</div>		
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div>
	<div class="product-info">	
		<h3 class="tabs active first" tab-target="tab-1">Deskripsi</h3>
		<h3 class="tabs " tab-target="tab-2">Spesifikasi</h3>
		<h3 class="tabs last" tab-target="tab-3">Review</h3>
		
		<div class="detail-box">			
			<div class="target-tab" id="tab-1"><?php echo $desc; ?></div>
			<div class="target-tab" id="tab-2" style="display: none;"><?php echo $spec; ?></div>
			<div class="target-tab" id="tab-3" style="display: none;"><?php echo $review; ?></div>		
		</div>	
	</div>
</div>

<div class="clear"></div>
<div class="related product-related">
	<h3 style="text-indent: 0px; ">Other Products</h3>
		<ul class="related-items clearfix">			
			<?php			
			// Include class


			loadPaging();


			

			

			






			$paging=new paging();
			$rowsPerPage=4;// Rows per page

			// Pager query
			$result=$paging->pagerQuery(FDBPrefix.'product','name,img1,id',"","RAND()",$rowsPerPage);	
			$no=0;



			
			foreach($result as $row){
				$link = make_permalink("?app=store&view=product&id=$row[id]");
				echo "<li>
						<a href=\"$link\" class=\"clearfix\">
							<img src='$row[img1]' align='center' width=\"128\"><span>$row[name]</span>
						</a>
					</li>";			
				$no++;	
			}
			echo "<div class=\"clear\"></div>";
			?>
	</ul>
</div>		
<?php
elseif(!isset($product -> memberonly)) :
	echo "<h3>Produk tidak ditemukan atau tidak dapat diperkenankan.</h3>Hubungi administrator jika anda ingin melihat produk yang anda maksud!"; 
endif;

?>

<script>
	$(function() {
		$('#product-thumbs a').click(function() {
			$('#product-thumbs a').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('rel');
			$('.item-slide').hide();
			$('.img'+target).show();
		});
		
		$('.product-info h3').click(function() {
			$('.product-info h3').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('tab-target');
			$('.target-tab').hide();
			$('#'+target).show();
		});	
		
		$('.delete-item-cart').click(function(e) {
			return false;
		});
		$('a.add-to-cart').click(function(e) {		
			// hide scrollbars!
			$('<div id="overlay"></div>')
			.css('top', '0')
			.css('opacity', '0')
			.animate({'opacity': '0.7'})
			.appendTo('body');
			var height = ($(window).height() - $('#lightbox').height()-400) / 4;
			var width = ($(window).width() - 790) / 2;
			$('<div id="lightbox"></div>')
			.css({
			'top': height,
			'left': width
			})
			.appendTo('body');
			
			$('<span class="closelight" title="Close">x</span><div class="cartbox">Loading...</div>')
			.load(function() {
				positionLightboxImage();
			})
			.appendTo('#lightbox');
			
			$.ajax({
				type: 'POST',
				url: '<?php echo FUrl; ?>/apps/app_store/controller/cart.php',
				data: 'id=<?php echo $id; ?>&do=add&url=<?php echo FUrl; ?>',
				cache: false,
				async: false,
				success: function(result) {
                                    
					$('.cartbox').html(result);
				},
				error: function(result) {
				
				}
			});
			
			$('.closelight')
			.click(function() {
				removeLightbox();
			})
			return false;
		});
		
		$('a.lightbox').click(function(e) {
			// hide scrollbars!
			$('<div id="overlay"></div>')
			.css('top', '0')
			.css('opacity', '0')
			.animate({'opacity': '0.7'})
			.appendTo('body');
			$('<div id="lightbox"></div>')
			.hide()
			.appendTo('body');
			$('<span class="closelight" title="Close">x</span><img class="imgbox"/>')
			.attr('src', $(this).attr('rel'))
			.load(function() {
				positionLightboxImage();
			})
			.appendTo('#lightbox');
			$('.closelight')
			.click(function() {
				removeLightbox();
			});
			$('#overlay')
			.click(function() {
				removeLightbox();
			});
			return false;
		});
	
		
	function positionLightboxImage() {
		var top = ($(window).height() - $('#lightbox').height()) / 2.5;
		var left = ($(window).width() - $('#lightbox').width()) / 2;
		$('#lightbox')
		.css({
		'top': top + $(document).scrollTop(),
		'left': left
		})
		.fadeIn();
	}
			
	function removeLightbox() {
		$('#overlay, #lightbox')
		.fadeOut('slow', function() {
		$(this).remove();
		$('body').css('overflow-y', 'auto'); // show scrollbars!
		});
	}
	});
</script>