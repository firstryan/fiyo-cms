<?php
/**
* @version		1.5.0
* @package		Fi Store
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/

defined('_FINDEX_') or die('Access Denied');

$store = new Store;

$store -> category($id = app_param('id'));
$title = @$store -> title;

if(isset($title)) :	
$category 	= $store -> category;
$catlink	= $store -> catlink;
$brand 		= $store -> brand;
$title		= $store -> title;
$name		= $store -> name;
$stock		= $store -> stock;
$price		= $store -> price;
$discount	= $store -> discount;
$thumb		= $store -> thumbs;
$desc		= $store -> desc;
$buy		= $store -> buy;
$link		= $store -> link;
$detail		= $store -> detail;
$perrows	= $store -> perrows;
for($x = 0; $x < $perrows; $x++) :
?>	
<div class="product">
	<div class="product-box">
		<a href="<?php echo $link[$x]; ?>" class="image" rel="<?php echo $id[$x]; ?>">
			<span class="rounded">
				<img src="<?php echo $thumb[$x]; ?>" alt="<?php echo $name[$x]; ?>">
			</span>
		</a>							
		<div class='title'><a href="$link"><?php echo $title[$x]; ?></a></div>		
		<div class='price'><span><?php echo $price[$x]; ?></span></div>
			<?php echo $detail[$x]; ?>
			<?php echo $buy[$x]; ?>
	</div>
</div>	
<?php
endfor;
elseif(!isset($product -> memberonly)) :
	echo "<h3>Produk tidak ditemukan atau tidak dapat diperkenankan.</h3>Hubungi administrator jika anda ingin melihat produk yang anda maksud!"; 
endif;

?>

<script>
	$(function() {
		$('#product-thumbs a').click(function() {
			$('#product-thumbs a').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('rel');
			$('.item-slide').hide();
			$('.img'+target).show();
		});
		
		$('.product-info a').click(function() {
			$('.product-info a').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('tab-target');
			$('.target-tab').hide();
			$('#'+target).show();
		});	
		
		$('.delete-item-cart').click(function(e) {
			alert("ASD");
			return false;
		});
		$('a.buy').click(function(e) {		
			// hide scrollbars!
			$('<div id="overlay"></div>')
			.css('top', '0')
			.css('opacity', '0')
			.animate({'opacity': '0.7'})
			.appendTo('body');
			var height = ($(window).height() - $('#lightbox').height()) / 6;
			var width = ($(window).width() - 850) / 2;
			var itemid = $(this).attr('rel');
			if($(window).width() < 768) width = 0;
			$('<div id="lightbox"></div>')
			.css({
			'top': height,
			'left': width
			})
			.appendTo('body');
			
			$('<span class="closelight" title="Close">x</span><div class="cartbox">Loading...</div>')
			.load(function() {
				positionLightboxImage();
			})
			.appendTo('#lightbox');			
			
			$.ajax({
				type: 'POST',
				url: '<?php echo FUrl; ?>/apps/app_store/controller/cart.php',
				data: 'url=<?php echo FUrl; ?>',
				cache: false,
				async: false,
				success: function(result) {
					$('.cartbox').html(result);
				},
				error: function(result) {				
				}
			});
			
			$('.closelight')
			.click(function() {
				removeLightbox();
			})
			return false;
		});
		
		$('a.lightbox').click(function(e) {
			// hide scrollbars!
			$('<div id="overlay"></div>')
			.css('top', '0')
			.css('opacity', '0')
			.animate({'opacity': '0.7'})
			.appendTo('body');
			$('<div id="lightbox"></div>')
			.hide()
			.appendTo('body');
			$('<span class="closelight" title="Close">x</span><img class="imgbox"/>')
			.attr('src', $(this).attr('rel'))
			.load(function() {
				positionLightboxImage();
			})
			.appendTo('#lightbox');
			$('.closelight')
			.click(function() {
				removeLightbox();
			});
			$('#overlay')
			.click(function() {
				removeLightbox();
			});
			return false;
		});
	
		
	function positionLightboxImage() {
		var top = ($(window).height() - $('#lightbox').height()) / 2.5;
		var left = ($(window).width() - $('#lightbox').width()) / 2;
		$('#lightbox')
		.css({
		'top': top + $(document).scrollTop(),
		'left': left
		})
		.fadeIn();
	}
			
	function removeLightbox() {
		$('#overlay, #lightbox')
		.fadeOut('slow', function() {
		$(this).remove();
		$('body').css('overflow-y', 'auto'); // show scrollbars!
		});
	}
	});
</script>