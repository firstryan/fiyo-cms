<?php
/**
* @version		1.5
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/


redirect(make_permalink("http://www.books-center.com/user/register"));
	
?>	
<h1 class="title" style="text-indent: 0px; ">Registrasi</h1>
<div>

<form action="" method="POST">
<table class="eshop cart store-login" style="width:50%" align="left">
<tbody>			
	<tr>
		<td colspan="2"><p>Silakan mendaftar untuk melanjutkan pembelian.</p></td>			
	</tr>
	<tr>
		<td>Username</td>			
		<td><input type='text' name='name' required></td>
	</tr>
	<tr>
		<td>Password</td>			
		<td><input type='password' name='password' size='12' required></td>
	</tr>
	<tr>
		<td>Confirm Password</td>			
		<td><input type='password' name='re-password' size='12' required></td>
	</tr>
	<tr>
		<td>Email</td>			
		<td><input size='30' type='text' name='email' required></td>
	</tr>
	<tr>
		<td></td>			
		<td><span>&nbsp;</span><input type="submit" name="login" value="<?php echo Register; ?>" class="button btn login"/> </td>	
	</tr>

</tbody>
</table>
</form>
<form action="" method="POST">
<table class="eshop cart store-login" style="width:45%" align="right">
<tbody>			
	<tr>
		<td colspan="2"><p>Masuk sebagai member untuk kenyamanan berbelanja.</p></td>			
	</tr>
	<tr>
		<td>Username</td>			
		<td><input type='text' name='name1'></td>
	</tr>
	<tr>
		<td>Password</td>			
		<td><input type='text' name='name2'></td>
	</tr>
	<tr>		
		<td></td>			
		<td><span>&nbsp;</span><input type="submit" name="login" value="<?php echo Login; ?>" class="button btn login"/> <a href="<?php echo make_permalink('?app=user&view=lost_password') ?>"><?php echo Lost_Password; ?>?</a></td>			
	</tr>

</tbody>
</table>
</form>
</div>
