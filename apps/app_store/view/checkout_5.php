<?php
/**
* @version		1.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/

$t = url_param('token');
if(empty($t) AND empty($_SESSION['STORE_CART']))
    redirect(make_permalink('?app=store&view=checkout'));
echo $_GET['token'];
if(!empty($t) AND !isset($_SESSION['STORE_CART'])) {
	$db = new FQuery();
	$row = $db->select(FDBPrefix.'store_order','*',"token = '$t'");
		if(count($row))
			$no = $row[0]['order_id'];
		else 
			redirect(make_permalink('?app=store&view=checkout'));
	}
else {
	$db = new FQuery();
	$last = $db->select(FDBPrefix.'store_order','id','','id DESC LIMIT 1');
	$last = (int)$last[0]['id']+1;
	$no = time().$last;
	$token = md5($no);
	$country = oneQuery('store_country','country_id',$_SESSION['STORE_RCVR']['country'],'name');
	$prov = oneQuery('store_state','state_id',$_SESSION['STORE_RCVR']['state'],'state') ;
	$city = oneQuery('store_city','city_id',$_SESSION['STORE_RCVR']['city'],'city');
	$uid = USER_ID;

	if(isset($_SESSION['STORE_CART'])) {
		$shipp_info = $_SESSION['STORE_SHIPPING_INFO'] ;
        $cost = $_SESSION['STORE_SHIPPING_COST'];
		$date = date("Y-m-d H:i:s");
		$json = addslashes(json_encode($_SESSION['STORE_CART']));
		$ins = $db -> insert(FDBPrefix.'store_order',array('',"$no","$uid",$_SESSION['STORE_RCVR']['name'],$_SESSION['STORE_RCVR']['email'],$_SESSION['STORE_RCVR']['telp'],$country, $prov, $city, $_SESSION['STORE_RCVR']['address'], $_SESSION['STORE_RCVR']['zip'], "", $date , $token, $json,$shipp_info,$cost));
		
	}
	if(isset($ins)) {
		
		
		
/* ini ************************************************/

$db = new FQuery();  
$user = $db->select(FDBPrefix."store_user","*","id = ".USER_ID);
$user = $user[0];
if(empty($user)) 
$user = $_SESSION['STORE_RCVR'];
$next = make_permalink("?app=store&view=checkout&step=2");


$city_p = oneQuery('store_city','city_id',$user[city_p],'city') ;
$state_p = oneQuery('store_state','state_id',$user[state_p],'state');
$country_p = oneQuery('store_country','country_id',$user[country_p],'name');

$city = oneQuery('store_city','city_id',$user[city],'city') ;
$state = oneQuery('store_state','state_id',$user[state],'state');
$country = oneQuery('store_country','country_id',$user[country],'name');
$table = "

<div style=' overflow: hidden;'><table class='data-buyer left'style='width:49%; line-height: 1.6em;' align='left'
<tbody>		
	<tr>
		<td colspan='2' style='background: #ddd; padding: 5px 10px'><b>Informasi Pembeli</b></b>			
	</tr>
	<tr>
		<th align='left'>Nama </th>			
		<td>$user[name]</td>
	</tr>
	<tr>
		<th align='left'>Email </th>			
		<td>$user[email]</td>
	</tr>
	<tr>
		<th align='left'>Telp.</th>			
		<td>$user[telp]</td>
	</tr>
	
	<tr>
		<th align='left'>Negara</th>			
		<td>$country</td></td>
	</tr>
	<tr>
		<th align='left'>Provinsi</th>			
		<td> $state </td>
	</tr>
	<tr>
		<th align='left'>Kota</th>
                <td>
                    $city
                  
                </td>
        </tr>
	<tr>
		<th valign='top' align='left'>Alamat</th>			
                <td>$user[address]</td>
	</tr>			
	<tr>
		<th align='left'>Kode Pos</th>			
		<td>$user[zip]</td>
	</tr>
</tbody>
</table>


<table class='data-buyer right data-dropship'style='width:48%; margin-right: 1px; 
    line-height: 1.6em;' align='right'
<tbody>		
	<tr>
        <td colspan='2' style='background: #ddd ; padding: 5px 10px'><b>Informasi Pengiriman</b></td>			
	</tr>		
	<tr>
		<th align='left'>Nama </th>			
		<td>$user[name]</td>
	</tr>
	<tr>
		<th align='left'>Telp.</th>			
		<td>$user[telp]</td>
	</tr>
	
	<tr>
		<th align='left'>Negara</th>			
		<td>$country</td></td>
	</tr>
	<tr>
		<th align='left'>Provinsi</th>			
		<td> $state </td>
	</tr>
	<tr>
		<th align='left'>Kota</th>
                <td>
                    $city
                  
                </td>
        </tr>
	<tr>
		<th valign='top' align='left'>Alamat</th>			
                <td>$user[address]</td>
	</tr>			
	<tr>
		<th align='left'>Kode Pos</th>			
		<td>$user[zip]</td>
	</tr>
</tbody>
</table></div>";



		
	$count = count($_SESSION['STORE_CART']);
	if($count) {
        $c = '<h3>Order Items</h3> ';
        $c .= "<table style='width: 100%;     border-collapse: collapse; '>";
        $c .= "<tr style='background: #ddd'>";
        $c .= "<th colspan='2' style='padding: 5px'>Product</th>"
        . "<th  style='padding: 5px'>Price</th>"
        . "<th style='width: 70px ; padding: 5px'>Weight</th>"
        . "<th style='width: 70px; padding: 5px'>Qty</th>"
        . "<th style='padding: 5px'>Sub-total</th>";
        $c .= "</tr>";
        $c .= "<tbody>";			
        $qts = $qti = $ttotal = $weight = $tweight= $zweight= $tqty= 0;
        for($i = 0; $i < $count; $i++) {
            if(isset($_SESSION['STORE_CART'][$i][0])) {
                $pid = $_SESSION['STORE_CART'][$i][0];
                $qty = $_SESSION['STORE_CART'][$i][1];
                $plus = $min =  0;
                $qts = $qts + $qty;
                if($qty > 0) $qti++;
                $img = productInfo('img1',$pid);
                if(empty($img)) $img = productInfo('img2',$pid);
                if(empty($img)) $img = productInfo('img3',$pid);
                if(empty($img)) $img = productInfo('img4',$pid);
                if(empty($img)) $img = productInfo('img5',$pid);
                $img = str_replace('media/','media/.thumbs/',$img);
                $name = productInfo('name',$pid);
                $link = make_permalink("?app=store&view=product&id=$pid");
                $brand = brandInfo('name',productInfo('brand',$pid));
                $price = productInfo('price',$pid);
                $ongkirock = productInfo('qty',$pid);

                $sweight = productInfo('shipp_weight',$pid);
                if(empty($sweight)) $sweight = 1;     
                               
                $weight += ceil($pweight/10);
                if($ongkirock == -1) $ongkirock = 9999;
                $ongkirotal = $qty * $price;
                $ttotal = $ttotal + $ongkirotal;
                $price	= storeConfig('currency')." ".angka($price);

                $pweight = productInfo('shipp_weight',$pid);
                if(empty($pweight)) $pweight = 1;        
                $weight = $pweight * $qty / 1000 ;
                if($weight< 1) $weight = $sweight;
                $shp = productInfo('shippment',$pid);
                $sweight = '';
                if($shp == 2){ 
                    $sweight = "<s>$weight Kg</s>";
                    $sweight .= "<br>Free Shipping!";
                }else {  $sweight = "<span>$weight Kg</span>"; 
                $zweight += $weight;}
                $tweight += $weight;
                $vtotal	= storeConfig('currency')." ".angka($ongkirotal);
                $img  = "<img src='$img' height='50'>";
                $tqty += $qty;
                $c .= "<tr>";
                $c .= "<td align='center' style='width: 60px; border-right:0;'>$img</td>"
                        . "<td style='width: 300px; border-left:0;'><a href='$link'>$name</a><br>$brand <br/></td>"
                        . "<td style='width: 125px' align='right'>$price</td>"
                        . "<td style='width: 125px' align='center' >$sweight </td>"
                        . "<td style='width: 70px' align='center'><span class='item-qty' rel='$pid' data-stock='$ongkirock'>$qty</span></td>"
                        . "<td style='width: 125px' align='right'>$vtotal</td>";
                $c .= "</tr>";					
            }
		}
        
        $cship = $cost * $weight;
        $tship = $cost * $zweight;
        $ctotal	= storeConfig('currency')." ".angka($cship);
        $vtotal	= storeConfig('currency')." ".angka($tship);

        if($tweight == $zweight) {
            $wprint = "$vtotal";
        } else {            
            $wprint = "<s>$ctotal</s><br>$vtotal ";
        }
        
        $c .= "<tr>";
        $c .= "<td colspan='5' style='text-align:center'>$shipp_info</td><td colspan='5' style='width: 125px' align='right' class='shipping-cost'>$wprint</td>";
		$c .= "</tr>"; 
        
        $ttotal = $ttotal + $tship ;
        $ttotal	= storeConfig('currency')." ".angka($ttotal);
        $c .= "<tr><th colspan='5' align='right'><span>Total</span></th><th align='right' class='total-cost'> $ttotal</th></tr>";

        $c .= "</tbody></table>";
	} 
	
	
		unset($_SESSION['STORE_CART']);
		redirect(make_permalink('?app=store&view=checkout&step=5')."?orderid=$no&token=$token");
		
		
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= "To: $user[name] <$user[email]>" . "\r\n";
$headers .= 'From: Books Center <no-reply@books-center.com>' . "\r\n";


$mail ="
<table width='100%' align='center' border='0' cellspacing='0' cellpadding='10'>
  <tbody><tr valign='top'> 
    <td width='53%' align='left'>Books Centre<br>
Jalan Siliwangi No.646<br>
Semarang<br>
Indonesia
<br>
Semarang, 50184</td>
    <td width='47%' align='right'>
<img src='http://www.books-center.com/themes/bookscenter/image/logo.png'></td>
  </tr>
  <tr>
      <td colspan='2'>The following order was received.
</td>
  </tr>    
  <tr bgcolor='white'> 
    <td colspan='2'>
      <h3>No Order : $no</h3>
	  Order Date : $date 
	  
    </td>
  </tr>
</tbody></table>
";
$subject = 'Purchase Order';
$footer = "<br><br><br><div>Thanks for your Order<br><b>Books Center</b></div>";
		mail("$user[email]","$subject",$mail. $table. $c. $footer, $headers);
		mail("firstryan@gmail.com","New Product Order",$mail. $table. $c. $footer, $headers);
	}
}

echo "<h3>Order Items</h3><table style='width: 100%' class='checkout-nav'>";
echo "<tr>";
echo "<th class='first'><span>1</span> Rincian Belanja</th><th><span>2</span> Data Pembeli</th><th><span>3</span> Metode Pembayaran</th><th><span>4</span> Cek Data</th><th class='last active'><span>5</span> Pembayaran</th>";
echo "</tr>";
echo "</table>";


if(!empty($t)) $no = $row[0]['order_id'];
?>	
<div style='overflow: hidden'>

<table class="data-buyer" style="width:100%" align="left">
<tbody>		
	<tr>
		<td style='border: 0'></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'>No.Transaksi : <?php echo $no; ?><br>Tanggal : <?php echo date("d F Y"); ?></td>			
	</tr>
	<tr>
		<td align='left' style='border: 0'>Halaman ini adalah bukti bahwa anda telah berhasil melakukan pemesanan barang. Anda bisa melakukan pembayaran agar barang yang dipesan segera dikirim. Terimakasih telah berbelanja di toko kami. </td>			
	</tr>
</tbody>
</table>
<?php
	$count = count( $row[0]['item']);
	if($count) {
        $flag  = false;
        echo "<table style='width: 80%'>";
        echo "<tr>";
        echo "<th colspan='2'>Product</th>"
        . "<th>Price</th>"
        . "<th style='width: 70px'>Weight</th>"
        . "<th style='width: 70px'>Qty</th>"
        . "<th>Sub-total</th>";
        echo "</tr>";
        echo "<tbody>";			
        $qts = $qti = $ttotal = $weight = $tweight= $zweight= $tqty= 0;
        for($i = 0; $i < $count; $i++) {
            if(isset($_SESSION['STORE_CART'][$i][0])) {
                $pid = $_SESSION['STORE_CART'][$i][0];
                $qty = $_SESSION['STORE_CART'][$i][1];
                $plus = $min =  0;
                $qts = $qts + $qty;
                if($qty > 0) $qti++;
                $img = productInfo('img1',$pid);
                if(empty($img)) $img = productInfo('img2',$pid);
                if(empty($img)) $img = productInfo('img3',$pid);
                if(empty($img)) $img = productInfo('img4',$pid);
                if(empty($img)) $img = productInfo('img5',$pid);
                $img = str_replace('media/','media/.thumbs/',$img);
                $name = productInfo('name',$pid);
                $link = make_permalink("?app=store&view=product&id=$pid");
                $brand = brandInfo('name',productInfo('brand',$pid));
                $price = productInfo('price',$pid);
                $ongkirock = productInfo('qty',$pid);

                $sweight = productInfo('shipp_weight',$pid);
                if(empty($sweight)) $sweight = 1;     
                               
                $weight += ceil($pweight/10);
                if($ongkirock == -1) $ongkirock = 9999;
                $ongkirotal = $qty * $price;
                $ttotal = $ttotal + $ongkirotal;
                $price	= storeConfig('currency')." ".angka($price);

                $pweight = productInfo('shipp_weight',$pid);
                if(empty($pweight)) $pweight = 1;        
                $weight = $pweight * $qty / 1000 ;
                if($weight< 1) $weight = $sweight;
                $shp = productInfo('shippment',$pid);
                $sweight = '';
                if($shp == 2){ 
                    $sweight = "<s>$weight Kg</s>";
                    $sweight .= "<br>Free Shipping!";
                }else {  $sweight = "<span>$weight Kg</span>"; 
                $zweight += $weight;}
                $tweight += $weight;
                $vtotal	= storeConfig('currency')." ".angka($ongkirotal);
                $img  = "<img src='$img' height='50'>";
                $tqty += $qty;
                echo "<tr>";
                echo "<td align='center' style='width: 60px; border-right:0;'>$img</td>"
                        . "<td style='width: 300px; border-left:0;'><a href='$link'>$name</a><br>$brand <br/></td>"
                        . "<td style='width: 125px' align='right'>$price</td>"
                        . "<td style='width: 125px' align='center' >$sweight </td>"
                        . "<td style='width: 70px' align='center'><span class='item-qty' rel='$pid' data-stock='$ongkirock'>$qty</span></td>"
                        . "<td style='width: 125px' align='right'>$vtotal</td>";
                echo "</tr>";					
            }
		}
		echo "</tbody></table>";
	} 
?>


<table class="data-buyer" style="width:48%" align="left">
<tbody>		
	<tr>
		<td style='border: 0' colspan='2'><h2>Konfirmasi Pembayaran</h2></td>		
	</tr>		
	<tr>
		<td align='left' style='border: 0' colspan='2'>Setelah melakukan transfer ke rekening yang telah ditentukan Anda bisa melakukan konfirmasi agar pesanan segera diproses oleh pihak kasir.</td>			
	</tr>
	<!--tr>
		<td align='left' style='border: 0'>Tanggal Transfer</td><td style='border: 0'><input type='text'></td>		
	</tr>
	<tr>
		<td align='left' style='border: 0'>Nominal Transfer</td><td style='border: 0'><input type='number'></td>		
	</tr>
	<tr>
		<td align='left' style='border: 0'>Pemilik Rekening</td><td style='border: 0'><input type='text'>		
	</tr>
	<tr>
		<td align='left' style='border: 0'>Bank</td><td style='border: 0'><input type='text'></td>		
	</tr>
	<tr>
		<td align='left' style='border: 0'></td><td style='border: 0'><input type='submit' value='Konfirmasi'></td>		
	</tr-->
</tbody>
</table>


<table class="data-buyer" style="width:49%; margin-right: 1px;" align="right">
<tbody>		
	<tr>
		<td style='border: 0'><h2>Rekening Kami</h2></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'>
<div>BCA
<p>No.Rek. 0095115191<br />
A.N. Taufiq Eka Virgo dahara</p>
</div>
	<div>Bank Mandiri<br />
<span>No.Rek. 1350006716458&nbsp;</span><br />
<span>A.N. Taufiq Eka Virgo dahara</span></div></td>			
	</tr>
</tbody>
</table>

</div>