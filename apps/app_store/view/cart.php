<?php
/**
* @version		Beta 1.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/
	
	
if(empty($_SESSION['STORE_CART'])) { echo "<br><h3>Keranjang anda kosong, anda belum memilih barang.</h3>Pilih barang terlebih dahulu sebelum melihat keranjang!<br>
	<a href=\"?app=app_store\">Klik disini</a> untuk belanja.";} else {

			
?>	


<h2>
	Keranjang Belanja
</h2>
	<?php
		$count = count($_SESSION['STORE_CART']);
			$flag  = false;
			echo "<table style='width: 100%'>";
					echo "<tr>";
					echo "<th colspan='2'>Product</th><th>Price</th><th style='width: 70px'>Qty</th><th>Sub-total</th>";
					echo "</tr>";
			echo "<tbody>";			
			$qts = $qti = $ttotal = $weight = 0;
			for($i = 0; $i < $count; $i++) {
				if(isset($_SESSION['STORE_CART'][$i][0])) {
					$pid = $_SESSION['STORE_CART'][$i][0];
					$qty = $_SESSION['STORE_CART'][$i][1];
					$plus = $min =  0;
					$qts = $qts + $qty;
					if($qty > 0) $qti++;
					$img = productInfo('img1',$pid);
					if(empty($img)) $img = productInfo('img2',$pid);
					if(empty($img)) $img = productInfo('img3',$pid);
					if(empty($img)) $img = productInfo('img4',$pid);
					if(empty($img)) $img = productInfo('img5',$pid);
					$img = str_replace('media/','media/.thumbs/',$img);
					$name = productInfo('name',$pid);
					$link = make_permalink("?app=store&view=product&id=$pid");
					$brand = brandInfo('name',productInfo('brand',$pid));
					$price = productInfo('price',$pid);
					$stock = productInfo('qty',$pid);
					$weight += ceil(productInfo('shipp_weight',$pid)/10);
					if($stock == -1) $stock = 9999;
					$stotal = $qty * $price;
					$ttotal = $ttotal + $stotal;
					$price	= storeConfig('currency')." ".angka($price);
					$stotal	= storeConfig('currency')." ".angka($stotal);
					$img  = "<img src='$img' height='50'>";
					
					echo "<tr>";
					echo "<td align='center' style='width: 60px; border-right:0;'>$img</td><td style='width: 300px; border-left:0;'><a href='$link'>$name</a><br>$brand <br/></td><td style='width: 125px' align='right'>$price</td><td style='width: 70px' align='center'><span class='item-qty' rel='$pid' data-stock='$stock'>$qty</span></td><td style='width: 125px' align='right'>$stotal</td>";
					echo "</tr>";					
				}
			}			
			$i--;
			$ttotal = storeConfig('currency')." ".angka($ttotal);
			$shop = make_permalink("?app=store");
			$cart = make_permalink("?app=store&view=checkout");
			
			echo "<tr><th colspan='4' align='right'><span>Total</span></th><th align='right'> $ttotal</th></tr>";
			echo "</tbody></table>";
			echo "<div class='cart-checkout'><a href='$shop' class='cart btn button'>Continue Shopping</a>  <a href='$cart' class='cart btn button'>Checkout</a> </div>";

}
?>