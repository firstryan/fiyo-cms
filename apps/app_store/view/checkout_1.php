<?php
/**
* @version		Beta 1.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/
	
	
if(empty($_SESSION['STORE_CART'])) { echo "<br><h3>Keranjang anda kosong, anda belum memilih barang.</h3>Pilih barang terlebih dahulu sebelum melihat keranjang!<br>
	<a href=\"?app=app_store\">Klik disini</a> untuk belanja.";} else {
	
	require_once("module/breadcrumb_checkout.php");


$db = new FQuery();  
$user = $db->select(FDBPrefix."store_user","*","id = ".USER_ID);
$user = $user[0];
if(empty($user)) 
$user = $_SESSION['STORE_RCVR'];
$next = make_permalink("?app=store&view=checkout&step=2");
?>	

<form action="<?php echo $next;?>" method="post" id="eshopcartform" class="eshop eshopcart">
<div style='overflow: hidden'>

<table class="data-buyer left" style="width:49%" align="left">
<tbody>		
	<tr>
		<td colspan='2' ><h2>Informasi Pembeli</h2></td>			
	</tr>		
	<tr>
		<th align='left'>Nama </th>			
		<td><input type='text' name='name_p' class="name" value="<?php echo $user['name'];?>" required></td>
	</tr>
	<tr>
		<th align='left'>Email</th>			
		<td><input type='email' name='email' class="email"   value="<?php echo $user['email'];?>" required></td>
	</tr>
	<tr>
		<th align='left'>Telp.</th>			
		<td><input type='text' name='telp_p' class="telp"   value="<?php echo $user['telp'];?>" required></td>
	</tr>
	<tr>
		<th align='left'>Negara</th>			
		<td>
                    <select name="country_p" class="country_p" required>
<option value='' > </option>
                        <?php
                            $c = $db->select(FDBPrefix."store_country",'*');
                            foreach($c as $co) {
                                if($user['country'] == $co['country_id']) $s = 'selected'; else $s = '';
                                echo " <option value='$co[country_id]' $s>$co[name]</option>";
                                
                            }
                        ?>
                    </select></td>
	</tr>
	<tr>
		<th align='left'>Provinsi</th>			
		<td> 
                    <select name="state_p" class="state_p" required <?php if(empty($user)) echo "disabled"; ?>>
<option value='' > </option>
                        <?php
                            $s = $db->select(FDBPrefix."store_state",'*','country=1');
                            foreach($s as $st) {
                                if($user['state'] == $st['state_id'])  $s = 'selected'; else $s = '';
                                echo " <option value='$st[state_id]' $s>$st[state]</option>";
                                
                            }
                        ?>
                    </select>
                </td>
	</tr>
	<tr>
		<th align='left'>Kota</th>
                <td>
                    <select name="city_p" class="city_p" required <?php if(empty($user)) echo "disabled"; ?>>
<option value='' > </option>
                        <?php
                            $s = $db->select(FDBPrefix."store_city",'*',"state=$user[state]");
                            foreach($s as $st) {
                                if($user['city'] == $st['city_id'])  $s = 'selected'; else $s = '';
                                echo " <option value='$st[city_id]' $s>$st[city]</option>";
                                
                            }
                        ?>
                    </select>
                </td>
        </tr>
	<tr>
		<th valign='top' align='left'>Alamat</th>			
                <td><textarea name='address_p' rows='5' class="address"  style='width: 90%;resize: none;' required><?php echo $user['address'];?></textarea></td>
	</tr>			
	<tr>
		<th align='left'>Kode Pos</th>			
		<td><input size='6' type='text' name='zip_p'  class="zip" value="<?php echo $user['zip'];?>" required></td>
	</tr>
	<tr>
		<td></td>			
		<td></td>
	</tr>
</tbody>
</table>


<table class="data-buyer right data-dropship" style="width:48%; margin-right: 1px;" align="right">
<tbody>		
	<tr>
            <td colspan='2'> 
                <?php
                    if(isset($_SESSION['STORE_RCVR'])) {
                        if(!isset($_SESSION['STORE_RCVR']['dropship'])) {
                            $rcvr = $_SESSION['STORE_RCVR'];
                        }else $rcvr = $user;
                    }
                        else $rcvr = $user
                ?>
                <label>
                    <input type="checkbox" value="1" 
                        <?php
                             if(isset($_SESSION['STORE_RCVR']['dropship'])) echo "checked='checked'";
                        ?>
                           class="dropship" name="dropship"/>Sama dengan data pembeli</label>
                <h2>Informasi Penerima</h2></td>			
	</tr>		
	<tr>
		<th align='left'>Nama </th>			
		<td>
                    <input type='text' name='name' value="<?php echo $rcvr['name'];?>" required>
                    <input type='text' name='name' value="<?php echo $user['name'];?>" class="readonly copy_name">
                </td>
	</tr>
	<tr>
		<th align='left'>Telp.</th>			
		<td>
                    <input type='text' name='telp'  value="<?php echo $rcvr['telp'];?>" required>
                    <input type='text' name='telp'  value="<?php echo $user['telp'];?>" class="readonly copy_telp">
                </td>
	</tr>
	
	<tr>
		<th align='left'>Negara</th>			
		<td>
                    <select name="country" class="country" required>
<option value='' > </option>
                        <?php
                            $c = $db->select(FDBPrefix."store_country",'*');
                            foreach($c as $co) {
                               
                                if ($rcvr['country'] == $co['country_id']) {
                                $s = 'selected'; 
                                $country = $co['name'];
                            } else
                                $s = '';
                            echo " <option value='$co[country_id]' $s> $co[name]</option>";
                                
                            }
                            if(!isset($country))$country = $co[name];
                        ?>
                    </select>
                <input type='text'   value="<?php echo $country ;?>" class="readonly copy_country">
                <input type='hidden' name='country'  value="<?php echo $country ;?>" class="readonly copy_country_id">
                </td></td>
	</tr>
	<tr>
		<th align='left'>Provinsi</th>			
		<td> 
                    <select name="state" required class="state" disabled>
                       
                    </select>
                <input type='text'  value="<?php echo oneQuery('store_state','state_id',$user['state'],'state') ;?>" class="readonly copy_state">
                <input type='hidden' name='state'  value="<?php echo oneQuery('store_state','state_id',$user['state'],'state') ;?>" class="readonly copy_state_id">
                </td>
	</tr>
	<tr>
		<th align='left'>Kota</th>
                <td>
                    <select name="city" class="city" required disabled>
                       
					   </select>
					<input type='text' value="<?php echo oneQuery('store_city','city_id',$user['city'],'city') ;?>" class="readonly copy_city">
					<input type='hidden' name='city'  value="<?php echo oneQuery('store_city','city_id',$user['city'],'city') ;?>" class="readonly copy_city_id">
                  
                </td>
        </tr>
	<tr>
		<th valign='top' align='left'>Alamat</th>			
                <td>
                    <textarea required name='address' rows='5' style='width: 90%;resize: none;' ><?php echo $rcvr['address'];?></textarea>
                    <textarea name='address' rows='5' class="copy_address readonly" style='width: 90%;resize: none;' ><?php echo $user['address'];?></textarea>
                </td>
	</tr>			
	<tr>
		<th align='left'>Kode Pos</th>			
		<td>
                    <input size='6' type='text' name='zip'  value="<?php echo $rcvr['zip'];?>" required>
                    <input size='6' type='text' name='zip' value="<?php echo $user['zip'];?>" class="readonly copy_zip">
                </td>
	</tr>
	<tr>
		<td></td>			
		<td></td>
	</tr>
</tbody>
</table>

</div>
<?php
	$shop = make_permalink("?app=store");
	echo "<div class='cart-checkout'><a href='$shop' class='cart btn btn-primary button left'>Continue Shopping</a>    <input type='submit' class='cart btn btn-success button right' value='".Next."' name='submit' /> </div>";
?>
</form>
<script>
    $(function() { 
        dropprop();
        $('.dropship').click(function() {
           dropprop();
        });

        $('.product-info a').click(function() {
                $('.product-info a').removeClass('active');
                $(this).addClass('active');
                var target = $(this).attr('tab-target');
                $('.target-tab').hide();
                $('#'+target).show();
        });	

        $('.delete-item-cart').click(function(e) {
                alert("ASD");
                return false;
		});
		
		  
	function dropprop() {
             if($('.dropship').prop("checked")) {
               $(".data-dropship  input[type='text'],.data-dropship select, .data-dropship textarea").each(function(){  
                    $(this).attr({readonly:"readonly",disabled:"disabled"}).hide();           
                    $(".readonly").show().removeAttr("disabled");    
					$(".copy_zip").val($(".zip").val()).removeAttr("disabled");
					$(".copy_address").val($(".address").val()).removeAttr("disabled");
					$(".copy_name").val($(".name").val()).removeAttr("disabled");
					$(".copy_telp").val($(".telp").val()).removeAttr("disabled");     
					$(".copy_country").val($(".country_p").find(":selected").text()).removeAttr("disabled");     
					$(".copy_state").val($(".state_p").find(":selected").text()).removeAttr("disabled");     
					$(".copy_city").val($(".city_p").find(":selected").text()).removeAttr("disabled");    
					
					$(".copy_country_id").val($(".country_p").find(":selected").val()).removeAttr("disabled");     
					$(".copy_state_id").val($(".state_p").find(":selected").val()).removeAttr("disabled");     
					$(".copy_city_id").val($(".city_p").find(":selected").val()).removeAttr("disabled");     
                });
           }
           else {  
               $(".data-dropship  input[type='text'],.data-dropship  select, .data-dropship textarea").each(function(){      
                   $(this).parent().find("span").remove();
                    $(this).removeAttr("readonly disabled") .show();           
                    $(".readonly").attr({readonly:"readonly",disabled:"disabled"}).hide();                    
                });
            }   
        }
		
	function positionLightboxImage() {
		var top = ($(window).height() - $('#lightbox').height()) / 2.5;
		var left = ($(window).width() - $('#lightbox').width()) / 2;
		$('#lightbox')
		.css({
		'top': top + $(document).scrollTop(),
		'left': left
		})
		.fadeIn();
	}
			
	function removeLightbox() {
		$('#overlay, #lightbox')
		.fadeOut('slow', function() {
		$(this).remove();
		$('body').css('overflow-y', 'auto'); // show scrollbars!
		});
	}
	
		$(".country_p").change(function(e){
			var c = $(this);
			var cv = c.val();
			$.ajax({			
				url: "<?php echo FUrl; ?>apps/app_store/controller/state_ajax.php",
				type: "POST",
				data: "country="+cv,
				success: function(data){
					$(".state_p").removeAttr('disabled');
					$(".state_p").html(data);									
				}
			});	
							
		});
		
		$(".state_p").change(function(e){
			var c = $(this);
			var cv = c.val();
			$.ajax({			
				url: "<?php echo FUrl; ?>apps/app_store/controller/city_ajax.php",
				type: "POST",
				data: "city="+cv,
				success: function(data){
					$(".city_p").removeAttr('disabled');
					$(".city_p").html(data);									
				}
			});	
							
		});
		
		
	
		$(".country").change(function(e){
			var c = $(this);
			var cv = c.val();
			$.ajax({			
				url: "<?php echo FUrl; ?>apps/app_store/controller/state_ajax.php",
				type: "POST",
				data: "country="+cv,
				success: function(data){
					$(".state").removeAttr('disabled');
					$(".state").html(data);									
				}
			});	
							
		});
		
		$(".state").change(function(e){
			var c = $(this);
			var cv = c.val();
			$.ajax({			
				url: "<?php echo FUrl; ?>apps/app_store/controller/city_ajax.php",
				type: "POST",
				data: "city="+cv,
				success: function(data){
					$(".city").removeAttr('disabled');
					$(".city").html(data);									
				}
			});								
		});

	});
</script>
<?php
}
?>