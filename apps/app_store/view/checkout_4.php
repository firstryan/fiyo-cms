<?php
/**
* @version		Beta 1.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/
	
	
	echo "<table style='width: 100%' class='checkout-nav'>";
		echo "<tr>";
		echo "<th class='first'><span>1</span> Rincian Belanja</th><th><span>2</span> Data Pembeli</th><th><span>3</span> Metode Pembayaran</th><th class='active'><span>4</span> Cek Data</th><th class='last'><span>5</span> Pembayaran</th>";
		echo "</tr>";
	echo "</table>";
        
        
 $db = new FQuery();
			
?>	
<p>

</p>
<div class='notice info'>Periksa kembali data barang, data pembeli dan pengiriman barang.
</div>

<h2>
	Rincian Belanja
</h2>
    <?php
        $count = count($_SESSION['STORE_CART']);
        $flag  = false;
        echo "<table style='width: 100%'>";
        echo "<tr>";
        echo "<th colspan='2'>Product</th>"
        . "<th>Price</th>"
        . "<th style='width: 70px'>Weight</th>"
        . "<th style='width: 70px'>Qty</th>"
        . "<th>Sub-total</th>";
        echo "</tr>";
        echo "<tbody>";			
        $qts = $qti = $ttotal = $weight = $tweight= $zweight= $tqty= 0;
        for($i = 0; $i < $count; $i++) {
            if(isset($_SESSION['STORE_CART'][$i][0])) {
                $pid = $_SESSION['STORE_CART'][$i][0];
                $qty = $_SESSION['STORE_CART'][$i][1];
                $plus = $min =  0;
                $qts = $qts + $qty;
                if($qty > 0) $qti++;
                $img = productInfo('img1',$pid);
                if(empty($img)) $img = productInfo('img2',$pid);
                if(empty($img)) $img = productInfo('img3',$pid);
                if(empty($img)) $img = productInfo('img4',$pid);
                if(empty($img)) $img = productInfo('img5',$pid);
                $img = str_replace('media/','media/.thumbs/',$img);
                $name = productInfo('name',$pid);
                $link = make_permalink("?app=store&view=product&id=$pid");
                $brand = brandInfo('name',productInfo('brand',$pid));
                $price = productInfo('price',$pid);
                $ongkirock = productInfo('qty',$pid);

                $sweight = productInfo('shipp_weight',$pid);
                if(empty($sweight)) $sweight = 1;     
                               
                $weight += ceil($sweight/10);
                if($ongkirock == -1) $ongkirock = 9999;
                $ongkirotal = $qty * $price;
                $ttotal = $ttotal + $ongkirotal;
                $price	= storeConfig('currency')." ".angka($price);

                $pweight = productInfo('shipp_weight',$pid);
                if(empty($pweight)) $pweight = 1;        
                $weight = $pweight * $qty / 1000 ;
                if($weight< 1) $weight = $sweight;
                $shp = productInfo('shippment',$pid);
                $sweight = '';
                if($shp == 2){ 
                    $sweight = "<s>$weight Kg</s>";
                    $sweight .= "<br>Free Shipping!";
                }else {  $sweight = "<span>$weight Kg</span>"; 
                $zweight += $weight;}
                $tweight += $weight;
                $vtotal	= storeConfig('currency')." ".angka($ongkirotal);
                $img  = "<img src='$img' height='50'>";
                $tqty += $qty;
                echo "<tr>";
                echo "<td align='center' style='width: 60px; border-right:0;'>$img</td>"
                        . "<td style='width: 300px; border-left:0;'><a href='$link'>$name</a><br>$brand <br/></td>"
                        . "<td style='width: 125px' align='right'>$price</td>"
                        . "<td style='width: 125px' align='center' >$sweight </td>"
                        . "<td style='width: 70px' align='center'><span class='item-qty' rel='$pid' data-stock='$ongkirock'>$qty</span></td>"
                        . "<td style='width: 125px' align='right'>$vtotal</td>";
                echo "</tr>";					
            }
	}			
	$i--;
        
        $ongkirotal = storeConfig('currency')." ".angka($ttotal);
        $shop = make_permalink("?app=store");
        $cart = make_permalink("?app=store&view=checkout&step=2");
	
        if($tweight == $zweight) {
            $wprint= "<span  class='total-weight'>$zweight Kg</span>";
        } else {            
            $wprint= "<s class='disc-weight'> $tweight Kg</s> &nbsp;<span  class='total-weight'>$zweight Kg</span> ";
        }
        // total weight & subtotal
        echo "<tr>";
        echo "<th colspan='2'><input type='hidden'data-val='$ttotal' class='total-item-price' / >Shipping</th>"
        . "<th align='right'>Sub-total</th>"
        . "<th>$wprint</th>"
        . "<th>$tqty</th>"
        . "<th align='right'>$ongkirotal</th>";
        echo "</tr>";
					
        echo "<tr>";
        echo "<td colspan='5'>";
        $shipper = null;

        $db = new FQuery();  
        $shp = FDBPrefix."store_shipping";
        $sip = FDBPrefix."store_shipping_price";
        $sar = FDBPrefix."store_city_area";
        $ct = FDBPrefix."store_city";
        if(isset($_SESSION['STORE_RCVR']['city']))
        $city = $_SESSION['STORE_RCVR']['city'];
        else $city = $_POST['city'];
        $origin = 399;
        $destination = $city;
        $weight = $tweight;
//echo $weight. " :: $city";
        if(!isset($_SESSION["STORE_SHIPPING_CACHE_COST"])){
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://api.fiyo.org/ongkir.php",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "origin=$origin&dest=$destination&destination=$destination&weight=$weight",
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: bed009171e41b896b3e443aec4944ee5"
              ),
            ));

            $oks = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
               $oks;
            }

            $oks = json_decode($oks,true);
            $_SESSION["STORE_SHIPPING_CACHE_COST"] = $oks;
        } else {
            $oks = $_SESSION["STORE_SHIPPING_CACHE_COST"];
        }
        if(count($oks)) {
            $s = $db->select("$ct",'*',"city_id=$city");
            $c = $s[0]['city'];
            $flag = null;
             $i = 0;
             if (is_array($oks) || is_object($oks))
            foreach($oks as $ongkir) {
                $ck = '';
                $sa = '';
                            
                if($shipper['id'] == $ongkir['code'])  $s = 'selected'; else $s = '';
                    
                    $flag = $ongkir['code'];
                    
                    
                    if(isset($_SESSION['STORE_SHIPPER'])) {
                        if($_SESSION['STORE_SHIPPER'] == $ongkir['code']) 
                            $ck = 'checked';
                        
                    }
                    
                    if(count($ongkir['costs']) AND $ck) {
                    echo $ongkir['name']." ";
                    
                        foreach($ongkir['costs'] as $svc) {       
                            $service = $svc['service'];
                            $description = $svc['description'];
                            
                            if(isset($_SESSION['STORE_SHIPPER']) AND isset($_SESSION['STORE_SHIPPING_COST'])) {
                                if($_SESSION['STORE_SHIPPER'] == $ongkir['code'] AND 
                                        $_SESSION['STORE_SHIPPING_COST'] == $svc['cost'][0]['value'])
                                    $sel = 'selected';
                                else
                                    $sel = '';
                                    
                            }
							if(!empty($sel)) {
								$_SESSION['STORE_SHIPPING_INFO'] = " $service ($description) "
                                . "Rp. ".angka($svc['cost'][0]['value'])." "
                                . "- Etd. ".$svc['cost'][0]['etd']." hari ";
								
								echo $_SESSION['STORE_SHIPPING_INFO'];
							}
                        }
                        echo "";
                    }
                    
                
                    
                    
                $i++;
            }
        }
        else {            
             echo "<div class='alert alert-danger'>Mohon maaf, tidak tersedia pengiriman ke kota (tempat tujuan)  pada data penerima barang. <br>"
            . "Hubungi kami segera untuk informasi pengiriman ke tempat tujuan!</div>";
        }
        
	//shipping	
        
        if(isset($_SESSION['STORE_SHIPPING_COST'])) {
            $cost = $_SESSION['STORE_SHIPPING_COST'];
        }
        
        $cship = $cost * $weight;
        $tship = $cost * $zweight;
        $ctotal	= storeConfig('currency')." ".angka($cship);
        $vtotal	= storeConfig('currency')." ".angka($tship);

        if($tweight == $zweight) {
            $wprint = "$vtotal";
        } else {            
            $wprint = "<s>$ctotal</s><br>$vtotal ";
        }
        
        echo "</td><td style='width: 125px' align='right' class='shipping-cost'>$wprint</td>";
	echo "</tr>"; 
        
        $ttotal = $ttotal + $tship ;
        $ttotal	= storeConfig('currency')." ".angka($ttotal);
        echo "<tr><th colspan='5' align='right'><span>Total</span></th><th align='right' class='total-cost'> $ttotal</th></tr>";

        echo "</tbody></table>";

	$prev = make_permalink('?app=store&view=checkout&step=3');
	
	?>
	
	
<div style='overflow: hidden'>

<table class="data-buyer" style="width:48%" align="left">
<tbody>		
	<tr>
		<td style='border: 0'><h2>Informasi Pembeli</h2></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'><b><?=$_SESSION['STORE_RCVR']['name_p'];?></b><br>
		<?=$_SESSION['STORE_RCVR']['address_p'];?><br><?=oneQuery('store_city','city_id',$_SESSION['STORE_RCVR']['city_p'],'city') ;?>, <?=oneQuery('store_state','state_id',$_SESSION['STORE_RCVR']['state_p'],'state') ;?>, <?=oneQuery('store_country','country_id',$_SESSION['STORE_RCVR']['country_p'],'name') ;?><br><?=$_SESSION['STORE_RCVR']['zip_p'];?></td>			
	</tr>
</tbody>
</table>


<table class="data-buyer" style="width:49%; margin-right: 1px;" align="right">
<tbody>		
	<tr>
		<td style='border: 0'><h2>Data Penerima</h2></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'><b><?=$_SESSION['STORE_RCVR']['name'];?></b><br>
		<?=$_SESSION['STORE_RCVR']['address'];?><br><?=oneQuery('store_city','city_id',$_SESSION['STORE_RCVR']['city'],'city') ;?>, <?=oneQuery('store_state','state_id',$_SESSION['STORE_RCVR']['state'],'state') ;?>, <?=oneQuery('store_country','country_id',$_SESSION['STORE_RCVR']['country'],'name') ;?><br><?=$_SESSION['STORE_RCVR']['zip'];?></td>			
	</tr>
</tbody>
</table>

<table class="data-buyer" style="width:49%; margin-right: 1px;" align="left">
<tbody>		
	<tr>
		<td style='border: 0'><h2>Metode Pembayaran</h2></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'>
                    <?php
                        $method = $db->select(FDBPrefix."store_payment_method","*","method_id = ".$_SESSION['STORE_PAYMENT_METHOD']);
                        $method = $method[0];
                    
                    ?>
                    
                    <b><?php echo $method['name'];?></b><br><?php echo $method['instruction'];?></td>			
	</tr>
</tbody>
</table>

<table class="data-buyer" style="width:49%; margin-right: 1px;" align="right">
<tbody>		
	<tr>
		<td style='border: 0'><h2>Perhatian!</h2></td>			
	</tr>		
	<tr>
		<td align='left' style='border: 0'>
<p>
Menekan tombol selanjutnya berarti anda akan masuk kehalaman Pembayaran dan tidak bisa menuju halaman sebelumnya. Keranjang belanja akan dihapus tetapi transaksi ini bisa dilihat dihalaman history transaksi.
</p></td>			
	</tr>
</tbody>
</table>
</div>
<form method='post'>
	<?php
	echo "<div class='cart-checkout'><a href='$prev' class='cart btn button'>".Prev."</a>  <input type='submit' class='cart btn button' value='".Next."' name='submit' /> </div>";
?>
</form>
