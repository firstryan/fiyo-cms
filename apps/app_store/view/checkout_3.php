<?php

/**
* @version		Beta 1.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
* @description	
**/
	
	
if(empty($_SESSION['STORE_CART'])) { echo "<br><h3>Keranjang anda kosong, anda belum memilih barang.</h3>Pilih barang terlebih dahulu sebelum melihat keranjang!<br>
	<a href=\"?app=app_store\">Klik disini</a> untuk belanja.";} else {
	
	echo "<table style='width: 100%' class='checkout-nav'>";
		echo "<tr>";
		echo "<th><span>1</span> Rincian Belanja</th><th><span>2</span> Data Pembeli</th><th class='active'><span>3</span> Metode Pembayaran</th><th><span>4</span> Cek Data</th><th class='last'><span>5</span> Pembayaran</th>";
		echo "</tr>";
	echo "</table>";
			
	
	
?>	

<form action="" method="post" id="eshopcartform" class="eshop eshopcart">
<div style='overflow: hidden'>

    <div>
       <h2>Metode Pembayaran</h2> 
            <ul class="payment-store">
               <?php

                    $db = new FQuery();
                    $pm = $db->select(FDBPrefix."store_payment_method",'*',"status = 1");
                    $i = 1;
                    foreach ($pm as $pay) {
                        if(isset($_SESSION['STORE_PAYMENT_METHOD']) AND $_SESSION['STORE_PAYMENT_METHOD'] == $pay['method_id']) {
                            $sel = "checked";
                        } else $sel = '';
                        echo " <li>
                            <a align='left' data-tab=$i><label>
                                <input type='radio' name='pays' value='$pay[method_id]' $sel><img src='".FUrl."$pay[logo]'>
                            <div>$pay[desc] </div></a></label>
                    </li>";
                    }
               ?>	
            </ul>
    </div>

</div>
<?php
	$prev = make_permalink('?app=store&view=checkout&step=2');
	$next = make_permalink("?app=store&view=checkout&step=4");
	echo "<div class='cart-checkout'><a href='$prev' class='cart btn button'>".Prev."</a>  <input type='submit' class='cart btn button' value='".Next."' name='submit' /> </div>";
?>
</form>
<script>
$(function() {
    $('input[name="pays"]').change(function() { 
        t = $(this);
            
            $('.payment-store li').removeClass('active');
            t.parents('li').addClass('active');
            h = t.parents('li').find('div').show().height();
            $('.payment-store').css({height: h + 100});  
    });	
    $('input[name="pays"]').each(function() { 
        t = $(this);
        if(t.is(':checked')) { 
            t.parents('li').addClass('active');
            h = t.parents('li').find('div').show().height();
            $('.payment-store').css({height: h + 100});            
        } else {
            t.parents('li').removeClass('active');
        }
    });	
    
    
});	

</script>
<?php
}
?>