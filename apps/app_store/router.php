<?php
/**
* @version		1.5.0
* @package		Fi Store
* @copyright	Copyright (C) 2011 Fiyo CMS.
* @license		GNU/GPL, see liCENSE.php
**/

defined('_FINDEX_') or die('Access Denied');


/****************************************/
/*		   Function View Type			*/
/****************************************/ 	
function store_view($type) {
	echo "<div id='store'>";
		require("apps/app_store/view/$type.php");
	echo "</div>";
}

/****************************************/
/*			 Load View Type				*/
/****************************************/ 	
$id = app_param('view');
switch($id)
{	
	default :
	 store_view("default");
	break;
	case 'category':	 
	 store_view("category");
	break;
	case 'brand':	 
	 store_view("brand");
	break;
	case 'product':
	 store_view("product");
	break;
	case 'cart':
	 store_view("cart");
	break;
	case 'checkout':
	 store_view("checkout");
	break;
	case 'login':
	 store_view("login");
	break;
}

?>
