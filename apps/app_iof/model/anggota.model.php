<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/


class IOFModel extends Model {

    public static $table = 'iof_anggota';
    public static $index = 'id_anggota';
    public static $fillable = [
        'nama_anggota', 
        'email_anggota', 
        'no_ktp', 
        'tanggal_lahir', 
        'tempat_lahir', 
        'jenis_kelamin', 
        'golongan_darah', 
        'alamat_anggota', 
        'foto_tkp', 
        'foto_kta', 
        'klub_id', 
        'status', 
        'created_at', 
        'deleted_at', 
        'updated_at', 
    ];
    public static $validator = [
        'nama_anggota' => 'required',
        'email_anggota' => 'required',
        'no_ktp' => 'required',
        'klub_id' => 'required',
    ];


    public function __construct () {
        parent::$table = self::$table;   
        parent::$index = self::$index; 
        parent::$field = self::$field; 
        parent::$fillable = self::$fillable;
        parent::$validator = self::$validator;
    }

    public static function field() {        
        parent::$table = self::$table;  
        return self::$fillable;
    }
    

    public static function view() {            
        parent::$limit = 1;    
        parent::$orderBy = 'nama_klub ASC';  
        parent::$where = "nama_klub = 'nama_klub'";
        
        return Model::listJson();
    }    
    
    public static function data($key, $value = 'value') {        
        $model =  Model::find("name = '$key'");
        return $model;        
    }
    
    public static function patch($preset = [], $reff = 'id') { 
        return Model::patch($preset, $reff);
    }
    
    public static function add($preset = []) {
        return Model::add($preset);
    }


}

