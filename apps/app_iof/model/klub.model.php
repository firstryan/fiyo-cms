<?php
/**
* @version		2.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2014 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.
**/


class KlubModel extends Model {

    public static $table = 'iof_klub';
    public static $index = 'id_klub';
    public static $fillable = [
        'alamat_klub', 
        'telp_klub', 
        'pengda', 
        'pengcab', 
        'nama_klub', 
        'email_klub', 
        'tanggal_daftar', 
        'tanggal_berdiri', 
        'bukti_registrasi', 
    ];
    public static $validator = [
        'nama_klub' => 'required',
        'email_klub' => 'required',
        'pengda' => 'required',
        'pengcab' => 'required',
    ];


    public function __construct () {
        parent::$table = self::$table;   
        parent::$index = self::$index; 
        parent::$field = self::$field; 
        parent::$fillable = self::$fillable;
        parent::$validator = self::$validator;
    }

    public static function field() {        
        parent::$table = self::$table;  
        return self::$fillable;
    }
    

    public static function view() {            
        parent::$limit = 1;    
        parent::$orderBy = 'nama_klub ASC';  
        parent::$where = "nama_klub = 'nama_klub'";
        
        return Model::listJson();
    }    
    
    public static function data($key, $value = 'value') {        
        $model =  Model::find("name = '$key'");
        return $model;        
    }
    
    public static function patch($preset = [], $reff = 'id') { 
        return Model::patch($preset, $reff);
    }
    
    public static function add($preset = []) {
        return Model::add($preset);
    }



}

