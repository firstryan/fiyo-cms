<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/
   
defined('_FINDEX_') or die('Access Denied'); ?>
<?=Form::model(Klub::form(),['class' => 'validate', 'validate' => true, 'ajax' => true]);?>
<h2>Form Pendaftaran Klub 
    <a href="<?=url('?app=iof&view=anggota&action=create');?>" class="btn btn-danger float-right">  <i class="fa fa-file-text"></i> &nbsp; Pendaftaran Anggota  </a>
</h2>
<hr>


<div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Klub</h3>

        <div class="form-group">            
            <?= Form::text('nama_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama Klub"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::email('email_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Email Klub"); ?>
        </div>
        
        <div class="form-group">            
                <?= Form::text('telp_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "No. Telp / No. WA"); ?>
        </div>

        <div class="form-group">            
                <?= Form::text('alamat_klub', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "Alamat"); ?>
        </div>

        <div class="form-group">        
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Pengda</label>   
            <?= Form::select('pengda', [], '',  
                [
                    "class" => "form-control pengda", 
                    "required", 
                    "data-list" => "?api=address&type=province",
                    "label-class" => "bmd-label-floating", 
                    "data-enable" => "33",
                    "data-default" => "",
                    "data-disable" => "all",
                    "data-trigger" => ".pengcab|?api=address&type=city&parent={val}",
                ], 
                "Pilih Pengurus Daerah ... "); ?>
        </div>
        
        <div class="form-group">    
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Pengcab</label>        
            <?= Form::select('pengcab', [], '', ["class" => "form-control pengcab", "label-class" => "bmd-label-floating", "required"], "Pilih Pengurus Cabang ..."); ?>
        </div>


        <div class="form-group">
            <?=Form::submit('Daftar Sekarang', ["class" => "btn-success align-right"]); ?>
        </div>
    </div>
</div>
<?=Form::close();?>