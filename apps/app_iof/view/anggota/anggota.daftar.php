<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/
   
defined('_FINDEX_') or die('Access Denied'); ?>

<?=Form::model(Anggota::form(),['class' => 'validate', 'validate' => true, 'ajax' => true, 'files' => true]);?>

<div class="row">
    <div class="col-lg-9  ">
        <h2 style="margin: 0">Form Pendaftaran Anggota 
        
        </h2>
    </div>
    <?php if(userAllowExcept([4,5])) : ?>
    <div class="col-lg-3 text-center">
       <a href="<?=url('?{app=}&view=klub&action=create');?>" class="btn btn-danger ">  <i class="fa fa-file-text"></i> &nbsp; Pendaftaran Klub  </a>
    </div>
    <?php endif; ?>
</div>

<hr>

<div class="row">
    <div class="col-lg-6 offset-lg-3 ">
        <h3> Data Anggota</h3>

        <div class="form-group">        
            <label for="klub_id" class="bmd-label-static bmd-label-floating">Klub</label>   
            <?= Form::select('klub_id', [], '',  
                [
                    "class" => "form-control pengda", 
                    "required", 
                    "data-list" => "?app=iof&api=klub|GET|id_klub|nama_klub",
                    "label-class" => "bmd-label-floating", 
                    "data-default" => "",
                    "data-trigger" => ".pengcab|?api=address&type=city&parent={val}",
                ], 
                "Pilih Klub ... "); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('nama_anggota', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nama sesuai KTP"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('no_ktp', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Nomor KTP"); ?>
        </div>
        
        <div class="form-group">            
            <?= Form::text('email_anggota', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Email"); ?>
        </div>

        <div class="row">
            <div class="col-lg-4">
                
                <div class="form-group">            
                    <?= Form::date('tanggal_lahir', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Tanggal Lahir"); ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">            
                    <?= Form::text('tempat_lahir', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Tempat Lahir"); ?>
                </div>
            </div>
        </div>

        
        
        <div class="form-group">        
            <label for="jenis_kelamin" class="bmd-label-static bmd-label-floating">Jenis Kelamin</label>
            <?php
                    $kel = [
                        "Laki-kali" => "Laki-kali",
                        "Perempuan" => "Perempuan",
                    ]
                
                ?>
                <?= Form::select('jenis_kelamin', $kel, '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pilih jenis kelamin"); ?>
            
        </div>
        
        <div class="form-group">  
            <label for="gol_darah" class="bmd-label-static bmd-label-floating">Golongan Darah</label>
                <?php
                    $gol = [
                        "A" => "Gol. A",
                        "B" => "Gol. B",
                        "AB" => "Gol. AB",
                        "O" => "Gol. O",
                        "-" => "Belum tahu",
                    ]
                
                ?>
                <?= Form::select('gol_darah', $gol, '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required"], "Pilih golongan darah"); ?>
            
        </div>


        <div class="form-group">            
                <?= Form::text('alamat_anggota', '', ["class" => "form-control", "label-class" => "bmd-label-floating", "required", "rows" => 3], "Alamat"); ?>
        </div>


        
        <div class="row">
            <div class="col-lg-6">               
                <div class="form-group">
                    <?= Form::fileimage('foto_ktp', '',["class" => "form-control full-image fileimage-75", "placeholder" => "Foto KTP", "required"]); ?>
                </div>
            </div>
            <div class="col-lg-6">        
                <div class="form-group">
                    <?= Form::fileimage('foto_kta', '',["class" => "form-control full-image fileimage-75", "placeholder" => "Foto KTA"]); ?>
                </div>
            </div>
        </div>



       
      



        <div class="form-group">
            <?=Form::submit('Daftar Sekarang', ["class" => "btn-success align-right"]); ?>
        </div>
    </div>

</div>
<?=Form::close();?>