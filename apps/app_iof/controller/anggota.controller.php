<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


defined('_FINDEX_') or die('Access Denied');

Class Anggota {

	public static function list() {					
		return Model::listObject();
	}
	
	public static function default() {
		$list = Model::item(1);		
		return $list;
	}
	public static function delete() {
		$del = Model::delete();		
		notice(Model::$message);
		if($del) redirect("?&");	
	}

	public static function move_file($file, $options = ['extension' => array('jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc'), 'path' => 'files', 'encrypt' => false]) {
		$target_folder = $options['path'];  //default path is media/
		if(empty($file['name']))
			return false;

		$file_name = $file['name'];
		$file_size = $file['size'];
		$file_type = $file['type'];
		$file_tmp = $file['tmp_name'];

		$ext = pathinfo($file_name, PATHINFO_EXTENSION);

		if($options['encrypt'])
			$new_name = md5_file($file_tmp);
		else
			$new_name = $file_name;
		$dest_path = "media/".$target_folder ."/$new_name";

		$allow_ext = $options['extension'];
		if(file_exists($dest_path))
		 return true;
		if (in_array($ext, $allow_ext)) {
			if(move_uploaded_file($file_tmp, $dest_path))
			{
				return true;
			}
			else
			{
			  	return false;
			}
		} 
		else 
			return false;
	}

	public static function form() {
		//set transaction database
		Model::transaction();
		
		if(Model::patch()) {
			Model::commit();
			notice(Model::$message);
			refresh();
		} 
		else if(Model::add()) {
			
			$post = Input::$post;
			//create user based on model
			$post = Input::$post;
			$data['name'] = $post['nama_anggota'];
			$data['user'] = $post['email_anggota'];
			$data['email'] = $post['email_anggota'];
			$data['level'] = 5;
			$data['status'] = 1;
			$data['password'] = 'jateng';

			if(User::register($data)) {
				//upload files
				foreach(Input::$file as $file)
					self::move_file($file);
				Model::commit();
				Api::redirect('?&action=created');
			}else {			
				Model::rollback();
				notice('error',DB::$error);
			}
		} 
		else {
			Model::rollback();
			notice(Model::$message);
		}
		
		//set model if requirement is valid
		if(app_param('id') AND app_param('action') == 'edit') {
			return Model::item(app_param('id'));
		}

		//set model for crate action
		else if(app_param('action') == 'create' AND app_param('id') === null) {
			return true;
		} 
		//block for any
		else {
			http_response_code(404);	
		}
			
	}

}



