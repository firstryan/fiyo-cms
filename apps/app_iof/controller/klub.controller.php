<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


defined('_FINDEX_') or die('Access Denied');


Class Klub {
	public function __construct() {	
		new KlubModel();
	}

	public static function list() {					
		return Model::listObject();
	}
	
	public static function default() {
		$list = Model::item(1);		
		return $list;
	}
	public static function delete() {
		$delete = Model::delete();
		Model::$redirect = "asd";
		notice(Model::$message);
		if($delete) refresh();
		/*
		ini deletenya masih belum stabil tolong diperbaiki 
		*/
	}

	public static function form() {
		//set transaction database
		Model::transaction();
		
		if(Model::patch()) {
			//commit transaction
			Model::commit();
			notice(Model::$message);
			refresh();
		} 
		else if(Model::add()) {
			//create user based on model
			$post = Input::$post;
			$data['name'] = $post['nama_klub'];
			$data['user'] = $post['email_klub'];
			$data['email'] = $post['email_klub'];
			$data['level'] = 4;
			$data['status'] = 1;
			$data['password'] = 'jateng';

			if(User::register($data)) {
				Model::commit();
				Api::redirect('?&action=created');
			}else {			
				Model::rollback();
				notice('error',DB::$error);
			}
		} 
		else {
			Model::rollback();
			notice(Model::$message);
		}
		
		//set model if requirement is valid
		if(app_param('id') AND app_param('action') == 'edit') {
			return Model::item(app_param('id'));
		}

		//set model for crate action
		else if(app_param('action') == 'create' AND app_param('id') === null) {
			return true;
		}		
		//block for any
		else {
			http_response_code(404);	
		}			
	}

} 
new Klub(); //initialize controller



