<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2020 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
**/


// header('Content-Type: application/json');     
defined('_FINDEX_') or die('Access Denied');

$id     = app_param('id');
$pengda = app_param('pengda');
$pengcab = app_param('pengcab');

$data   = false;
$filter = app_param('filter');
$filter = str_replace("%20", ' ', $filter);

$pengda = ($pengda) ? " AND pengda = '$pengda'" : '';
$pengcab = ($pengcab) ? " AND pengda = '$pengcab'" : '';
$filter = ($filter) ? " AND nama_klub LIKE '%$filter%'" : '';

$id = ($id) ? " AND id_klub = '$id'" : '';

$data = DB::table("iof_klub")->where("1=1$id$pengda$pengcab$filter")->get();

Api::render($data);