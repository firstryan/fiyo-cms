<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/

Route::view('', function() {	
	if(!isHomePage)
		redirect('?apps={apps}&view=klub&action=daftar');
	
});

Route::view('klub', function() {	
			
	Route::action('', function() {
		View::load('list');
	});

	Route::action('create', function() {
		View::load('klub.form');
	})->link('klub/daftar');

	Route::action('created', function() {
		View::load('klub.notice.sukses');
	})->link('anggota/daftar/sukses');

	Route::action('edit', function() {
		View::load('klub.form');
	})->link('{app}/{view}/{action}/{$id}');
		
	Route::action('delete', function() {
		Klub::delete();
	})->link('{app}/{view}/{action}/{$id}');
});


Route::view('anggota', function() {	
	Route::action('', function() {
		View::load('list');
	});		
	Route::action('create', function() {
		View::load('anggota.daftar');
	})->link('anggota/daftar');
	Route::action('created', function() {
		View::load('anggota.daftar.sukses');
	})->link('anggota/daftar/sukses');
	Route::action('edit', function() {
		View::load('anggota.daftar');
	})->link('{app}/{view}/{action}/{$id}');
	Route::action('delete', function() {
		View::load('anggota.daftar');
	})->link('{app}/{view}/{action}/{$id}');
})->link('anggota');


Route::api('klub', function() {
	Api::load('klub');
});