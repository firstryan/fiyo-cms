<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


defined('_FINDEX_') or die('Access Denied');

Class Admin {

	public static function list() {					
		return Model::listObject();

	}
	
	public static function default() {
		$list = Model::item(1);		
		return $list;
	}

	public static function form() {
		if(Model::patch()) {
			notice(Model::$message);
			refresh();
		} 
		else if(Model::add()) {
			notice(Model::$message);
			redirect("?app=admin&action=edit&id=". Model::$last_id);
		} 
		else {
			notice(Model::$message);
		}
		
		if(app_param('id') AND app_param('action') == 'edit')
			return Model::item(app_param('id'));
		else if(app_param('action') == 'create' AND app_param('id') === null) {
			return true;
		} else {
			http_response_code(404);
		}
			
	}

}



