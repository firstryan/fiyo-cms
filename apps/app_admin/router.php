<?php 
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
* @description	
**/


defined('_FINDEX_') or die('Access Denied');

Route::view('', function() {
	Route::action('', function() {
		View::list('default');
	});
	
	Route::action('create', function() {
		View::create('form');
	});
	
	Route::action('edit', function() {
		View::edit('form');
	});
	
	Route::action('delete', function() {
		View::delete('klub');
	});
});


Route::api('test', function() {
	Route::action('', function() {
		Api::file('default');
	});
});

Route::api('2', function() {
		echo "ads";
});

