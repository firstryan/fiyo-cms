<div class="main">
    <div class="app-header">
        <h2>Tugas Belajar <i class="icon-angle-right"></i> Pemberian</h2>
    </div>

    
    <div class="tbib">
        <div class="app-sub-header">
            <a class="btn btn-default panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view').""); ?>"> 
           <i class="icon-arrow-left"></i> Kembali
            </a> 
        </div>
        
            

        <div class="list-menu">
            <a class=" panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=apbd"); ?>"> 
                <div class="label-big" style="">
                    <div class="img">
                        <i class="icon-check"></i>
                    </div>
                    <h2>APBD</h2>
                    <p>
                        Keterangan tentang menu ini
                    <p>
                </div>
            </a> 
            
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=nonapbd"); ?>"> 
            <div class="label-big" style="">
                        <div class="img">
                            <i class="icon-exclamation"></i>
                        </div>
                        <h2>Non-APBD</h2>
                        <p>
                            Keterangan tentang menu ini
                        <p>
                    </div>
            </a> 
        
        
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=sharing"); ?>"> 
            <div class="label-big" style="">
                        <div class="img">
                            <i class="icon-money"></i>
                        </div>
                        <h2>Cost Sharing</h2>
                        <p>
                            Keterangan tentang menu ini
                        <p>
                    </div>
            </a>         

           
        </div> 
    </div> 
</div> 
