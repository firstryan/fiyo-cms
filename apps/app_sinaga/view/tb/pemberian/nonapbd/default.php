<div class="main">
    <div class="app-header">
        <h2>Tugas Belajar <i class="icon-angle-right"></i> Pemberian <i class="icon-angle-right"></i> Non-APBD</h2>
    </div>

    <div class="tbib">
		<div class="app-sub-header">
			<a class="btn btn-default panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')); ?>"> 
			<i class="icon-arrow-left"></i> Kembali
            </a> 
            </a> 
        </div>
            

        <div class="list-menu">
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=".app_param('cat')."&std=s1"); ?>"> 
			<div class="label-big" style="">
                    <div class="img">
                        <i class="icon-bookmark"></i>
                    </div>
                    <h2>S1 / D4</h2>
                    <p>
                        Keterangan tentang menu ini
                    <p>
                </div>
            </a> 
            
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=".app_param('cat')."&std=s2"); ?>"> 
			<div class="label-big" style="">
                    <div class="img">
                        <i class="icon-bookmark"></i>
                    </div>
                    <h2>S2 / PPDS</h2>
                    <p>
                        Keterangan tentang menu ini
                    <p>
                </div>
            </a> 
        
        
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=".app_param('cat')."&std=s3"); ?>">                 
				<div class="label-big" style="">
                    <div class="img">
                        <i class="icon-bookmark"></i>
                    </div>
                    <h2>S3 / PPD Sub-Spesialis</h2>
                    <p>
                        Keterangan tentang menu ini
                    <p>
                </div>
			</a> 
			
            <a class="panel" href="<?php echo make_permalink("?app=".app_param('app')."&view=".app_param('view')."&type=".app_param('type')."&cat=".app_param('cat')."&std=tk"); ?>"> 
			<div class="label-big" style="">
                    <div class="img">
                        <i class="icon-ambulance"></i>
                    </div>
                    <h2>Tenaga Kesehatan</h2>
                    <p>
                        Keterangan tentang menu ini
                    <p>
                </div>
            </a> 
        

           
        </div> 
    </div> 
</div> 
