<!--
 =========================================================
 * Material Kit - v2.0.6
 =========================================================

 * Product Page: https://www.creative-tim.com/product/material-kit
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
   Licensed under MIT (https://github.com/creativetimofficial/material-kit/blob/master/LICENSE.md)


 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>
    {title}
  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="url" content="{homeurl}" />
  <meta name="viewport" content="width=device-width">

  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <!--     Fonts and icons     -->
  <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
  <!-- CSS Files -->
  
  <link href="/plugins/plg_js/loader.css" rel="stylesheet" />
  <link href="./assets/css/material-kit{mobile}.css" rel="stylesheet" />
  <link href="./assets/css/iof.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
  <script src="./assets/js/core/jquery.min.js" data-static="true"></script>
  <script src="./assets/js/core/popper.min.js" data-static="true" ></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js" data-static="true"></script>


  <script src="./assets/js/plugins/moment.min.js"  data-static="true"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js" data-static="true"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <!-- <script src="./assets/js/plugins/nouislider.min.js" ></script> -->
  <!--  Google Maps Plugin    -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-kit.js?v=2.0.6"  data-static="true"></script>
  <script src="/plugins/plg_js/loader.js"  data-static="true"></script>	
  <script src="/plugins/plg_js/main.js"  data-static="true"></script>	
  
  </head>

<body class="index-page sidebar-collapse material-ui">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="{homeurl}">
          {sitetitle} </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
      {module:mainmenu}
        <!-- <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="#" class="nav-link" data-toggle="dropdown">
              <i class="material-icons">assignment</i> Pendaftaran
            </a>
        </li>
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="material-icons">apps</i> Components
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <a href="./index.html" class="dropdown-item">
                <i class="material-icons">layers</i> All Components
              </a>
              <a href="https://demos.creative-tim.com/material-kit/docs/2.1/getting-started/introduction.html" class="dropdown-item">
                <i class="material-icons">content_paste</i> Documentation
              </a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)" onclick="scrollToDownload()">
              <i class="material-icons">cloud_download</i> Download
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
              <i class="fa fa-facebook-square"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
        </ul> -->
      </div>
    </div>
  </nav>
  <div class="page-header header-filter header-{home}" data-parallax="true" style="background-image: url('./assets/img/bg1.jpg');">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
  <?php if(isHomePage()) : ?>
          {module:slidehome}
  
          <?php else : ?>
          {module:slide}
  <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <?php if(!isHomePage()) : ?>
  <div class="main main-raised">
    <div class="section section-basic">
      <div class="container">
      {module:atas}
      {loadApps}


        
      </div>
    </div>
  </div>
  
  
  <?php endif; ?>
  
  <script src="/plugins/plg_js/footer.js"  data-static="true"></script>	
</body>

</html>
