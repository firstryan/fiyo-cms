<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{sitetitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{sitedesc}">
    <meta name="keywords" content="{sitekeys}">



<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
<link href="css/ionicons.css" rel="stylesheet">
<link rel="stylesheet" href="css/jquery.classycountdown.css" />
<link href="css/styles-off.css" rel="stylesheet">
<link href="css/responsive-off.css" rel="stylesheet">
</head>
<body>
<div class="main-area">
<section class="left-section">
</section>
<section class="right-section full-height">
<div class="display-table">
<div class="display-table-cell">
<div class="main-content">
<h1 class="title"><b>Under Construction</b></h1>
<p class="desc">Our website is currently undergoing scheduled maintenance.
We Should be back shortly. Thank you for your patience.</p>
<div class="email-input-area">
</div>
<p class="post-desc">Happy coding... <i class="ion-heart" style="color: red"></i></p>
</div>
</div>
</div>
<ul class="footer-icons">
<li>Stay in touch : </li>
<li><a href="#"><i class="ion-social-facebook"></i></a></li>
<li><a href="#"><i class="ion-social-twitter"></i></a></li>
<li><a href="#"><i class="ion-social-googleplus"></i></a></li>
<li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
<li><a href="#"><i class="ion-social-pinterest"></i></a></li>
</ul>
</section>
</div>

<script src="js/jquery.min.js" type="4bc0120ca9603d331da0f3d6-text/javascript"></script>
<script src="js/jquery.countdown.min.js" type="4bc0120ca9603d331da0f3d6-text/javascript"></script>
<script src="js/scripts-off.js" type="4bc0120ca9603d331da0f3d6-text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="4bc0120ca9603d331da0f3d6-text/javascript"></script>
<script type="4bc0120ca9603d331da0f3d6-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script><script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="4bc0120ca9603d331da0f3d6-|49" defer=""></script></body>
</html>