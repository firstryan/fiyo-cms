<?php
/**
* @version		3.0
* @package		Fi ImageSlider
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, thanks to WOWSlider :)
* @description	
**/

defined('_FINDEX_') or die('Access Denied');


?>

<?php

if(userAllow([4])) : ?>

<?php


$klub = DB::table(FDBPrefix.'iof_klub')->where("email_klub = '". USER_EMAIL ."'")->first();




$jml_member = DB::table(FDBPrefix.'iof_anggota')->where("klub_id = $klub[id_klub]")->count();
$jml_event =  DB::table(FDBPrefix.'iof_event')->where("klub_id = $klub[id_klub]")->count();
// $jml_member = DB::table(FDBPrefix.'iof_event')->where("klub_id = $klub[id_klub]")->count();

?>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
            <a class="btn btn-primary float-right" style="color: #fff" href="<?= url("?app=iof&view=daftar&type=anggota");?>">Tambah Anggota &nbsp; <i class="fa fa-user"></i></a>
        <H3 class="card-title"><?=$jml_member;?></H3>
        <p>Jumlah Anggota</p>
      </div>
      <div class="card-body">
      ...
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
            <a class="btn btn-danger float-right" style="color: #fff" href="?app=iof&view=event&type=register">Pengajuan Event Baru &nbsp; <i class="fa fa-calendar"></i></a>
        <H3 class="card-title"><?=$jml_event;?></H3>
        <p>Jumlah Event</p>
        </div>
        <div class="card-body">
        
            <?php


              $sql =  DB::table(FDBPrefix.'iof_event')
                ->select("*,regencies.name as kota")
                ->where("klub_id = $klub[id_klub]")
                ->leftJoin("regencies","kota = id")
              ->get();
              $no = 1;
              foreach($sql as $data) :
              
                            
                            
              $s = $data['status'];
              if($s == 0) {
                $status = '<label class="badge badge-default"> menunggu persetujuan dari PENGDA</label>';
              }
              if($s == 1) {
                $status = '<label class="badge badge-primary"> PENGDA menyutujui Tanggal Booking</label> 
                
                
                <br /> <u><a href="" class="btn-status"> Kirim pengajuan registrasi </a></u>';
              }
              elseif($s == 2) {
                $status = '<label class="badge badge-warning"> PKLUB pengajuan Registrasi ke PENGCAB </label>';
              }
              elseif($s == 3) {
                $status = '<label class="badge badge-danger"> PENGCAB tidak menyetujui Registrasi</label>';
              }
              elseif($s == 4) {
                $status = '<label class="badge badge-primary"> PENGCAB menyetujui Registrasi</label>';
              }
              elseif($s == 5) {
                $status = '<label class="badge badge-success"> Persyaratan Event Lengkap</label>';
              }
  
              ?>
                <div>
                    <b style="font-size: 16px"><?=$data['nama_event'];?></b>
                    <br>                    
                    <?=$data['kota'];?>, <?=$data['tanggal_booking'];?>
                    <br>
                    <?=$status;?>
                    
                </div>


              <?php $no++; endforeach; ?>
              
        </div>
    </div>
  </div>
</div>
<script>

$(function () {

  $(".btn-status").click(function (e) {
    e.preventDefault();
		status = $(this).data('status');
		
		$.post("index.php?app=iof&api=event&type=status&status=1&id=<?=$klub['id_klub'];?>", '_token='+ $('[name=_token').val() + '&id_event=<?=Req::get('id');?>&status='+ status)
		.done(function (result) {
				notice(result);
			id = parseInt(result.id); 
		})
		.fail(function(e) {
      
			logs(e.responseText);
		});	
  });

});
  


</script>
<?php elseif(userAllow([5])) : ?>

<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <H3 class="card-title">44</H3>
        <p>Jumlah Member</p>

        Pengajuan Event
      </div>
      <div class="card-body">
      ...
      </div>
    </div>
  </div>

  

<?php endif;?> 
