
<?php
/**
* @version		3.0
* @package		Fiyo CMS
* @copyright	Copyright (C) 2019 Fiyo CMS.
* @license		GNU/GPL, see LICENSE.txt
*/

defined('_FINDEX_') or die('Access Denied');

$path = "modules/mod_testimonial";
addCss("$path/assets/css/testimonial.css");
addJs("$path/assets/js/testimonial.js");


$param = $qr['parameter'];
if(checkLocalhost()) {
	$param = str_replace(FLocal."media/","media/",$param);
	$param = str_replace("/media/",FUrl."media/",$param);				
}

$param = htmlspecialchars_decode($param);
$param = str_replace("&#039;", '"', $param);


?>

<div class="testimonial">
  <div class="testimonial-buttons-bar">
    <div class="testimonial-buttons">
    <a id="testimonial-prev" href="#">&lsaquo;</a><a id="testimonial-next" href="#">&rsaquo;</a> 
    </div>
  </div>
    <div class="testimonial-slides">
        <?php echo $param; ?>
       
    </div>
</div>