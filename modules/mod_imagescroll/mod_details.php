<?php
/**
* @version		v 1.0.0
* @package		Fi ImageScroll
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL
* @description	
**/

$module_name='Fi ImageScroll';
$module_version='1.2.0';
$module_date='9 Februari 2012';
$module_author='Fiyo Developers';
$module_author_url='http://dev.fiyo,org';
$module_author_email='dev@fiyo.org';
$module_desc='Membuat Gambar berjalan dengan memasukan path folder,<br> dan akan memuat semua gambar yang ada di folder <br>dengan menyesuaikan ekstensi pada gambar yang ada.';
?>