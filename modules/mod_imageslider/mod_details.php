<?php
/**
* @version		v 1.0.0
* @package		Fi ImageSlider
* @copyright	Copyright (C) 2012 Fiyo CMS.
* @license		GNU/GPL
* @description	
**/

defined('_FINDEX_') or die('Access Denied');

$module_name='Fi ImageSlider';
$module_version='1.0.0';
$module_date='13 Februari 2012';
$module_author='Fiyo Developers';
$module_author_url='http://dev.fiyo,org';
$module_author_email='dev@fiyo.org';
$module_desc='Membuat gambar berjalan dengan memasukan path folder, dan akan memuat semua gambar yang ada di folder dengan menyesuaikan ekstensi pada gambar yang ada.';
?>